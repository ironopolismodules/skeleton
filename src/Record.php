<?php

namespace Ironopolis\Skeleton;

use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
  protected $fillable = [
    'user_id', 'slug', 'data', 'path'
  ];
}
