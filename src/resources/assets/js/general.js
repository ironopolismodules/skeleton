import { createApp } from 'vue'
import { createStore } from 'vuex'

import sApp from './components/App.vue'
import sWrapper from './components/Element/Element.vue'
import sInput from './components/Input/Input.vue'
import sLoop from './components/Loop/Loop.vue'
import sSvg from './components/Svg/Svg.vue'
import sShape from './components/Shape/Shape.vue'
import sForm from './components/Form/Form.vue'
import sSource from './components/Source/Source.vue'
import sInputAdmin from './components/InputAdmin.vue'
import sFreestyle from './components/Freestyle/Freestyle.vue'
import sTransition from './components/Transition/Transition.vue'

// Create a new store instance.
const store = createStore({
  state () {
    return {
      content: window.App.content,
      page: window.App.body,
      settings: window.App.settings,
      users: window.App.users,
      state: window.App.state,
      user: window.App.user,
      entity: window.App.entity,
      profile: window.App.profile
    }
  },
  mutations: {
    addRecord(state, {name, value, source}) {
      if (state.content[source] && state.content[source].fields) {
        let record = {};
        state.content[source].fields.forEach(field => {
          if (field.field == name) {
            record[field.field] = value;
          } else if (typeof field.defaultValue != 'undefined') {
            record[field.field] = field.defaultValue;
          } else {
            record[field.field] = null;
          }
        });
        if (state.content[source].persist) {
          store.commit('persist');
        } else {
          var uniq = String((new Date()).getTime());
          state.content[uniq] = record;
          state.content[source].data.push(uniq);
        }
      }
    },
    removeRecord(state, {source, record}) {
      let index = state.content[source].data.indexOf(record);
      if (index !== -1) {
        state.content[source].data.splice(index, 1);
      }
      if (state.content[source].persist) {
        store.commit('persist');
      }
    },
    async removeRecords(state, {source, store, target}) {
      await state[store][target].forEach(record => {
        var index = state.content[source].data.indexOf(record);
        if (index !== -1) {
          state.content[source].data.splice(index, 1);
        }
        var originalIndex = state.content[source].original.indexOf(record);
        if (originalIndex !== -1) {
          state.content[source].original.splice(originalIndex, 1);
        }
      });
    },
    removeFilters(state, {source}) {
      state.content[source].data = state.content[source].original.slice()
    },
    async filterSource(state, {source, filters, store, target, display}) {
      if (filters) {
        state[store][target] = [];
        await state.content[source].data.forEach(record => {
          let keys = Object.keys(state.content[record]);
          keys.forEach(key => {
            let results = filters.forEach(filter => {
              if (filter.operator == 'equals') {
                //console.log(filter, state.content[record][key])
                if (key == filter.key && state.content[record][key] == filter.value) {
                  state[store][target].push(record);
                }
              }
            });
          });
        });
        if (display) {
          state.content[source].data = state[store][target]
        }
      } else {
        if (typeof state.content[source].original != 'undefined') {
          state.content[source].data = state.content[source].original.slice()
        }
      }
    },
    persistRecord(state, {source, record}) {
      if (state.content[source] && state.content[source].data && state.content[source].data.length) {

      }
    },
    persist(state, { storage, targetStore, target, targetName}) {
      if (storage == 'disk') {
        console.log('store in state on disk');
      } else if (storage == 'session') {
        if (target) {
          sessionStorage.setItem(target+targetName, JSON.stringify(state[targetStore][target][targetName]));
        } else {
          sessionStorage.setItem(targetName, JSON.stringify(state[targetStore][targetName]));
        }
      } else if (storage == 'local') {
        if (target) {
          localStorage.setItem(target+targetName, JSON.stringify(state[targetStore][target][targetName]));
        } else {
          localStorage.setItem(targetName, JSON.stringify(state[targetStore][targetName]));
        }
      }
    },
    findRecords({source, name, value} = {}) {
      if (typeof name != 'undefined' && (typeof value != 'undefined' || typeof condition != 'undefined') && this.$store.state.content[source] && this.$store.state.content[source].data && this.$store.state.content[source].data.length) {
        let filteredResults = [];
        let checkValue = (value == 'true' || value == 'false' || value == 'on') ? Boolean(value) : value;
        this.$store.state.content[source].data.forEach(record => {
          var recordValue = this.$store.state.content[record][name] == 'true' || this.$store.state.content[record][name] == 'false' ? Boolean(this.$store.state.content[record][name]) : this.$store.state.content[record][name];
          if (recordValue == checkValue) {
            filteredResults.push(record)
          }
        });
        return filteredResults;
      }
    },
    selectRecord(state, {record}) {
      if (this.selectedRecords.includes(record)) {
        let index = this.selectedRecords.indexOf(record);
        if (index !== -1) {
          this.selectedRecords.splice(index, 1);
        }
      } else {
        this.selectedRecords.push(record);
      }
    },
    removeSelectedRecords() {
      this.selectedRecords = [];
    },
    updateRecords(state, {name, value}) {
      
    },
    updateAllRecords(state, {source, key, value}) {
      if (state.content[source] && state.content[source].data != 'undefined' && state.content[source].data.length) {
        state.content[source].data.forEach(record => {
          state.content[record][key] = value;
        });
        store.commit('persist', source);
      }
    },
    updateRecord(state, {source, record, name, value}) {
      if (state.content[record]) {
        if (typeof state.content[record][name] == "boolean") {
          state.content[record][name] = !state.content[record][name];
        } else if (state.content[record][name]) {
          state.content[record][name] = value;
        }
        store.commit('persist', source);
      }
    },
    push(state, {objectStore, targetStore, target, object, targetName, objectName, value}) {
      if (
        object && typeof state[objectStore][object][objectName] == 'undefined' &&
        target && typeof state[targetStore][target][targetName] != 'undefined' &&
        Array.isArray(state[targetStore][target][targetName])
      ) {
        state[targetStore][target][targetName].push(state[objectStore][object][objectName]);
      } else if (
        objectStore && typeof state[objectStore][objectName] != 'undefined' &&
        targetStore && typeof state[targetStore][targetName] != 'undefined' &&
        Array.isArray(state[targetStore][targetName])
      ) {
        state[targetStore][targetName].push(state[objectStore][objectName]);
      } else if (
        target && 
        typeof state[targetStore][target][targetName] != 'undefined' &&
        Array.isArray(state[targetStore][target][targetName])
      ) {
        state[targetStore][target][targetName].push(value);
      } else if (Array.isArray(state[targetStore][targetName])) {
        state[targetStore][targetName].push(value);
      }
    },
    pop(state, {objectStore, targetStore, target, object, targetName, objectName, value}) {
      if (
        object && typeof state[objectStore][object][objectName] != 'undefined' &&
        target && typeof state[store][target][targetName] != 'undefined'
      ) {
        state[targetStore][target][targetName] += state[objectStore][object][objectName].pop();
      } else if (
        objectStore && typeof state[objectStore][objectName] != 'undefined' &&
        targetStore && typeof state[targetStore][targetName] != 'undefined'
      ) {
        state[targetStore][targetName] += state[objectStore][objectName].pop();
      }
    },
    assign(state, {objectStore, targetStore, target, object, targetName, objectName, value}) {
      if (
        object && typeof state[objectStore][object][objectName] != 'undefined' &&
        target && typeof state[targetStore][target][targetName] != 'undefined'
      ) {
        state[targetStore][target][targetName] = state[objectStore][object][objectName];
      } else if (
        objectStore && typeof state[objectStore][objectName] != 'undefined' &&
        targetStore && typeof state[targetStore][targetName] != 'undefined'
      ) {
        state[targetStore][targetName] = state[objectStore][objectName];
      } else if (target && typeof state[targetStore][target][targetName] != 'undefined') {
          state[targetStore][target][targetName] = value;
      } else {
        state[targetStore][targetName] = value;
      }
    },
    append(state, {objectStore, targetStore, target, object, targetName, objectName, value}) {
      if (
        object && typeof state[objectStore][object][objectName] != 'undefined' &&
        target && typeof state[store][target][targetName] != 'undefined'
      ) {
        state[targetStore][target][targetName] += state[objectStore][object][objectName].toString();
      } else if (
        objectStore && typeof state[objectStore][objectName] != 'undefined' &&
        targetStore && typeof state[targetStore][targetName] != 'undefined'
      ) {
        state[targetStore][targetName] += state[objectStore][objectName].toString();
      } else if (target && typeof state[targetStore][target][targetName] != 'undefined') {
          state[targetStore][target][targetName] += value.toString();
      } else if (typeof state[targetStore][targetName] != 'undefined') {
        state[targetStore][targetName] += value.toString();
      }
    }
  }
})

const app = createApp({
  methods: {
    async methodFactory(event) {
      if (this.$store.state.page[event]) {
          for (const func of this.$store.state.page[event]) {
            let event = {};
            event.target = {};
            event.target.value = func;
            await this.runCommand(event);
          }
      }
      this.event = null;
    },
    async runCommand(event) {
      let command = this.currentCommand || event.target.value;
      let result = null;
      let conditionResult = true;
      let quotedStrings = event.target.value.match(/['"].*?['"]/g);
      if (quotedStrings) {
        for (let i = 0; i < quotedStrings.length; i++) {
          command = command.replace(quotedStrings[i].slice(1, -1), encodeURIComponent(quotedStrings[i].slice(1, -1)))
        }
      }
      command = command.split(' ');
      if (command.length == 5) {
          let condition = command.shift();
          conditionResult = await this.execute(decodeURI(condition));
          if (conditionResult) {
              command = command.slice(0,2);
          } else {
              command = command.slice(2,4);
          }
      }
      if (command.length == 3) {
          let condition = command.shift();
          conditionResult = await this.execute(decodeURI(condition));
          if (!conditionResult) {
              return;
          }
      }
      if (command.length == 2) {
        result = await this.execute(decodeURI(command[1]));
        var lhs = null;
        if (command[0][0] == '$') {
          lhs = command[0].substring(1, command[0].length - 1).split('.');
        } else {
          lhs = command[0].split('.');
        }
        if (lhs[2]) {
          this.$store.commit('assign', {value: result, targetStore: lhs[0] , target: lhs[1], targetName: lhs[2]});
        } else {
          this.$store.commit('assign', {value: result, targetStore: lhs[0], targetName: lhs[1]});
        }
      } else {
        result = await this.execute(decodeURI(command[0]));
      }
      event.target.value = '';
    },
    execute(code) {
      var placeholders = code.match(/\$(.*?)\$/g)
      if (placeholders) {
        placeholders.forEach(function(placeholder) {
          var phText = placeholder.substring(1, placeholder.length - 1).split('.');
          if (phText[2]) {
            var hydrate = this.$store.state[phText[0]][phText[1]][phText[2]];
            if (hydrate == '') {
              hydrate = "''";
            }
            if (Array.isArray(this.$store.state[phText[0]][phText[1]][phText[2]])) {
              hydrate = JSON.stringify(this.$store.state[phText[0]][phText[1]][phText[2]])
            }
            code = code.replace(placeholder, hydrate);
          } else if (typeof this.$store.state[phText[0]][phText[1]] != 'undefined'){
            var hydrate = this.$store.state[phText[0]][phText[1]];
            if (hydrate == '') {
              hydrate = "''";
            }
            if (Array.isArray(this.$store.state[phText[0]][phText[1]])) {
              hydrate = JSON.stringify(this.$store.state[phText[0]][phText[1]])
            } 
            code = code.replace(placeholder, hydrate);
          }
        }.bind(this))
      }
      try {
        let result = eval(code);
        return result;
        //Function('return ' + code)();
      } catch(error) {
       
      }
    }
  },
  mounted() {
    const intersectionObserver = new IntersectionObserver((entries, observer) => {
      entries.forEach((entry) => {
        if (entry.intersectionRatio > 0) {
          entry.target.classList.add(this.$store.state.content[entry.target.id].enabledClasses);
        }
        observer.unobserve(entry.target);
      });
    });
    const elements = [...document.querySelectorAll('.observe')];
    elements.forEach((element) => intersectionObserver.observe(element));
    this.methodFactory('mounted');
  }
})
.use(store)
.component('s-app', sApp)
.component('s-wrapper', sWrapper)
.component('s-control', sInputAdmin)
.component('s-input', sInput)
.component('s-loop', sLoop)
.component('s-svg', sSvg)
.component('s-shape', sShape)
.component('s-transition', sTransition)
.component('s-form', sForm)
.component('s-freestyle', sFreestyle)
.component('s-source', sSource)
.mount('#app')