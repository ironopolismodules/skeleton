import { createApp } from 'vue'


import debounce from 'lodash/debounce';
import { Dropdown, Autocomplete } from '@oruga-ui/oruga-next';
import '@oruga-ui/oruga-next/dist/oruga.css';

const app = createApp({
    data() {
        return {
            path: '',
            collections: [],
            attributes: [],
            filterables: {},
            currentFilterable: {},
            menu: '',
            editorPosition: 'bottom',
            showEditor: false,
            userDropdownActive: false,
            settings: window.App.settings,
            bodies: window.App.bodies,
            newRecord: {},
            openWelcome: true,
            following: false,
            hover: false,
            content: {},
            forms: [],
            formSubmissions: [],
            display: 'content',
            searchData: [],
            data: [],
            currentRecord: null,
            columns: [],
            isSaving: false,
            dropFiles: [],
            displayPageOptions: false,
            navigator: true,
            currentSource: '',
            source: null,
            sources: [],
            field: '',
            profilePhoto: null,
            profile: null,
            label: '',
            showTask: false,
            highlighted: false,
            selectedRequest: '',
            currentType: null,
            length: null,
            defaultValue: null,
            pageSelection: null,
            displayMobileMenu: false,
            displaySourceSettings: false,
            user: [],
            editorConfig: {
                // The configuration of the editor.
            },
            loadingState: false,
            assets: [],
            assetsTab: null,
            imageDialogVisible: false,
            imageDialogSelection: 'upload',
            type: ''
        }
    },
    computed: {
        csrf() {
          return document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        }
    },
    methods: {
        filesChange(fieldName, fileList) {
            console.log(fieldName, fileList)
            // handle file changes
            const formData = new FormData();
    
            if (!fileList.length) return;
    
            let reader = new FileReader();
            reader.onload = function () {
                this.$set(this, 'profilePhoto', reader.result)
            }.bind(this)
            if (fileList[0]) {
                reader.readAsDataURL(fileList[0]);
            }
            
            const config = {
                headers: { 'content-type': 'multipart/form-data' }
            }
            formData.append('image', fileList[0]);

            this.$http.post(
                '/storeProfilePhoto', 
                formData,
                config
            ).then(response => {
              this.$set(this.user, 'profilePhoto', response.data.response)
            }).catch((error) => {
                throw error
            });
        },
        follow() {
            this.following = !this.following;
        },
        duplicate() {
            alert('This feature is only available on paid plans.')
        },
        displayImageDialog() {
            this.$set(this, 'imageDialogVisible', true);
        },
        setCurrentType() {
            let decodeType = JSON.parse(this.type);
            this.currentType = decodeType.type;
        },
        setPriceId(priceId) {
            this.priceId = priceId;
        },
        saveUser: debounce(function () {
            if (!this.settings.locked) {
              fetch('saveUser', {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json',
                  'X-CSRF-TOKEN': this.csrf
                },
                user: JSON.stringify(this.user),
              })
              // .then(data => {
              //   console.log('Success:', data);
              // })
              .catch((error) => {
                console.error('Error:', error);
              });
            }
        }, 500),
        selectSource(item) {
            if (item != this.source.slug) {
                this.source = this.content[item]
                if (this.source.data) {
                    this.fetchRecords();
                }
            }
        },
        selectPage(option) {
            this.$set(this, 'pageSelection', option);
        },
        getAsyncData: debounce(function (event) {
            if (typeof this.user.slug != 'undefined') {
                if (typeof event.target.value == 'undefined' || !event.target.value.length) {
                    this.searchData = []
                    return
                }
                fetch(`/fetchSources?source=${event.target.value}`)
                    .then(response => response.json())
                    .then(data => {
                        this.searchData = []
                        data.forEach((item) => this.searchData.push(item))
                    })
                    .catch((error) => {
                        this.searchData = []
                        throw error
                    })
            } else {
                alert('You must be signed in to access data sources.')
            }
        }, 500),
        setPath: debounce(function () {
            if (typeof this.user.slug != 'undefined') {
                this.$http.post(
                    '/setPath', 
                    {path: this.path, slug: this.currentRecord},
                    {
                        headers: {
                            'Content-Type': 'application/json',
                        }
                    }
                ).then(response => {
                    
                }).catch((error) => {
                    throw error
                });
            } else {
                alert('You must be signed in to access data sources.')
            }
        }, 500),
        selection(option) {
            if (option == null) {
                this.source = {};
            } else {
                this.source = option;
                this.content[this.source.slug] = this.source;
                this.sources.push(this.source.slug)
                this.fetchRecords();
            }
        },
        pageOptions() {
           this.$set(this, 'displayPageOptions', true);
        },
        duplicateRecord() {

        },
        saveRecord: debounce(function () {
            if (typeof this.user.slug != 'undefined') {
                this.$http.post(
                    '/saveRecord', 
                    {slug: this.currentRecord, record: this.content[this.currentRecord]},
                    {
                        headers: {
                            'Content-Type': 'application/json',
                        }
                    }
                ).then(response => {
                    
                }).catch((error) => {
                    throw error
                });
            } else {
                alert('You must be signed in to access data sources.')
            }
        }, 500),
        saveSource: debounce(function () {
            if (!this.settings.locked && this.source && this.source.slug) {
                this.$http.post(
                    '/saveSource', 
                    this.source,
                    {
                        headers: {
                            'Content-Type': 'application/json',
                        }
                    }
                ).then(response => {
                    
                }).catch((error) => {
                    throw error
                });
            }
        }, 500),
        save: debounce(function (text) {
            this.$set(this.content[this.currentSelection], 'text', text)
            let current = this.currentSelection;
            this.$set(this, 'currentSelection', null)
            Vue.nextTick(function () {
                this.$set(this, 'currentSelection', current)
            }.bind(this))
        }, 500),
        clearAll() {
            if (this.object.type && typeof(Storage) !== "undefined") {
                this.object = null;
                sessionStorage.setItem('object', JSON.stringify(this.object));
            }
        },
        saveEdit() {
            if (this.object.type && typeof(Storage) !== "undefined") {
                sessionStorage.setItem('object', JSON.stringify(this.object));
                window.location.assign('/edit-attributes?edit');
            }
        },
        addSource(type) {
            if (this.user.slug) {
                this.loadingState = true;
                this.$http.post(
                    '/createSource',
                    {type: type},
                    {
                        headers: {
                            'Content-Type': 'application/json',
                        }
                    }
                ).then((response) => {
                    this.$set(this, 'source', JSON.parse(response.data.data));
                    this.content[this.source.slug] = this.source;
                    this.sources.push(this.source.slug);
                    this.loadingState = false;
                }).catch((error) => {
                    throw error
                });
            } else {
                alert('You must be signed in to add a table.')
                //window.location.href = '/register';
            }
        },
        addField() {
            let object = JSON.parse(this.type);
            object.field = this.field;
            object.label = this.label;
            object.length = this.length;
            object.defaultValue = this.defaultValue;
            if (typeof this.source.fields == 'undefined') {
                this.source.fields = [];
            }
            this.source.fields.push(object);
            this.type = {};
            this.field = '';
            this.label = '';
            this.length = '';
            this.defaultValue = '';
            this.saveSource();
            this.$forceUpdate();
        },
        removeField(index) {
            this.source.fields.splice(index, 1);
            this.$forceUpdate();
        },
        addRecord() {
            this.$http.post(
                '/addRecord', 
                {slug: this.source.slug, record: this.newRecord},
                {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                }
            ).then(response => {
                this.data.push(this.newRecord);
                this.$set(this, 'newRecord', {});
                this.$set(this, 'display', 'content');
                this.fetchRecords();
            }).catch((error) => {
                throw error
            });
        },
        openEditor(menu, section, record) {
            if (menu) {
                this.menu = menu;
            } else {
                this.menu = null;
            }
            this.currentRecord = record;
            if (this.menu == 'Filterables') {
                this.fetchFilterables();
            }
            this.showEditor = true;
        },
        closeEditor() {
            this.showEditor = false;
        },
        toggleEditorPosition() {
            if (this.editorPosition == 'bottom') {
              this.editorPosition = 'right';
            } else {
              this.editorPosition = 'bottom';
            }
            if (this.backgroundEditorPosition == 'bottom') {
              this.backgroundEditorPosition = 'right';
            } else {
              this.backgroundEditorPosition = 'bottom';
            }
        },
        editRecord(record) {
            this.currentRecord = record;
            this.display = 'record';
        },
        deleteRecord(record) {
            fetch(`/deleteRecord?record=${record}&source=${this.source.slug}`)
                .then(response => response.json())
                .then(data => {
                    let index = this.source.data.indexOf(record);
                    this.source.data.splice(index, 1);
                })
                .catch((error) => {
                    this.loading = false;
                    throw error;
                });
        },
        fetchRecords() {
            fetch(`/fetchRecords?slug=${this.source.slug}`)
                .then(response => response.json())
                .then(data => {
                    this.data = [];
                    this.source.data = [];
                    data.forEach((item) => {
                        this.content[item.slug] = JSON.parse(item.data);
                        this.source.data.push(item.slug);
                    });
                }).catch((error) => {
                    this.data = [];
                    throw error
                });
        },
        addAttribute() {
            console.log(this.attribute)
        },
        deleteSource() {
            if (confirm('Are you sure you want to delete this source table?')) {
                fetch('saveUser', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'X-CSRF-TOKEN': this.csrf
                    },
                    source: this.source.slug,
                    })
                    .then(response => response.json())
                    .then(data => {
                        let index = this.sources.indexOf(this.source.slug);
                        this.sources.splice(index, 1);
                        this.source = null;
                        this.data = null;
                        this.displaySourceSettings = null;
                    }).catch((error) => {
                        throw error
                    });
            }
        },
        saveFilterables() {
            if (!this.settings.locked) {
                this.$http.post(
                    '/saveFilterables', 
                    this.filterables,
                    {
                        headers: {
                            'Content-Type': 'application/json',
                        }
                    }
                ).then(response => {
                    
                }).catch((error) => {
                    throw error
                });
            }
        },
        saveSettings: debounce(function () {
            if (!this.settings.locked) {
              this.$http.post(
                  '/storeSettings', 
                  this.settings,
                  {
                      headers: {
                          'Content-Type': 'application/json',
                      }
                  }
              ).then(response => {
                  
              }).catch((error) => {
                  throw error
              });
            }
        }, 500),
        storeSession(name, value) {
            sessionStorage.setItem(name , value);
        },
        fetchAssets() {
            this.$http.get(`/fetchAssets?slug=${this.assetTab}`)
                .then(({ data }) => {
                    this.assets = [];
                    data.forEach((item) => {
                        this.assets[item.slug] = JSON.parse(item.data);
                        this.$forceUpdate();
                    });
                }).catch((error) => {
                    this.assets = [];
                    throw error
                });
        },
        fetchFilterables() {
            this.$http.get(`/fetchFilterables?slug=${this.currentRecord}`)
                .then(({ data }) => {
                    this.filterables = {};
                    this.$set(this, 'filterables', data);
                }).catch((error) => {
                    this.filterables = {};
                    throw error
                });
        },
        fetchCollections: debounce(function (name) {
            if (typeof this.user.slug != 'undefined') {
                this.$http.get(`/fetchCollections?slug=${name}`)
                    .then(({ data }) => {
                        this.collections = []
                        data.forEach((item) => this.collections.push(item))
                    })
                    .catch((error) => {
                        this.collections = []
                        throw error
                    })
                    .finally(() => {
    
                    })
            } else {
                alert('You must be signed in to access data sources.')
            }
        }, 500),
        selectCollection(option) {
            if (option == null) {
                this.$set(this.currentFilterable, 'collection_id', '');
            } else {
                this.$set(this.currentFilterable, 'collection_id', option.slug);
            }
        },
        fetchAttributes: debounce(function (name) {
            if (typeof this.user.slug != 'undefined') {
                this.$http.get(`/fetchAttributes?slug=${name}`)
                    .then(({ data }) => {
                        this.attributes = []
                        data.forEach((item) => this.collections.push(item))
                    })
                    .catch((error) => {
                        this.attributes = []
                        throw error
                    })
                    .finally(() => {
    
                    })
            } else {
                alert('You must be signed in to access data sources.')
            }
        }, 500),
        selectAttributes(option) {
            if (option == null) {
                this.$set(this.currentFilterable, 'attribute_id', '');
            } else {
                this.$set(this.currentFilterable, 'attribute_id', option.slug);
            }
        },
        fetchProfile() {
            if (localStorage.cms) {
              this.profile = JSON.parse(localStorage.cms);
            }
            if (!this.profile) {
                fetch(`/fetchProfile?profile=cms`)
                    .then(response => response.json())
                    .then(data => {
                        this.profile = data;
                        localStorage.setItem('cms', JSON.stringify(data));
                    })
                    .catch((error) => {
                        this.profile = [];
                        throw error
                    })
            }
        }
    },
    beforeMount() {
        this.user = window.App.user;
    },
    mounted() {
        if (this.user.slug) {
            this.fetchProfile();
        }
    }
})
.use(Dropdown)
.use(Autocomplete)
.mount('#app');
