import { createApp } from 'vue'
import debounce from 'lodash/debounce';

import { Dropdown, Autocomplete } from '@oruga-ui/oruga-next';

const app = createApp({
    data() {
        return {
            displayMobileMenu: false,
            code: '',
            category: '',
            link: '',
            script: '',
            pwa: false,
            subcategory: 'pages',
            userDropdownActive: false,
            siteData: window.App.siteData,
            settings: window.App.settings,
            bodies: window.App.bodies,
            subscription: window.App.subscription,
            plan: window.App.plan,
            userData: window.App.userData,
            newRecord: {},
            country: 'US',
            openWelcome: true,
            tab: 'flows',
            following: false,
            hover: false,
            content: {},
            forms: [],
            formSubmissions: [],
            display: 'content',
            searchData: [],
            data: [],
            currentRecord: null,
            columns: [],
            isSaving: false,
            dropFiles: [],
            // data: [
            //     { 'id': 1, 'first_name': 'Jesse', 'last_name': 'Simmons', 'date': '2016-10-15 13:43:27', 'gender': 'Male' },
            //     { 'id': 2, 'first_name': 'John', 'last_name': 'Jacobs', 'date': '2016-12-15 06:00:53', 'gender': 'Male' },
            //     { 'id': 3, 'first_name': 'Tina', 'last_name': 'Gilbert', 'date': '2016-04-26 06:26:28', 'gender': 'Female' },
            //     { 'id': 4, 'first_name': 'Clarence', 'last_name': 'Flores', 'date': '2016-04-10 10:28:46', 'gender': 'Male' },
            //     { 'id': 5, 'first_name': 'Anne', 'last_name': 'Lee', 'date': '2016-12-06 14:38:38', 'gender': 'Female' }
            // ],
            // columns: [
            //     {
            //         field: 'id',
            //         label: 'ID',
            //         width: '40',
            //         numeric: true
            //     },
            //     {
            //         field: 'first_name',
            //         label: 'First Name',
            //     },
            //     {
            //         field: 'last_name',
            //         label: 'Last Name',
            //     },
            //     {
            //         field: 'date',
            //         label: 'Date',
            //         centered: true
            //     },
            //     {
            //         field: 'gender',
            //         label: 'Gender',
            //     }
            // ],
            displayPageOptions: false,
            type: '',
            term: 'annual',
            prices: {
                'price_1IaLP9L2EQC83td80GqdB4Bz': '$108',
                'price_1IaLQUL2EQC83td8gdQmNoXD': '$348',
                'price_1IaLSUL2EQC83td8SwJGAWLq': '$708',
                'price_1IaMdWL2EQC83td8fmKRH4ki': '$13',
                'price_1IaMevL2EQC83td8jkSvNInn': '$33',
                'price_1IaMfoL2EQC83td8ovJUZgfc': '$63'

            },
            navigator: true,
            priceId: '',
            currentSource: '',
            source: null,
            sources: [],
            field: '',
            profilePhoto: null,
            label: '',
            showTask: false,
            highlighted: false,
            selectedRequest: '',
            currentType: null,
            length: null,
            pageSelection: null,
            displayMobileMenu: false,
            displaySourceSettings: false,
            user: [],
            editorConfig: {
                // The configuration of the editor.
            },
            loadingState: false,
            assets: [],
            assetsTab: null,
            imageDialogVisible: false,
            imageDialogSelection: 'upload'
        }
    },
    created() {
        var vm = this
    },
    computed: {
        csrf() {
          return document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        }
    },
    methods: {
        highlighter(code) {
            return code;
        },
        addTeamMember() {
            alert('You must sign up to our Team or Enterprise development plan before you add team members to your project.');
        },
        displayCategory(category, subcategory) {
            this.category = category;
            if (subcategory) {
                this.subcategory = subcategory;
            }
        },
        addLink() {
            if (typeof this.settings.links == 'undefined') {
                this.settings.links = [];
            }
            this.settings.links.push({"href": this.link});
            this.saveSettings();
            this.link = '';
        },
        removeLink(index) {
            this.settings.links.splice(index, 1);
            this.saveSettings();
        },
        addScript() {
            if (typeof this.settings.scripts == 'undefined') {
                this.settings.scripts = [];
            }
            if (this.script != '') {
                this.settings.scripts.push({"src": this.script});
                this.saveSettings();
                this.script = '';
            }
        },
        removeScript(index) {
            this.settings.scripts.splice(index, 1);
            this.saveSettings();
        },
        filesChange(fieldName, fileList) {
            console.log(fieldName, fileList)
            // handle file changes
            const formData = new FormData();
    
            if (!fileList.length) return;
    
            let reader = new FileReader();
            reader.onload = function () {
                this.$set(this, 'profilePhoto', reader.result)
            }.bind(this)
            if (fileList[0]) {
                reader.readAsDataURL(fileList[0]);
            }
            
            const config = {
                headers: { 'content-type': 'multipart/form-data' }
            }
            formData.append('image', fileList[0]);

            this.$http.post(
                '/storeProfilePhoto', 
                formData,
                config
            ).then(response => {
              this.$set(this.user, 'profilePhoto', response.data.response)
            }).catch((error) => {
                throw error
            });
        },
        duplicate() {
            alert('This feature is only available on paid plans.')
        },
        setCurrentType() {
            let decodeType = JSON.parse(this.type);
            this.currentType = decodeType.type;
        },
        setPriceId(priceId) {
            this.priceId = priceId;
        },
        saveUser: debounce(function () {
            if (typeof this.settings.locked != 'undefined' && !this.settings.locked) {
                fetch(
                    '/saveUser', 
                    this.user,
                    {
                        headers: {
                            'Content-Type': 'application/json',
                            'X-CSRF-TOKEN': this.csrf
                        }
                    }
                ).then(response => {
                    
                }).catch((error) => {
                    throw error
                });
            }
        }, 500),
        saveSettings: debounce(function () {
            if (!this.settings.locked) {
                fetch(
                  '/storeSettings', 
                  this.settings,
                  {
                      headers: {
                          'Content-Type': 'application/json',
                          'X-CSRF-TOKEN': this.csrf
                      }
                  }
              ).then(response => {
                  
              }).catch((error) => {
                  throw error
              });
            }
        }, 500),
        storeSession(name, value) {
            sessionStorage.setItem(name , value);
        }
    },
    beforeMount() {
        this.user = window.App.user;
    }
})
.use(Dropdown)
.use(Autocomplete)
.mount('#app');
