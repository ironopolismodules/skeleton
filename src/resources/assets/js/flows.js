window.Vue = require('vue');
import Buefy from 'buefy';
import axios from 'axios';
axios.defaults.headers.common['X-CSRF-TOKEN'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
Vue.prototype.$http = axios;

Vue.use(Buefy);
Vue.component('s-inputadmin', require('./components/InputAdmin.vue').default);
Vue.component('s-task', require('./components/Task/Task.vue').default);

import debounce from 'lodash/debounce';
import CKEditor from '@ckeditor/ckeditor5-vue';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import moment from 'moment';
import { getMonthNames, getWeekdayNames, matchWithGroups } from './components/utils/helpers'
const app = new Vue({
    el: '#app',
    props: {
        dateCreator: {
            type: Function,
            default: () => {
                return new Date()
            }
        },
        firstDayOfWeek: {
            type: Number,
            default: () => {
                return 0
            }
        },
        focused: Object
    },
    data() {
        const focusedDate = (Array.isArray(this.value) ? this.value[0] : (this.value)) ||
            this.focusedDate || this.dateCreator()
        if (!this.value && this.maxDate && this.maxDate.getFullYear() < focusedDate.getFullYear()) {
            focusedDate.setFullYear(this.maxDate.getFullYear())
        }
        return {
            timeline: [],
            times: ['0.00', '1.00', '2.00', '3.00', '4.00', '5.00', '6.00','7.00', '8.00', '9.00', '10.00', '11.00', '12.00', '13.00', '14.00', '15.00', '16.00', '17.00', '18.00', '19.00', '20.00', '21.00', '22.00', '23.00'],
            days: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
            dayInitials: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
            range: [9, 18],
            timelineWidth: 0,
            timelineSeconds: 0,
            items: [],
            flows: [],
            user: {},
            openWelcome: true,
            displayCurrentTime: true,
            menu: null,
            selectionMenu: 'attributes',
            attributes: {},
            editorPosition: 'bottom',
            currentSelection: null,
            focusedDateData: {
                day: focusedDate.getDate(),
                month: focusedDate.getMonth(),
                year: focusedDate.getFullYear()
            },
            weeksInSelectedMonth: [],
            daysInMonth: 31,
            displayTaskDetail: false,
            showEditor: false,
            showTask: false,
            showFlow: false,
            currentFlow: null,
            profile: window.App.profile,
            content: window.App.content,
            user: window.App.user,
            settings: window.App.settings
        };
    },
    watch: {
        value(value) {
            this.updateInternalState(value)
            if (!this.multiple) this.togglePicker(false)
        },
        focusedDate(value) {
            if (value) {
                this.focusedDateData = {
                    day: value.getDate(),
                    month: value.getMonth(),
                    year: value.getFullYear()
                }
            }
        }
    },
    computed: {
        newDayNames() {
            if (Array.isArray(this.dayNames)) {
                return this.dayNames
            }
            return getWeekdayNames(this.locale)
        },
        visibleDayNames() {
            const visibleDayNames = []
            let index = this.firstDayOfWeek
            while (visibleDayNames.length < this.newDayNames.length) {
                const currentDayName = this.newDayNames[(index % this.newDayNames.length)]
                visibleDayNames.push(currentDayName)
                index++
            }
            if (this.showWeekNumber) visibleDayNames.unshift('')
            return visibleDayNames
        }
    },
    created() {
        var vm = this
    },
    components: {
        // Use the <ckeditor> component in this view.
        ckeditor: CKEditor.component
    },
    methods: {
        updateInternalState(value) {
            console.log('value', value)
            if (this.dateSelected === value) return
            const currentDate = Array.isArray(value)
                ? (!value.length ? this.dateCreator() : value[value.length - 1])
                : (!value ? this.dateCreator() : value)
            if (Array.isArray(value) && value.length > this.dateSelected.length) {
                this.focusedDateData = {
                    day: currentDate.getDate(),
                    month: currentDate.getMonth(),
                    year: currentDate.getFullYear()
                }
            }
            this.dateSelected = value
        },
        toggleMode() {
            if (this.settings.editMode) {
                window.location.href = '/flows'
            } else {
                window.location.href = '/flows?edit'
            }
        },
        save() {
            this.$forceUpdate();
        },
        openEditor(menu, section) {
            if (menu) {
              this.$set(this, 'menu', menu);
            } else {
              this.$set(this, 'menu', null);
            }
            this.$set(this, 'showEditor', true);
        },
        closeEditor() {
            this.$set(this, 'showEditor', false);
        },
        toggleEditorPosition() {
            if (this.editorPosition == 'bottom') {
              this.editorPosition = 'right';
            } else {
              this.editorPosition = 'bottom';
            }
        },
        fetchClasses: debounce(function () {
            this.saveBody();
            this.$http
              .post(`/fetchClasses`, {links: this.body.links})
              .then(({ data }) => {
                  this.$set(this, 'attributes', data);
                  localStorage.setItem('attributes', JSON.stringify(data));
                  //location.reload();
              })
              .catch((error) => {
                  this.classes = {};
                  throw error
              })
        }, 500),
        addFlow() {
            var uniq = String((new Date()).getTime());
            this.content[uniq] = {type: 's-flow', slug: uniq, name: 'New Flow', tasks: [], classes: []};
            if (this.attributes.colours) {
                this.content[uniq].classes.push('bg-' + this.attributes.colours[Math.floor(Math.random() * this.attributes.colours.length) + 1]);
            }
            this.flows.push(uniq);
            this.$set(this, 'currentFlow', uniq);
        },
        addTask() {
            var uniq = String((new Date()).getTime());
            this.content[uniq] = {type: 's-task', slug: uniq, name: 'New Task', duration: 3600, classes: []};
            if (typeof this.content[this.currentFlow].tasks == 'undefined') {
                this.content[this.currentFlow].tasks = [];
              }
            this.content[this.currentFlow].tasks.push(uniq);
            this.$forceUpdate();
        },
        storeSession(name, value) {
            sessionStorage.setItem(name , value);
        },
        setDuration: debounce(function (event) {
            this.$set(this.content[this.currentSelection], 'duration', event.target.value * 60);
            this.$forceUpdate();
        }, 500),
        selectFlow(slug) {
            if (this.settings.editMode) {
                this.$set(this, 'currentFlow', slug);
                this.openEditor('Flow');
            } else {
                this.$set(this, 'currentFlow', slug);
            }
        },
        calculateTimeline() {
            this.timelineWidth = this.$refs.timeline.offsetWidth;
            this.timeline = this.times.slice(this.range[0], this.range[1]);
            this.timelineSeconds = this.timeline.length * 3600;
            this.getAdjustedSecondsElapsed();
            this.$forceUpdate();
        },
        getSecondsElapsedToday() {
            let d = new Date();
            return d.getHours() * 3600 + d.getMinutes() * 60 + d.getSeconds();
        },
        getAdjustedSecondsElapsed() {
            return this.getSecondsElapsedToday() - (this.range[0] * 3600);
        },
        /*
        * Return array of all weeks in the specified month
        */
        weeksInThisMonth() {
            this.validateFocusedDay()
            const month = this.focusedDateData.month
            const year = this.focusedDateData.year
            const weeksInThisMonth = []
            let startingDay = 1
            while (weeksInThisMonth.length < 6) {
                const newWeek = this.weekBuilder(startingDay, month, year)
                weeksInThisMonth.push(newWeek)
                startingDay += 7
            }
            this.weeksInSelectedMonth = weeksInThisMonth;
        },
        
        weekBuilder(startingDate, month, year) {
            const thisMonth = new Date(year, month)
            const thisWeek = []
            const dayOfWeek = new Date(year, month, startingDate).getDay()
            const end = dayOfWeek >= this.firstDayOfWeek
                ? (dayOfWeek - this.firstDayOfWeek)
                : ((7 - this.firstDayOfWeek) + dayOfWeek)
            let daysAgo = 1
            for (let i = 0; i < end; i++) {
                thisWeek.unshift(new Date(
                    thisMonth.getFullYear(),
                    thisMonth.getMonth(),
                    startingDate - daysAgo)
                )
                daysAgo++
            }
            thisWeek.push(new Date(year, month, startingDate))
            let daysForward = 1
            while (thisWeek.length < 7) {
                thisWeek.push(new Date(year, month, startingDate + daysForward))
                daysForward++
            }
            return thisWeek
        },
        validateFocusedDay() {
            const focusedDate = new Date(this.focusedDateData.year, this.focusedDateData.month, this.focusedDateData.day)
            let day = 0
            // Number of days in the current month
            const monthDays = new Date(this.focusedDateData.year, this.focusedDateData.month + 1, 0).getDate()
            let firstFocusable = null
            while (!firstFocusable && ++day < monthDays) {
                const date = new Date(this.focusedDateData.year, this.focusedDateData.month, day)
            }
        }
    },
    beforeMount() {
        this.weeksInThisMonth();
        if (Array.isArray(this.content)) {
            this.content = {};
        }
        this.getAdjustedSecondsElapsed();

        if (sessionStorage.openFlowWelcome) {
            this.openWelcome = sessionStorage.openFlowWelcome == 'true';
        }
        // this.flows = [
        //     "b3817386-9d14-11eb-a8b3-0242ac130003",
        //     "b757e670-9d14-11eb-a8b3-0242ac130003"
        // ];
        // this.content = {
        //     "b3817386-9d14-11eb-a8b3-0242ac130003": {
        //         "tasks": [
        //             "daf70e12-9d14-11eb-a8b3-0242ac130003"
        //         ]
        //     },
        //     "b757e670-9d14-11eb-a8b3-0242ac130003": {
        //         "tasks": [
        //             "f1b2f616-9d14-11eb-a8b3-0242ac130003"
        //         ]
        //     },
        //     "daf70e12-9d14-11eb-a8b3-0242ac130003": {
        //         slug: "daf70e12-9d14-11eb-a8b3-0242ac130003",
        //         name: "New Task 1",
        //         duration: 3600,
        //         type: 's-input'
        //     },
        //     "f1b2f616-9d14-11eb-a8b3-0242ac130003": {
        //         slug: "f1b2f616-9d14-11eb-a8b3-0242ac130003",
        //         name: "New Task 2",
        //         duration: 3600,
        //         type: 's-input'
        //     }
        // }
    },
    mounted() {
        if (localStorage.attributes) {
            this.attributes = JSON.parse(localStorage.attributes);
        } else {
            this.fetchClasses();
        }
        this.calculateTimeline();
        this.$root.$on('selectTask', (slug) => {
            if (this.settings.editMode) {
                this.$set(this, 'currentSelection', slug);
                this.openEditor('Task');
            } else {
                this.$set(this, 'currentSelection', slug);
                this.$set(this, 'showTask', true);
            }
        });
    },
    created() {
        window.addEventListener("resize", this.calculateTimeline);
    },
    destroyed() {
        window.removeEventListener("resize", this.calculateTimeline);
    },
});
