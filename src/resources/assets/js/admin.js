import { createApp } from 'vue'

import stripedeveloperelements from './components/Stripe/DeveloperElements.vue'
import stripehostingelements from './components/Stripe/HostingElements.vue'
import stripeelements from './components/Stripe/Elements.vue'

import { Dropdown, Autocomplete } from '@oruga-ui/oruga-next';

import debounce from 'lodash/debounce';

const app = createApp({
    data() {
        return {
            userDropdownActive: false,
            settings: window.App.settings,
            bodies: window.App.bodies,
            subscription: window.App.subscription,
            plan: window.App.plan,
            newRecord: {},
            country: 'US',
            openWelcome: true,
            tab: 'flows',
            following: false,
            hover: false,
            content: {},
            forms: [],
            formSubmissions: [],
            display: 'content',
            searchData: [],
            data: [],
            columns: [],
            isSaving: false,
            dropFiles: [],
            // data: [
            //     { 'id': 1, 'first_name': 'Jesse', 'last_name': 'Simmons', 'date': '2016-10-15 13:43:27', 'gender': 'Male' },
            //     { 'id': 2, 'first_name': 'John', 'last_name': 'Jacobs', 'date': '2016-12-15 06:00:53', 'gender': 'Male' },
            //     { 'id': 3, 'first_name': 'Tina', 'last_name': 'Gilbert', 'date': '2016-04-26 06:26:28', 'gender': 'Female' },
            //     { 'id': 4, 'first_name': 'Clarence', 'last_name': 'Flores', 'date': '2016-04-10 10:28:46', 'gender': 'Male' },
            //     { 'id': 5, 'first_name': 'Anne', 'last_name': 'Lee', 'date': '2016-12-06 14:38:38', 'gender': 'Female' }
            // ],
            // columns: [
            //     {
            //         field: 'id',
            //         label: 'ID',
            //         width: '40',
            //         numeric: true
            //     },
            //     {
            //         field: 'first_name',
            //         label: 'First Name',
            //     },
            //     {
            //         field: 'last_name',
            //         label: 'Last Name',
            //     },
            //     {
            //         field: 'date',
            //         label: 'Date',
            //         centered: true
            //     },
            //     {
            //         field: 'gender',
            //         label: 'Gender',
            //     }
            // ],
            displayPageOptions: false,
            type: '',
            term: 'annual',
            prices: {
                'price_1Idd7nL2EQC83td8R0wVn2sH': '$33',
                'price_1IaLXpL2EQC83td857t3vpf7': '$348'

            },
            navigator: true,
            priceId: '',
            currentSource: '',
            source: null,
            sources: [],
            field: '',
            profilePhoto: null,
            label: '',
            showTask: false,
            highlighted: false,
            selectedRequest: '',
            currentType: null,
            length: null,
            pageSelection: null,
            displayMobileMenu: false,
            displaySourceSettings: false,
            user: [],
            editorConfig: {
                // The configuration of the editor.
            },
            loadingState: false,
            assets: [],
            assetsTab: null,
            imageDialogVisible: false,
            imageDialogSelection: 'upload'
        }
    },
    watch: {
        user: {
            handler(val){
              this.saveUser();
            },
            deep: true
        },
        source: {
            handler(val){
                this.saveSource();
            },
            deep: true
        },
        settings: {
            handler(val){
              this.saveSettings();
            },
            deep: true
        }
    },
    created() {
        var vm = this
    },
    methods: {
        filesChange(fieldName, fileList) {
            console.log(fieldName, fileList)
            // handle file changes
            const formData = new FormData();
    
            if (!fileList.length) return;
    
            let reader = new FileReader();
            reader.onload = function () {
                this.$set(this, 'profilePhoto', reader.result)
            }.bind(this)
            if (fileList[0]) {
                reader.readAsDataURL(fileList[0]);
            }
            
            const config = {
                headers: { 'content-type': 'multipart/form-data' }
            }
            formData.append('image', fileList[0]);

            this.$http.post(
                '/storeProfilePhoto', 
                formData,
                config
            ).then(response => {
              this.$set(this.user, 'profilePhoto', response.data.response)
            }).catch((error) => {
                throw error
            });
        },
        follow() {
            this.following = !this.following;
        },
        duplicate() {
            alert('This feature is only available on paid plans.')
        },
        displayImageDialog() {
            this.$set(this, 'imageDialogVisible', true);
        },
        setCurrentType() {
            let decodeType = JSON.parse(this.type);
            this.currentType = decodeType.type;
        },
        setPriceId(priceId) {
            this.priceId = priceId;
        },
        saveUser: debounce(function () {
            if (typeof this.settings.locked != 'undefined' && !this.settings.locked) {
                this.$http.post(
                    '/saveUser', 
                    this.user,
                    {
                        headers: {
                            'Content-Type': 'application/json',
                        }
                    }
                ).then(response => {
                    
                }).catch((error) => {
                    throw error
                });
            }
        }, 500),
        selectPage(option) {
            this.$set(this, 'pageSelection', option);
        },
        getAsyncData: debounce(function (name) {
            if (typeof this.user.slug != 'undefined') {
                if (typeof name == 'undefined' || !name.length) {
                    this.searchData = []
                    return
                }
                this.$http.get(`/fetchSources?source=${name}`)
                    .then(({ data }) => {
                        this.searchData = []
                        data.forEach((item) => this.searchData.push(item))
                    })
                    .catch((error) => {
                        this.searchData = []
                        throw error
                    })
                    .finally(() => {
    
                    })
            } else {
                alert('You must be signed in to access data sources.')
            }
        }, 500),
        selection(option) {
            if (option == null) {
                this.$set(this, 'source', {});
            } else {
                this.$set(this, 'source', option);
                this.content[this.source.slug] = this.source;
                this.sources.push(this.source.slug)
                this.fetchRecords();
            }
        },
        pageOptions() {
           this.$set(this, 'displayPageOptions', true);
        },
        save: debounce(function (text) {
            this.$set(this.content[this.currentSelection], 'text', text)
            let current = this.currentSelection;
            this.$set(this, 'currentSelection', null)
            Vue.nextTick(function () {
                this.$set(this, 'currentSelection', current)
            }.bind(this))
        }, 500),
        clearAll() {
            if (this.object.type && typeof(Storage) !== "undefined") {
                this.object = null;
                sessionStorage.setItem('object', JSON.stringify(this.object));
            }
        },
        saveEdit() {
            if (this.object.type && typeof(Storage) !== "undefined") {
                sessionStorage.setItem('object', JSON.stringify(this.object));
                window.location.assign('/edit-attributes?edit');
            }
        },
        addAttribute() {
            console.log(this.attribute)
        },
        createPage() {
            this.$http.post(
                '/createPage',
                {type: this.pageSelection},
                {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                }
            ).then((response) => {
                window.location.replace(response.data);
            }).catch((error) => {
                throw error
            });
        },
        saveSettings: debounce(function () {
            if (!this.settings.locked) {
              this.$http.post(
                  '/storeSettings', 
                  this.settings,
                  {
                      headers: {
                          'Content-Type': 'application/json',
                      }
                  }
              ).then(response => {
                  
              }).catch((error) => {
                  throw error
              });
            }
        }, 500),
        storeSession(name, value) {
            sessionStorage.setItem(name , value);
        },
        fetchAssets() {
            this.$http.get(`/fetchAssets?slug=${this.assetTab}`)
            .then(({ data }) => {
                this.assets = [];
                data.forEach((item) => {
                    this.assets[item.slug] = JSON.parse(item.data);
                    this.$forceUpdate();
                });
            }).catch((error) => {
                this.assets = [];
                throw error
            });
        },
        fetchForms() {
            this.$http.get(`/fetchForm`)
            .then(({ data }) => {
                this.forms = [];
                let keys = Object.keys(data);
                keys.forEach((item) => this.forms.push(data[item]))
            }).catch((error) => {
                this.forms = [];
                throw error
            });
        },
        fetchFormSubmissions(slug) {
            this.$http.get(`/fetchFormSubmissions?slug=${slug}`)
            .then(({ data }) => {
                this.formSubmissions = [];
                let keys = Object.keys(data);
                keys.forEach((item) => this.formSubmissions.push(data[item]))
            }).catch((error) => {
                this.formSubmissions = [];
                throw error
            });
        },
        delete(slug) {
            console.log(slug)
        }
    },
    beforeMount() {
        if (this.plan && this.plan.length) {
            this.priceId = this.plan;
        }
        this.user = window.App.user;
    }
})
.use(Dropdown)
.use(Autocomplete)
.component('s-stripedeveloperelements', stripedeveloperelements)
.component('s-stripehostingelements', stripehostingelements)
.component('s-stripeelements', stripeelements)
.mount('#app');
