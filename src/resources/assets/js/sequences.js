window.Vue = require('vue');
import Buefy from 'buefy';
import axios from 'axios';
axios.defaults.headers.common['X-CSRF-TOKEN'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
Vue.prototype.$http = axios;

Vue.component('s-app', require('./components/App.vue').default);
Vue.component('s-inputadmin', require('./components/InputAdmin.vue').default);
Vue.component('s-sequences', require('./components/Sequences/Sequences.vue').default);

const app = new Vue({
    el: '#app',
    data: {
        displaySend: false,
        body: window.App.body,
        profile: window.App.profile,
        content: window.App.content,
        settings: window.App.settings
    },
    watch: {

    },
    created() {
        var vm = this
    },
    methods: {
        toggleMode() {
            if (this.settings.editMode) {
                window.location.href = '/sequences'
            } else {
                window.location.href = '/sequences?edit'
            }
        },
        send() {
            this.displaySend = !this.displaySend;
        }
    },
    beforeMount() {
        
    }
});
