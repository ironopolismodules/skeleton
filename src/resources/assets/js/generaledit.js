import { createApp, toHandlers } from 'vue'
import { createStore } from 'vuex'

import sApp from './components/App.vue'
import sWrapper from './components/Element/Element.vue'
import sInput from './components/Input/Input.vue'
import sSvg from './components/Svg/Svg.vue'
import sShape from './components/Shape/Shape.vue'
import sForm from './components/Form/Form.vue'
import sLoop from './components/Loop/Loop.vue'
import sSource from './components/Source/Source.vue'
import sInputAdmin from './components/InputAdmin.vue'
import sFreestyle from './components/Freestyle/Freestyle.vue'
import sMedia from './components/Media/Media.vue'
import sTransition from './components/Transition/Transition.vue'
import sEmbed from './components/Embed/Embed.vue'
import debounce from 'lodash/debounce';

import draggable from 'vuedraggable'
import { Dropdown, Autocomplete } from '@oruga-ui/oruga-next';
import '@oruga-ui/oruga-next/dist/oruga.css';
import { result } from 'lodash'

// Create a new store instance.
const store = createStore({
  state () {
    return {
      editor: window.App.editor,
      classes: {},
      undoStack: [],
      redoStack: [],
      content: window.App.content,
      page: window.App.body,
      settings: window.App.settings,
      users: window.App.users,
      state: window.App.state,
      user: window.App.user,
      entity: window.App.entity,
      profile: window.App.profile
    }
  },
  mutations: {
    scrollToTop(element) {
      element.scrollIntoView({ behavior: "smooth", block: "start" });
    },
    uuid() {
      return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
      );
    },
    async makeRequest(state, { url, query, name, key, method, mode, accept, cache, credentials, headers, redirect, referrerPolicy, data }) {
      var placeholders = url.match(/\$(.*?)\$/g)
      if (placeholders) {
        placeholders.forEach(function(placeholder) {
          let param = placeholder.substring(1, placeholder.length - 1)
          url = url.replace(placeholder, params[param]);
        });
      }
      var placeholders = query.match(/\$(.*?)\$/g)
      if (placeholders) {
        placeholders.forEach(function(placeholder) {
          let param = placeholder.substring(1, placeholder.length - 1)
          query = query.replace(placeholder, params[param]);
        });
      }
      await fetch(url + query, {
        method: method,
        mode: mode,
        accept: accept,
        cache: cache,
        credentials: credentials,
        headers: headers,
        redirect: redirect,
        referrerPolicy: referrerPolicy, 
        body: JSON.stringify(data)
      })
      .then(response => response.json())
      .then(data => {
        if (key) {
          if (name) {
            state.entity[name] = [];
          }
          data[key].forEach((item, index) => {
            let uuid = uuid;
            if (name) {
              state.entity[name].push(uuid);
              state.content[uuid] = item;
            } else {
              state.entity[index] = item;
            }
          })
        } else {
          data.forEach((item) => {
            let uuid = uuid;
            if (name) {
              state.entity[name].push(uuid);
              state.content[uuid] = item;
            } else {
              state.entity[index] = item;
            }
          })
        }
      })
      .catch((error) => {
        console.error('Error:', error);
      });
    },
    createElement(state, { slug, type, parent }) {
      state.content[slug] = {type: type, slug: slug, tag: 'div', classes: []};
      if (parent) {
        state.content[slug].parent = parent;
      }
    },
    destroyElement(state, { slug }) {
      delete state.content[slug];
    },
    insertElement(state, { slug, target }) {
      if (typeof state[target].data == 'undefined') {
        state[target].data = [];
      }
      state[target].data.push(slug);
    },
    insertNestedElement(state, { slug, target, selection }) {
      if (typeof state[target][selection].data == 'undefined') {
        state[target][selection].data = [];
      }
      state[target][selection].data.push(slug);
    },
    insertSectionElement(state, { slug, target, section }) {
      if (typeof state[target][section] == 'undefined') {
        state[target][section] = [];
      }
      state[target][section].push(slug);
    },
    removeElement(state, { section, target, selection, selectionIndex }) {
      if (section) {
        state[target][section].splice(selectionIndex, 1);
      } else if (selection) {
        state[target][selection].data.splice(selectionIndex, 1);
      } else {
        state[target].data.splice(selectionIndex, 1);
      }
    },
    clearAllElements(state) {
      state.settings.header = [];
      state.settings.footer = [];
      if (state.body.header) {
        state.settings.header = [];
      }
      if (state.body.footer) {
        state.settings.footer = [];
      }
      state.body.data = [];
    },
    clearElements(state, { section }) {
      if (section) {
        if (state.page[section]) {
          state.page[section] = [];
        } else if (state.settings[section]) {
          state.settings[section] = [];
        }
      } else {
        state.page.data = [];
      }
      return;
    },
    clearElementAttributes(state) {
      let template = {
        type: state.content[state.editor.currentSelection].type,
        slug: state.content[state.editor.currentSelection].slug,
        tag: 'div',
        classes: []
      } 
      state.content[state.editor.currentSelection] = template;
    },
    addRecord(state, {name, value, source}) {
      if (state.content[source] && state.content[source].fields) {
        let record = {};
        state.content[source].fields.forEach(field => {
          if (field.field == name) {
            record[field.field] = value;
          } else if (typeof field.defaultValue != 'undefined') {
            record[field.field] = field.defaultValue;
          } else {
            record[field.field] = null;
          }
        });
        if (state.content[source].persist) {
          store.commit('persist');
        } else {
          var uniq = String((new Date()).getTime());
          state.content[uniq] = record;
          state.content[source].data.push(uniq);
        }
      }
    },
    removeRecord(state, {source, record}) {
      let index = state.content[source].data.indexOf(record);
      if (index !== -1) {
        state.content[source].data.splice(index, 1);
      }
      if (state.content[source].persist) {
        store.commit('persist');
      }
    },
    async removeRecords(state, {source, store, target}) {
      await state[store][target].forEach(record => {
        var index = state.content[source].data.indexOf(record);
        if (index !== -1) {
          state.content[source].data.splice(index, 1);
        }
        var originalIndex = state.content[source].original.indexOf(record);
        if (originalIndex !== -1) {
          state.content[source].original.splice(originalIndex, 1);
        }
      });
    },
    removeFilters(state, {source}) {
      state.content[source].data = state.content[source].original.slice()
    },
    async filterSource(state, {source, filters, store, target, display}) {
      if (filters) {
        state[store][target] = [];
        await state.content[source].data.forEach(record => {
          let keys = Object.keys(state.content[record]);
          keys.forEach(key => {
            let results = filters.forEach(filter => {
              if (filter.operator == 'equals') {
                //console.log(filter, state.content[record][key])
                if (key == filter.key && state.content[record][key] == filter.value) {
                  state[store][target].push(record);
                }
              }
            });
          });
        });
        if (display) {
          state.content[source].data = state[store][target]
        }
      } else {
        if (typeof state.content[source].original != 'undefined') {
          state.content[source].data = state.content[source].original.slice()
        }
      }
    },
    persistRecord(state, {source, record}) {
      if (state.content[source] && state.content[source].data && state.content[source].data.length) {

      }
    },
    persist(state, { storage, targetStore, target, targetName}) {
      if (storage == 'disk') {
        console.log('store in state on disk');
      } else if (storage == 'session') {
        if (target) {
          sessionStorage.setItem(target+targetName, JSON.stringify(state[targetStore][target][targetName]));
        } else {
          sessionStorage.setItem(targetName, JSON.stringify(state[targetStore][targetName]));
        }
      } else if (storage == 'local') {
        if (target) {
          localStorage.setItem(target+targetName, JSON.stringify(state[targetStore][target][targetName]));
        } else {
          localStorage.setItem(targetName, JSON.stringify(state[targetStore][targetName]));
        }
      }
    },
    findRecords({source, name, value} = {}) {
      if (typeof name != 'undefined' && (typeof value != 'undefined' || typeof condition != 'undefined') && this.$store.state.content[source] && this.$store.state.content[source].data && this.$store.state.content[source].data.length) {
        let filteredResults = [];
        let checkValue = (value == 'true' || value == 'false' || value == 'on') ? Boolean(value) : value;
        this.$store.state.content[source].data.forEach(record => {
          var recordValue = this.$store.state.content[record][name] == 'true' || this.$store.state.content[record][name] == 'false' ? Boolean(this.$store.state.content[record][name]) : this.$store.state.content[record][name];
          if (recordValue == checkValue) {
            filteredResults.push(record)
          }
        });
        return filteredResults;
      }
    },
    selectRecord(state, {record}) {
      if (this.selectedRecords.includes(record)) {
        let index = this.selectedRecords.indexOf(record);
        if (index !== -1) {
          this.selectedRecords.splice(index, 1);
        }
      } else {
        this.selectedRecords.push(record);
      }
    },
    removeSelectedRecords() {
      this.selectedRecords = [];
    },
    updateRecords(state, {name, value}) {
      
    },
    updateAllRecords(state, {source, key, value}) {
      if (state.content[source] && state.content[source].data != 'undefined' && state.content[source].data.length) {
        state.content[source].data.forEach(record => {
          state.content[record][key] = value;
        });
        store.commit('persist', source);
      }
    },
    updateRecord(state, {source, record, name, value}) {
      if (state.content[record]) {
        if (typeof state.content[record][name] == "boolean") {
          state.content[record][name] = !state.content[record][name];
        } else if (state.content[record][name]) {
          state.content[record][name] = value;
        }
        store.commit('persist', source);
      }
    },
    push(state, {objectStore, targetStore, target, object, targetName, objectName, value}) {
      //console.log('running push', objectStore, targetStore, target, object, targetName, objectName, value)
      if (
        object && typeof state[objectStore][object][objectName] == 'undefined' &&
        target && typeof state[targetStore][target][targetName] != 'undefined' &&
        Array.isArray(state[targetStore][target][targetName])
      ) {
        state[targetStore][target][targetName].push(state[objectStore][object][objectName]);
      } else if (
        objectStore && typeof state[objectStore][objectName] != 'undefined' &&
        targetStore && typeof state[targetStore][targetName] != 'undefined' &&
        Array.isArray(state[targetStore][targetName])
      ) {
        state[targetStore][targetName].push(state[objectStore][objectName]);
      } else if (
        target && 
        typeof state[targetStore][target][targetName] != 'undefined' &&
        Array.isArray(state[targetStore][target][targetName])
      ) {
        state[targetStore][target][targetName].push(value);
      } else if (Array.isArray(state[targetStore][targetName])) {
        state[targetStore][targetName].push(value);
      }
    },
    pop(state, {objectStore, targetStore, target, object, targetName, objectName, value}) {
      if (
        object && typeof state[objectStore][object][objectName] != 'undefined' &&
        target && typeof state[store][target][targetName] != 'undefined'
      ) {
        state[targetStore][target][targetName] += state[objectStore][object][objectName].pop();
      } else if (
        objectStore && typeof state[objectStore][objectName] != 'undefined' &&
        targetStore && typeof state[targetStore][targetName] != 'undefined'
      ) {
        state[targetStore][targetName] += state[objectStore][objectName].pop();
      }
    },
    assign(state, {objectStore, targetStore, target, object, targetName, objectName, value}) {
      //console.log('running assign', objectStore, targetStore, target, object, targetName, objectName, value)
      if (
        object && typeof state[objectStore][object][objectName] != 'undefined' &&
        target && typeof state[targetStore][target][targetName] != 'undefined'
      ) {
        state[targetStore][target][targetName] = state[objectStore][object][objectName];
      } else if (
        objectStore && typeof state[objectStore][objectName] != 'undefined' &&
        targetStore && typeof state[targetStore][targetName] != 'undefined'
      ) {
        state[targetStore][targetName] = state[objectStore][objectName];
      } else if (target && typeof state[targetStore][target][targetName] != 'undefined') {
          state[targetStore][target][targetName] = value;
      } else {
        state[targetStore][targetName] = value;
      }
    },
    append(state, {objectStore, targetStore, target, object, targetName, objectName, value}) {
      //console.log('running append', objectStore, targetStore, target, object, targetName, objectName, value);
      if (
        object && typeof state[objectStore][object][objectName] != 'undefined' &&
        target && typeof state[store][target][targetName] != 'undefined'
      ) {
        state[targetStore][target][targetName] += state[objectStore][object][objectName].toString();
      } else if (
        objectStore && typeof state[objectStore][objectName] != 'undefined' &&
        targetStore && typeof state[targetStore][targetName] != 'undefined'
      ) {
        state[targetStore][targetName] += state[objectStore][objectName].toString();
      } else if (target && typeof state[targetStore][target][targetName] != 'undefined') {
          state[targetStore][target][targetName] += value.toString();
      } else if (typeof state[targetStore][targetName] != 'undefined') {
        state[targetStore][targetName] += value.toString();
      }
    }
  }
})

const app = createApp({
  components: {
    draggable,
  },
  data() {
    return {
      id: 1,
      code: '',
      event: null,
      eventTrigger: null,
      eventIndex: null,
      request: null,
      requestIndex: null,
      newParam: '',
      width: null,
      classes: [],
      section: null,
      copiedItem: false,
      targetSelectMode: false,
      objectSelectMode: false,
      watchSelectMode: false,
      currentIndicator: null,
      currentSelection: null,
      currentSource: null,
      previousSelection: [],
      classSelection: 'classes',
      target: null,
      clipboardPos: -1,
      prefix: '',
      sizeSpace: '',
      sign: '',
      statePrefix: '',
      editorPosition: 'bottom',
      backgroundEditorPosition: 'right',
      height: null,
      sources: [],
      fields: [],
      data: [],
      options: true,
      columns: [],
      displayComponents: true,
      displayNav: true,
      showEditor: false,
      menu: null,
      field: 'field',
      source: null,
      record: null,
      fonts: [],
      methods: false,
      searchData: [],
      sectionData: [],
      currentGradient: 0,
      components: [
        'Layout',
        'Basic',
        'Typography',
        'Transitions',
        'Media',
        'Forms',
        'Components'
      ],
      tree: [],
      pageSelectionMenu: 'seo',
      values: [],
      template: {},
      loadingState: false,
      attributes: {},
      selectedComponent: [],
      loadContent: {},
      selectionMenu: 'settings',
      selectedRecords: [],
      pageSettingsSelection: 'meta',
      currentKeyframe: 0,
      calculation: [],
      operand: null,
      operandType: 'number',
      attributeSelection: [],
      currentAttribute: '',
      script: '',
      link: '',
      currentClass: '',
      indicatorIndex: 0,
      sections: ['header', 'footer', null],
      sectionIndex: 0,
      submittedHtml: '',
      currentChild: null,
      templates: [],
      currentCommand: '',
      commandIndex: null,
      attributeSelectMode: false,
      currentKey: '',
      currentValue: ''
    }
  },
  computed: {
    csrf() {
      return document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    },
    separatorsAsRegExp() {
      const sep = [' '];
      return sep.length ? new RegExp(sep.map((s) => {
          return s ? s.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&') : null
      }).join('|'), 'g') : null
    },
  },
  methods: {
    insertKeyValue(store) {
      if (store && this.currentKey != '' && this.currentValue != '') {
        this.$store.commit('assign', {targetStore: store, targetName: this.currentKey, value: this.currentValue});
        this.currentKey = '';
        this.currentValue = '';
      }
    },
    store(storageType, name, data) {
      if (storageType) {
        storageType.setItem(name, JSON.stringify(data));
      }
    },
    sleep(ms) {
      return new Promise(resolve => setTimeout(resolve, ms))
    },
    async traverseChildElements(el, parent) {
      if (el.hasChildNodes()) {
        let children = el.childNodes;
        for (let i = 0; i < children.length; i++) {
          if (children[i].nodeType === 1 && children[i].nodeName != 'SCRIPT'){
            //need a unique reference instead of localname
            this.id = (this.id + 1).toString();
            const src = children[i].getAttribute('src') ? children[i].getAttribute('src') : false;
            const text = [].reduce.call(children[i].childNodes, function(a, b) { return a + (b.nodeType === 3 ? b.textContent : ''); }, '');
            if (parent) {
              this.$store.state.content[this.id] = {type: 's-wrapper', d: children[i].getAttribute('d') ? children[i].getAttribute('d') : '', slug: this.id, tag: children[i].tagName.toLowerCase(), data: [], classes: children[i].getAttribute('class') ? children[i].getAttribute('class').split(' ') : [], parent: parent, src: src, text: text};
              this.$store.state.content[parent].data.push(this.id);
            } else if (this.currentSelection) {
              this.$store.state.content[this.id] = {type: 's-wrapper', d: children[i].getAttribute('d') ? children[i].getAttribute('d') : '', slug: this.id, tag: children[i].tagName.toLowerCase(), data: [], classes: children[i].getAttribute('class') ? children[i].getAttribute('class').split(' ') : [], parent: this.currentSelection, src: src, text: text};
              if (typeof this.$store.state.content[this.currentSelection].data == 'undefined') {
                this.$store.state.content[this.currentSelection].data = [];
              }
              this.$store.state.content[this.currentSelection].data.push(this.id);
              this.currentSelection = null;
            } else if (this.section) {
              this.$store.state.content[this.id] = {type: 's-wrapper', d: children[i].getAttribute('d') ? children[i].getAttribute('d') : '', slug: this.id, tag: children[i].tagName.toLowerCase(), data: [], classes: children[i].getAttribute('class') ? children[i].getAttribute('class').split(' ') : [], parent: this.currentSelection, src: src, text: text};
              if (typeof this.$store.state.page[this.section] == 'undefined') {
                if (typeof this.$store.state.settings[this.section] == 'undefined') {
                  this.$store.state.settings[this.section] = [];
                }
                this.$store.state.settings[this.section].push(this.id);
              } else {
                this.$store.state.page[this.section].push(this.id);
              }
              this.section = null;
            } else {
              this.$store.state.content[this.id] = {type: 's-wrapper', d: children[i].getAttribute('d') ? children[i].getAttribute('d') : '', slug: this.id, tag: children[i].tagName.toLowerCase(), data: [], classes: children[i].getAttribute('class') ? children[i].getAttribute('class').split(' ') : [], src: src, text: text};
              this.$store.state.page.data.push(this.id);
            }
            if (children[i].hasChildNodes()) {
              this.traverseChildElements(children[i], this.id)
            }
          }
        }
      }
    },
    htmlTree(obj) {
      const el = obj || document.getElementById('submittedHtml');
      if (el.hasChildNodes()) {
        this.traverseChildElements(el, null);
      }
    },
    indicateBlock(element) {
      this.$store.state.editor.currentIndicator = element;
    },
    fetchClasses() {
      fetch('/fetchClasses')
        .then(response => response.json())
        .then(data => {
          localStorage.setItem('classes', JSON.stringify(data));
          this.classes = data
        })
        .catch(error => console.error(error));
    },
    clearElements(section) {
      if (!this.$store.state.settings.locked) {
        if (confirm("Are you sure you want to continue?")) {
          this.loading(true);
          let request = `/clearBlocks?`;
          if (key == 'all') {
            request += `&key=${element}&body=${this.$store.state.page.slug}`;
          } else if (key) {
            request += `&key=${element}`;
          } else {
            request += `&body=${this.$store.state.page.slug}`;
          }
          this.$http.get(request)
            .then(({ data }) => {
              if (data) {
                location.reload();
              }
            })
            .catch((error) => {
              this.loading(false);
              throw error;
            });
        }
      } else {
        if (section) {
          this.$store.commit('clearElements', {section: section});
        } else {
          this.$store.commit('clearElements', {section: null});
        }
      }
      this.clearSelection();
    },
    openEditor({menu, selectionMenu, pageSelectionMenu}) {
      this.showEditor = true;
      this.menu = menu;
      if (pageSelectionMenu) {
        this.pageSelectionMenu = pageSelectionMenu;
      } else {
        this.selectionMenu = selectionMenu;
      }
    },
    clearSelection() {
      this.section = null;
      this.$store.state.editor.currentSelection = null;
      this.currentSelection = null;
    },
    removeElement(item, section) {
      if (!this.$store.state.settings.locked) {
        let proceed = true;
        if (this.$store.state.content[item].data && this.$store.state.content[item].data.length) {
          proceed = confirm("This element has children attached to it. Are you sure you want to continue?");
        }
        if (proceed) {
          this.loading(true);
          fetch(`/deleteBlock?selection=${item}&body=${this.$store.state.page.slug}`)
          .then(response => response.json())
          .then(data => {
            if (data) {
              location.reload();
            }
          })
          .catch((error) => {
            throw error;
          });
        } 
      } else {
        if (this.currentSelection) {
          let selectionIndex = this.$store.state.content[this.currentSelection].data.indexOf(item);
          if (this.indicatorIndex != null) {
            this.currentSelection = this.$store.state.content[this.currentSelection].data[this.indicatorIndex];
          }
          if (selectionIndex !== -1) {
            this.$store.commit('removeElement', {target: 'content', selection: this.currentSelection, selectionIndex: selectionIndex});
          }
        } else if (section) {
          let target = this.$store.state.page[section] ? 'page' : 'settings';
          let index = this.$store.state[target][section].indexOf(item);
          if (index !== -1) {
            this.$store.commit('removeElement', {target: target, section: section, selectionIndex: index});
          }
        } else {
          let blocksIndex = this.$store.state.page.data.indexOf(item);
          if (blocksIndex !== -1) {
            this.$store.commit('removeElement', {target: 'page', selectionIndex: blocksIndex});
          }
        }
      }
    },
    selectPreviousElement() {
      if (this.currentSelection && this.$store.state.content[this.currentSelection].parent) {
        this.currentSelection = this.$store.state.content[this.currentSelection].parent;
      } else {
        this.currentSelection = null;
      }
    },
    constructNavigationTree() {
      if (this.$store.state.content[this.currentSelection]) {
        this.tree = [];
        let current = this.currentSelection;
        while(typeof this.$store.state.content[current] != 'undefined' && this.$store.state.content[current].parent) {
          this.tree.push(this.$store.state.content[current].parent);
          current = this.$store.state.content[current].parent;
        }
      }
    },
    insertElement(type, section) {
      if (section) {
        this.section = section;
      }
      if (!this.$store.state.settings.locked && this.loadingState == false) {
        this.loading(true);
        let request = `/insertBlock?type=${type}&body=${this.$store.state.page.slug}`;
        if (this.currentSelection) {
          request += `&selection=${this.currentSelection}`;
        }
        if (this.section) {
          request += `&key=${this.section}`;
        }
        fetch(request)
        .then(response => response.json())
        .then(data => {
          this.$store.state.content[data.slug] = JSON.parse(data.data);
          if (this.section) {
            if (!this.currentSelection) {
              if (typeof this.$store.state.settings[this.section] == 'undefined') {
                this.$store.state.settings[this.section] = [];
              }
              this.$store.state.settings[this.section].push(data.slug)
            } else {
              if (typeof this.$store.state.content[this.currentSelection].data == 'undefined') {
                this.$store.state.content[this.currentSelection].data = [];
              }
              this.$store.state.content[this.currentSelection].data.push(data.slug) 
            }
            if (typeof this.$store.state.settings['blocks'] == 'undefined') {
              this.$store.state.settings['blocks'] = [];
            }
            this.$store.state.settings['blocks'].push(data.slug)

          } else if (this.currentSelection) {
            if (typeof this.$store.state.page['blocks'] == 'undefined') {
              this.$store.state.page['blocks'] = [];
            }
            this.$store.state.page['blocks'].push(data.slug)
            if (typeof this.$store.state.content[this.currentSelection].data == 'undefined') {
              this.$store.state.content[this.currentSelection].data = [];
            }
            this.$store.state.content[this.currentSelection].data.push(data.slug) 
          } else {
            if (typeof this.$store.state.page['blocks'] == 'undefined') {
              this.$store.state.page['blocks'] = [];
            }
            this.$store.state.page['blocks'].push(data.slug)
            this.$store.state.page.data.push(data.slug);
          }
          if (this.$store.state.editor.selectOnInsert) {
            this.currentSelection = data.slug;
            this.$nextTick(() => {
              this.$refs.classinput.focus();
            });
          }
          this.loading(false);
        })
        .catch((error) => {
          this.loading(false);
          throw error;
        })
      } else {
        let slug = String((new Date()).getTime());
        this.$store.commit('createElement', {type: 's-wrapper', slug: slug, parent: this.currentSelection ? this.currentSelection : null});
        if (this.section) {
          if (typeof this.$store.state.page[this.section] != 'undefined') {
            this.$store.commit('insertSectionElement', {target: 'page', section: this.section, slug: slug});
          } else {
            this.$store.commit('insertSectionElement', {target: 'settings', section: this.section, slug: slug});
          }
        } else {
          if (this.currentSelection) {
            this.$store.commit('insertNestedElement', {target: 'content', slug: slug, selection: this.currentSelection});
          } else {
            this.$store.commit('insertElement', {target: 'page', slug: slug});
          }
        }
        if (this.$store.state.editor.selectOnInsert) {
          this.currentSelection = slug;
          this.$nextTick(() => {
            this.$refs.classinput.focus();
          });
        }
      }
    },
    selectElement(slug, section) {
      this.section = section;
      this.currentSelection = slug;
      this.$store.state.editor.currentSelection = slug;
      this.indicatorIndex = 0;
      this.previousSelection.push(this.currentSelection);
      //selects source so that child elements display source fields in menu.
      if (this.$store.state.content[this.currentSelection].source) {
        this.source = this.$store.state.content[this.currentSelection].source;
        if (this.$store.state.content[this.source] && this.$store.state.content[this.source].data) {
          this.record = this.$store.state.content[this.source].data[0]
        }
      }
      this.$nextTick(() => {
        this.$refs.classinput.focus();
      });
      this.constructNavigationTree();
    },
    undo() {
      if (this.clipboardPos > -1) {
        if (this.clipboardPos == 0) {
          this.$store.state.content[this.clipboard[this.clipboardPos].slug] = this.loadContent[this.clipboard[this.clipboardPos].slug]
          this.clipboardPos -= 1;
        } else {
          this.clipboardPos -= 1;
          this.$store.state.content[this.clipboard[this.clipboardPos].slug] = this.clipboard[this.clipboardPos]
        }
      }
    },
    redo() {
      if (this.clipboardPos < this.clipboard.length - 1) {
        this.clipboardPos += 1;
        this.$store.state.content[this.clipboard[this.clipboardPos].slug] = this.clipboard[this.clipboardPos]
      }
    },
    copy() {
      this.copiedItem = this.$store.state.content[this.currentSelection].slug;
    },
    paste() {
      if (!this.$store.state.settings.locked && this.copiedItem) {
        this.loading(true);
        let request = `/pasteBlock?item=${this.copiedItem}&body=${this.$store.state.page.slug}`;
        if (this.currentSelection) {
          request += `&selection=${this.currentSelection}`;
        }
        if (this.section) {
          request += `&key=${this.section}`;
        }
        this.$http.get(request)
          .then(({ data }) => {
            if (data) {
              location.reload();
            }
          })
          .catch((error) => {
            this.loading(false);
            throw error;
          })
      } else if (this.copiedItem) {
        if (this.copiedItem == this.$store.state.content[this.currentSelection].slug) {
          alert('You cannot attach an element to itself as you will create a recursive loop.');
          return;
        }
        if (this.currentSelection) {
          if (typeof this.$store.state.content[this.currentSelection].data == 'undefined') {
            this.$store.state.content[this.currentSelection].data = [];
          }
          this.$store.state.content[this.currentSelection].data.push(this.copiedItem);
        } else if (this.section) {
          this.$store.state.settings[this.section].push(this.copiedItem);
        } else {
          this.$store.state.page.data.push(this.copiedItem);
        }
      }
    },
    loadTemplate(template) {
      this.loading(true);
      let request = '';
      if (!this.$store.state.settings.locked) {
        request = `/loadTemplate?template=${template}&body=${this.$store.state.page.slug}`;
      } else {
        request = `/loadDemoTemplate?template=${template}&body=${this.$store.state.page.slug}`;
      }
      if (this.section) {
        request += `&key=${this.$store.state.editor.section}`;
      } 
      if (this.currentSelection) {
        request += `&selection=${this.$store.state.editor.currentSelection}`;
      } 
      fetch(request)
      .then(response => response.json())
      .then(data => {
        data.content.forEach((item) => {
          this.$store.state.content[item.slug] = item;
        });
        if (this.currentSelection) {
          if (typeof this.$store.state.content[this.$store.state.editor.currentSelection].data == 'undefined') {
            this.$store.state.content[this.$store.state.editor.currentSelection].data = [];
          }
          this.$store.state.content[this.$store.state.editor.currentSelection].data.push(data.root);
          if (typeof this.$store.state.page['blocks'] == 'undefined' || this.$store.state.page['blocks'].length == 0) {
            this.$store.state.page['blocks'] = data.blocks;
          } else if (data.blocks) {
            data.blocks.forEach((item) => {
              this.$store.state.page['blocks'].push(item);
            });
          }
        } else if (this.section) {
          if (typeof this.$store.state.settings[this.$store.state.editor.section] == 'undefined') {
            this.$store.state.settings[this.$store.state.editor.section] = [];
          }
          if (typeof this.$store.state.settings['blocks'] == 'undefined' || this.$store.state.settings['blocks'].length == 0) {
            this.$store.state.settings['blocks'] = data.blocks;
          } else if (data.blocks) {
            data.blocks.forEach((item) => {
              this.$store.state.settings['blocks'].push(item);
            });
          }
          this.$store.state.settings[this.section].push(data.root);
        } else {
          if (typeof this.$store.state.page['blocks'] == 'undefined' || this.$store.state.page['blocks'].length == 0) {
            this.$store.state.page['blocks'] = data.blocks;
          } else if (data.blocks) {
            data.blocks.forEach((item) => {
              this.$store.state.page['blocks'].push(item);
            });
          }
          this.$store.state.page.data.push(data.root);
        }
        this.$store.state.editor.currentSelection = data.root;
        this.$store.state.editor.showEditor = false;
        this.loading(false);
      })
      .catch(error => {
        this.loading(false);
        throw error;
      })
    },
    addClass(item) {
      const itemToAdd = item || this.currentClass.trim()
      if (itemToAdd) {
          const reg = this.separatorsAsRegExp;
          if (reg && itemToAdd.match(reg)) {
            itemToAdd.split(reg)
              .map((t) => t.trim())
              .filter((t) => t.length !== 0)
              .map(this.addClass);
            return
          }
          if (typeof this.$store.state.content[this.currentSelection][this.classSelection] == 'undefined') {
            this.$store.state.content[this.currentSelection][this.classSelection] = []
          }
          if (this.$store.state.content[this.currentSelection][this.classSelection].indexOf(itemToAdd) === -1) {
            this.$store.state.content[this.currentSelection][this.classSelection].push(itemToAdd);
            this.currentClass = '';
          }
      }
    },
    removeClass(index) {
        const item = this.$store.state.content[this.$store.state.editor.currentSelection][this.classSelection].splice(index, 1)[0]
        this.$nextTick(() => {
          this.$refs.classinput.focus();
        });
        return item;
    },
    removeAllClasses() {
      if (this.$store.state.content[this.$store.state.editor.currentSelection][this.classSelection].length > 0) {
        this.$store.state.content[this.$store.state.editor.currentSelection][this.classSelection] = [];
      }
    },
    removeLastClass() {
        if (this.$store.state.content[this.$store.state.editor.currentSelection][this.classSelection].length > 0) {
            this.removeClass(this.$store.state.content[this.$store.state.editor.currentSelection][this.classSelection].length - 1)
        }
    },
    consoleKeydown(event) {
      const { key } = event;
      if (sessionStorage.commands && (key == 'ArrowUp' || key == 'ArrowDown')) {
        let storedCommands = JSON.parse(sessionStorage.getItem("commands"));
        if (this.commandIndex == null) {
          this.commandIndex = 0;
          this.currentCommand = storedCommands[this.commandIndex];
        } else {
          if (key == 'ArrowUp') {
            this.commandIndex++;
            this.currentCommand = storedCommands[Math.abs(this.commandIndex % storedCommands.length)];
          }
          if (key == 'ArrowDown') {
            this.commandIndex--;
            this.currentCommand = storedCommands[Math.abs((this.commandIndex + storedCommands.length) % storedCommands.length)];
          }
        }
      }
      if (key == 'ArrowLeft') {
        this.currentCommand = '';
        this.commandIndex = 0;
      }
    },
    keydown(event) {
      const { key } = event
      if (key === 'Backspace' && !this.currentClass) {
          this.removeLastClass();
          this.save();
      }
      if (key === 'Enter' && !this.isComposing) {
        this.addClass();
        this.save();
      }
    },
    addMethod() {
      if (this.eventTrigger) {
        if (typeof this.$store.state.content[this.currentSelection][this.event] == 'undefined') {
          this.$store.state.content[this.currentSelection][this.event] = []
        }
        this.$store.state.content[this.currentSelection][this.event].push(this.eventTrigger)
        if (typeof this.$store.state.content[this.currentSelection][this.eventTrigger] == 'undefined') {
          this.$store.state.content[this.currentSelection][this.eventTrigger] = [];
        }
        this.$store.state.content[this.currentSelection][this.eventTrigger].push({});
        this.save();
      }
    },
    addCommand(store) {
      if (store == 'page') {
        if (typeof this.$store.state['page'][this.event] == 'undefined') {
          this.$store.state['page'][this.event] = []
        }
        this.$store.state['page'][this.event].push(this.currentCommand)
        this.currentCommand = null;
        this.saveBody();
      } else {
        if (typeof this.$store.state.content[this.currentSelection][this.event] == 'undefined') {
          this.$store.state.content[this.currentSelection][this.event] = []
        }
        this.$store.state.content[this.currentSelection][this.event].push(this.currentCommand)
        this.currentCommand = null;
        this.save();
      }
    },
    addConditionaRenderingLogic() {
      if (typeof this.$store.state.content[this.currentSelection]['conditionalLogic'] == 'undefined') {
        this.$store.state.content[this.currentSelection]['conditionalLogic'] = []
      }
      this.$store.state.content[this.currentSelection]['conditionalLogic'].push(this.currentCommand)
      this.currentCommand = null;
      this.save();
    },
    saveSource() {

    },
    copyClassesToClipboard() {
      if (navigator.clipboard && this.$store.state.content[this.$store.state.editor.currentSelection].classes) {
        navigator.clipboard.writeText(this.$store.state.content[this.$store.state.editor.currentSelection].classes.join(' ')).then(function() {

        }, function(err) {
          console.error('Could not copy classes: ', err);
        });
      }
    },
    highlighter(code) {
      return code;
    },
    selectKeyframe(index) {
      this.currentKeyframe = index;
    },
    newKeyframe() {
      if (typeof this.$store.state.content[this.currentSelection].keyframes == 'undefined') {
        this.$store.state.content[this.currentSelection].keyframes = [];
        if (typeof  this.$store.state.content[this.currentSelection].timing == 'undefined') {
          this.$store.state.content[this.currentSelection].timing = {};
          this.$store.state.content[this.currentSelection].timing.duration = 3000;
          this.$store.state.content[this.currentSelection].timing.iterations = "Infinity";
        }
      }
      this.$store.state.content[this.currentSelection].keyframes.push({});
      this.currentKeyframe = this.$store.state.content[this.currentSelection].keyframes.length - 1;
      this.save();
    },
    deleteAnimation() {
      delete this.$store.state.content[this.currentSelection]['keyframes'];
    },
    removeKeyframe() {
      this.$store.state.content[this.currentSelection].keyframes.splice(this.currentKeyframe, 1);
    },
    addAttribute(key, value) {
      if (typeof key != 'undefined' && typeof value != 'undefined') {
        this.$store.state.content[this.currentSelection].keyframes[this.currentKeyframe][key] = value;
      } else {
        this.$store.state.content[this.currentSelection].keyframes[this.currentKeyframe][this.currentAttribute] = '';
      }
      this.save()
    },
    removeAttribute(index) {
      delete this.$store.state.content[this.currentSelection].keyframes[this.currentKeyframe][index];
      this.save();
    },
    addLink() {
      if (typeof this.$store.state.page.links == 'undefined') {
        this.$store.state.page.links = [];
      }
      this.$store.state.page.links.push({"href": this.link});
      this.saveBody();
      this.link = '';
    },
    removeLink(index) {
      this.$store.state.page.links.splice(index, 1);
      this.saveBody();
    },
    addScript() {
      if (typeof this.$store.state.page.scripts == 'undefined') {
        this.$store.state.page.scripts = [];
      }
      if (this.script != '') {
        this.$store.state.page.scripts.push({"src": this.script});
        this.saveBody();
        this.script = '';
      }
    },
    removeScript(index) {
      this.$store.state.page.scripts.splice(index, 1);
      this.saveBody();
    },
    removeRequest() {
      console.log('test')
    },
    selectInput(item) {
      this.addOperand(item);
      this.calcSelectMode = null;
      this.showEditor = null;
    },
    async methodFactory(event) {
      if (this.$store.state.page[event]) {
          for (const func of this.$store.state.page[event]) {
            let event = {};
            event.target = {};
            event.target.value = func;
            await this.runCommand(event);
          }
      }
      this.event = null;
    },
    async runCommand(event) {
      let command = this.currentCommand || event.target.value;
      var storedCommands = [];
      if (typeof sessionStorage != 'undefined') {
        if (sessionStorage.commands) {
          storedCommands = JSON.parse(sessionStorage.getItem("commands"));
        }
        if (storedCommands.length > 10) {
          storedCommands.shift();
        }
        storedCommands.push(command);
        sessionStorage.setItem("commands", JSON.stringify(storedCommands));
      }
      let result = null;
      let conditionResult = true;
      let quotedStrings = event.target.value.match(/['"].*?['"]/g);
      if (quotedStrings) {
        for (let i = 0; i < quotedStrings.length; i++) {
          command = command.replace(quotedStrings[i].slice(1, -1), encodeURIComponent(quotedStrings[i].slice(1, -1)))
        }
      }
      command = command.split(' ');
      if (command.length == 5) {
          let condition = command.shift();
          conditionResult = await this.execute(decodeURI(condition));
          if (conditionResult) {
              command = command.slice(0,2);
          } else {
              command = command.slice(2,4);
          }
      }
      if (command.length == 3) {
          let condition = command.shift();
          conditionResult = await this.execute(decodeURI(condition));
          if (!conditionResult) {
              return;
          }
      }
      if (command.length == 2) {
        result = await this.execute(decodeURI(command[1]));
        var lhs = null;
        if (command[0][0] == '$') {
          lhs = command[0].substring(1, command[0].length - 1).split('.');
        } else {
          lhs = command[0].split('.');
        }
        if (lhs[2]) {
          this.$store.commit('assign', {value: result, targetStore: lhs[0] , target: lhs[1], targetName: lhs[2]});
        } else {
          this.$store.commit('assign', {value: result, targetStore: lhs[0], targetName: lhs[1]});
        }
      } else {
        result = await this.execute(decodeURI(command[0]));
      }
      this.currentCommand = '';
      event.target.value = '';
    },
    execute(code) {
      var placeholders = code.match(/\$(.*?)\$/g)
      if (placeholders) {
        placeholders.forEach(function(placeholder) {
          var phText = placeholder.substring(1, placeholder.length - 1).split('.');
          if (phText[2]) {
            var hydrate = this.$store.state[phText[0]][phText[1]][phText[2]];
            if (hydrate == '') {
              hydrate = "''";
            }
            if (Array.isArray(this.$store.state[phText[0]][phText[1]][phText[2]])) {
              hydrate = JSON.stringify(this.$store.state[phText[0]][phText[1]][phText[2]])
            }
            code = code.replace(placeholder, hydrate);
          } else if (typeof this.$store.state[phText[0]][phText[1]] != 'undefined'){
            var hydrate = this.$store.state[phText[0]][phText[1]];
            if (hydrate === '') {
              hydrate = "''";
            }
            if (Array.isArray(this.$store.state[phText[0]][phText[1]])) {
              hydrate = JSON.stringify(this.$store.state[phText[0]][phText[1]])
            } 
            code = code.replace(placeholder, hydrate);
          }
        }.bind(this))
      }
      try {
        if (this.$store.state.editor.debugging) {
          console.log('Evaluating...', code);
        }
        let result = eval(code);
        if (this.$store.state.editor.debugging) {
            console.log('Result...', result);
        }
        return result;
        //Function('return ' + code)();
      } catch(error) {
        if (this.$store.state.editor.debugging) {
          console.log('Error...', error);
        }
      }
    },
    removeEvent(event, store) {
      if (store == 'page') {
        let index = this.$store.state.page[this.event].indexOf(event);
        this.$store.state.page[this.event].splice(index, 1);
        this.saveBody();
      } else if (store == 'condition') {
        this.$store.state.content[this.currentSelection]['conditionalLogic'].splice(event, 1);
        this.save();
      } else {
        let index = this.$store.state.content[this.currentSelection][this.event].indexOf(event);
        this.$store.state.content[this.currentSelection][this.event].splice(index, 1);
        this.save();
      }
    },
    displayDialog(type, text) {
      if (type == 'alert') {
          alert(text);
      }
      if (type == 'alert') {
          confirm(text);
      }
      if (type == 'alert') {
        prompt(this.items.text, this.items.placholder);
      }
    },
    removeBodySource(source) {
      let sourceIndex =  this.$store.state.page.sources.indexOf(source);
      if (sourceIndex !== -1) {
        this.$store.state.page.sources.splice(sourceIndex, 1);
      }
    },
    removeBlockSource() {
      this.$store.state.content[this.currentSelection].source = null;
    },
    toggleAutosave() {
      this.$store.state.settings.locked = !this.$store.state.settings.locked;
      if (this.$store.state.settings.locked == false) {
        location.reload();
      }
    },
    loading(state) {
      if (state) {
        this.loadingState = true;
      } else {
        this.loadingState = false;
      }
    },
    changeTag: debounce(function (event) {
      this.$store.state.content[this.currentSelection].tag = event.target.value;
      this.save();
    }, 500),
    getAsyncData: debounce(function (event) {
      if (typeof event.target.value == 'undefined' || !event.target.value.length) {
        this.searchData = []
        return
      }
      let route = '';
      if (this.$store.state.user.slug) {
        route = `/fetchSources?source=${event.target.value}`;
      } else {
        route = `/demoFetchSources?source=${event.target.value}`;
      }
      fetch(route)
      .then(response => response.json())
      .then(data => {
          this.searchData = []
          data.forEach((item) => this.searchData.push(item))
      })
      .catch((error) => {
          this.searchData = []
          throw error
      })
    }, 500),
    setSource() {
      if (this.$store.state.content[this.currentSelection].source) {
        this.source = this.$store.state.content[this.currentSelection].source;
        this.save();
      }
    },
    selection(option) {
      this.$store.state.content[option.slug] = option;
      if (typeof this.$store.state.page.sources == 'undefined') {
        this.$store.state.page.sources = [];
      }
      this.$store.state.page.sources.push(option.slug);
      this.source = option.slug;
      this.saveBody();
      this.fetchRecords();
    },
    getAsyncSection: debounce(function (name) {
      this.$http.get(`/fetchSections?section=${name}`)
      .then(response => response.json())
      .then(data => {
          this.sectionData = []
          data.forEach((item) => this.sectionData.push(item))
      })
      .catch((error) => {
          this.sectionData = []
          throw error
      })
    }, 500),
    sectionSelection(option) {
      this.$store.state.content[this.currentSelection]['asyncSection'] = option.slug;
    },
    fetchRecords() {
      if (this.$store.state.user.slug) {
        fetch(`/fetchRecords?slug=${this.source}`)
        .then(response => response.json())
        .then(data => {
            this.data = [];
            data.forEach((item) => {
                this.$store.state.content[item.slug] = JSON.parse(item.data);
            });
        }).catch((error) => {
            this.data = [];
            throw error
        });
      } else {
        fetch(`/testFetchRecords?slug=${this.source}`)
        .then(response => response.json())
        .then(data => {
            this.data = [];
            data.forEach((item) => {
                this.$store.state.content[item.slug] = JSON.parse(item.data);
            });
        }).catch((error) => {
            this.data = [];
            throw error
        });
      }
    },
    fetchComponent(category) {
      if (localStorage[category]) {
        this.selectedComponent = JSON.parse(localStorage[category]);
      } else {
        fetch(`fetchComponent?category=${category}`, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
          }
        })
        .then(response => response.json())
        .then(data => {
          this.selectedComponent = [];
          this.selectedComponent[category] = data[category];
          localStorage.setItem(category, JSON.stringify(data));
        })
        .catch((error) => {
            this.selectedComponent = [];
            throw error
        })
      }
    },
    storeAsTemplate() {
      if (!this.$store.state.settings.locked && this.currentSelection && this.template) {
        this.loading(true);
        this.template.root = this.currentSelection;
        this.$http.post(
          '/storeAsTemplate', 
          this.template,
          {
              headers: {
                  'Content-Type': 'application/json',
              }
          }
        ).then(response => {
          location.reload();
        }).catch((error) => {
            throw error
        });
      } else {
        alert("You must first create an account before storing templates.")
      }
    },
    targetSelect() {
      this.showEditor = false;
      if (this.currentSelection) {
        this.targetSelectMode = this.currentSelection;
      }
    },
    objectSelect() {
      this.showEditor = false;
      if (this.currentSelection) {
        this.objectSelectMode = this.currentSelection;
      }
    },
    attributeSelect() {
      this.showEditor = false;
      this.attributeSelectMode = true;
    },
    watchSelect() {
      this.showEditor = false;
      this.watchSelectMode = this.currentSelection;
    },
    selectTarget(store, slug, key) {
      if (this.eventTrigger && this.eventIndex != null) {
        if (slug) {
          this.$store.state.content[this.targetSelectMode][this.eventTrigger][this.eventIndex]['target'] = slug;
        }
        this.$store.state.content[this.targetSelectMode][this.eventTrigger][this.eventIndex]['targetStore'] = store;
        this.$store.state.content[this.targetSelectMode][this.eventTrigger][this.eventIndex]['targetName'] = key;
      }
      this.save();
      this.currentSelection = this.targetSelectMode;
      this.targetSelectMode = false;
      this.showEditor = true;
    },
    selectObject(store, slug, key) {
      if (this.eventTrigger && this.eventIndex != null) {
        if (slug) {
          this.$store.state.content[this.currentSelection][this.eventTrigger][this.eventIndex]['object'] = slug;
        }
        this.$store.state.content[this.currentSelection][this.eventTrigger][this.eventIndex]['objectStore'] = store;
        this.$store.state.content[this.currentSelection][this.eventTrigger][this.eventIndex]['objectName'] = key;
      }
      this.save();
      this.objectSelectMode = false;
      this.showEditor = true;
    },
    selectWatch(store, slug, key) {
      if (this.attributeSelectMode) {
        if (slug) {
          this.currentCommand += '$' + store + '.' + slug + '.' + key + '$';
        } else {
          this.currentCommand += '$' + store + '.' + key + '$';
        }
        if (this.currentSelection) {
          this.showEditor = true;
        }
      } else {
        if (slug) {
          this.$store.state.content[this.watchSelectMode]['target'] = slug;
        }
        this.$store.state.content[this.watchSelectMode]['targetStore'] = store;
        this.$store.state.content[this.watchSelectMode]['targetName'] = key;
        this.save();
        this.$store.state.editor.currentSelection = this.watchSelectMode;
        this.currentSelection = this.watchSelectMode;
      }
      this.attributeSelectMode = false;
      this.watchSelectMode = false;
    },
    fetchTemplates(event) {
        fetch(`/fetchTemplates?type=${event.target.value}`)
        .then(response => response.json())
        .then(data => {
            this.templates = [];
            data.forEach((item) => {
                this.templates.push(item)
            });
        }).catch((error) => {
            this.templates = [];
            throw error
        });
    },
    storeSession(name, value) {
      sessionStorage.setItem(name , value);
    },
    onDragEnd() {
      this.save();
    },
    onFileChange(event) {
      const value = event.target.files || event.dataTransfer.files;
      if (!this.$store.state.settings.locked) {
        const config = {
            headers: { 'content-type': 'multipart/form-data' }
        }
        let data = new FormData();
        data.append('image', value[0]);
        this.$http.post(
            '/storeImageAjax', 
            data,
            config
        ).then(response => {
          this.$store.state.content[this.currentSelection].src = response.data.response;
        }).catch((error) => {
            throw error
        });
      } else {
        let reader = new FileReader();
        if (this.$store.state.content[this.currentSelection].tag == 'img') {
          reader.onload = function () {
            this.$store.state.content[this.currentSelection].src = reader.result;
          }.bind(this)
        } else {
          reader.onload = function () {
            this.$store.state.content[this.currentSelection].url = reader.result;
          }.bind(this)
        }
        if (value[0]) {
          reader.readAsDataURL(value[0]);
        }
      }
    },
    fontSelection(event) {
      let font = '';
      if (typeof event.target == 'undefined') {
        font = event;
      } else {
        font = event.target.value;
      }
      this.$store.state.content[this.currentSelection]['fontFamily'] = font;
      WebFont.load({
          google: {
              families: [this.$store.state.content[this.currentSelection].fontFamily]
          }
      });
      this.save();
    },
    loadFonts() {

    },
    fetchSources() {
      if (this.sources.length == 0) {
        fetch(`/fetchAllSources`)
        .then(response => response.json())
        .then(data => {
            this.sources = []
            data.forEach((item) => this.sources.push(JSON.parse(item.data)))
        })
        .catch((error) => {
            this.sources = []
            throw error
        })
        .finally(() => {

        })
      }
    },
    fetchFields(source) {
      if (this.$store.state.content[this.currentSelection].source) {
        fetch(`/fetchFields?source=${this.$store.state.content[this.currentSelection].source}`)
        .then(response => response.json())
        .then(data => {
          if (typeof this.$store.state.page.sources == 'undefined') {
            this.$store.state.page.sources = [];
          }
          if (!this.$store.state.page.sources[data.source.slug]) {
            this.$store.state.page.sources.push(data.source.slug);
          }
          this.$store.state.content[data.source.slug] = data.source;
          if (data.source.fields.length) {
            this.columns = data.source.fields;
          }
          if (data.records) {
            data.records.forEach((item) => {
              this.$store.state.content[item.slug] = JSON.parse(item.data);
              this.data.push(JSON.parse(item.data));
            });
          } else {
            alert('We were unable to find any records, please try again.');
          }
        })
        .catch((error) => {
            this.fields = []
            throw error
        })
        .finally(() => {

        })
      }
    },
    fetchEntities(filters) {
      fetch(`/fetchEntities?path=${window.location.pathname}&filters=${filters}`)
      .then(response => response.json())
      .then(data => {
        let keys = Object.keys(data.data);
        keys.forEach((item) => {
          this.$store.state.content[item] = data.data[item];
        });
        this.entity = data.entity;
      })
      .catch((error) => {
        this.entity = [];
        throw error
      })
    },
    save: debounce(function () {
      if (!this.$store.state.settings.locked && this.$store.state.content[this.$store.state.editor.currentSelection]) {
        fetch('saveBlock', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN': this.csrf
          },
          body: JSON.stringify(this.$store.state.content[this.$store.state.editor.currentSelection]),
        })
        // .then(data => {
        //   console.log('Success:', data);
        // })
        .catch((error) => {
          console.error('Error:', error);
        });
      }
    }, 500),
    saveBody: debounce(function () {
      if (!this.$store.state.settings.locked) {
        fetch('saveBody', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN': this.csrf
          },
          body: JSON.stringify(this.$store.state.page),
        })
        // .then(data => {
        //   console.log('Success:', data);
        // })
        .catch((error) => {
          console.error('Error:', error);
        });
      }
    }, 500),
    saveUser: debounce(function () {
      if (!this.$store.state.settings.locked) {
        fetch('saveUser', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN': this.csrf
          },
          user: JSON.stringify(this.user),
        })
        // .then(data => {
        //   console.log('Success:', data);
        // })
        .catch((error) => {
          console.error('Error:', error);
        });
      }
    }, 500),
    saveSettings: debounce(function () {
      if (!this.$store.state.settings.locked) {
        fetch('storeSettings', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN': this.csrf
          },
          settings: JSON.stringify(this.$store.state.settings),
        })
        // .then(data => {
        //   console.log('Success:', data);
        // })
        .catch((error) => {
          console.error('Error:', error);
        });
      }
    }, 500)
  },
  mounted() {
    //fetch classes
    if (localStorage.classes) {
      this.classes = JSON.parse(localStorage.classes);
    } else {
      this.fetchClasses();
    }
    //fetch components
    if (localStorage.Layout) {
      this.selectedComponent = JSON.parse(localStorage.Layout);
    } else {
      this.fetchComponent('Layout');
    }
    this.$store.state.state.innerHeight = window.innerHeight;
    this.$store.state.state.innerWidth = window.innerWidth;
    this.$store.state.state.scrollX = window.pageXOffset;
    this.$store.state.state.scrollY = window.pageYOffset;
    //handle keypress
    window.addEventListener("keypress", e => {
      if (this.showEditor == false) {
        if (e.key == 'i' && e.ctrlKey) {
          this.insertElement('s-wrapper', null);
        }
        if (e.key == 'o' && e.ctrlKey) {
          this.$store.state.editor.selectOnInsert = !this.$store.state.editor.selectOnInsert;
        }
        if (e.key == 'g' && e.ctrlKey) {
          this.$store.state.editor.guidelines = !this.$store.state.editor.guidelines;
        }
        if (e.key == 'n' && e.ctrlKey) {
          this.$store.state.editor.navigator = !this.$store.state.editor.navigator;
        }
        if (e.key == 'd' && e.ctrlKey) {
          this.$store.state.editor.classbar = !this.$store.state.editor.classbar;
        }
        if (e.key == 'w' && e.ctrlKey) {
          this.$store.state.editor.showComponents = !this.$store.state.editor.showComponents;
        }
        if (e.key == 'u' && e.ctrlKey) {
          this.$store.state.editor.toolbar = !this.$store.state.editor.toolbar;
        }
        if (e.key == 'q' && e.ctrlKey) {
          this.currentSelection = null;
        }
        if (e.key == 'l' && e.ctrlKey) {
          this.selectPreviousElement();
        }
        if (e.key == 't' && e.ctrlKey) {
          if (this.currentSelection) {
            this.$store.state.editor.currentIndicator = this.$store.state.content[this.currentSelection].data[this.indicatorIndex % this.$store.state.content[this.currentSelection].data.length]
          } else if (this.section) {
            if (this.$store.state.page[this.section]) {
              this.$store.state.editor.currentIndicator = this.$store.state.page[this.section][this.indicatorIndex % this.$store.state.page[this.section].length]
            } else {
              this.$store.state.editor.currentIndicator = this.$store.state.settings[this.section][this.indicatorIndex % this.$store.state.settings[this.section].length];
            }
          } else {
            this.$store.state.editor.currentIndicator = this.$store.state.page.data[this.indicatorIndex % this.$store.state.page.data.length]
            this.indicatorIndex++
          }
        }
        if (e.key == 's' && e.ctrlKey) {
          if (this.$store.state.editor.currentIndicator) {
            this.selectElement(this.$store.state.editor.currentIndicator, this.section);
          }
        }
        if (e.key == 'y' && e.ctrlKey) {
          this.section = this.sections[this.sectionIndex % this.sections.length];
          this.indicatorIndex = 0;
          this.sectionIndex++;
        }
        if (e.key == 'r' && e.ctrlKey) {
          this.removeElement(this.currentSelection);
        }
        this.$store.state.state.lastPressed = e.key;
      }
      if (this.currentSelection && e.key == 'e' && e.ctrlKey) {
        this.showEditor = !this.showEditor;
      }
    });
    //handle resize
    window.addEventListener('resize', e => {
      this.$store.state.state.innerHeight = window.innerHeight;
      this.$store.state.state.innerWidth = window.innerWidth;
    });
    //handle resize
    window.addEventListener('scroll', e => {
      this.$store.state.state.scrollX = window.pageXOffset;
      this.$store.state.state.scrollY = window.pageYOffset;
    });
    //observed elements code
    const intersectionObserver = new IntersectionObserver((entries, observer) => {
      entries.forEach((entry) => {
        if (entry.intersectionRatio > 0) {
          entry.target.classList.add(this.$store.state.content[entry.target.id].enabledClasses);
        }
        observer.unobserve(entry.target);
      });
    });
    const elements = [...document.querySelectorAll('.observe')];
    elements.forEach((element) => intersectionObserver.observe(element));
    //load font previews
    if (this.classes && this.classes.families) {
      let fontConfig = [];
      for (let font in this.classes.families.slice(0, 6)) {
        fontConfig.push(this.classes.families[font].name)
      }
      WebFont.load({
          google: {
              families: fontConfig
          }
      });
    }
    this.methodFactory('mounted');
  }
})
.use(store)
.use(Dropdown)
.use(Autocomplete)
.component('s-app', sApp)
.component('s-wrapper', sWrapper)
.component('s-control', sInputAdmin)
.component('s-input', sInput)
.component('s-svg', sSvg)
.component('s-shape', sShape)
.component('s-loop', sLoop)
.component('s-transition', sTransition)
.component('s-form', sForm)
.component('s-media', sMedia)
.component('s-embed', sEmbed)
.component('s-freestyle', sFreestyle)
.component('s-source', sSource)
.mount('#app')