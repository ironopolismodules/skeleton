<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Contact Enquiry</title>
  </head>

  <body>
    <h1>Contact Enquiry</h1>
    @foreach($data['content'] as $field => $value)
      <p><strong>{{ title_case($field) }}: </strong>{{ $value }}</p>
    @endforeach
  </body>
</html>