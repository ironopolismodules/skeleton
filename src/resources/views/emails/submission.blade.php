<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Website Enquiry</title>
  </head>
  <body>
    <h1>Website Submission</h1>
    @foreach($fields as $field => $value)
    <p><strong>{{ title_case($field) }}: </strong>{{ $value }}</p>
    @endforeach
  </body>
</html>
