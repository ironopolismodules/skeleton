@extends('layouts.app')
@section('content')
<div v-cloak class="h-full">
	<header class="bg-white">
		<nav class="relative bg-gray-900 z-50">
			<div class="flex justify-between">
				<div class="flex items-center">
					<a href="/control">
						<svg class="h-8 w-8 text-white p-1" viewBox="0 0 24 24">
							<path fill="currentColor" d="M12,15.4V6.1L13.71,10.13L18.09,10.5L14.77,13.39L15.76,17.67M22,9.24L14.81,8.63L12,2L9.19,8.63L2,9.24L7.45,13.97L5.82,21L12,17.27L18.18,21L16.54,13.97L22,9.24Z" />
						</svg>
					</a>
					<div class="relative">
						<a @click.stop="navigator = !navigator" title="Toggle Navigator" class="w-8 h-8 inline-flex items-center justify-center text-gray-300 hover:text-white focus:outline-none" role="menuitem">
							<span class="sr-only">Toggle Navigator</span>
							<svg class="h-4 w-4 text-gray-300 hover:text-white" viewBox="0 0 24 24" stroke="none" fill="currentColor">
								<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17.27 6.73L13.03 16.86L11.71 13.44L11.39 12.61L10.57 12.29L7.14 10.96L17.27 6.73M21 3L3 10.53V11.5L9.84 14.16L12.5 21H13.46L21 3Z" />
							</svg>
						</a>
					</div>
					<div v-show="loadingState" class="relative w-8 h-8 inline-flex items-center justify-center text-gray-300 hover:text-white focus:outline-none">
						<svg class="animate-spin h-4 w-4" viewBox="0 0 24 24">
							<path fill="currentColor" d="M12,6V9L16,5L12,1V4A8,8 0 0,0 4,12C4,13.57 4.46,15.03 5.24,16.26L6.7,14.8C6.25,13.97 6,13 6,12A6,6 0 0,1 12,6M18.76,7.74L17.3,9.2C17.74,10.04 18,11 18,12A6,6 0 0,1 12,18V15L8,19L12,23V20A8,8 0 0,0 20,12C20,10.43 19.54,8.97 18.76,7.74Z" />
						</svg>
					</div>
				</div>
				<div class="flex items-center relative mr-3">
					@if (!empty($user->initials))
					<o-dropdown class="relative" aria-role="list">
						<button slot="trigger" type="button" class="flex text-gray-300 hover:text-white focus:outline-none" id="user-menu-button" aria-expanded="false" aria-haspopup="true">
							<span class="sr-only">Open user menu</span>
							<svg class="h-4 w-4" viewBox="0 0 24 24">
								<path fill="currentColor" d="M12,4A4,4 0 0,1 16,8A4,4 0 0,1 12,12A4,4 0 0,1 8,8A4,4 0 0,1 12,4M12,14C16.42,14 20,15.79 20,18V20H4V18C4,15.79 7.58,14 12,14Z" />
							</svg>
						</button>
						<div class="origin-top-right absolute z-10 right-0 mt-2 w-48 border border-black bg-gray-800 focus:outline-none" role="menu" aria-orientation="vertical" aria-labelledby="user-menu-button" tabindex="-1">
							<span class="sr-only">Sign out</span>
							<a href="/logout" class="block py-2 px-4 text-xs text-gray-300 hover:text-white hover:bg-gray-700" role="menuitem" tabindex="-1" id="user-menu-item-2">Sign out</a>
						</div>
					</o-dropdown>
					@endif
				</div>
			</div>
		</nav>
	</header>
	<div class="flex overflow-hidden h-screen">
		<aside v-if="navigator" class="hidden sm:block bg-gray-700 text-gray-300 flex-shrink-0 border-l border-black overflow-hidden">
			<div class="w-64 h-full overflow-x-scroll">
				<nav class="flex-1 text-xs">
					<div class="flex items-center justify-between p-2 bg-gray-900">
						<h2 class="flex font-medium">
							<svg class="h-4 w-4" viewBox="0 0 21 15">
								<path fill="currentColor" d="M15 1H1v3h14V1zM6 6v3h14V6H6zm0 8h14v-3H6v3z"></path>
							</svg>
							<span class="ml-2 uppercase text-xs">Navigator</span>
						</h2>
						<div class="flex">
							<button @click.stop="navigator = !navigator" class="hover:text-white">
								<span class="sr-only">Close panel</span> 
								<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true" class="h-4 w-8">
									<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path>
								</svg>
							</button>
						</div>
					</div>
					<div class="relative min-w-md bg-gray-400">
						<label for="search" class="sr-only">Search</label>
						<div class="relative rounded-md shadow-sm">
							<div class="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
								<svg class="h-5 w-5 text-black" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
									<path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd" />
								</svg>
							</div>
							<o-autocomplete
								:data="searchData"
								placeholder="Search"
								field="name"
								@input="getAsyncData"
								@select="selection">
							</o-autocomplete>
							<div class="absolute inset-y-0 right-0 pl-3 flex items-center" title="New table">
								<button @click.stop="addSource" class="cursor-pointer w-8 h-8 inline-flex items-center justify-center text-gray-900 focus:outline-none" aria-expanded="true">
									<span class="sr-only">New table</span>
									<svg class="h-4 w-4" viewBox="0 0 24 24">
										<path fill="currentColor" d="M21 13H14.4L19.1 17.7L17.7 19.1L13 14.4V21H11V14.3L6.3 19L4.9 17.6L9.4 13H3V11H9.6L4.9 6.3L6.3 4.9L11 9.6V3H13V9.4L17.6 4.8L19 6.3L14.3 11H21V13Z" />
									</svg>
								</button>
							</div>
						</div>
					</div>
					<div class="p-2">
						<nav class="mt-3 space-y-1">
							<ul v-if="sources.length" class="m-2">
								<li v-for="(item, index) in sources" :key="index" class="col-span-1 flex shadow-sm">
									<div class="flex-1 flex items-center justify-between border border-black bg-gray-800 truncate">
										<div class="flex-1 px-4 py-2 text-xs leading-5 truncate">
											<button @click.stop="selectSource(item)" class="font-medium transition ease-in-out duration-150 inline-flex items-center justify-center rounded-full bg-transparent hover:text-white focus:outline-none transition ease-in-out duration-150">
												@{{ content[item].name  }}
											</button>
										</div>
										<div class="flex-shrink-0 pr-2">
											<button @click.stop="display = 'content'" class="w-8 h-8 inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
												<svg viewBox="0 0 18 14" class="w-4 h-4"><path fill="currentColor" d="M9.028 1C4.596 1 1 6.94 1 6.94s3.596 6.1 8.028 6.1c4.434 0 8.027-6.1 8.027-6.1S13.462 1 9.028 1zM9 11a4 4 0 01-4-4c0-2.027 1.512-3.683 3.467-3.946A2.48 2.48 0 008 4.5 2.5 2.5 0 0010.5 7a2.49 2.49 0 002.234-1.4c.164.437.266.906.266 1.4a4 4 0 01-4 4z"></path></svg>
											</button>
										</div>
										<div class="flex-shrink-0 pr-2">
											<button @click.stop="display = 'structure'" class="w-8 h-8 inline-flex items-center justify-center bg-transparent focus:outline-none">
												<svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
													<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20.71,7.04C21.1,6.65 21.1,6 20.71,5.63L18.37,3.29C18,2.9 17.35,2.9 16.96,3.29L15.12,5.12L18.87,8.87M3,17.25V21H6.75L17.81,9.93L14.06,6.18L3,17.25Z" />
												</svg>
											</button>
										</div> 
										<div class="flex-shrink-0 pr-2">
											<button @click.stop="openEditor('Table', null, null)" class="w-8 h-8 inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
												<svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" stroke="currentColor" viewBox="0 0 24 24" stroke="none">
													<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z" />
													<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
												</svg>
											</button>
										</div>
									</div>
								</li>
							</ul>
						</nav>
					</div>
				</nav>
			</div>
		</aside>
		<main v-if="source" class="flex-1 overflow-y-scroll">
			<div v-if="display == 'content'">
				<div v-if="columns">
					<table class="w-full overflow-x-scroll divide-y divide-black">
						<thead v-if="source.fields" class="bg-gray-800">
							<tr>
								<th class="w-8"></th>
								<th v-for="(column, index) in source.fields" :key="index + 'header'" class="p-1 text-gray-300 hover:text-white text-left text-xs font-medium uppercase tracking-wider">
									@{{ column.label }}
								</th>
							</tr>
						</thead>
						<tbody v-if="source.data">
							<tr v-for="(row, index) in source.data" :key="index" :class="index % 2 ? 'bg-white' : 'bg-gray-200'" class="overflow-x-auto">
								<td class="w-8 p-2">
									<div class="relative flex justify-end items-center">
										<o-dropdown class="relative z-50" aria-role="list">
											<template v-slot:trigger>
												<button slot="trigger" id="project-options-menu-0" aria-haspopup="true" type="button" class="w-8 h-8 inline-flex items-center justify-center focus:outline-none">
													<span class="sr-only">Open options</span>
													<svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
														<path d="M10 6a2 2 0 110-4 2 2 0 010 4zM10 12a2 2 0 110-4 2 2 0 010 4zM10 18a2 2 0 110-4 2 2 0 010 4z" />
													</svg>
												</button>
											</template>
											<div class="mx-3 origin-top-left absolute left-7 top-0 w-48 mt-1 z-10 bg-gray-800 text-xs text-gray-300 hover:text-white border border-black" role="menu" aria-orientation="vertical" aria-labelledby="project-options-menu-0">
												<div class="py-1" role="none">
													<a href="#" @click="editRecord(row)" class="group flex items-center px-4 py-2 hover:bg-gray-700" role="menuitem">
														<svg class="mr-3 h-4 w-4 group-hover:text-gray-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
															<path d="M17.414 2.586a2 2 0 00-2.828 0L7 10.172V13h2.828l7.586-7.586a2 2 0 000-2.828z" />
															<path fill-rule="evenodd" d="M2 6a2 2 0 012-2h4a1 1 0 010 2H4v10h10v-4a1 1 0 112 0v4a2 2 0 01-2 2H4a2 2 0 01-2-2V6z" clip-rule="evenodd" />
														</svg>
														<span>Edit</span>
													</a>
												</div>
												<div class="py-1" role="none">
													<a href="#" @click="deleteRecord(row)" class="group flex items-center px-4 py-2 hover:bg-gray-700" role="menuitem">
														<svg class="mr-3 h-4 w-4 group-hover:text-gray-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
															<path fill-rule="evenodd" d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z" clip-rule="evenodd" />
														</svg>
														<span>Delete</span>
													</a>
												</div>
												<div class="py-1" role="none">
													<a href="#" @click="duplicateRecord(row)" class="group flex items-center px-4 py-2 hover:bg-gray-700" role="menuitem">
														<svg class="mr-3 h-4 w-4 group-hover:text-gray-500" viewBox="0 0 24 24">
															<path fill="currentColor" d="M11,17H4A2,2 0 0,1 2,15V3A2,2 0 0,1 4,1H16V3H4V15H11V13L15,16L11,19V17M19,21V7H8V13H6V7A2,2 0 0,1 8,5H19A2,2 0 0,1 21,7V21A2,2 0 0,1 19,23H8A2,2 0 0,1 6,21V19H8V21H19Z" />
														</svg>
														<span>Duplicate</span>
													</a>
												</div>
												<div class="py-1" role="none">
													<a href="#" @click="openEditor('Resource', null, row)" class="group flex items-center px-4 py-2 hover:bg-gray-700" role="menuitem">
														<svg class="mr-3 h-4 w-4 group-hover:text-gray-500" viewBox="0 0 24 24">
															<path fill="currentColor" d="M13 19.3V12.6L19 9.2V13C19.7 13 20.4 13.1 21 13.4V7.5C21 7.1 20.8 6.8 20.5 6.6L12.6 2.2C12.4 2.1 12.2 2 12 2S11.6 2.1 11.4 2.2L3.5 6.6C3.2 6.8 3 7.1 3 7.5V16.5C3 16.9 3.2 17.2 3.5 17.4L11.4 21.8C11.6 21.9 11.8 22 12 22S12.4 21.9 12.6 21.8L13.5 21.3C13.2 20.7 13.1 20 13 19.3M12 4.2L18 7.5L16 8.6L10.1 5.2L12 4.2M11 19.3L5 15.9V9.2L11 12.6V19.3M12 10.8L6 7.5L8 6.3L14 9.8L12 10.8M20 15V18H23V20H20V23H18V20H15V18H18V15H20Z" />
														</svg>
														<span>Assign as a Resource</span>
													</a>
												</div>
												<div class="py-1" role="none">
													<a href="#" @click="openEditor('Filterables', null, row)" class="group flex items-center px-4 py-2 hover:bg-gray-700" role="menuitem">
														<svg class="mr-3 h-4 w-4 group-hover:text-gray-500" viewBox="0 0 24 24">
															<path fill="currentColor" d="M12 12V19.88C12.04 20.18 11.94 20.5 11.71 20.71C11.32 21.1 10.69 21.1 10.3 20.71L8.29 18.7C8.06 18.47 7.96 18.16 8 17.87V12H7.97L2.21 4.62C1.87 4.19 1.95 3.56 2.38 3.22C2.57 3.08 2.78 3 3 3H17C17.22 3 17.43 3.08 17.62 3.22C18.05 3.56 18.13 4.19 17.79 4.62L12.03 12H12M15 17H18V14H20V17H23V19H20V22H18V19H15V17Z" />
														</svg>
														<span>Add filterables</span>
													</a>
												</div>
											</div>
										</o-dropdown>
									</div>
								</td>
								<td v-for="(column, colindex) in source.fields" :key="index + ':' + colindex" style="max-width: 100px;" class="overflow-auto px-1 py-1 whitespace-nowrap text-xs">
									<span v-if="content[row] && typeof content[row][column.field] == 'Array'">
									<ul>
										<li v-for="(listitem, index) in content[row][column.field]">@{{ listitem }}</li>
									</ul>
									</span>
									<span v-else>
										@{{ content[row] ? content[row][column.field] : '' }}
									</span>
								</td>
							</tr>
						</tbody>
						<tfoot class="bg-gray-800">
							<tr>
								<td class="whitespace-nowrap text-sm font-medium">
									<button @click="display = 'record'" type="button" class="flex justify-center items-center p-1 text-gray-300 hover:text-white">
										<svg class="h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" stroke="none" fill="currentColor">
											<path fill-rule="evenodd" d="M21 13H14.4L19.1 17.7L17.7 19.1L13 14.4V21H11V14.3L6.3 19L4.9 17.6L9.4 13H3V11H9.6L4.9 6.3L6.3 4.9L11 9.6V3H13V9.4L17.6 4.8L19 6.3L14.3 11H21V13Z" clip-rule="evenodd" />
										</svg>
									</button>
								</td>
								<td v-for="(column, colindex) in source.fields" :key="colindex"></td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<div v-if="display == 'structure'">
				<div class="flex flex-col">
					<div class="overflow-x-auto sm:-mx-6 lg:-mx-8">
						<div class="align-middle inline-block min-w-full sm:px-6 lg:px-8">
							<div class="overflow-hidden">
								<table class="min-w-full text-xs">
									<thead class="bg-gray-800 text-xs font-medium text-gray-300 hover:text-white">
										<tr>
											<th scope="col" class="p-1 text-left uppercase tracking-wider">
												Type
											</th>
											<th scope="col" class="p-1 text-left uppercase tracking-wider">
												Field
											</th>
											<th scope="col" class="p-1 text-left uppercase tracking-wider">
												Label
											</th>
											<th scope="col" class="p-1 text-left uppercase tracking-wider">
												Length
											</th>
											<th scope="col" class="p-1 text-left uppercase tracking-wider">
												Default
											</th>
											<th scope="col" class="p-1 text-left uppercase tracking-wider">
											</th>
										</tr>
									</thead>
									<tbody>
										<tr v-for="(item, index) in source.fields" :key="index" :class="index % 2 ? 'bg-white' : 'bg-gray-200'">
											<!-- <td class="w-8 p-2">
												<div class="relative flex justify-end items-center">
													<o-dropdown class="relative z-50" aria-role="list">
														<button slot="trigger" id="project-options-menu-0" aria-haspopup="true" type="button" class="w-8 h-8 inline-flex items-center justify-center focus:outline-none">
															<span class="sr-only">Open options</span>
															<svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
																<path d="M10 6a2 2 0 110-4 2 2 0 010 4zM10 12a2 2 0 110-4 2 2 0 010 4zM10 18a2 2 0 110-4 2 2 0 010 4z" />
															</svg>
														</button>
														<div class="mx-3 origin-top-left absolute left-7 top-0 w-48 mt-1 z-10 bg-gray-800 text-xs text-gray-300 hover:text-white border border-black" role="menu" aria-orientation="vertical" aria-labelledby="project-options-menu-0">
															<div class="py-1" role="none">
																<a href="#" @click="editRecord(item)" class="group flex items-center px-4 py-2 hover:bg-gray-700" role="menuitem">
																	<svg class="mr-3 h-4 w-4 group-hover:text-gray-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
																		<path d="M17.414 2.586a2 2 0 00-2.828 0L7 10.172V13h2.828l7.586-7.586a2 2 0 000-2.828z" />
																		<path fill-rule="evenodd" d="M2 6a2 2 0 012-2h4a1 1 0 010 2H4v10h10v-4a1 1 0 112 0v4a2 2 0 01-2 2H4a2 2 0 01-2-2V6z" clip-rule="evenodd" />
																	</svg>
																	<span>Edit</span>
																</a>
															</div>
															<div class="py-1" role="none">
																<a href="#" @click="deleteRecord(item)" class="group flex items-center px-4 py-2 hover:bg-gray-700" role="menuitem">
																	<svg class="mr-3 h-4 w-4 group-hover:text-gray-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
																		<path fill-rule="evenodd" d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z" clip-rule="evenodd" />
																	</svg>
																	<span>Delete</span>
																</a>
															</div>
														</div>
													</o-dropdown>
												</div>
											</td> -->
											<td v-for="(row, index) in item" class="p-1 whitespace-nowrap font-medium">
												<span>@{{ row }}</span>
											</td>
											<td class="p-1 whitespace-nowrap text-sm font-medium">
												<button @click="removeField(index)" type="button" class="inline-flex items-center p-1 border border-transparent rounded-full shadow-sm text-black focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-black">
													<svg class="h-4 w-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
														<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
													</svg>
												</button>
											</td>
										</tr>
										<tr class="bg-gray-700">
											<td class="p-1 whitespace-nowrap">
												<select id="type" v-model="type" @change="setCurrentType" class="text-xs placeholder-gray-300 text-gray-300 bg-gray-800 h-8 block">
													<option  value="" disabled selected>Type</option>
													<option value='{"type": "text"}'>Text</option>
													<option value='{"type": "enum"}'>Options</option>
													<option value='{"type": "boolean"}'>Boolean</option>
													<option value='{"type": "editor"}'>Markup</option>
													<option value='{"type": "event"}'>Event</option>
													<option value='{"type": "eventTrigger"}'>EventTrigger</option>
												</select>
											</td>
											<td class="px-1 whitespace-nowrap">
												<input v-model="field" type="text" name="field" id="field" placeholder="Field Name" class="text-xs placeholder-gray-300 text-gray-300 bg-gray-800 h-8 block">
											</td>
											<td class="px-1 whitespace-nowrap">
												<input v-model="label" type="text" name="label" id="label" placeholder="Label" class="text-xs placeholder-gray-300 text-gray-300 bg-gray-800 h-8 block">
											</td>
											<td class="px-1 whitespace-nowrap">
												<input v-if="currentType == 'text'" v-model="length" type="text" placeholder="Length" name="length" id="length" class="text-xs placeholder-gray-300 text-gray-300 bg-gray-800 h-8 block">
												<span v-if="currentType == 'boolean'">1</span>
											</td>
											<td class="px-1 whitespace-nowrap">
												<input v-model="defaultValue" type="text" placeholder="Default" name="default" id="default" class="text-xs placeholder-gray-300 text-gray-300 bg-gray-800 h-8 block">
											</td>
											<td class="p-1 whitespace-nowrap text-sm font-medium">
												<button @click="addField()" type="button" class="inline-flex items-center p-1 text-gray-300 hover:text-white">
													<svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" stroke="none" fill="currentColor">
														<path fill-rule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clip-rule="evenodd" />
													</svg>
												</button>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div v-if="display == 'record'" class="p-1">
				<dl class="space-y-4 text-xs">
					<div class="sm:col-span-1" v-for="(item, index) in this.source.fields" :key="index">
						<dt class=" font-medium text-gray-700">
							@{{ item.label }}
						</dt>
						<dd v-if="currentRecord" class="mt-1 text-gray-900">
							<input v-if="item.type == 'text'" @input="saveRecord" v-model="content[currentRecord][item.field]" type="text" class="bg-gray-800 h-8 block text-gray-300 text-xs">
							<input v-if="item.type == 'number'" v-model="content[currentRecord][item.field]" type="number" class="bg-gray-800 h-8 block text-gray-300 text-xs">
							<input v-if="item.type == 'boolean'" type="checkbox" @input="saveRecord" v-model="content[currentRecord][item.field]" class="block focus:outline-none focus:border-none focus:shadow-none h-4 w-4 text-blue-600 border-gray-300 rounded">
							<select v-else-if="item.type == 'event'" class="block text-xs bg-gray-800 h-8" @change="save" v-model="content[currentRecord][item.field]">
								<option disabled value="">Please select an event type</option>
								<option v-for="(option, index) in profile.events" :key="index" :value="option.value">@{{ option.name }}</option>
							</select>
							<select v-else-if="item.type == 'eventTrigger'" class="block text-xs bg-gray-800 h-8" @change="save" v-model="content[currentRecord][item.field]">
								<option disabled value="">Please select an event trigger</option>
								<option v-for="(option, index) in profile.eventTriggers" :key="index" :value="option.value">@{{ option.name }}</option>
							</select>
							<b-taginput v-else-if="item.type == 'enum'" @input="saveRecord" v-model="content[currentRecord][item.field]"></b-taginput>
						</dd>
						<dd v-else class="mt-1 text-gray-900">
							<input v-if="item.type == 'text'" v-model="newRecord[item.field]" type="text" class="bg-gray-800 h-8 block text-gray-300 text-xs">
							<input v-if="item.type == 'number'" v-model="newRecord[item.field]" type="number" class="bg-gray-800 h-8 block text-gray-300 text-xs">
							<input v-if="item.type == 'boolean'" type="checkbox" v-model="newRecord[item.field]" class="block focus:outline-none focus:border-none focus:shadow-none h-4 w-4 text-blue-600 border-gray-300 rounded">
							
							<b-taginput v-else-if="item.type == 'enum'" v-model="newRecord[item.field]"></b-taginput>
						</dd>
					</div>
				</dl>
				<button v-if="currentRecord" type="submit" @click="currentRecord = null" class="mt-6 inline-flex justify-center py-2 px-2 border border-black font-medium bg-gray-800 hover:bg-gray-700 text-gray-300 hover:text-white group cursor-pointer font-mono text-xs"><span class="sr-only">Create data source</span>
					Deselect
				</button>
				<button v-else type="submit" @click="addRecord" class="mt-6 inline-flex justify-center py-2 px-2 border border-black font-medium bg-gray-800 hover:bg-gray-700 text-gray-300 hover:text-white group cursor-pointer font-mono text-xs"><span class="sr-only">Create data source</span>
					Insert
				</button>
          	</div>
		</main>
		<footer class="absolute bottom-0 left-0 right-0 text-xs">
			<div v-if="showEditor" class="fixed z-50 inset-0 overflow-hidden shadow-xl">
				<div class="absolute inset-0 overflow-hidden">
					<section class="absolute right-0 max-w-full flex" :class="editorPosition == 'bottom' ? 'bottom-0 left-0' : 'pl-10 sm:pl-16 inset-y-0'" aria-labelledby="slide-over-heading">
						<transition
							enter-active-class="transform transition ease-in-out duration-500 sm:duration-700"
							enter-class="translate-x-full"
							enter-to-class="translate-x-0"
							leave-active-class="transform transition ease-in-out duration-500 sm:duration-700"
							leave-class="otranslate-x-0"
							leave-to-class="translate-x-full">
							<div :class="editorPosition == 'bottom' ? 'w-screen' : 'w-64'" :style="editorPosition == 'bottom' ? 'height: 50vh' : ''">
								<div class="h-full flex flex-col bg-gray-700 text-gray-300 border-t border-l border-black">
									<div class="flex items-center justify-between bg-gray-900">
										<h2 id="slide-over-heading" class="pl-2  uppercase font-medium">
											@{{ menu ? menu : 'Edit Record' }}
										</h2>
										<div class="ml-3 h-7 flex items-center mr-2">
											<button v-show="editorPosition == 'bottom'" @click="toggleEditorPosition" class="mr-3 hover:text-white focus:outline-none">
												<span class="sr-only">Editor align bottom</span>
												<svg class="h-4 w-4" viewBox="0 0 24 24" fill="none" stroke="currentColor" aria-hidden="true">
													<path fill="currentColor" d="M6,2H18A2,2 0 0,1 20,4V20A2,2 0 0,1 18,22H6A2,2 0 0,1 4,20V4A2,2 0 0,1 6,2M14,8V16H18V8H14Z" />
												</svg>
											</button>
											<button v-show="editorPosition == 'right'" @click="toggleEditorPosition" class="mr-3 rounded-md hover:text-white focus:outline-none">
												<span class="sr-only">Editior align top</span>
												<svg class="h-4 w-4" viewBox="0 0 24 24" fill="none" stroke="currentColor" aria-hidden="true">
													<path fill="currentColor" d="M6,2H18A2,2 0 0,1 20,4V20A2,2 0 0,1 18,22H6A2,2 0 0,1 4,20V4A2,2 0 0,1 6,2M6,16V20H18V16H6Z" />
												</svg>
											</button>
											<button @click="closeEditor" class="hover:text-white focus:outline-none">
												<span class="sr-only">Close panel</span>
												<svg class="h-4 w-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
													<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
												</svg>
											</button>
										</div>
									</div>
									<div class="overflow-y-scroll h-full">
										<div v-if="menu == 'Resource'" class="p-2">
											<div class="mt-2">
												<label for="path" class="block font-medium">Path</label>
												<input @input="saveRecord" class="block bg-gray-800 h-8" v-model="content[currentRecord].path" placeholder="e.g. black-tee" type="text" autocomplete="off">
											</div>
										</div>
										<div v-if="menu == 'Table' && this.source" class="p-2">
											<div class="mt-2">
												<label for="tableName" class="block font-medium">Table name</label>
												<input @input="saveSource" class="block bg-gray-800 h-8" id="tableName" v-model="source.name" placeholder="e.g. black-tee" type="text" autocomplete="off">
											</div>
											<div class="mt-2">
												<label for="tablePath" class="block font-medium">Path</label>
												<input @input="saveSource" class="block bg-gray-800 h-8" id="tablePath" v-model="source.path" placeholder="/woman" type="text" autocomplete="off">
											</div>
											<div v-if="this.source.data" class="mt-2">
												<label for="path" class="block font-medium">Created Date</label>
												<label for="path" class="block">@{{ Date(this.source.created_at) }}</label>
											</div>
											<div v-if="this.source.data" class="mt-2">
												<label for="path" class="block font-medium">Updated Date</label>
												<label for="path" class="block">@{{ Date(this.source.updated_at) }}</label>
											</div>
											<div v-if="this.source.data" class="mt-2">
												<label for="path" class="block font-medium">Rows</label>
												<label for="path" class="block">@{{ this.source.data.length }}</label>
											</div>
										</div>
										<div v-if="menu == 'Filterables'" class="p-2">
											<div class="mt-2">
												<label for="path" class="block font-medium">Price</label>
												<input class="block bg-gray-800 h-8" v-model="currentFilterable.price" placeholder="0.00" type="text" autocomplete="off">
											</div>
											<div class="mt-2">
												<label for="path" class="block font-medium">Collection</label>
												<s-autocompleteadmin
													:data="collections"
													placeholder="Search"
													field="name"
													@input="fetchCollections"
													@select="selectCollection">
												</s-autocompleteadmin>
												@{{ currentFilterable.collection }}
											</div>
											<div class="mt-2">
												<label for="path" class="block font-medium">Attribute</label>
												<s-autocompleteadmin
													:data="attributes"
													placeholder="Search"
													field="name"
													@input="fetchAttributes"
													@select="selectAttributes">
												</s-autocompleteadmin>
												@{{ currentFilterable.attribute }}
											</div>
											<div class="mt-2">
												<button type="button" class="mt-5 inline-flex items-center px-4 py-2 border border-transparent text-sm font-mono border border-black focus:outline-none focus:ring-2 focus:ring-offset-2">
													Save filterable
												</button>
											</div>
											<div v-if="filterables" class="mt-5 text-black text-xs">
												<table class="min-w-full overflow-x-scroll divide-y divide-gray-200">
													<thead class="p-1 text-gray-300 text-left font-medium uppercase tracking-wider">
														<tr>
															<th>Collection</th>
															<th>Attribute</th>
															<th>Attribute Group</th>
															<th>Price</th>
														</tr>
													</thead>
													<tbody v-if="filterables">
														<tr v-for="(row, index) in filterables" :key="index" :class="index % 2 ? 'bg-white' : 'bg-gray-50'" class="overflow-x-auto">
															<td v-for="(column, colindex) in row" :key="index + ':' + colindex" style="max-width: 100px;" class="overflow-auto px-2 py-4 whitespace-nowrap font-medium">
																@{{ column }}
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</transition>
					</section>
				</div>
			</div>
		</footer>
	</div>
</div>
@endsection
