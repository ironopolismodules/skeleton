<!DOCTYPE html>
<html lang="en">
    <head>
        <title>{{ !empty($body->meta_title) ? $body->meta_title : (!empty($settings['meta_title']) ? $settings['meta_title'] : '') }}</title>
        <link rel='canonical' href="{{ url()->current() }}" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <meta name="keywords" content="{{ !empty($body->meta_keywords) ? $body->meta_keywords : (!empty($settings['meta_keywords']) ? $settings['meta_keywords'] : '') }}">
        <meta name="description" content="{{ !empty($body->meta_description) ? $body->meta_description : (!empty($settings['meta_description']) ? $settings['meta_description'] : '') }}">

        <meta property="og:title" content="Stellify">
        <meta property="og:type" content="website">
        <meta property="og:description" content="Cloud Based Web Development IDE, CMS, and hosting platform | Stellisoft">
        <meta property="og:site_name" content="Stellify Software">
        <meta property="og:locale" content="en_GB">
        <meta property="article:author" content="https://www.stellisoft.com/about">
        <meta property="article:section" content="Home">
        <meta property="og:url" content="https://www.stellisoft.com/">
        <meta property="og:image" content="https://stellifysoftware.s3.eu-west-2.amazonaws.com/webbanner.jpg">
        <meta property="fb:pages" content="108909887213697">

        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="@stellisoft">
        <meta name="twitter:title" content="Stellisoft">
        <meta name="twitter:description" content="Cloud Based Web Development IDE, CMS, and hosting platform | Stellisoft">
        <meta name="twitter:creator" content="@stellisoft">
        <meta name="twitter:image:src" content="https://stellifysoftware.s3.eu-west-2.amazonaws.com/webbanner.jpg">
        <meta name="twitter:image:alt" content="Stellisoft Logo">
        <meta name="twitter:domain" content="www.bbc.co.uk">

        @if (!empty($editMode))
        <style>
            [v-cloak] {
                display: none;
            }
            @if (!empty($body->checkedCheckboxSvg))
            [type=checkbox]:checked {
                background-image: url({!! $body->checkedCheckboxSvg !!})
            }
            @endif
            @if (!empty($body->uncheckedCheckboxSvg))
            [type=checkbox] {
                background-image: url({!! $body->uncheckedCheckboxSvg !!})
            }
            @endif
            @if (!empty($body->checkedRadioSvg))
            [type=radio]:checked {
                background-image: url({!! $body->checkedRadioSvg !!})
            }
            @endif
            @if (!empty($body->uncheckedRadioSvg))
            [type=radio] {
                background-image: url({!! $body->uncheckedRadioSvg !!})
            }
            @endif
            .checked {
                background-image: url("data:image/svg+xml,%3csvg viewBox='0 0 16 16' stroke='black' fill='white' xmlns='http://www.w3.org/2000/svg'%3e%3cpath d='M12.207 4.793a1 1 0 010 1.414l-5 5a1 1 0 01-1.414 0l-2-2a1 1 0 011.414-1.414L6.5 9.086l4.293-4.293a1 1 0 011.414 0z'/%3e%3c/svg%3e");
            }
            .guidelines .grid > *, .guidelines .sm\:grid > *, .guidelines .md\:grid > *, .guidelines .lg\:grid > *, .guidelines .xl\:grid > * {
                border: red dashed 1px;
            }
            .guidelines .flex > *, .guidelines .sm\:flex > *, .guidelines .md\:flex > *, .guidelines .lg\:flex > *, .guidelines .xl\:flex > * {
                border: red dashed 1px;
            }
            .checkered {
                background-image: linear-gradient(45deg, #808080 25%, transparent 25%), linear-gradient(-45deg, #808080 25%, transparent 25%), linear-gradient(45deg, transparent 75%, #808080 75%), linear-gradient(-45deg, transparent 75%, #808080 75%);
                background-size: 20px 20px;
                background-position: 0 0, 0 10px, 10px -10px, -10px 0px;
            }
            .font-effect-fire {
                text-shadow: 0 -0.05em 0.2em #FFF, 0.01em -0.02em 0.15em #FE0, 0.01em -0.05em 0.15em #FC0, 0.02em -0.15em 0.2em #F90, 0.04em -0.20em 0.3em #F70, 0.05em -0.25em 0.4em #F70, 0.06em -0.2em 0.9em #F50, 0.1em -0.1em 1.0em #F40;
                color: #ffe;
            }
            .font-effect-anaglyph {
                text-shadow: -0.06em 0 red, 0.06em 0 cyan;
            }
            .font-effect-neon {
                text-shadow: 0 0 0.1em #fff, 0 0 0.2em #fff, 0 0 0.3em #fff, 0 0 0.4em #f7f, 0 0 0.6em #f0f, 0 0 0.8em #f0f, 0 0 1em #f0f, 0 0 1.2em #f0f;
                color: #fff;
            }
            .font-effect-outline {
                text-shadow: 0 1px 1px #000, 0 -1px 1px #000, 1px 0 1px #000, -1px 0 1px #000;
                color: #fff;
            }
            .font-effect-shadow-multiple {
                text-shadow: 0.04em 0.04em 0 #fff, 0.08em 0.08em 0 #aaa;
                -webkit-text-shadow: .04em .04em 0 #fff, .08em .08em 0 #aaa;
            }
            .font-effect-3d {
                text-shadow: 0px 1px 0px #c7c8ca, 0px 2px 0px #b1b3b6, 0px 3px 0px #9d9fa2, 0px 4px 0px #8a8c8e, 0px 5px 0px #77787b, 0px 6px 0px #636466, 0px 7px 0px #4d4d4f, 0px 8px 7px #001135;
                color: #fff;
            }
            .font-effect-3d-float {
                text-shadow: 0 0.032em 0 #b0b0b0, 0px 0.15em 0.11em rgb(0 0 0 / 15%), 0px 0.25em 0.021em rgb(0 0 0 / 10%), 0px 0.32em 0.32em rgb(0 0 0 / 10%);
                color: #fff;
            }
            @-webkit-keyframes font-effect-fire-animation-keyframes {
                0% {
                    text-shadow: 0 -0.05em 0.2em #FFF, 0.01em -0.02em 0.15em #FE0, 0.01em -0.05em 0.15em #FC0, 0.02em -0.15em 0.2em #F90, 0.04em -0.20em 0.3em #F70,0.05em -0.25em 0.4em #F70, 0.06em -0.2em 0.9em #F50, 0.1em -0.1em 1.0em #F40;
                }
                25% {
                    text-shadow: 0 -0.05em 0.2em #FFF, 0 -0.05em 0.17em #FE0, 0.04em -0.12em 0.22em #FC0, 0.04em -0.13em 0.27em #F90, 0.05em -0.23em 0.33em #F70, 0.07em -0.28em 0.47em #F70, 0.1em -0.3em 0.8em #F50, 0.1em -0.3em 0.9em #F40;
                }
                50% {    text-shadow: 0 -0.05em 0.2em #FFF, 0.01em -0.02em 0.15em #FE0, 0.01em -0.05em 0.15em #FC0, 0.02em -0.15em 0.2em #F90, 0.04em -0.20em 0.3em #F70,0.05em -0.25em 0.4em #F70, 0.06em -0.2em 0.9em #F50, 0.1em -0.1em 1.0em #F40;
                }
                75% {
                    text-shadow: 0 -0.05em 0.2em #FFF, 0 -0.06em 0.18em #FE0, 0.05em -0.15em 0.23em #FC0, 0.05em -0.15em 0.3em #F90, 0.07em -0.25em 0.4em #F70, 0.09em -0.3em 0.5em #F70, 0.1em -0.3em 0.9em #F50, 0.1em -0.3em 1.0em #F40;
                }
                100% {
                    text-shadow: 0 -0.05em 0.2em #FFF, 0.01em -0.02em 0.15em #FE0, 0.01em -0.05em 0.15em #FC0, 0.02em -0.15em 0.2em #F90, 0.04em -0.20em 0.3em #F70,0.05em -0.25em 0.4em #F70, 0.06em -0.2em 0.9em #F50, 0.1em -0.1em 1.0em #F40;
                }
            }
            .font-effect-fire-animation {
                -webkit-animation-duration:0.8s;
                -webkit-animation-name:font-effect-fire-animation-keyframes;
                -webkit-animation-iteration-count:infinite;
                -webkit-animation-direction:alternate;
                color: #ffe;
            }
        </style>
        @endif
        <style>
        .prose ul>li:before {
            background-color: #000 !important;
        }
        .s-prose kbd {
            display: inline-block;
            margin: 0 0.1em;
            padding: 0.1em 0.6em;
            font-size: 8px;
            color: black;
            text-shadow: 0 1px 0 white;
            background-color: rgb(227, 230, 232);
            border: 1px solid rgb(159, 166, 173);
            border-radius: 3px;
            box-shadow: 0 1px 1px hsl(210deg 8% 5% / 15%), inset 0 1px 0 0 hsl(0deg 0% 100%);
            overflow-wrap: break-word;
        }
        </style>
        <!-- <meta http-equiv="Content-Security-Policy" content="connect-src 'self' accounts.google.com"> -->
        @component('head', ['settings' => $settings, 'fonts' => $fonts, 'body' => $body, 'editMode' => $editMode, 'scripts' => $scripts])
        
        @endcomponent
        @if (empty($editMode) && !empty($settings['google-analytics-tracking-code']))
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id={!! $settings['google-analytics-tracking-code'] !!})"></script>
            <script>
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());
                gtag('config', '{!! $settings['google-analytics-tracking-code'] !!}');
            </script>
        @endif
        @if (!empty($editMode))
            <script defer src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
        @endif
    </head>
    <body>
        <div v-cloak id="app" class="h-screen">
            @if (!empty($editMode))
            <div class="flex flex-col min-h-screen" :class="{'bg-gray-300': this.$store.state.settings.editMode}" :style="{backgroundColor: this.$store.state.editor.editorColor}">
                <header class="bg-white border-b border-black">
                    <nav class="relative bg-black z-50">
                        <div class="flex justify-between">
                            <div class="flex items-center">
                                <a href="/control">
                                    <svg class="h-8 w-8 text-white p-1" viewBox="0 0 24 24">
                                        <path fill="currentColor" d="M12,15.4V6.1L13.71,10.13L18.09,10.5L14.77,13.39L15.76,17.67M22,9.24L14.81,8.63L12,2L9.19,8.63L2,9.24L7.45,13.97L5.82,21L12,17.27L18.18,21L16.54,13.97L22,9.24Z" />
                                    </svg>
                                </a>
                                <div class="relative">
                                    <button @click="openEditor({menu: 'Page', selectionMenu: 'seo'})" title="View page settings" class="py-1 px-2 inline-flex items-center justify-center text-gray-300 hover:text-white focus:outline-none text-xs" aria-expanded="true">
                                        <span class="sr-only">View page settings</span>
                                        <span>Page:&nbsp;</span>
                                        <span style="max-width: 90px;" class="truncate"> @{{ this.$store.state.page.name }}</span>
                                    </button>
                                </div>
                                <div class="relative">
                                    <a :href="this.$store.state.page.path == '/' ? this.$store.state.page.path : '/' + this.$store.state.page.path" target="_blank" title="Preview page"  class="w-8 h-8 inline-flex items-center justify-center focus:outline-none" aria-expanded="true">
                                        <span class="sr-only">Preview page</span>
                                        <svg class="h-4 w-4 text-gray-300 hover:text-white" viewBox="0 0 18 14">
                                            <path fill="currentColor" d="M9.028 1C4.596 1 1 6.94 1 6.94s3.596 6.1 8.028 6.1c4.434 0 8.027-6.1 8.027-6.1S13.462 1 9.028 1zM9 11a4 4 0 01-4-4c0-2.027 1.512-3.683 3.467-3.946A2.48 2.48 0 008 4.5 2.5 2.5 0 0010.5 7a2.49 2.49 0 002.234-1.4c.164.437.266.906.266 1.4a4 4 0 01-4 4z" />
                                        </svg>
                                    </a>
                                </div>
                                <div class="hidden sm:block relative">
                                    <a @click="openEditor({menu: 'Commit History', selectionMenu: null})" href="#" title="View current branch info" class="w-8 h-8 inline-flex items-center justify-center text-gray-300 hover:text-white focus:outline-none" aria-expanded="true">
                                        <span class="sr-only">View current project info</span>
                                        <svg class="h-4 w-4 text-gray-300 hover:text-white" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M13,14C9.64,14 8.54,15.35 8.18,16.24C9.25,16.7 10,17.76 10,19A3,3 0 0,1 7,22A3,3 0 0,1 4,19C4,17.69 4.83,16.58 6,16.17V7.83C4.83,7.42 4,6.31 4,5A3,3 0 0,1 7,2A3,3 0 0,1 10,5C10,6.31 9.17,7.42 8,7.83V13.12C8.88,12.47 10.16,12 12,12C14.67,12 15.56,10.66 15.85,9.77C14.77,9.32 14,8.25 14,7A3,3 0 0,1 17,4A3,3 0 0,1 20,7C20,8.34 19.12,9.5 17.91,9.86C17.65,11.29 16.68,14 13,14M7,18A1,1 0 0,0 6,19A1,1 0 0,0 7,20A1,1 0 0,0 8,19A1,1 0 0,0 7,18M7,4A1,1 0 0,0 6,5A1,1 0 0,0 7,6A1,1 0 0,0 8,5A1,1 0 0,0 7,4M17,6A1,1 0 0,0 16,7A1,1 0 0,0 17,8A1,1 0 0,0 18,7A1,1 0 0,0 17,6Z" />
                                        </svg>
                                    </a>
                                </div>
                                <div class="hidden sm:block relative">
                                    <a @click="this.$store.state.editor.debugging = !this.$store.state.editor.debugging" href="#" title="Toggle debugging output" class="w-8 h-8 inline-flex items-center justify-center text-gray-300 hover:text-white focus:outline-none" aria-expanded="true">
                                        <span class="sr-only">Toggle debugging output</span>
                                        <svg v-if="this.$store.state.editor.debugging" class="h-4 w-4 text-gray-300 hover:text-white" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M18,7H15.19C14.74,6.2 14.12,5.5 13.37,5L15,3.41L13.59,2L11.42,4.17C10.96,4.06 10.5,4 10,4C9.5,4 9.05,4.06 8.59,4.17L6.41,2L5,3.41L6.62,5C5.87,5.5 5.26,6.21 4.81,7H2V9H4.09C4.03,9.33 4,9.66 4,10V11H2V13H4V14C4,14.34 4.03,14.67 4.09,15H2V17H4.81C6.26,19.5 9.28,20.61 12,19.65C12,19.43 12,19.22 12,19C12,18.43 12.09,17.86 12.25,17.31C11.59,17.76 10.8,18 10,18A4,4 0 0,1 6,14V10A4,4 0 0,1 10,6A4,4 0 0,1 14,10V14C14,14.19 14,14.39 13.95,14.58C14.54,14.04 15.24,13.62 16,13.35V13H18V11H16V10C16,9.66 15.97,9.33 15.91,9H18V7M21.34,16L17.75,19.59L16.16,18L15,19.16L17.75,22.16L22.5,17.41L21.34,16M12,9V11H8V9H12M12,13V15H8V13H12Z" />
                                        </svg>
                                        <svg v-else class="h-4 w-4 text-gray-300 hover:text-white" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M20,8H17.19C16.74,7.2 16.12,6.5 15.37,6L17,4.41L15.59,3L13.42,5.17C12.96,5.06 12.5,5 12,5C11.5,5 11.05,5.06 10.59,5.17L8.41,3L7,4.41L8.62,6C7.87,6.5 7.26,7.21 6.81,8H4V10H6.09C6.03,10.33 6,10.66 6,11V12H4V14H6V15C6,15.34 6.03,15.67 6.09,16H4V18H6.81C8.47,20.87 12.14,21.84 15,20.18C15.91,19.66 16.67,18.9 17.19,18H20V16H17.91C17.97,15.67 18,15.34 18,15V14H20V12H18V11C18,10.66 17.97,10.33 17.91,10H20V8M16,15A4,4 0 0,1 12,19A4,4 0 0,1 8,15V11A4,4 0 0,1 12,7A4,4 0 0,1 16,11V15M14,10V12H10V10H14M10,14H14V16H10V14Z" />
                                        </svg>
                                    </a>
                                </div>
                                <div class="hidden sm:block relative">
                                    <button @click="openEditor({menu: 'Editor', selectionMenu: null})" title="Editor preferences" aria-haspopup="true" class="w-8 h-8 inline-flex items-center justify-center text-gray-300 hover:text-white focus:outline-none" aria-expanded="true">
                                        <span class="sr-only">Editor preferences</span>
                                        <svg class="h-4 w-4 text-gray-300 hover:text-white" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6V4m0 2a2 2 0 100 4m0-4a2 2 0 110 4m-6 8a2 2 0 100-4m0 4a2 2 0 110-4m0 4v2m0-6V4m6 6v10m6-2a2 2 0 100-4m0 4a2 2 0 110-4m0 4v2m0-6V4" />
                                        </svg>
                                    </button>
                                </div>
                                <div class="relative">
                                    <a href="/control" target="_blank" title="Manage application" aria-haspopup="true" class="w-8 h-8 inline-flex items-center justify-center text-gray-300 hover:text-white focus:outline-none" aria-expanded="true">
                                        <span class="sr-only">Manage application</span>
                                        <svg class="h-4 w-4 text-gray-300 hover:text-white" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M21.7 18.6V17.6L22.8 16.8C22.9 16.7 23 16.6 22.9 16.5L21.9 14.8C21.9 14.7 21.7 14.7 21.6 14.7L20.4 15.2C20.1 15 19.8 14.8 19.5 14.7L19.3 13.4C19.3 13.3 19.2 13.2 19.1 13.2H17.1C16.9 13.2 16.8 13.3 16.8 13.4L16.6 14.7C16.3 14.9 16.1 15 15.8 15.2L14.6 14.7C14.5 14.7 14.4 14.7 14.3 14.8L13.3 16.5C13.3 16.6 13.3 16.7 13.4 16.8L14.5 17.6V18.6L13.4 19.4C13.3 19.5 13.2 19.6 13.3 19.7L14.3 21.4C14.4 21.5 14.5 21.5 14.6 21.5L15.8 21C16 21.2 16.3 21.4 16.6 21.5L16.8 22.8C16.9 22.9 17 23 17.1 23H19.1C19.2 23 19.3 22.9 19.3 22.8L19.5 21.5C19.8 21.3 20 21.2 20.3 21L21.5 21.4C21.6 21.4 21.7 21.4 21.8 21.3L22.8 19.6C22.9 19.5 22.9 19.4 22.8 19.4L21.7 18.6M18 19.5C17.2 19.5 16.5 18.8 16.5 18S17.2 16.5 18 16.5 19.5 17.2 19.5 18 18.8 19.5 18 19.5M12.3 22H3C1.9 22 1 21.1 1 20V4C1 2.9 1.9 2 3 2H21C22.1 2 23 2.9 23 4V13.1C22.4 12.5 21.7 12 21 11.7V6H3V20H11.3C11.5 20.7 11.8 21.4 12.3 22Z" />
                                        </svg>
                                    </a>
                                </div>
                                <div class="relative">
                                    <a href="/cms" title="Manage Datasources" target="_blank" title="CMS" class="w-8 h-8 inline-flex items-center justify-center text-gray-300 hover:text-white focus:outline-none" role="menuitem">
                                        <span class="sr-only">Manage Datasources</span>
                                        <svg class="h-4 w-4 text-gray-300 hover:text-white" viewBox="0 0 24 24" stroke="currentColor" fill="none">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 7v10c0 2.21 3.582 4 8 4s8-1.79 8-4V7M4 7c0 2.21 3.582 4 8 4s8-1.79 8-4M4 7c0-2.21 3.582-4 8-4s8 1.79 8 4m0 5c0 2.21-3.582 4-8 4s-8-1.79-8-4" />
                                        </svg>
                                    </a>
                                </div>
                                <div class="py-1">
                                    <a href="/manage-media" title="Manage Media" target="_blank" class="w-8 h-8 inline-flex items-center justify-center text-gray-300 hover:text-white focus:outline-none" role="menuitem">
                                        <span class="sr-only">Manage Media</span>
                                        <svg class="h-4 w-4 text-gray-300 hover:text-white" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M21,17H7V3H21M21,1H7A2,2 0 0,0 5,3V17A2,2 0 0,0 7,19H21A2,2 0 0,0 23,17V3A2,2 0 0,0 21,1M3,5H1V21A2,2 0 0,0 3,23H19V21H3M15.96,10.29L13.21,13.83L11.25,11.47L8.5,15H19.5L15.96,10.29Z" />
                                        </svg>
                                    </a>
                                </div>
                                <div v-if="this.currentSelection" class="hidden sm:block relative">
                                    <button @click="this.$store.state.editor.toolbar = !this.$store.state.editor.toolbar" title="Toggle Toolbar" aria-haspopup="true" class="w-8 h-8 inline-flex items-center justify-center text-gray-300 hover:text-white focus:outline-none" aria-expanded="true">
                                        <span class="sr-only">Toggle toolbar</span>
                                        <svg class="h-4 w-4 text-gray-300 hover:text-white" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M3 17.25V21H6.75L17.81 9.93L14.06 6.18L3 17.25M22.61 18.36L18.36 22.61L13.16 17.41L14.93 15.64L15.93 16.64L18.4 14.16L19.82 15.58L18.36 17L19.42 18L20.84 16.6L22.61 18.36M6.61 10.83L1.39 5.64L5.64 1.39L7.4 3.16L4.93 5.64L6 6.7L8.46 4.22L9.88 5.64L8.46 7.05L9.46 8.05L6.61 10.83M20.71 7C21.1 6.61 21.1 6 20.71 5.59L18.37 3.29C18 2.9 17.35 2.9 16.96 3.29L15.12 5.12L18.87 8.87L20.71 7Z" />
                                        </svg>
                                    </button>
                                </div>
                                <div class="relative">
                                    <a href="#" @click.stop="this.$store.state.editor.navigator = !this.$store.state.editor.navigator" title="Toggle Navigator" class="w-8 h-8 inline-flex items-center justify-center text-gray-300 hover:text-white focus:outline-none" role="menuitem">
                                        <span class="sr-only">Toggle Navigator</span>
                                        <svg class="h-4 w-4 text-gray-300 hover:text-white" viewBox="0 0 24 24" stroke="none" fill="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17.27 6.73L13.03 16.86L11.71 13.44L11.39 12.61L10.57 12.29L7.14 10.96L17.27 6.73M21 3L3 10.53V11.5L9.84 14.16L12.5 21H13.46L21 3Z" />
                                        </svg>
                                    </a>
                                </div>
                                <div class="relative">
                                    <a href="#" @click.stop="this.$store.state.editor.showComponents = !this.$store.state.editor.showComponents" title="Toggle Page Components" class="w-8 h-8 inline-flex items-center justify-center text-gray-300 hover:text-white focus:outline-none" role="menuitem">
                                        <span class="sr-only">Toggle Page Components</span>
                                        <svg viewBox="0 0 24 24" class="h-4 w-4" stroke="none" fill="currentColor">
                                            <path fill="currentColor" d="M19,19V5H5V19H19M19,3A2,2 0 0,1 21,5V19A2,2 0 0,1 19,21H5A2,2 0 0,1 3,19V5C3,3.89 3.9,3 5,3H19M11,7H13V11H17V13H13V17H11V13H7V11H11V7Z"></path>
                                        </svg>
                                    </a>
                                </div>
                                <div class="relative">
                                    <a href="#" @click.stop="this.$store.state.editor.classbar = !this.$store.state.editor.classbar" title="Toggle Class Input" class="w-8 h-8 inline-flex items-center justify-center text-gray-300 hover:text-white focus:outline-none" role="menuitem">
                                        <span class="sr-only">Toggle Classbar</span>
                                        <svg class="h-4 w-4 text-gray-300 hover:text-white" viewBox="0 0 24 24" stroke="none" fill="currentColor">
                                            <path fill="currentColor" d="M19 3H5C3.9 3 3 3.9 3 5V19C3 20.1 3.9 21 5 21H19C20.1 21 21 20.1 21 19V5C21 3.9 20.1 3 19 3M11 8H9V10C9 11.1 8.1 12 7 12C8.1 12 9 12.9 9 14V16H11V18H9C7.9 18 7 17.1 7 16V15C7 13.9 6.1 13 5 13V11C6.1 11 7 10.1 7 9V8C7 6.9 7.9 6 9 6H11V8M19 13C17.9 13 17 13.9 17 15V16C17 17.1 16.1 18 15 18H13V16H15V14C15 12.9 15.9 12 17 12C15.9 12 15 11.1 15 10V8H13V6H15C16.1 6 17 6.9 17 8V9C17 10.1 17.9 11 19 11V13Z" />
                                        </svg>
                                    </a>
                                </div>
                                <div class="relative">
                                    <a href="#" @click.stop="this.$store.state.editor.console = !this.$store.state.editor.console" title="Toggle Console" class="w-8 h-8 inline-flex items-center justify-center text-gray-300 hover:text-white focus:outline-none" role="menuitem">
                                        <span class="sr-only">Toggle Console</span>
                                        <svg class="h-4 w-4 text-gray-300 hover:text-white" viewBox="0 0 24 24" stroke="none" fill="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13,19V16H21V19H13M8.5,13L2.47,7H6.71L11.67,11.95C12.25,12.54 12.25,13.5 11.67,14.07L6.74,19H2.5L8.5,13Z" />
                                        </svg>
                                    </a>
                                </div>
                                <div class="hidden sm:block relative">
                                    <button @click="openEditor({menu: 'Templates', selectionMenu: null})" title="View Templates" aria-haspopup="true" class="w-8 h-8 inline-flex items-center justify-center text-gray-300 hover:text-white focus:outline-none" aria-expanded="true">
                                        <span class="sr-only">View Templates</span>
                                        <svg class="h-4 w-4 text-gray-300 hover:text-white" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M4 5a1 1 0 011-1h14a1 1 0 011 1v2a1 1 0 01-1 1H5a1 1 0 01-1-1V5zM4 13a1 1 0 011-1h6a1 1 0 011 1v6a1 1 0 01-1 1H5a1 1 0 01-1-1v-6zM16 13a1 1 0 011-1h2a1 1 0 011 1v6a1 1 0 01-1 1h-2a1 1 0 01-1-1v-6z" />
                                        </svg>
                                    </button>
                                </div>
                                <div class="hidden sm:block relative">
                                    <button @click="openEditor({menu: 'Import', selectionMenu: null})" title="Import Markup" aria-haspopup="true" class="w-8 h-8 inline-flex items-center justify-center text-gray-300 hover:text-white focus:outline-none" aria-expanded="true">
                                        <span class="sr-only">Import Markup</span>
                                        <svg class="h-4 w-4 text-yellow-400 hover:text-white" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M9,16V10H5L12,3L19,10H15V16H9M5,20V18H19V20H5Z" />
                                        </svg>
                                    </button>
                                </div>
                                <div class="hidden sm:block relative">
                                    <button @click.stop="this.$store.state.editor.selectOnInsert = !this.$store.state.editor.selectOnInsert" title="Toggle select on insert" aria-haspopup="true" class="w-8 h-8 inline-flex items-center justify-center text-gray-300 hover:text-white focus:outline-none" aria-expanded="true">
                                        <span class="sr-only">Select on Insert</span>
                                        <svg class="h-4 w-4 hover:text-white" :class="this.$store.state.editor.selectOnInsert ? 'text-green-300' : 'text-gray-300'" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M14 12L10 8V11H2V13H10V16M22 12A10 10 0 0 1 2.46 15H4.59A8 8 0 1 0 4.59 9H2.46A10 10 0 0 1 22 12Z" />
                                        </svg>
                                    </button>
                                </div>
                                <div class="hidden sm:block relative">
                                    <button @click.stop="openEditor({menu: 'Key Mappings', selectionMenu: null})" title="Display Keyboard Shortcuts" aria-haspopup="true" class="w-8 h-8 inline-flex items-center justify-center text-gray-300 hover:text-white focus:outline-none" aria-expanded="true">
                                        <span class="sr-only">Display Keyboard Shortcuts</span>
                                        <svg class="h-4 w-4 hover:text-white" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M4,5A2,2 0 0,0 2,7V17A2,2 0 0,0 4,19H20A2,2 0 0,0 22,17V7A2,2 0 0,0 20,5H4M4,7H20V17H4V7M5,8V10H7V8H5M8,8V10H10V8H8M11,8V10H13V8H11M14,8V10H16V8H14M17,8V10H19V8H17M5,11V13H7V11H5M8,11V13H10V11H8M11,11V13H13V11H11M14,11V13H16V11H14M17,11V13H19V11H17M8,14V16H16V14H8Z" />
                                        </svg>
                                    </button>
                                </div>
                            </div>
                            <div class="flex items-center relative mr-3">
                                <div v-show="loadingState" class="ml-3 relative w-8 h-8 inline-flex items-center justify-center text-gray-300 hover:text-white focus:outline-none">
                                    <svg class="animate-spin h-4 w-4" viewBox="0 0 24 24">
                                        <path fill="currentColor" d="M12,6V9L16,5L12,1V4A8,8 0 0,0 4,12C4,13.57 4.46,15.03 5.24,16.26L6.7,14.8C6.25,13.97 6,13 6,12A6,6 0 0,1 12,6M18.76,7.74L17.3,9.2C17.74,10.04 18,11 18,12A6,6 0 0,1 12,18V15L8,19L12,23V20A8,8 0 0,0 20,12C20,10.43 19.54,8.97 18.76,7.74Z" />
                                    </svg>
                                </div>
                                @if (!empty($user))
                                <o-dropdown class="relative" aria-role="list">
                                    <template v-slot:trigger>
                                        <button slot="trigger" title="User options" type="button" class="flex text-gray-300 hover:text-white focus:outline-none" id="user-menu-button" aria-expanded="false" aria-haspopup="true">
                                            <span class="sr-only">Open user menu</span>
                                            <svg class="h-4 w-4" viewBox="0 0 24 24">
                                                <path fill="currentColor" d="M12,4A4,4 0 0,1 16,8A4,4 0 0,1 12,12A4,4 0 0,1 8,8A4,4 0 0,1 12,4M12,14C16.42,14 20,15.79 20,18V20H4V18C4,15.79 7.58,14 12,14Z" />
                                            </svg>
                                        </button>
                                    </template>
                                    <div class="origin-top-right absolute z-10 right-0 mt-2 w-48 border border-black bg-gray-800 focus:outline-none" role="menu" aria-orientation="vertical" aria-labelledby="user-menu-button">
                                        <span class="sr-only">Sign out</span>
                                        <a href="/logout" class="block py-2 px-4 text-xs text-gray-300 hover:text-white hover:bg-gray-700" role="menuitem" id="user-menu-item-2">Sign out</a>
                                    </div>
                                </o-dropdown>
                                @endif
                                <a href="/hosting-plans" title="Launch on the WWW" class="hidden ml-6 sm:flex text-gray-300 hover:text-white focus:outline-none" id="user-menu-button" aria-expanded="false" aria-haspopup="true">
                                    <span class="sr-only">Launch</span>
                                    <svg class="h-4 w-4" viewBox="0 0 18 18">
                                        <path fill="currentColor" d="M12.995 9.076c0-.026.005-.05.005-.076 0-3.304-1.582-6.22-4-8-2.418 1.78-4 4.696-4 8 0 .026.004.05.005.076A7.99 7.99 0 001 16h2.012a7.526 7.526 0 013.095-2.406A9.797 9.797 0 009 17a9.797 9.797 0 002.893-3.406A7.526 7.526 0 0114.988 16H17a7.99 7.99 0 00-4.005-6.924zM9 4.436a1.564 1.564 0 010 3.13 1.564 1.564 0 110-3.13z" />
                                    </svg>
                                    <span class="ml-2 text-xs uppercase">Launch</span>
                                </a>
                            </div>
                        </div>
                        <div v-if="this.$store.state.editor.toolbar && this.currentSelection" class="bg-gray-900 w-full relative hidden items-center sm:inline-flex">
                            <a title="Undo" type="button" @click.stop="this.$store.commit('undo')" :class="false ? 'text-white': 'text-gray-500'" class="cursor-pointer relative inline-flex items-center px-2 py-2 text-sm font-medium">
                                <span class="sr-only">Undo</span>
                                <svg class="h-4 w-4" viewBox="0 0 17 15">
                                    <path fill="currentColor" d="M7 5.035V1L1 7.498 7 14V9.957c2.17-.03 7.258.312 8.562 4.043h.436S15.858 5.624 7 5.035z" />
                                </svg>
                            </a>
                            <a title="Redo" type="button" @click.stop="this.$store.commit('redo')" :class="false ? 'text-white': 'text-gray-500'" class="cursor-pointer -ml-px relative inline-flex items-center px-2 py-2 text-sm font-medium">
                                <span class="sr-only">Redo</span>
                                <svg class="h-4 w-4" viewBox="0 0 17 15">
                                    <path fill="currentColor" d="M16 7.498L10 1v4.035C1.14 5.625 1.002 14 1.002 14h.435C2.742 10.27 7.83 9.928 10 9.957V14l6-6.502z" />
                                </svg>
                            </a>
                            <a title="Copy" type="button" @click.stop="copy" class="cursor-pointer relative inline-flex items-center px-2 py-2 text-gray-300 hover:text-white">
                                <span class="sr-only">Copy</span>
                                <svg class="h-4 w-4" viewBox="0 0 24 24">
                                    <path fill="currentColor" d="M19,21H8V7H19M19,5H8A2,2 0 0,0 6,7V21A2,2 0 0,0 8,23H19A2,2 0 0,0 21,21V7A2,2 0 0,0 19,5M16,1H4A2,2 0 0,0 2,3V17H4V3H16V1Z" />
                                </svg>
                            </a>
                            <a v-if="copiedItem" title="Paste" type="button" @click.stop="paste" class="cursor-pointer relative inline-flex items-center px-2 py-2 text-gray-300 hover:text-white">
                                <span class="sr-only">Paste</span>
                                <svg class="h-4 w-4" viewBox="0 0 24 24">
                                    <path fill="currentColor" d="M19,20H5V4H7V7H17V4H19M12,2A1,1 0 0,1 13,3A1,1 0 0,1 12,4A1,1 0 0,1 11,3A1,1 0 0,1 12,2M19,2H14.82C14.4,0.84 13.3,0 12,0C10.7,0 9.6,0.84 9.18,2H5A2,2 0 0,0 3,4V20A2,2 0 0,0 5,22H19A2,2 0 0,0 21,20V4A2,2 0 0,0 19,2Z" />
                                </svg>
                            </a>
                            <a v-if="this.$store.state.user.id" @click="this.$store.state.editor.locked = !this.$store.state.editor.locked" title="Disable Autosave. Changes will be lost." type="button" class="cursor-pointer -ml-px relative inline-flex items-center px-2 py-2 text-gray-300 hover:text-white">
                                <span class="sr-only">Toggle Autosave</span>
                                <svg v-if="this.$store.state.settings.locked" class="h-4 w-4" style="width:24px;height:24px" fill="none" stroke="currentColor" viewBox="0 0 28 28">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 15v2m-6 4h12a2 2 0 002-2v-6a2 2 0 00-2-2H6a2 2 0 00-2 2v6a2 2 0 002 2zm10-10V7a4 4 0 00-8 0v4h8z" />
                                </svg>
                                <svg v-else class="h-4 w-4" style="width:24px;height:24px" fill="none" stroke="currentColor" viewBox="0 0 28 28">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 11V7a4 4 0 118 0m-4 8v2m-6 4h12a2 2 0 002-2v-6a2 2 0 00-2-2H6a2 2 0 00-2 2v6a2 2 0 002 2z" />
                                </svg>
                            </a>
                            <o-dropdown v-if="this.currentSelection" v-model="this.$store.state.content[this.currentSelection].type" class="relative" aria-role="list" @change="save">
                                <template v-slot:trigger>
                                    <button slot="trigger" type="button" class="whitespace-nowrap inline-flex justify-center w-full px-2 py-2 text-xs font-medium text-gray-300 hover:text-white focus:outline-none" id="options-menu" aria-haspopup="true" aria-expanded="true">
                                        Type
                                    </button>
                                </template> 
                                <div class="z-50 origin-top-right m-1 items-center justify-center absolute left-0 w-56 text-gray-300 bg-gray-800" role="listbox" aria-haspopup="true">
                                    <o-dropdown-item v-for="(item, index) in this.classes.elements" :key="index" :value="item.id" aria-role="listitem">
                                        <div :class="this.$store.state.content[this.currentSelection].type == item.id ? 'bg-gray-600' : null" class="hover:bg-gray-700 cursor-pointer text-xs hover:text-white" role="menuitemcheckbox" aria-disabled="false" aria-checked="false" aria-haspopup="true" id=":gr" style="user-select: none;">
                                            <div class="px-3 py-3" style="user-select: none;">
                                                <span>@{{ item.name }}</span>
                                            </div>
                                        </div>
                                    </o-dropdown-item>
                                </div>
                            </o-dropdown>
                            <o-dropdown v-model="this.$store.state.content[this.currentSelection].tag" class="relative" aria-role="list" @change="save">
                                <template v-slot:trigger>
                                    <button slot="trigger" type="button" class="whitespace-nowrap inline-flex justify-center w-full px-2 py-2 text-xs font-medium text-gray-300 hover:text-white focus:outline-none" id="options-menu" aria-haspopup="true" aria-expanded="true">
                                        Tag: @{{ this.$store.state.content[this.currentSelection].tag }}
                                    </button>
                                </template>
                                <div class="z-50 origin-top-right grid grid-cols-5 m-1 items-center justify-center absolute left-0 w-72 text-gray-300 bg-gray-800" role="listbox" aria-haspopup="true">
                                    <span class="col-span-5 text-xs font-medium p-2">Choose a tag:</span>
                                    <o-dropdown-item v-for="(item, index) in this.classes.tags" :key="index" :value="item" aria-role="listitem">
                                        <div class="text-center hover:bg-gray-700 cursor-pointer text-xs hover:text-white" role="menuitemcheckbox" aria-disabled="false" aria-checked="false" aria-haspopup="true" id=":gr" style="user-select: none;">
                                            <div class="p-3" style="user-select: none;">
                                                <span>@{{ item }}</span>
                                            </div>
                                        </div>
                                    </o-dropdown-item>
                                    <span class="col-span-5 text-xs font-medium p-2">Or type a tag:</span>
                                    <input @input="changeTag" type="text" class="block w-full bg-gray-600 text-white flex-1 sm:text-sm col-span-5" placeholder="Add tag">
                                </div>
                            </o-dropdown>
                            <a @click="prefix = ''" title="Mobile" type="button" :class="{'bg-gray-800': prefix == ''}" class="cursor-pointer relative inline-flex items-center px-2 py-2 text-sm text-gray-300 font-medium">
                                <span class="sr-only">Mobile</span>
                                <svg class="h-4 w-4" viewBox="0 0 24 24">
                                    <path fill="currentColor" d="M16,18H7V4H16M11.5,22A1.5,1.5 0 0,1 10,20.5A1.5,1.5 0 0,1 11.5,19A1.5,1.5 0 0,1 13,20.5A1.5,1.5 0 0,1 11.5,22M15.5,1H7.5A2.5,2.5 0 0,0 5,3.5V20.5A2.5,2.5 0 0,0 7.5,23H15.5A2.5,2.5 0 0,0 18,20.5V3.5A2.5,2.5 0 0,0 15.5,1Z" />
                                </svg>
                            </a>
                            <a @click="prefix = 'sm:'" title="Smart Phone (640px)" type="button" :class="{'bg-gray-800': prefix == 'sm:'}" class="cursor-pointer relative inline-flex items-center px-2 py-2 text-sm text-gray-300 font-medium">
                                <span class="sr-only">Smart Phone</span>
                                <svg class="h-4 w-4" viewBox="0 0 24 24">
                                    <path fill="currentColor" d="M19.25,19H4.75V3H19.25M14,22H10V21H14M18,0H6A3,3 0 0,0 3,3V21A3,3 0 0,0 6,24H18A3,3 0 0,0 21,21V3A3,3 0 0,0 18,0Z" />
                                </svg>
                            </a>
                            <a @click="prefix = 'md:'" title="Tablet (768px)" type="button" :class="{'bg-gray-800': prefix == 'md:'}" class="cursor-pointer relative inline-flex items-center px-2 py-2 text-sm text-gray-300 font-medium">
                                <span class="sr-only">Tablet</span>
                                <svg class="h-4 w-4" viewBox="0 0 24 24">
                                    <path fill="currentColor" d="M19,19H4V3H19M11.5,23A1.5,1.5 0 0,1 10,21.5A1.5,1.5 0 0,1 11.5,20A1.5,1.5 0 0,1 13,21.5A1.5,1.5 0 0,1 11.5,23M18.5,0H4.5A2.5,2.5 0 0,0 2,2.5V21.5A2.5,2.5 0 0,0 4.5,24H18.5A2.5,2.5 0 0,0 21,21.5V2.5A2.5,2.5 0 0,0 18.5,0Z" />
                                </svg>
                            </a>
                            <a @click="prefix = 'lg:'" title="Laptop (1024px)" type="button" :class="{'bg-gray-800': prefix == 'lg:'}" class="cursor-pointer relative inline-flex items-center px-2 py-2 text-sm text-gray-300 ont-medium">
                                <span class="sr-only">Laptop</span>
                                <svg class="h-4 w-4" viewBox="0 0 24 24">
                                    <path fill="currentColor" d="M12,19A1,1 0 0,1 11,18A1,1 0 0,1 12,17A1,1 0 0,1 13,18A1,1 0 0,1 12,19M4,5H20V16H4M20,18A2,2 0 0,0 22,16V5C22,3.89 21.1,3 20,3H4C2.89,3 2,3.89 2,5V16A2,2 0 0,0 4,18H0A2,2 0 0,0 2,20H22A2,2 0 0,0 24,18H20Z" />
                                </svg>
                            </a>
                            <a @click="prefix = 'xl:'" title="Desktop (1280px)" type="button" :class="{'bg-gray-800': prefix == 'xl:'}" class="cursor-pointer relative inline-flex items-center px-2 py-2 text-sm text-gray-300 font-medium">
                                <span class="sr-only">Desktop</span>
                                <svg class="h-4 w-4" viewBox="0 0 24 24">
                                    <path fill="currentColor" d="M21,14H3V4H21M21,2H3C1.89,2 1,2.89 1,4V16A2,2 0 0,0 3,18H10L8,21V22H16V21L14,18H21A2,2 0 0,0 23,16V4C23,2.89 22.1,2 21,2Z" />
                                </svg>
                            </a>
                            <a @click="prefix = '2xl:'" title="Widescreen (1536px)" type="button" :class="{'bg-gray-800': prefix == '2xl:'}"  class="cursor-pointer relative inline-flex items-center px-2 py-2 text-sm text-gray-300 font-medium">
                                <span class="sr-only">Widescreen</span>
                                <svg class="h-4 w-4" viewBox="0 0 24 24">
                                    <path fill="currentColor" d="M21,17H3V5H21M21,3H3A2,2 0 0,0 1,5V17A2,2 0 0,0 3,19H8V21H16V19H21A2,2 0 0,0 23,17V5A2,2 0 0,0 21,3Z" />
                                </svg>
                            </a>
                            <o-dropdown v-model="this.statePrefix" class="relative" aria-role="list" @change="save">
                                <template v-slot:trigger>
                                    <button title="State prefixes" type="button" class="whitespace-nowrap inline-flex justify-center w-full px-2 py-2 text-xs font-medium text-gray-300 hover:text-white focus:outline-none" id="options-menu" aria-haspopup="true" aria-expanded="true">
                                        <svg class="h-4 w-4" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M10.76,8.69A0.76,0.76 0 0,0 10,9.45V20.9C10,21.32 10.34,21.66 10.76,21.66C10.95,21.66 11.11,21.6 11.24,21.5L13.15,19.95L14.81,23.57C14.94,23.84 15.21,24 15.5,24C15.61,24 15.72,24 15.83,23.92L18.59,22.64C18.97,22.46 19.15,22 18.95,21.63L17.28,18L19.69,17.55C19.85,17.5 20,17.43 20.12,17.29C20.39,16.97 20.35,16.5 20,16.21L11.26,8.86L11.25,8.87C11.12,8.76 10.95,8.69 10.76,8.69M15,10V8H20V10H15M13.83,4.76L16.66,1.93L18.07,3.34L15.24,6.17L13.83,4.76M10,0H12V5H10V0M3.93,14.66L6.76,11.83L8.17,13.24L5.34,16.07L3.93,14.66M3.93,3.34L5.34,1.93L8.17,4.76L6.76,6.17L3.93,3.34M7,10H2V8H7V10" />
                                        </svg>
                                    </button>
                                </template>
                                <div class="z-50 origin-top-right m-1 items-center justify-center absolute left-0 w-56 text-gray-300 bg-gray-800" role="listbox" aria-haspopup="true">
                                    <o-dropdown-item v-for="(item, index) in this.classes.states" :key="index" :value="item" aria-role="listitem">
                                        <div :class="this.statePrefix == item ? 'bg-gray-600' : null" class="hover:bg-gray-700 cursor-pointer text-xs hover:text-white" role="menuitemcheckbox" aria-disabled="false" aria-checked="false" aria-haspopup="true" id=":gr" style="user-select: none;">
                                            <div class="px-3 py-3" style="user-select: none;">
                                                <span>@{{ item }}</span>
                                            </div>
                                        </div>
                                    </o-dropdown-item>
                                </div>
                            </o-dropdown>
                            <o-dropdown class="relative" aria-role="list">
                                <template v-slot:trigger>
                                    <button title="Layout" type="button" class="-ml-px cursor-pointer relative inline-flex items-center px-2 py-2 text-sm font-medium text-gray-300 hover:text-white focus:outline-none" id="options-menu" aria-haspopup="true" aria-expanded="true">
                                        <span class="sr-only">Layout</span>
                                        <svg class="h-4 w-4" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M19,3H5A2,2 0 0,0 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V5A2,2 0 0,0 19,3M19,19H5V5H19V19M17,17H7V7H17V17Z" />
                                        </svg>
                                    </button>
                                </template>
                                <div class="z-50 origin-top-right m-1 items-center justify-center absolute right-0 w-96 text-gray-300 bg-gray-800">
                                    <div class="text-xs font-medium pt-3 px-3">Display</div>
                                    <ul class="flex flex-wrap space-x-1 p-1">
                                        <li v-for="(item, index) in this.classes.display" :key="item.class + index" :class="[{'bg-gray-500': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + item.class)}]" class="block text-xs text-gray-300 hover:text-white">
                                            <label :for="prefix + statePrefix + item.class" class="flex p-3 cursor-pointer">@{{ item.name }}</label>
                                        </li>
                                    </ul>
                                    <div class="text-xs font-medium pt-3 px-3">Position</div>
                                    <ul class="flex flex-wrap space-x-1 p-1">
                                        <li v-for="(item, index) in this.classes.position" :key="item.class + index" :class="[{'bg-gray-500': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + item.class)}]" class="block text-xs text-gray-300 hover:text-white">
                                            <label :for="prefix + statePrefix + item.class" class="flex p-3 cursor-pointer">@{{ item.name }}</label>
                                        </li>
                                    </ul>
                                    <div class="text-xs font-medium pt-3 px-3">Container</div>
                                    <label for="container" class="-ml-px cursor-pointer relative inline-flex items-center px-2 py-2 text-sm font-medium text-gray-300 hover:text-white focus:z-10 focus:outline-none">
                                        <span class="sr-only">Container</span>
                                        <input type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" @input="save" name="container" class="ml-2" id="container" :value="prefix + statePrefix + 'container'">
                                    </label>
                                    <div class="text-xs font-medium pt-3 px-3">Isolate</div>
                                    <label for="container" class="-ml-px cursor-pointer relative inline-flex items-center px-2 py-2 text-sm font-medium text-gray-300 hover:text-white focus:z-10 focus:outline-none">
                                        <span class="sr-only">Isolate</span>
                                        <input type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" @input="save" name="container" class="ml-2" id="container" :value="prefix + statePrefix + 'isolate'">
                                    </label>
                                    <div class="text-xs font-medium pt-3 px-3">Box</div>
                                    <ul class="flex flex-wrap space-x-1 p-1">
                                        <li v-for="(item, index) in this.classes.box" :key="item.class + index" :class="[{'bg-gray-500': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + item.class)}]" class="block text-xs text-gray-300 hover:text-white">
                                            <label :for="prefix + statePrefix + item.class" class="flex p-3 cursor-pointer">@{{ item.name }}</label>
                                        </li>
                                    </ul>
                                    <div class="text-xs font-medium pt-3 px-3">z-Index</div>
                                    <ul class="flex flex-wrap space-x-1 p-1">
                                        <li v-for="(item, index) in this.classes.zIndex" :key="item + index" :class="[{'bg-gray-500': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + 'z-' + item)}]" class="block text-xs text-gray-300 hover:text-white">
                                            <label :for="prefix + statePrefix + 'z-' + item" class="flex p-3 cursor-pointer">@{{ item }}</label>
                                        </li>
                                    </ul>
                                    <div class="text-xs font-medium pt-3 px-3">Float</div>
                                    <ul class="flex flex-wrap space-x-1 p-1">
                                        <li v-for="(item, index) in this.classes.float" :key="item.class + index" :class="[{'bg-gray-500': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + item.class)}]" class="block text-xs text-gray-300 hover:text-white">
                                            <label :for="prefix + statePrefix + item.class" class="flex p-3 cursor-pointer">@{{ item.name }}</label>
                                        </li>
                                    </ul>
                                    <div class="text-xs font-medium pt-3 px-3">Visibility</div>
                                    <ul class="flex flex-wrap space-x-1 p-1">
                                        <li v-for="(item, index) in this.classes.visibility" :key="item.class + index" :class="[{'bg-gray-500': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + item.class)}]" class="block text-xs text-gray-300 hover:text-white">
                                            <label :for="prefix + statePrefix + item.class" class="flex p-3 cursor-pointer">@{{ item.name }}</label>
                                        </li>
                                    </ul>
                                </div>
                                <input v-for="(item, index) in this.classes.display" :key="item.class + index" @input="save" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" name="display" :id="prefix + statePrefix + item.class" :value="prefix + statePrefix + item.class">
                                <input v-for="(item, index) in this.classes.position" :key="item.class + index" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" name="position" :id="prefix + statePrefix + item.class" :value="prefix + statePrefix + item.class">
                                <input v-for="(item, index) in this.classes.box" :key="item.class + index" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" name="box" :id="prefix + statePrefix + item.class" :value="prefix + statePrefix + item.class">
                                <input v-for="(item, index) in this.classes.zIndex" :key="item + index" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" name="zIndex" :id="prefix + statePrefix + 'z-' + item" :value="prefix + statePrefix + 'z-' + item">
                                <input v-for="(item, index) in this.classes.float" :key="item.class + index" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" name="box" :id="prefix + statePrefix + item.class" :value="prefix + statePrefix + item.class">
                                <input v-for="(item, index) in this.classes.visibility" :key="item.class + index" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" name="box" :id="prefix + statePrefix + item.class" :value="prefix + statePrefix + item.class">
                            </o-dropdown>
                            <o-dropdown v-model="this.sizeSpace" class="relative" aria-role="list" @change="save">
                                <template v-slot:trigger>
                                    <button title="Dimensions" type="button" class="whitespace-nowrap inline-flex justify-center w-full px-2 py-2 text-xs font-medium text-gray-300 hover:text-white focus:outline-none" id="options-menu" aria-haspopup="true" aria-expanded="true">
                                        <svg class="h-4 w-4" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M3,5V21H9V19.5H7V18H9V16.5H5V15H9V13.5H7V12H9V10.5H5V9H9V5H10.5V9H12V7H13.5V9H15V5H16.5V9H18V7H19.5V9H21V3H5A2,2 0 0,0 3,5M6,7A1,1 0 0,1 5,6A1,1 0 0,1 6,5A1,1 0 0,1 7,6A1,1 0 0,1 6,7Z" />
                                        </svg>
                                    </button>
                                </template>
                                <div class="z-50 origin-top-right m-1 items-center justify-center absolute left-0 w-96 grid grid-cols-4 text-gray-300 bg-gray-800" role="listbox" aria-haspopup="true">
                                    <o-dropdown-item v-for="(item, index) in this.classes.sizeSpace" :key="index" :value="item.value" aria-role="listitem">
                                        <div :class="this.sizeSpace == item.value ? 'bg-gray-600' : null" class="hover:bg-gray-700 cursor-pointer text-xs hover:text-white" role="menuitemcheckbox" aria-disabled="false" aria-checked="false" aria-haspopup="true" id=":gr" style="user-select: none;">
                                            <div class="px-3 py-3" style="user-select: none;">
                                                <span>@{{ item.name }}</span>
                                            </div>
                                        </div>
                                    </o-dropdown-item>
                                </div>
                            </o-dropdown>
                            <o-dropdown class="relative" aria-role="list">
                                <template v-slot:trigger>
                                    <button title="Border Options" slot="trigger" type="button" class="-ml-px cursor-pointer relative inline-flex items-center px-2 py-2 text-sm font-medium text-gray-300 hover:text-white focus:outline-none" id="options-menu" aria-haspopup="true" aria-expanded="true">
                                        <span class="sr-only">Apply border</span>
                                        <svg class="h-4 w-4" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M3,21V3H21V21H3M5,5V19H19V5H5Z" />
                                        </svg>
                                    </button>
                                </template>
                                <div class="z-50 origin-top-left m-1 items-center justify-center absolute right-0 w-96 text-gray-300 bg-gray-800">
                                    <div class="text-xs font-medium pt-3 px-3">Borders</div>
                                    <ul class="flex flex-wrap space-x-1 p-1">
                                        <li v-for="(item, index) in this.classes.borders" :key="index" class="mt-2 flex-1 cursor-pointer" :class="{'ml-1': index == 0}">
                                            <label :for="prefix + statePrefix + 'border' + item" :title="'border' + item" class="block h-8 w-8 border-white bg-gray-400" :class="[{'bg-gray-700': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + 'border' + item)}, 'border' + item]"></label>
                                        </li>
                                    </ul>
                                    <div class="text-xs font-medium pt-3 px-3">Border colour</div>
                                    <ul class="pl-2 flex flex-wrap">
                                        <li v-for="(item, index) in this.classes.colours" :key="index" class="mt-2 cursor-pointer">
                                            <label :for="prefix + statePrefix + 'border-' + item" :title="'border-' + item" class="border border-transparent block w-6 h-6 hover:border-red-400" :class="[{checked: this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + 'border-' + item)}, item == 'transparent' ? 'checkered' : 'bg-' + item]"></label>
                                        </li>
                                    </ul>
                                    <div class="text-xs font-medium pt-3 px-3">Border style</div>
                                    <ul class="p-3 flex">
                                        <li v-for="(item, index) in this.classes.borderStyles" :key="index" class="mr-2 cursor-pointer">
                                            <label :for="prefix + statePrefix + 'border-' + item" :title="'border-' + item" class="block border-2 h-8 w-8 border-white bg-gray-400" :class="[{'bg-gray-700': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + 'border-' + item)}, 'border-' + item]"></label>
                                        </li>
                                    </ul>
                                    <div class="text-xs font-medium pt-3 px-3">Border radius</div>
                                    <ul class="p-3 flex">
                                        <li v-for="(item, index) in this.classes.borderRadius" :key="index" class="flex-1 cursor-pointer">
                                            <label :for="prefix + statePrefix + 'rounded' + item" :title="'rounded' + item" class="block border-2 h-8 w-8 border-white bg-gray-400" :class="[{'bg-gray-700': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + 'rounded' + item)}, 'rounded' + item]"></label>
                                        </li>
                                    </ul>
                                </div>
                                <input v-for="(item, index) in this.classes.borders" :key="index + item" @input="save" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" :id="prefix + statePrefix + 'border' + item" :value="prefix + statePrefix + 'border' + item">
                                <input v-for="(item, index) in this.classes.colours" :key="index + item" @input="save" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" :id="prefix + statePrefix + 'border-' + item" :value="prefix + statePrefix + 'border-' + item">
                                <input v-for="(item, index) in this.classes.borderStyles" :key="index + item" @input="save" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" :id="prefix + statePrefix + 'border-' + item" :value="prefix + statePrefix + 'border-' + item">
                                <input v-for="(item, index) in this.classes.borderRadius" :key="index + item" @input="save" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" :id="prefix + statePrefix + 'rounded' + item" :value="prefix + statePrefix + 'rounded' + item">
                            </o-dropdown>
                            <o-dropdown class="relative" aria-role="list">
                                <template v-slot:trigger>
                                    <button title="Box Shadow" slot="trigger" type="button" class="inline-flex justify-center w-full px-2 py-2 text-sm font-medium text-gray-300 hover:text-white focus:outline-none" id="options-menu" aria-haspopup="true" aria-expanded="true">
                                        <span class="sr-only">Box Shadow</span>
                                        <svg class="h-4 w-4" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M3,3H18V18H3V3M19,19H21V21H19V19M19,16H21V18H19V16M19,13H21V15H19V13M19,10H21V12H19V10M19,7H21V9H19V7M16,19H18V21H16V19M13,19H15V21H13V19M10,19H12V21H10V19M7,19H9V21H7V19Z" />
                                        </svg>
                                    </button>
                                </template>
                                <ul class="z-50 origin-top-right m-1 items-center justify-center absolute cursor-pointer left-0 w-56 text-gray-300 bg-gray-800">
                                    <li v-for="(item, index) in this.classes.shadow" :key="item.class + index" :class="[{'bg-gray-500': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + 'shadow' + item.class)}]" class="block text-xs text-gray-300 hover:text-white">
                                        <label :for="prefix + statePrefix + 'shadow' + item.class" class="flex p-3 cursor-pointer">@{{ item.name }}</label>
                                    </li>
                                </ul>
                                <input v-for="(item, index) in this.classes.shadow" :key="index" @input="save" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" name="overflow" :id="prefix + statePrefix + 'shadow' + item.class" :value="prefix + statePrefix + 'shadow' + item.class">
                            </o-dropdown>
                            <o-dropdown class="relative" aria-role="list">
                                <template v-slot:trigger>
                                    <button title="Animations and Transitions" slot="trigger" type="button" class="inline-flex justify-center w-full px-2 py-2 text-sm font-medium text-gray-300 hover:text-white focus:outline-none" id="options-menu" aria-haspopup="true" aria-expanded="true">
                                        <span class="sr-only">Animation</span>
                                        <svg class="h-4 w-4" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M15,2A7,7 0 0,1 22,9C22,11.71 20.46,14.05 18.22,15.22C17.55,16.5 16.5,17.55 15.22,18.22C14.05,20.46 11.71,22 9,22A7,7 0 0,1 2,15C2,12.29 3.54,9.95 5.78,8.78C6.45,7.5 7.5,6.45 8.78,5.78C9.95,3.54 12.29,2 15,2M12,19A7,7 0 0,1 5,12C4.37,12.84 4,13.87 4,15A5,5 0 0,0 9,20C10.13,20 11.16,19.63 12,19M15,16A7,7 0 0,1 8,9H8C7.37,9.84 7,10.87 7,12A5,5 0 0,0 12,17C13.13,17 14.16,16.63 15,16V16M15,4C13.87,4 12.84,4.37 12,5V5A7,7 0 0,1 19,12H19C19.63,11.16 20,10.13 20,9A5,5 0 0,0 15,4M10,9A5,5 0 0,0 15,14C15.6,14 16.17,13.9 16.7,13.7C16.9,13.17 17,12.6 17,12A5,5 0 0,0 12,7C11.4,7 10.83,7.1 10.3,7.3C10.1,7.83 10,8.4 10,9Z" />
                                        </svg>
                                    </button>
                                </template>
                                <div class="z-50 origin-top-left m-1 items-center w-96 justify-center absolute right-0 text-gray-300 bg-gray-800">
                                    <div class="text-xs font-medium pt-3 px-3">Animations</div>
                                    <ul class="flex">
                                        <li v-for="(item, index) in this.classes.animations" :key="index" :class="[{'bg-gray-700': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + item)}]" class="block px-4 py-2 hover:bg-gray-700">
                                            <label :for="prefix + statePrefix + item" style="line-height: 0;" class="relative inline-flex h-3 w-3 bg-purple-500" :class="item"></label>
                                        </li>
                                    </ul>
                                    <div class="text-xs font-medium pt-3 px-3">Tranistion Properties</div>
                                    <ul class="flex flex-wrap space-x-1 p-1">
                                        <li v-for="(item, index) in this.classes.transitionProperties" :key="item.class + index" :class="[{'bg-gray-500': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + 'transition' + item.class)}]" class="block text-xs text-gray-300 hover:text-white">
                                            <label :for="prefix + statePrefix + 'transition' + item.class" class="flex p-3 cursor-pointer">@{{ item.name }}</label>
                                        </li>
                                    </ul>
                                    <div class="text-xs font-medium pt-3 px-3">Duration</div>
                                    <ul class="flex flex-wrap space-x-1 p-1">
                                        <li v-for="(item, index) in this.classes.duration" :key="item + index" :class="[{'bg-gray-500': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + 'duration-' + item)}]" class="block text-xs text-gray-300 hover:text-white">
                                            <label :for="prefix + statePrefix + 'duration-' + item" class="flex p-3 cursor-pointer">@{{ item }}</label>
                                        </li>
                                    </ul>
                                    <div class="text-xs font-medium pt-3 px-3">Timing</div>
                                    <ul class="flex flex-wrap space-x-1 p-1">
                                        <li v-for="(item, index) in this.classes.timing" :key="item.class + index" :class="[{'bg-gray-500': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + 'ease-' + item.class)}]" class="block text-xs text-gray-300 hover:text-white">
                                            <label :for="prefix + statePrefix + 'ease-' + item.class" class="flex p-3 cursor-pointer">@{{ item.name }}</label>
                                        </li>
                                    </ul>
                                    <div class="text-xs font-medium pt-3 px-3">Delay</div>
                                    <ul class="flex flex-wrap space-x-1 p-1">
                                        <li v-for="(item, index) in this.classes.duration" :key="item + index" :class="[{'bg-gray-500': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + 'delay-' + item)}]" class="block text-xs text-gray-300 hover:text-white">
                                            <label :for="prefix + statePrefix + 'delay-' + item" class="flex p-3 cursor-pointer">@{{ item }}</label>
                                        </li>
                                    </ul>
                                    <a href="#" class="text-xs font-medium p-3 block" @click.stop="openEditor({menu: 'Animation', selectionMenu: 'settings'})">More animation options...</a>
                                </div>
                                <input v-for="(item, index) in this.classes.animations" :key="index + item" @input="save" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" name="color-chooser" :id="prefix + statePrefix + item" :value="prefix + statePrefix + item">
                                <input v-for="(item, index) in this.classes.transitionProperties" :key="item.class + index" @input="save" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" name="display" :id="prefix + statePrefix + 'transition' + item.class" :value="prefix + statePrefix + 'transition' + item.class">
                                <input v-for="(item, index) in this.classes.duration" :key="item + index" @input="save" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" name="display" :id="prefix + statePrefix + 'duration-' + item" :value="prefix + statePrefix + 'duration-' + item">
                                <input v-for="(item, index) in this.classes.timing" :key="item.class + index" @input="save" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" name="display" :id="prefix + statePrefix + 'ease-' + item.class" :value="prefix + statePrefix + 'ease-' + item.class">
                                <input v-for="(item, index) in this.classes.duration" :key="item + '-' + index" @input="save" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" name="display" :id="prefix + statePrefix + 'delay-' + item" :value="prefix + statePrefix + 'delay-' + item">
                            </o-dropdown>
                            <o-dropdown class="relative" aria-role="list">
                                <template v-slot:trigger>
                                    <button title="Transforms" slot="trigger" type="button" class="inline-flex justify-center w-full px-2 py-2 text-sm font-medium text-gray-300 hover:text-white focus:outline-none" id="options-menu" aria-haspopup="true" aria-expanded="true">
                                        <span class="sr-only">Transform Options</span>
                                        <svg class="h-4 w-4" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M4,2C2.89,2 2,2.89 2,4V14H4V4H14V2H4M8,6C6.89,6 6,6.89 6,8V18H8V8H18V6H8M12,10C10.89,10 10,10.89 10,12V20C10,21.11 10.89,22 12,22H20C21.11,22 22,21.11 22,20V12C22,10.89 21.11,10 20,10H12Z" />
                                        </svg>
                                    </button>
                                </template>
                                <div class="z-50 origin-top-left m-1 items-center w-96 justify-center absolute right-0 text-gray-300 bg-gray-800">
                                    <div class="text-xs font-medium pt-3 px-3">Transform</div>
                                        <ul class="flex flex-wrap space-x-1 p-1">
                                            <li v-for="(item, index) in this.classes.transform" :key="item.class + index" :class="[{'bg-gray-500': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + 'transform' + item.class)}]" class="block text-xs text-gray-300 hover:text-white">
                                                <label :for="prefix + statePrefix + 'transform' + item.class" class="flex p-3 cursor-pointer">@{{ item.name }}</label>
                                            </li>
                                        </ul>
                                    <div class="text-xs font-medium pt-3 px-3">Transform Origin</div>
                                    <ul class="flex flex-wrap space-x-1 p-1">
                                        <li v-for="(item, index) in this.classes.transformOrigin" :key="item.class + index" :class="[{'bg-gray-500': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + 'origin' + item.class)}]" class="block text-xs text-gray-300 hover:text-white">
                                            <label :for="prefix + statePrefix + 'origin' + item.class" class="flex p-3 cursor-pointer">@{{ item.name }}</label>
                                        </li>
                                    </ul>
                                </div>
                                <input v-for="(item, index) in this.classes.transform" :key="item.class + index" @input="save" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" name="display" :id="prefix + statePrefix + 'transform' + item.class" :value="prefix + statePrefix + 'transform' + item.class">
                                <input v-for="(item, index) in this.classes.transformOrigin" :key="item.class + index" @input="save" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" name="display" :id="prefix + statePrefix + 'origin' + item.class" :value="prefix + statePrefix + 'origin' + item.class">
                            </o-dropdown>
                            <o-dropdown class="relative" aria-role="list">
                                <template v-slot:trigger>
                                    <button title="Flex/ Grid Options" slot="trigger" type="button" class="-ml-px cursor-pointer relative inline-flex items-center px-2 py-2 text-sm font-medium text-gray-300 hover:text-white focus:outline-none" id="options-menu" aria-haspopup="true" aria-expanded="true">
                                        <span class="sr-only">Flex/Grid</span>
                                        <svg class="h-4 w-4" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M10,4V8H14V4H10M16,4V8H20V4H16M16,10V14H20V10H16M16,16V20H20V16H16M14,20V16H10V20H14M8,20V16H4V20H8M8,14V10H4V14H8M8,8V4H4V8H8M10,14H14V10H10V14M4,2H20A2,2 0 0,1 22,4V20A2,2 0 0,1 20,22H4C2.92,22 2,21.1 2,20V4A2,2 0 0,1 4,2Z" />
                                        </svg>
                                    </button>
                                </template>
                                <div class="z-50 origin-top-right m-1 items-center justify-center absolute right-0 w-96 text-gray-300 bg-gray-800">
                                    <div class="text-xs font-medium pt-3 px-3">Flex Direction</div>
                                    <ul class="flex flex-wrap space-x-1 p-1">
                                        <li v-for="(item, index) in this.classes.flexDirection" :key="item.class + index" :class="[{'bg-gray-500': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + item.class)}]" class="block text-xs text-gray-300 hover:text-white">
                                            <label :for="prefix + statePrefix + item.class" class="flex p-3 cursor-pointer">@{{ item.name }}</label>
                                        </li>
                                    </ul>
                                    <div class="text-xs font-medium pt-3 px-3">Flex Wrap</div>
                                    <ul class="flex flex-wrap space-x-1 p-1">
                                        <li v-for="(item, index) in this.classes.flexWrap" :key="item.class + index" :class="[{'bg-gray-500': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + item.class)}]" class="block text-xs text-gray-300 hover:text-white">
                                            <label :for="prefix + statePrefix + item.class" class="flex p-3 cursor-pointer">@{{ item.name }}</label>
                                        </li>
                                    </ul>
                                    <div class="text-xs font-medium pt-3 px-3">Flex Grow/Shrink</div>
                                    <ul class="flex flex-wrap space-x-1 p-1">
                                        <li v-for="(item, index) in this.classes.flex" :key="item.class + index" :class="[{'bg-gray-500': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + 'flex' + item.class)}]" class="block text-xs text-gray-300 hover:text-white">
                                            <label :for="prefix + statePrefix + 'flex' + item.class" class="flex p-3 cursor-pointer">@{{ item.name }}</label>
                                        </li>
                                    </ul>
                                </div>
                                <input v-for="(item, index) in this.classes.flexDirection" :key="item.class + index" @input="save" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" name="display" :id="prefix + statePrefix + item.class" :value="prefix + statePrefix + item.class">
                                <input v-for="(item, index) in this.classes.flexWrap" :key="item.class + index" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" name="position" :id="prefix + statePrefix + item.class" :value="prefix + statePrefix + item.class">
                                <input v-for="(item, index) in this.classes.flex" :key="item.class + index" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" name="position" :id="prefix + statePrefix + 'flex' + item.class" :value="prefix + statePrefix + 'flex' + item.class">
                            </o-dropdown>
                            <o-dropdown class="relative" aria-role="list">
                                <template v-slot:trigger>
                                    <button title="Overflow" slot="trigger" type="button" class="inline-flex justify-center w-full px-2 py-2 text-sm font-medium text-gray-300 hover:text-white focus:outline-none" id="options-menu" aria-haspopup="true" aria-expanded="true">
                                        <span class="sr-only">Overflow</span>
                                        <svg class="h-4 w-4" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M7,21H5V3H7V21M14,3H12V9H14V3M14,15H12V21H14V15M19,12L16,9V11H9V13H16V15L19,12Z" />
                                        </svg>
                                    </button>
                                </template>
                                <ul class="z-50 origin-top-right m-1 items-center justify-center absolute cursor-pointer left-0 w-56 text-gray-300 bg-gray-800">
                                    <li v-for="(item, index) in this.classes.overflow" :key="index" :class="[{'bg-gray-500': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + 'overflow-' + item)}, 'overflow-' + item]" class="block text-xs hover:bg-gray-300 hover:text-white">
                                        <label :for="prefix + statePrefix + 'overflow-' + item" class="flex p-3">@{{ item }}</label>
                                    </li>
                                </ul>
                                <input v-for="(item, index) in this.classes.overflow" :key="index" @input="save" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" name="overflow" :id="prefix + statePrefix + 'overflow-' + item" :value="prefix + statePrefix + 'overflow-' + item">
                            </o-dropdown>
                            <o-dropdown class="relative" aria-role="list">
                                <template v-slot:trigger>
                                    <button title="Text Decoration" slot="trigger" type="button" class="inline-flex justify-center w-full px-2 py-2 text-sm font-medium text-gray-300 hover:text-white focus:outline-none" id="options-menu" aria-haspopup="true" aria-expanded="true">
                                        <span class="sr-only">Cursor</span>
                                        <svg class="h-4 w-4" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M10.07,14.27C10.57,14.03 11.16,14.25 11.4,14.75L13.7,19.74L15.5,18.89L13.19,13.91C12.95,13.41 13.17,12.81 13.67,12.58L13.95,12.5L16.25,12.05L8,5.12V15.9L9.82,14.43L10.07,14.27M13.64,21.97C13.14,22.21 12.54,22 12.31,21.5L10.13,16.76L7.62,18.78C7.45,18.92 7.24,19 7,19A1,1 0 0,1 6,18V3A1,1 0 0,1 7,2C7.24,2 7.47,2.09 7.64,2.23L7.65,2.22L19.14,11.86C19.57,12.22 19.62,12.85 19.27,13.27C19.12,13.45 18.91,13.57 18.7,13.61L15.54,14.23L17.74,18.96C18,19.46 17.76,20.05 17.26,20.28L13.64,21.97Z" />
                                        </svg>
                                    </button>
                                </template>
                                <div class="z-50 origin-top-right m-1 items-center justify-center absolute right-0 w-96 text-gray-300 bg-gray-800">
                                    <div class="text-xs font-medium pt-3 px-3">Cursor</div>
                                    <div class="flex flex-wrap space-x-1 p-1">
                                        <button v-for="(item, index) in this.classes.cursor" :key="item + index" :class="[{'bg-gray-500': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + 'cursor-' + item)}]" class="ml-1 block text-xs text-gray-300 hover:text-white">
                                            <label :for="prefix + statePrefix + 'cursor-' + item" class="flex p-3" :class="'cursor-' + item">@{{ item }}</label>
                                        </button>
                                    </div>
                                    <input v-for="(item, index) in this.classes.cursor" :key="item + index" @input="save" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" name="display" :id="prefix + statePrefix + 'cursor-' + item" :value="prefix + statePrefix + 'cursor-' + item">
                                </div>
                            </o-dropdown>
                            <o-dropdown class="relative" aria-role="list">
                                <template v-slot:trigger>
                                    <button title="Font Weight" slot="trigger" type="button" class="inline-flex justify-center w-full px-2 py-2 text-sm font-medium text-gray-300 hover:text-white focus:outline-none" id="options-menu" aria-haspopup="true" aria-expanded="true">
                                        <span class="sr-only">Bold</span>
                                        <svg class="h-4 w-4" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M13.5,15.5H10V12.5H13.5A1.5,1.5 0 0,1 15,14A1.5,1.5 0 0,1 13.5,15.5M10,6.5H13A1.5,1.5 0 0,1 14.5,8A1.5,1.5 0 0,1 13,9.5H10M15.6,10.79C16.57,10.11 17.25,9 17.25,8C17.25,5.74 15.5,4 13.25,4H7V18H14.04C16.14,18 17.75,16.3 17.75,14.21C17.75,12.69 16.89,11.39 15.6,10.79Z" />
                                        </svg>
                                    </button>
                                </template>
                                <ul class="z-50 origin-top-right m-1 items-center justify-center absolute cursor-pointer left-0 w-56 text-gray-300 bg-gray-800">
                                    <li v-for="(item, index) in this.classes.weights" :key="index" :class="[{'bg-gray-500': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + 'font-' + item)}, 'font-' + item]" class="block text-xs hover:bg-gray-300 hover:text-white">
                                        <label :for="prefix + statePrefix + 'font-' + item" class="flex p-3">@{{ item }}</label>
                                    </li>
                                </ul>
                                <input v-for="(item, index) in this.classes.weights" :key="index" @input="save" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" name="color-chooser" :id="prefix + statePrefix + 'font-' + item" :value="prefix + statePrefix + 'font-' + item">
                            </o-dropdown>
                            <o-dropdown class="relative" aria-role="list">
                                <template v-slot:trigger>
                                    <button title="Text Decoration" slot="trigger" type="button" class="inline-flex justify-center w-full px-2 py-2 text-sm font-medium text-gray-300 hover:text-white focus:outline-none" id="options-menu" aria-haspopup="true" aria-expanded="true">
                                        <span class="sr-only">Text Decoration</span>
                                        <svg class="h-4 w-4" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M20.06,18C20,17.83 19.91,17.54 19.86,17.11C19.19,17.81 18.38,18.16 17.45,18.16C16.62,18.16 15.93,17.92 15.4,17.45C14.87,17 14.6,16.39 14.6,15.66C14.6,14.78 14.93,14.1 15.6,13.61C16.27,13.12 17.21,12.88 18.43,12.88H19.83V12.24C19.83,11.75 19.68,11.36 19.38,11.07C19.08,10.78 18.63,10.64 18.05,10.64C17.53,10.64 17.1,10.76 16.75,11C16.4,11.25 16.23,11.54 16.23,11.89H14.77C14.77,11.46 14.92,11.05 15.22,10.65C15.5,10.25 15.93,9.94 16.44,9.71C16.95,9.5 17.5,9.36 18.13,9.36C19.11,9.36 19.87,9.6 20.42,10.09C20.97,10.58 21.26,11.25 21.28,12.11V16C21.28,16.8 21.38,17.42 21.58,17.88V18H20.06M17.66,16.88C18.11,16.88 18.54,16.77 18.95,16.56C19.35,16.35 19.65,16.07 19.83,15.73V14.16H18.7C16.93,14.16 16.04,14.63 16.04,15.57C16.04,16 16.19,16.3 16.5,16.53C16.8,16.76 17.18,16.88 17.66,16.88M5.46,13.71H9.53L7.5,8.29L5.46,13.71M6.64,6H8.36L13.07,18H11.14L10.17,15.43H4.82L3.86,18H1.93L6.64,6Z" />
                                        </svg>
                                    </button>
                                </template>
                                <div class="z-50 origin-top-right m-1 items-center justify-center absolute right-0 w-96 text-gray-300 bg-gray-800">
                                    <div class="text-xs font-medium pt-3 px-3">Text Decoration</div>
                                    <div class="flex items-center">
                                        <label for="underline" class="-ml-px cursor-pointer relative inline-flex items-center p-4 text-sm font-medium text-gray-300 hover:text-white focus:z-10 focus:outline-none">
                                            <span class="sr-only">Underline</span>
                                            <svg class="h-4 w-4" viewBox="0 0 24 24">
                                                <path fill="currentColor" d="M5,21H19V19H5V21M12,17A6,6 0 0,0 18,11V3H15.5V11A3.5,3.5 0 0,1 12,14.5A3.5,3.5 0 0,1 8.5,11V3H6V11A6,6 0 0,0 12,17Z" />
                                            </svg>
                                        </label>
                                        <input type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" @input="save" class="hidden" name="color-chooser" id="underline" :value="prefix + statePrefix + 'underline'">
                                        <label for="lineThrough" class="-ml-px cursor-pointer relative inline-flex items-center p-4 text-sm font-medium text-gray-300 hover:text-white focus:z-10 focus:outline-none">
                                            <span class="sr-only">Line Through</span>
                                            <svg class="h-4 w-4" viewBox="0 0 24 24">
                                                <path fill="currentColor" d="M3,14H21V12H3M5,4V7H10V10H14V7H19V4M10,19H14V16H10V19Z" />
                                            </svg>
                                        </label>
                                        <input type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" @input="save" class="hidden" name="lineThrough" id="lineThrough" :value="prefix + statePrefix + 'line-through'">
                                        <label for="noUnderline" class="-ml-px cursor-pointer relative inline-flex items-center p-4 text-xs font-medium text-gray-300 hover:text-white focus:z-10 focus:outline-none">
                                            <span class="sr-only">None</span>
                                            None
                                        </label>
                                        <input type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" @input="save" class="hidden" name="noUnderline" id="noUnderline" :value="prefix + statePrefix + 'no-underline'">
                                    </div>
                                    <div class="text-xs font-medium pt-3 px-3">Italic</div>
                                    <div class="flex items-center">
                                        <label for="italic" class="-ml-px cursor-pointer relative inline-flex items-center p-4 text-sm font-medium text-gray-300 hover:text-white focus:z-10 focus:outline-none">
                                            <span class="sr-only">Italic</span>
                                            <svg class="h-4 w-4" viewBox="0 0 24 24">
                                                <path fill="currentColor" d="M10,4V7H12.21L8.79,15H6V18H14V15H11.79L15.21,7H18V4H10Z" />
                                            </svg>
                                        </label>
                                        <input type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" @input="save" class="hidden" name="color-chooser" id="italic" :value="prefix + statePrefix + 'italic'">
                                        <label for="notItalic" class="-ml-px cursor-pointer relative inline-flex items-center p-4 text-xs font-medium text-gray-300 hover:text-white focus:z-10 focus:outline-none">
                                            <span class="sr-only">None</span>
                                            None
                                        </label>
                                        <input type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" @input="save" class="hidden" name="notItalic" id="notItalic" :value="prefix + statePrefix + 'not-italic'">
                                    </div>
                                    <div class="text-xs font-medium pt-3 px-3">Text Transform</div>
                                    <div class="flex items-center">
                                        <label for="uppercase" class="-ml-px cursor-pointer relative inline-flex items-center p-4 text-sm font-medium text-gray-300 hover:text-white focus:z-10 focus:outline-none">
                                            <span class="sr-only">Uppercase</span>
                                            <svg class="h-4 w-4" viewBox="0 0 24 24">
                                                <path fill="currentColor" d="M20.06,18C20,17.83 19.91,17.54 19.86,17.11C19.19,17.81 18.38,18.16 17.45,18.16C16.62,18.16 15.93,17.92 15.4,17.45C14.87,17 14.6,16.39 14.6,15.66C14.6,14.78 14.93,14.1 15.6,13.61C16.27,13.12 17.21,12.88 18.43,12.88H19.83V12.24C19.83,11.75 19.68,11.36 19.38,11.07C19.08,10.78 18.63,10.64 18.05,10.64C17.53,10.64 17.1,10.76 16.75,11C16.4,11.25 16.23,11.54 16.23,11.89H14.77C14.77,11.46 14.92,11.05 15.22,10.65C15.5,10.25 15.93,9.94 16.44,9.71C16.95,9.5 17.5,9.36 18.13,9.36C19.11,9.36 19.87,9.6 20.42,10.09C20.97,10.58 21.26,11.25 21.28,12.11V16C21.28,16.8 21.38,17.42 21.58,17.88V18H20.06M17.66,16.88C18.11,16.88 18.54,16.77 18.95,16.56C19.35,16.35 19.65,16.07 19.83,15.73V14.16H18.7C16.93,14.16 16.04,14.63 16.04,15.57C16.04,16 16.19,16.3 16.5,16.53C16.8,16.76 17.18,16.88 17.66,16.88M5.46,13.71H9.53L7.5,8.29L5.46,13.71M6.64,6H8.36L13.07,18H11.14L10.17,15.43H4.82L3.86,18H1.93L6.64,6M2,20H13V22H2V20Z" />
                                            </svg>
                                        </label>
                                        <input type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" @input="save" class="hidden" name="color-chooser" id="uppercase" :value="prefix + statePrefix + 'uppercase'">
                                        <label for="lowercase" class="-ml-px cursor-pointer relative inline-flex items-center p-4 text-sm font-medium text-gray-300 hover:text-white focus:z-10 focus:outline-none">
                                            <span class="sr-only">Lowercase</span>
                                            <svg class="h-4 w-4" viewBox="0 0 24 24">
                                                <path fill="currentColor" d="M20.06,18C20,17.83 19.91,17.54 19.86,17.11C19.19,17.81 18.38,18.16 17.45,18.16C16.62,18.16 15.93,17.92 15.4,17.45C14.87,17 14.6,16.39 14.6,15.66C14.6,14.78 14.93,14.1 15.6,13.61C16.27,13.12 17.21,12.88 18.43,12.88H19.83V12.24C19.83,11.75 19.68,11.36 19.38,11.07C19.08,10.78 18.63,10.64 18.05,10.64C17.53,10.64 17.1,10.76 16.75,11C16.4,11.25 16.23,11.54 16.23,11.89H14.77C14.77,11.46 14.92,11.05 15.22,10.65C15.5,10.25 15.93,9.94 16.44,9.71C16.95,9.5 17.5,9.36 18.13,9.36C19.11,9.36 19.87,9.6 20.42,10.09C20.97,10.58 21.26,11.25 21.28,12.11V16C21.28,16.8 21.38,17.42 21.58,17.88V18H20.06M17.66,16.88C18.11,16.88 18.54,16.77 18.95,16.56C19.35,16.35 19.65,16.07 19.83,15.73V14.16H18.7C16.93,14.16 16.04,14.63 16.04,15.57C16.04,16 16.19,16.3 16.5,16.53C16.8,16.76 17.18,16.88 17.66,16.88M5.46,13.71H9.53L7.5,8.29L5.46,13.71M6.64,6H8.36L13.07,18H11.14L10.17,15.43H4.82L3.86,18H1.93L6.64,6M22,20V22H14.5V20H22Z" />
                                            </svg>
                                        </label>
                                        <input type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" @input="save" class="hidden" name="color-chooser" id="lowercase" :value="prefix + statePrefix + 'lowercase'">
                                        <label for="capitalize" class="-ml-px cursor-pointer relative inline-flex items-center p-4 text-sm font-medium text-gray-300 hover:text-white focus:z-10 focus:outline-none">
                                            <span class="sr-only">Capitalise</span>
                                            <svg class="h-4 w-4" viewBox="0 0 24 24">
                                                <path fill="currentColor" d="M20.06,18C20,17.83 19.91,17.54 19.86,17.11C19.19,17.81 18.38,18.16 17.45,18.16C16.62,18.16 15.93,17.92 15.4,17.45C14.87,17 14.6,16.39 14.6,15.66C14.6,14.78 14.93,14.1 15.6,13.61C16.27,13.12 17.21,12.88 18.43,12.88H19.83V12.24C19.83,11.75 19.68,11.36 19.38,11.07C19.08,10.78 18.63,10.64 18.05,10.64C17.53,10.64 17.1,10.76 16.75,11C16.4,11.25 16.23,11.54 16.23,11.89H14.77C14.77,11.46 14.92,11.05 15.22,10.65C15.5,10.25 15.93,9.94 16.44,9.71C16.95,9.5 17.5,9.36 18.13,9.36C19.11,9.36 19.87,9.6 20.42,10.09C20.97,10.58 21.26,11.25 21.28,12.11V16C21.28,16.8 21.38,17.42 21.58,17.88V18H20.06M17.66,16.88C18.11,16.88 18.54,16.77 18.95,16.56C19.35,16.35 19.65,16.07 19.83,15.73V14.16H18.7C16.93,14.16 16.04,14.63 16.04,15.57C16.04,16 16.19,16.3 16.5,16.53C16.8,16.76 17.18,16.88 17.66,16.88M5.46,13.71H9.53L7.5,8.29L5.46,13.71M6.64,6H8.36L13.07,18H11.14L10.17,15.43H4.82L3.86,18H1.93L6.64,6Z" />
                                            </svg>
                                        </label>
                                        <label for="capitalize" class="-ml-px cursor-pointer relative inline-flex items-center p-4 text-xs font-medium text-gray-300 hover:text-white focus:z-10 focus:outline-none">
                                            <span class="sr-only">Normal</span>
                                            Normal Case
                                        </label>
                                        <input type="checkbox" @input="save" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" name="color-chooser" id="capitalize" :value="prefix + statePrefix + 'capitalize'">
                                    </div>
                                </div>
                            </o-dropdown>
                            <o-dropdown class="relative" aria-role="list">
                                <template v-slot:trigger>
                                    <button title="Background Colour" slot="trigger" type="button" class="inline-flex justify-center w-full px-2 py-2 text-sm font-medium text-gray-300 hover:text-white focus:outline-none" id="options-menu" aria-haspopup="true" aria-expanded="true">
                                        <span class="sr-only">Background colour</span>
                                        <svg class="h-4 w-4 mt-1" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M19,11.5C19,11.5 17,13.67 17,15A2,2 0 0,0 19,17A2,2 0 0,0 21,15C21,13.67 19,11.5 19,11.5M5.21,10L10,5.21L14.79,10M16.56,8.94L7.62,0L6.21,1.41L8.59,3.79L3.44,8.94C2.85,9.5 2.85,10.47 3.44,11.06L8.94,16.56C9.23,16.85 9.62,17 10,17C10.38,17 10.77,16.85 11.06,16.56L16.56,11.06C17.15,10.47 17.15,9.5 16.56,8.94Z" />
                                        </svg>
                                    </button>
                                </template>
                                <div class="z-50 origin-top-right m-1 items-center justify-center absolute cursor-pointer left-0 w-56 text-gray-300 hover:text-white bg-gray-800">
                                    <ul class="flex flex-wrap">
                                        <li v-for="(item, index) in this.classes.colours" :key="index">
                                            <label :for="prefix + statePrefix + 'bg-' + item" :title="'bg-' + item" class="border border-transparent block w-8 h-8 hover:border-red-300" :class="[{'border-red-300 border-4': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + 'bg-' + item)}, item == 'transparent' ? 'checkered' : 'bg-' + item]"></label>
                                        </li>
                                    </ul>
                                    <div class="text-xs font-medium p-2" @click.stop="openEditor({menu: 'Gradient', selectionMenu: 'settings'})">Apply gradients...</div>
                                    <div class="text-xs font-medium p-2" @click.stop="openEditor({menu: 'Opacity', selectionMenu: 'settings'})">Choose opacity...</div>
                                </div>
                                <input v-for="(item, index) in this.classes.colours" :key="index" @input="save" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" name="color-chooser" :id="prefix + statePrefix + 'bg-' + item" :value="prefix + statePrefix + 'bg-' + item">
                            </o-dropdown>
                            <o-dropdown class="relative" aria-role="list">
                                <template v-slot:trigger>
                                    <button title="Text Colour" slot="trigger" type="button" class="inline-flex justify-center w-full px-2 py-2 text-sm font-medium text-gray-300 hover:text-white focus:outline-none" id="options-menu" aria-haspopup="true" aria-expanded="true">
                                        <span class="sr-only">Text colour</span>
                                        <svg class="h-4 w-4 mt-1" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M9.62,12L12,5.67L14.37,12M11,3L5.5,17H7.75L8.87,14H15.12L16.25,17H18.5L13,3H11Z" />
                                        </svg>
                                    </button>
                                </template>
                                <div class="z-50 rigin-top-right m-1 items-center justify-center absolute cursor-pointer left-0 w-56 text-gray-300 hover:text-white bg-gray-800">
                                    <ul class="flex flex-wrap">
                                        <li v-for="(item, index) in this.classes.colours" :key="index">
                                            <label :for="prefix + statePrefix + 'text-' + item" :title="'text-' + item" class="border border-transparent block w-8 h-8 hover:border-red-300" :class="[{'border-red-300 border-4': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + 'text-' + item)}, item == 'transparent' ? 'checkered' : 'bg-' + item]"></label>
                                        </li>
                                    </ul>
                                </div>
                                <input v-for="(item, index) in this.classes.colours" :key="index" @input="save" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" name="color-chooser" :id="prefix + statePrefix + 'text-' + item" :value="prefix + statePrefix + 'text-' + item">
                            </o-dropdown>
                            <o-dropdown class="relative" aria-role="list">
                                <template v-slot:trigger>
                                    <button title="Font Family" slot="trigger" type="button" class="inline-flex justify-center w-full px-2 py-2 text-sm font-medium text-gray-300 hover:text-white focus:outline-none" id="options-menu" aria-haspopup="true" aria-expanded="true">
                                        <span class="sr-only">Font Family</span>
                                        <svg class="h-4 w-4" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M17,8H20V20H21V21H17V20H18V17H14L12.5,20H14V21H10V20H11L17,8M18,9L14.5,16H18V9M5,3H10C11.11,3 12,3.89 12,5V16H9V11H6V16H3V5C3,3.89 3.89,3 5,3M6,5V9H9V5H6Z" />
                                        </svg>
                                    </button>
                                </template>
                                <div class="z-50 origin-top-right m-1 items-center justify-center absolute cursor-pointer left-0 w-56 text-xs text-gray-300 bg-gray-800" role="listbox" aria-haspopup="true">
                                    <label for="fontSans" class="relative inline-flex items-center hover:bg-gray-700 cursor-pointer px-3 py-2 text-xs font-base text-gray-300 hover:text-white w-full">
                                        <span class="sr-only">Sans</span>
                                        Sans
                                    </label>
                                    <input type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" @input="save" class="hidden" name="fontSans" id="fontSans" :value="prefix + statePrefix + 'font-sans'">
                                    <label for="fontSerif" class="relative inline-flex items-center hover:bg-gray-700 cursor-pointer px-3 py-2 text-xs font-base text-gray-300 hover:text-white w-full">
                                        <span class="sr-only">Serif</span>
                                        Serif
                                    </label>
                                    <input type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" @input="save" class="hidden" name="fontSerif" id="fontSerif" :value="prefix + statePrefix + 'font-serif'">
                                    <label for="fontMono" class="relative inline-flex items-center hover:bg-gray-700 cursor-pointer px-3 py-2 text-xs font-base text-gray-300 hover:text-white w-full">
                                        <span class="sr-only">Mono</span>
                                        Mono
                                    </label>
                                    <input type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" @input="save" class="hidden" name="fontMono" id="fontMono" :value="prefix + statePrefix + 'font-mono'">
                                    <!-- <div v-for="font in this.classes.families.slice(0, 6)" @click="fontSelection(font.name)" :value="font.name" aria-role="listitem">
                                        <div class="hover:bg-gray-700 cursor-pointer" role="menuitemcheckbox" aria-disabled="false" aria-checked="false" aria-haspopup="true" id=":gr" style="user-select: none;">
                                            <div class="px-3 py-2" style="user-select: none;">
                                                <span class="text-gray-300 hover:text-white" :style="{fontFamily: font.name}">@{{ font.name }}</span>
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="text-xs font-medium p-3" @click.stop="openEditor({menu: 'Fonts', selectionMenu: 'settings'})">More fonts...</div>
                                    <div class="text-xs font-medium px-3 pb-3" @click.stop="openEditor({menu: 'Typography', selectionMenu: 'settings'})">Typography options...</div>
                                </div>
                            </o-dropdown>
                            <o-dropdown class="relative" aria-role="list">
                                <template v-slot:trigger>
                                    <button title="Font Size" slot="trigger" type="button" class="inline-flex justify-center w-full px-2 py-2 text-sm font-medium text-gray-300 hover:text-white focus:outline-none" id="options-menu" aria-haspopup="true" aria-expanded="true">
                                        <span class="sr-only">Font Size</span>
                                        <svg class="h-4 w-4" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M2 4V7H7V19H10V7H15V4H2M21 9H12V12H15V19H18V12H21V9Z" />
                                        </svg>
                                    </button>
                                </template>
                                <ul class="z-50 origin-top-right m-1 items-center justify-center absolute cursor-pointer left-0 w-56 text-gray-300 bg-gray-800">
                                    <li v-for="(item, index) in this.classes.fontSizes" :key="index" :class="[{'bg-gray-500': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + 'text-' + item)}]" class="block text-xs hover:bg-gray-300 hover:text-white">
                                        <label :for="prefix + statePrefix + 'text-' + item" class="flex p-3">@{{ item }}</label>
                                    </li>
                                </ul>
                                <input v-for="(item, index) in this.classes.fontSizes" :key="index" @input="save" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" name="color-chooser" :id="prefix + statePrefix + 'text-' + item" :value="prefix + statePrefix + 'text-' + item">
                            </o-dropdown>
                            <o-dropdown class="relative" aria-role="list">
                                <template v-slot:trigger>
                                    <button title="Alignment Options" slot="trigger" type="button" class="inline-flex justify-center w-full px-2 py-2 text-sm font-medium text-gray-300 hover:text-white focus:outline-none" id="options-menu" aria-haspopup="true" aria-expanded="true">
                                        <span class="sr-only">Alignment</span>
                                        <svg class="h-4 w-4" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M3,3H21V5H3V3M3,7H21V9H3V7M3,11H21V13H3V11M3,15H21V17H3V15M3,19H21V21H3V19Z" />
                                        </svg>
                                    </button>
                                </template>
                                <div class="z-50 origin-top-right m-1 items-center justify-center absolute right-0 w-96 text-gray-300 bg-gray-800">
                                    <div class="text-xs font-medium pt-3 px-3">Text Align</div>
                                    <label for="text-left" class="-ml-px cursor-pointer relative inline-flex items-center p-3 text-sm font-medium text-gray-300 hover:text-white focus:outline-none">
                                        <span class="sr-only">Text left</span>
                                        <svg class="h-4 w-4" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M3,3H21V5H3V3M3,7H15V9H3V7M3,11H21V13H3V11M3,15H15V17H3V15M3,19H21V21H3V19Z" />
                                        </svg>
                                    </label>
                                    <input type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" @input="save" class="hidden" name="text-left" id="text-left" :value="prefix + statePrefix + 'text-left'" />
                                    <label for="text-center" class="-ml-px cursor-pointer relative inline-flex items-center p-3 text-sm font-medium text-gray-300 hover:text-white focus:outline-none">
                                        <span class="sr-only">Text centre</span>
                                        <svg class="h-4 w-4" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M3,3H21V5H3V3M7,7H17V9H7V7M3,11H21V13H3V11M7,15H17V17H7V15M3,19H21V21H3V19Z" />
                                        </svg>
                                    </label>
                                    <input type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" @input="save" class="hidden" name="text-center" id="text-center" :value="prefix + statePrefix + 'text-center'" />
                                    <label for="text-right" class="-ml-px cursor-pointer relative inline-flex items-center p-3 text-sm font-medium text-gray-300 hover:text-white focus:outline-none">
                                        <span class="sr-only">Text right</span>
                                        <svg class="h-4 w-4" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M3,3H21V5H3V3M9,7H21V9H9V7M3,11H21V13H3V11M9,15H21V17H9V15M3,19H21V21H3V19Z" />
                                        </svg>
                                    </label>
                                    <input type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" @input="save" class="hidden" name="text-right" id="text-right" :value="prefix + statePrefix + 'text-right'" />
                                    <label for="text-justify" class="-ml-px cursor-pointer relative inline-flex items-center p-3 text-sm font-medium text-gray-300 hover:text-white focus:outline-none">
                                        <span class="sr-only">Text justify</span>
                                        <svg class="h-4 w-4" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M3,3H21V5H3V3M3,7H21V9H3V7M3,11H21V13H3V11M3,15H21V17H3V15M3,19H21V21H3V19Z" />
                                        </svg>
                                    </label>
                                    <input type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" @input="save" class="hidden" name="text-justify" id="text-justify" :value="prefix + statePrefix + 'text-justify'" />
                                    <div class="text-xs font-medium pt-3 px-3">Justify Content</div>
                                    <ul class="flex flex-wrap space-x-1 p-1">
                                        <li v-for="(item, index) in this.classes.justifyContent" :key="item.class + index" :class="[{'bg-gray-500': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + item.class)}]" class="block text-xs text-gray-300 hover:text-white">
                                            <label :for="prefix + statePrefix + item.class" class="flex p-3 cursor-pointer">@{{ item.name }}</label>
                                        </li>
                                    </ul>
                                    <div class="text-xs font-medium pt-3 px-3">Justify Items</div>
                                    <ul class="flex flex-wrap space-x-1 p-1">
                                        <li v-for="(item, index) in this.classes.justifyItems" :key="item.class + index" :class="[{'bg-gray-500': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + item.class)}]" class="block text-xs text-gray-300 hover:text-white">
                                            <label :for="prefix + statePrefix + item.class" class="flex p-3 cursor-pointer">@{{ item.name }}</label>
                                        </li>
                                    </ul>
                                    <div class="text-xs font-medium pt-3 px-3">Justify Self</div>
                                    <ul class="flex flex-wrap space-x-1 p-1">
                                        <li v-for="(item, index) in this.classes.justifySelf" :key="item.class + index" :class="[{'bg-gray-500': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + item.class)}]" class="block text-xs text-gray-300 hover:text-white">
                                            <label :for="prefix + statePrefix + item.class" class="flex p-3 cursor-pointer">@{{ item.name }}</label>
                                        </li>
                                    </ul>
                                    <div class="text-xs font-medium pt-3 px-3">Align Content</div>
                                    <ul class="flex flex-wrap space-x-1 p-1">
                                        <li v-for="(item, index) in this.classes.alignContent" :key="item.class + index" :class="[{'bg-gray-500': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + item.class)}]" class="block text-xs text-gray-300 hover:text-white">
                                            <label :for="prefix + statePrefix + item.class" class="flex p-3 cursor-pointer">@{{ item.name }}</label>
                                        </li>
                                    </ul>
                                    <div class="text-xs font-medium pt-3 px-3">Align Items</div>
                                    <ul class="flex flex-wrap space-x-1 p-1">
                                        <li v-for="(item, index) in this.classes.alignItems" :key="item.class + index" :class="[{'bg-gray-500': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + item.class)}]" class="block text-xs text-gray-300 hover:text-white">
                                            <label :for="prefix + statePrefix + item.class" class="flex p-3 cursor-pointer">@{{ item.name }}</label>
                                        </li>
                                    </ul>
                                    <div class="text-xs font-medium pt-3 px-3">Align Self</div>
                                    <ul class="flex flex-wrap space-x-1 p-1">
                                        <li v-for="(item, index) in this.classes.alignSelf" :key="item.class + index" :class="[{'bg-gray-500': this.$store.state.content[this.currentSelection].classes && this.$store.state.content[this.currentSelection].classes.includes(prefix + statePrefix + 'self' + item.class)}]" class="block text-xs text-gray-300 hover:text-white">
                                            <label :for="prefix + statePrefix + 'self' + item.class" class="flex p-3 cursor-pointer">@{{ item.name }}</label>
                                        </li>
                                    </ul>
                                    <input v-for="(item, index) in this.classes.justifyContent" :key="item.class + index" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" name="display" :id="prefix + statePrefix + item.class" :value="prefix + statePrefix + item.class">
                                    <input v-for="(item, index) in this.classes.justifyItems" :key="item.class + index" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" name="position" :id="prefix + statePrefix + item.class" :value="prefix + statePrefix + item.class">
                                    <input v-for="(item, index) in this.classes.justifySelf" :key="item.class + index" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" name="position" :id="prefix + statePrefix + item.class" :value="prefix + statePrefix + item.class">
                                    <input v-for="(item, index) in this.classes.alignContent" :key="item.class + index" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" name="display" :id="prefix + statePrefix + item.class" :value="prefix + statePrefix + item.class">
                                    <input v-for="(item, index) in this.classes.alignItems" :key="item.class + index" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" name="position" :id="prefix + statePrefix + item.class" :value="prefix + statePrefix + item.class">
                                    <input v-for="(item, index) in this.classes.alignSelf" :key="item.class + index" type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" class="hidden" name="position" :id="prefix + statePrefix + 'self' + item.class" :value="prefix + statePrefix + 'self' + item.class">
                                </div>
                            </o-dropdown>
                        </div>
                        <div v-if="this.sizeSpace && this.currentSelection" class="relative hidden items-center sm:inline-flex w-full overflow-x-scroll">
                            <button @click="this.sizeSpace = '';this.prefix = '';this.statePrefix = ''" type="button" class="inline-flex justify-center w-full px-2 py-2 text-sm font-medium text-gray-300 hover:text-white focus:outline-none" id="options-menu" aria-haspopup="true" aria-expanded="true">
                                <svg class="h-4 w-4" viewBox="0 0 24 24">
                                    <path fill="currentColor" d="M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z" />
                                </svg>
                            </button>
                            <label for="minus" class="-ml-px cursor-pointer relative inline-flex items-center px-2 py-2 text-sm font-medium text-gray-300 hover:text-white focus:z-10 focus:outline-none">
                                <span class="sr-only">Plus/Minus</span>
                                <svg v-if="sign" class="h-4 w-4" viewBox="0 0 24 24">
                                    <path fill="currentColor" d="M19,13H5V11H19V13Z" />
                                </svg>
                                <svg v-else class="h-4 w-4" viewBox="0 0 24 24">
                                    <path fill="currentColor" d="M19,13H13V19H11V13H5V11H11V5H13V11H19V13Z" />
                                </svg>
                            </label>
                            <input type="checkbox" @input="save" v-model="sign" class="hidden" name="minus" id="minus" value="-">
                            <div v-for="(item, index) in this.classes.sizes" :key="index">
                                <label :for="'dim' + item" class="-ml-px cursor-pointer relative inline-flex items-center px-2 py-2 text-xs font-medium text-gray-300 hover:text-white focus:outline-none">
                                    @{{ item }}
                                </label>
                                <input type="checkbox" v-model="this.$store.state.content[this.currentSelection].classes" @input="save" class="hidden" :name="'dim' + item" :id="'dim' + item" :value="prefix + this.statePrefix + (this.sign ? '-' : '') + this.sizeSpace + item">
                            </div>
                        </div>
                        <div v-if="this.currentSelection && this.$store.state.editor.classbar" class="flex bg-gray-400 text-gray-800 border-t border-black">
                            <div class="flex flex-col flex-1">
                                <div class="box-border clear-both flex-grow">
                                    <div class="inline-flex flex-wrap h-full justify-start items-center block w-full overflow-x-scroll">
                                        <span v-for="(item, index) in this.$store.state.content[this.currentSelection][this.classSelection]" :key="index" style="margin: 3px;" class="font-mono inline-flex items-center py-1 pl-2 pr-1 rounded-md text-xs font-medium bg-gray-800 text-white">
                                            <span>@{{ item }}</span> 
                                            <a role="button" @click="removeClass(index)" class="flex-shrink-0 ml-2 h-4 w-4 rounded-full inline-flex items-center justify-center text-white hover:bg-gray-300 hover:text-black focus:outline-none">
                                                <span class="sr-only">Remove class</span>
                                                <svg stroke="currentColor" fill="none" viewBox="0 0 8 8" class="h-2 w-2"><path stroke-linecap="round" stroke-width="1.5" d="M1 1l6 6m0-6L1 7"></path></svg>
                                            </a>
                                        </span>
                                        <div class="static flex-1 border-box clear-both">
                                            <div>
                                                <input ref="classinput" tabindex="-1" v-model="currentClass" @keydown="keydown" type="text" autocomplete="off" placeholder="Type classname, hit ⏎" class="bg-gray-400 text-gray-800 flex-1 font-mono text-xs pl-2 border-none shadow-none focus:outline-none w-full" style="box-shadow: none !important;">
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <o-dropdown v-model="classSelection" aria-role="list">
                                <template v-slot:trigger>
                                    <button type="button" slot="trigger" class="inline-flex hover:text-white justify-center w-full items-center h-full px-2 py-2 text-xs font-medium focus:outline-none" id="options-menu" aria-haspopup="true" aria-expanded="true">
                                        @{{ classSelection }}
                                    </button>
                                </template>
                                <div v-if="this.$store.state.profile['general'][this.$store.state.content[currentSelection].type]" class="z-50 origin-top-right m-1 items-center justify-center absolute left-0 w-56 text-gray-300 bg-gray-800" role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
                                    <o-dropdown-item v-for="(item, index) in this.$store.state.profile['general'][this.$store.state.content[currentSelection].type].classNames" :key="index" :value="item" aria-role="listitem">
                                        <div class="hover:bg-gray-700 cursor-pointer text-xs hover:text-white"  role="menuitemcheckbox" aria-disabled="false" aria-checked="false" aria-haspopup="true" id=":gr" style="user-select: none;">
                                            <div class="px-3 py-3" style="user-select: none;">
                                                <span>@{{ item }}</span>
                                            </div>
                                        </div>
                                    </o-dropdown-item>
                                </div>
                            </o-dropdown>
                            <div class="flex-shrink">
                                <div @click="removeAllClasses" title="Clear classes" class="w-8 h-10 cursor-pointer inline-flex items-center justify-center hover:text-white">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" class="w-4 h-4"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path></svg>
                                </div>
                            </div>
                            <div class="flex-shrink">
                                <a href="https://tailwindcss.com/docs" title="Visit Tailwind Docs" target="_blank" class="h-10 w-8 inline-flex items-center justify-center hover:text-green-400">
                                    <svg class="w-4 h-4" fill="currentColor" viewBox="0 0 24 24">
                                        <path d="M12 6C9.33 6 7.67 7.33 7 10C8 8.67 9.17 8.17 10.5 8.5C11.26 8.69 11.81 9.24 12.41 9.85C13.39 10.85 14.5 12 17 12C19.67 12 21.33 10.67 22 8C21 9.33 19.83 9.83 18.5 9.5C17.74 9.31 17.2 8.76 16.59 8.15C15.61 7.15 14.5 6 12 6M7 12C4.33 12 2.67 13.33 2 16C3 14.67 4.17 14.17 5.5 14.5C6.26 14.69 6.8 15.24 7.41 15.85C8.39 16.85 9.5 18 12 18C14.67 18 16.33 16.67 17 14C16 15.33 14.83 15.83 13.5 15.5C12.74 15.31 12.2 14.76 11.59 14.15C10.61 13.15 9.5 12 7 12Z" />
                                    </svg>
                                </a>
                            </div>
                            <div class="flex-shrink">
                                <a href="#" @click.stop="copyClassesToClipboard" title="Copy classes to clipboard" class="w-8 h-10 inline-flex items-center justify-center hover:text-white">
                                    <span class="sr-only">Copy classes</span>
                                    <svg viewBox="0 0 24 24" class="h-4 w-4"><path fill="currentColor" d="M19,21H8V7H19M19,5H8A2,2 0 0,0 6,7V21A2,2 0 0,0 8,23H19A2,2 0 0,0 21,21V7A2,2 0 0,0 19,5M16,1H4A2,2 0 0,0 2,3V17H4V3H16V1Z"></path></svg>
                                </a>
                            </div>
                            <div class="flex-shrink">
                                <a href="#" @click.stop="this.$store.state.editor.classbar = !this.$store.state.editor.classbar" title="Close panel" class="w-8 h-10 inline-flex items-center justify-center hover:text-white">
                                    <span class="sr-only">Close panel</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-4 h-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path>
                                    </svg>
                                </a>
                            </div>
                        </div>
                        <div v-if="this.$store.state.editor.console" class="flex items-center justify-between bg-gray-800  border-t border-black">
                            <div class="flex-shrink animate-pulse">
                                <svg class="h-4 text-green-400" viewBox="0 0 24 24">
                                    <path fill="currentColor" d="M8.59,16.58L13.17,12L8.59,7.41L10,6L16,12L10,18L8.59,16.58Z" />
                                </svg>
                            </div>
                            <input @keyup.enter="runCommand" @keydown="consoleKeydown" v-model="currentCommand" style="box-shadow: none !important;" placeholder="Type a command, then hit ⏎, cycle commands using ⇧ ⇩" class="bg-gray-800 text-gray-300 flex-1 font-mono text-xs pl-2 border-none shadow-none focus:outline-none w-full">
                            <div class="flex-shrink">
                                <a v-if="attributeSelectMode" @click="attributeSelectMode = false;" href="#" class="inline-flex items-center justify-center" style="height: 2.5em; width: 2.5em;">
                                    <span class="sr-only">Exit insert attribute node</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true" class="h-4 w-4 text-gray-300 hover:text-white">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13.875 18.825A10.05 10.05 0 0112 19c-4.478 0-8.268-2.943-9.543-7a9.97 9.97 0 011.563-3.029m5.858.908a3 3 0 114.243 4.243M9.878 9.878l4.242 4.242M9.88 9.88l-3.29-3.29m7.532 7.532l3.29 3.29M3 3l3.59 3.59m0 0A9.953 9.953 0 0112 5c4.478 0 8.268 2.943 9.543 7a10.025 10.025 0 01-4.132 5.411m0 0L21 21" />
                                    </svg>
                                </a>
                                <a v-else @click="attributeSelect" href="#" class="inline-flex items-center justify-center" style="height: 2.5em; width: 2.5em;">
                                    <span class="sr-only">Enter insert attribute mode</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true" class="h-4 w-4 text-gray-300 hover:text-white">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"></path> <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"></path>
                                    </svg>
                                </a>
                            </div>
                            <div class="flex-shrink">
                                <a href="#" @click.stop="currentCommand = ''" class="inline-flex items-center justify-center" style="height: 2.5em; width: 2.5em;">
                                    <span class="sr-only">Clear console</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true" class="h-4 w-4 text-gray-300 hover:text-white">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path>
                                    </svg>
                                </a>
                            </div>
                            <div class="flex-shrink">
                                <a href="#" @click.stop="this.$store.state.editor.console = !this.$store.state.editor.console" class="inline-flex items-center justify-center" style="height: 2.5em; width: 2.5em;">
                                    <span class="sr-only">Close panel</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true" class="h-4 w-4 text-gray-300 hover:text-white">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </nav>
                </header>
                <div class="flex flex-grow overflow-hidden h-full">
                    <aside v-if="this.$store.state.editor.showComponents" class="hidden sm:block bg-gray-700 text-gray-300 flex-shrink-0 border-l border-black overflow-hidden">
                        <div class="w-64 h-full overflow-x-scroll">
                            <nav class="flex-1 text-xs">
                                <div class="flex items-center justify-between p-2 bg-gray-900">
                                    <div class="flex font-medium">
                                        <svg class="h-4 w-4" viewBox="0 0 24 24">
                                            <path fill="currentColor" d="M19,19V5H5V19H19M19,3A2,2 0 0,1 21,5V19A2,2 0 0,1 19,21H5A2,2 0 0,1 3,19V5C3,3.89 3.9,3 5,3H19M11,7H13V11H17V13H13V17H11V13H7V11H11V7Z" />
                                        </svg>
                                        <span class="ml-2 uppercase">Page Components</span>
                                    </div>
                                    <button @click.stop="this.$store.state.editor.showComponents = !this.$store.state.editor.showComponents">
                                        <span class="sr-only">Close panel</span> 
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" aria-hidden="true" class="h-4 w-8 hover:text-white">
                                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path>
                                        </svg>
                                    </button>
                                </div>
                                <div v-for="(category, index) in this.components" :index="index" :key="category" class="cursor-pointer">
                                    <div class="flex items-center justify-between p-2 bg-gray-900" @click="fetchComponent(category)">
                                        <div class="flex items-center justify-center">
                                            <svg aria-hidden="true" focusable="false" width="16" height="16" viewBox="0 0 16 16" class="text-gray-300 block">
                                                <path d="M4 6l3 .01h2L12 6l-4 4-4-4z" fill="currentColor"></path>
                                            </svg>
                                            <span class="ml-2 uppercase">@{{ category }}</span>
                                        </div>
                                    </div>
                                    <div v-if="this.selectedComponent[category]" class="flex flex-wrap">
                                        <div v-for="(item, index) in this.selectedComponent[category]" :index="index" :key="item.slug" class="pb-1 overflow-hidden box-border border-r border-b border-gray-800" style="flex-basis: 33.3333%;" @click="loadTemplate(item.slug)">
                                            <div class="relative flex flex-col justify-center items-center" style="height: 92px;">
                                                <div class="flex justify-center items-center w-full" style="flex:0 0 64px;">
                                                    <div v-html="item.icon"></div>
                                                </div>
                                                <div class="flex flex-1 flex-col items-end justify-center text-center px-2">@{{ item.name }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </nav>
                        </div>
                    </aside>
                    <main v-if="this.currentSelection" class="overflow-x-scroll flex-1 relative" :class="{'guidelines': this.$store.state.editor.guidelines}">
                        <component
                            :is="this.$store.state.content[this.currentSelection].type" 
                            :uuid="this.currentSelection"
                            :key="this.currentSelection">
                        </component>
                    </main>
                    <main v-else class="relative overflow-x-scroll flex-1" :class="{'guidelines': this.$store.state.editor.guidelines}">
                        <s-app></s-app>
                    </main>
                    <aside v-if="this.$store.state.editor.navigator && this.currentSelection" class="hidden sm:block bg-gray-700 text-gray-300 flex-shrink-0 border-l border-black overflow-hidden">
                        <div class="w-64 h-full overflow-x-scroll">
                            <nav class="flex-1 text-xs">
                                <div class="flex items-center justify-between p-2 bg-gray-900">
                                    <h2 class="flex font-medium">
                                        <svg class="h-4 w-4" viewBox="0 0 21 15">
                                            <path fill="currentColor" d="M15 1H1v3h14V1zM6 6v3h14V6H6zm0 8h14v-3H6v3z"></path>
                                        </svg>
                                        <span class="ml-2 uppercase text-xs">Navigator</span>
                                    </h2>
                                    <div class="flex">
                                        <button @click.prevent="selectPreviousElement()" class="w-8 h-4 cursor-pointer inline-flex items-center justify-center hover:text-white">
                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" class="h-4 w-4">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 19l-7-7 7-7"></path>
                                            </svg>
                                        </button>
                                        <button @click.stop="clearSelection()" class="w-8 h-4 cursor-pointer inline-flex items-center justify-center hover:text-white">
                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" class="h-4 w-4">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 19l-7-7 7-7m8 14l-7-7 7-7"></path>
                                            </svg>
                                        </button>
                                        <button @click.stop="this.$store.state.editor.navigator = false" class="hover:text-white">
                                            <span class="sr-only">Close panel</span> 
                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true" class="h-4 w-8">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                                <div class="p-2">
                                    <div v-if="this.$store.state.content[this.currentSelection]">
                                        <button class="button w-full mt-2 py-2 px-4 bg-gray-700 border border-black text-xs mt-2 focus:outline-none" @click.stop="openEditor({menu: null, selectionMenu: 'settings'})">
                                            Edit selected element?
                                        </button>
                                        <button @click.stop="insertElement('s-wrapper', null)" class="button w-full py-2 px-4 bg-gray-700 border border-black text-xs mt-2 focus:outline-none">
                                            Insert new element
                                        </button>
                                        <div class="flex mt-2">
                                            <input type="text" @change="save" @input="save" v-model="this.$store.state.content[this.currentSelection]['text']" placeholder="Enter text" class="w-full py-2 px-4 bg-gray-700 border border-black text-xs focus:outline-none" />
                                            <div class="flex-shrink-0 pr-2">
                                                <button v-if="watchSelectMode" @click="watchSelectMode = null;" class="w-8 h-8 inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true" class="h-4 w-4 text-gray-300 hover:text-white">
                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13.875 18.825A10.05 10.05 0 0112 19c-4.478 0-8.268-2.943-9.543-7a9.97 9.97 0 011.563-3.029m5.858.908a3 3 0 114.243 4.243M9.878 9.878l4.242 4.242M9.88 9.88l-3.29-3.29m7.532 7.532l3.29 3.29M3 3l3.59 3.59m0 0A9.953 9.953 0 0112 5c4.478 0 8.268 2.943 9.543 7a10.025 10.025 0 01-4.132 5.411m0 0L21 21" />
                                                    </svg>
                                                </button>
                                                <button v-else @click="watchSelect" class="w-8 h-8 inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" aria-hidden="true" class="w-4 h-4">
                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"></path> <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"></path>
                                                    </svg>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div v-else class="text-xs uppercase mt-2">
                                        You have reached the last child
                                    </div>
                                </div>
                                <div class="mt-2">
                                    <div v-if="this.$store.state.content[this.currentSelection].data && this.$store.state.content[this.currentSelection].data.length">
                                        <div class="flex items-center justify-between p-2 bg-gray-900">
                                            <div class="flex font-medium">
                                                <span class="ml-2 uppercase">Child Elements</span>
                                            </div>
                                        </div>
                                        <div class="my-2 px-2 space-y-1 overflow-y-scroll" style="max-height:10rem;">
                                            <draggable v-model="this.$store.state.content[this.currentSelection].data" @end="onDragEnd" :component-data="{name:'fade'}" item-key="id">
                                                <template #item="{element}" class="col-span-1 flex shadow-sm">
                                                    <div class="flex-1 flex items-center justify-between border border-black bg-gray-800 truncate" :class="[{'border border-red-400': typeof this.$store.state.editor != 'undefined' && this.$store.state.editor.currentIndicator == element}]" @mouseover="indicateBlock(element)" @mouseleave="indicateBlock(null)">
                                                        <div class="flex-1 px-4 py-2 text-xs leading-5 truncate">
                                                            <button @click.stop="selectElement(element, null);" class="font-medium transition ease-in-out duration-150 inline-flex items-center justify-center bg-transparent hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                                <@{{ this.$store.state.content[element].type == 's-wrapper' ? (this.$store.state.content[element].tag ? this.$store.state.content[element].tag : 'div') : this.$store.state.content[element].type.substring(2) }}>
                                                            </button>
                                                        </div>
                                                        <div v-show="targetSelectMode" class="flex-shrink-0 pr-2">
                                                            <button @click.stop="selectTarget(element)" class="w-8 h-8 inline-flex items-center justify-center bg-transparent focus:outline-none">
                                                                <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 24 24" stroke="none">
                                                                    <path fill="currentColor" d="M11,2V4.07C7.38,4.53 4.53,7.38 4.07,11H2V13H4.07C4.53,16.62 7.38,19.47 11,19.93V22H13V19.93C16.62,19.47 19.47,16.62 19.93,13H22V11H19.93C19.47,7.38 16.62,4.53 13,4.07V2M11,6.08V8H13V6.09C15.5,6.5 17.5,8.5 17.92,11H16V13H17.91C17.5,15.5 15.5,17.5 13,17.92V16H11V17.91C8.5,17.5 6.5,15.5 6.08,13H8V11H6.09C6.5,8.5 8.5,6.5 11,6.08M12,11A1,1 0 0,0 11,12A1,1 0 0,0 12,13A1,1 0 0,0 13,12A1,1 0 0,0 12,11Z" />
                                                                </svg>
                                                            </button>
                                                        </div>
                                                        <div class="flex-shrink-0 pr-2">
                                                            <button @click.stop="selectElement(element, null);openEditor({menu: null, selectionMenu: 'settings'});" class="w-8 h-8 inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                                <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20.71,7.04C21.1,6.65 21.1,6 20.71,5.63L18.37,3.29C18,2.9 17.35,2.9 16.96,3.29L15.12,5.12L18.87,8.87M3,17.25V21H6.75L17.81,9.93L14.06,6.18L3,17.25Z" />
                                                                </svg>
                                                            </button>
                                                        </div>
                                                        <div class="flex-shrink-0 pr-2">
                                                            <button @click.stop="removeElement(element)" class="w-8 h-8 inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                                <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                                                </svg>
                                                            </button>
                                                        </div>
                                                        <div class="handle cursor-move flex-shrink-0 pr-2">
                                                            <button class="w-8 h-8 cursor-move inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                                <svg class="w-4 h-4" viewBox="0 0 20 20" fill="currentColor"><path d="M7 2a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 2zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 8zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 14zm6-8a2 2 0 1 0-.001-4.001A2 2 0 0 0 13 6zm0 2a2 2 0 1 0 .001 4.001A2 2 0 0 0 13 8zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 13 14z"></path></svg>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </template>
                                            </draggable>
                                        </div>
                                    </div>
                                    <div class="flex items-center justify-between p-2 bg-gray-900">
                                        <div class="flex font-medium">
                                            <span class="ml-2 uppercase">Element Attributes</span>
                                        </div>
                                        <button @click.stop="this.$store.state.editor.elementAttributes = !this.$store.state.editor.elementAttributes">
                                            <span class="sr-only">Show/hide Element Attributes</span> 
                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 18 14" aria-hidden="true" class="h-4 w-4 mr-2 hover:text-white">
                                                <path fill="currentColor" d="M9.028 1C4.596 1 1 6.94 1 6.94s3.596 6.1 8.028 6.1c4.434 0 8.027-6.1 8.027-6.1S13.462 1 9.028 1zM9 11a4 4 0 01-4-4c0-2.027 1.512-3.683 3.467-3.946A2.48 2.48 0 008 4.5 2.5 2.5 0 0010.5 7a2.49 2.49 0 002.234-1.4c.164.437.266.906.266 1.4a4 4 0 01-4 4z"></path>
                                            </svg>
                                        </button>
                                    </div>
                                    <div v-show="this.$store.state.editor.elementAttributes" class="mt-2 px-2 space-y-1 h-80 overflow-y-scroll">
                                        <div class="" v-for="(value, key) in this.$store.state.content[this.currentSelection]" :key="key">
                                            <div v-if="typeof value != 'object' && typeof value != 'array'" class="border border-black mb-1 flex-1 flex items-center justify-between bg-gray-800 px-2 py-2 text-xs truncate">
                                                <div class="flex-1 text-xs leading-5 truncate">
                                                    @{{ key }}: @{{ value }}
                                                </div>
                                                <div v-if="key != 'type' && key != 'slug' && key != 'parent'" class="flex-shrink-0">
                                                    <button @click.stop="delete this.$store.state.content[this.currentSelection][key]" class="w-6 h-6 inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                        <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                                        </svg>
                                                    </button>
                                                </div>
                                                <div v-if="targetSelectMode" class="flex-shrink-0 pr-2">
                                                    <button @click.stop="selectTarget('content', this.currentSelection, key)" class="w-6 h-6 inline-flex items-center justify-center bg-transparent focus:outline-none">
                                                        <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 24 24" stroke="none">
                                                            <path fill="currentColor" d="M11,2V4.07C7.38,4.53 4.53,7.38 4.07,11H2V13H4.07C4.53,16.62 7.38,19.47 11,19.93V22H13V19.93C16.62,19.47 19.47,16.62 19.93,13H22V11H19.93C19.47,7.38 16.62,4.53 13,4.07V2M11,6.08V8H13V6.09C15.5,6.5 17.5,8.5 17.92,11H16V13H17.91C17.5,15.5 15.5,17.5 13,17.92V16H11V17.91C8.5,17.5 6.5,15.5 6.08,13H8V11H6.09C6.5,8.5 8.5,6.5 11,6.08M12,11A1,1 0 0,0 11,12A1,1 0 0,0 12,13A1,1 0 0,0 13,12A1,1 0 0,0 12,11Z" />
                                                        </svg>
                                                    </button>
                                                </div>
                                                <div v-show="watchSelectMode || attributeSelectMode" class="flex-shrink-0 pr-2">
                                                    <button @click.stop="selectWatch('content', this.currentSelection, key)" class="w-6 h-6 inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" aria-hidden="true" class="w-4 h-4">
                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"></path> <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"></path>
                                                        </svg>
                                                    </button>
                                                </div>
                                                <div v-if="objectSelectMode" class="flex-shrink-0 pr-2">
                                                    <button @click.stop="selectObject('content', this.currentSelection, key)" class="w-6 h-6 inline-flex items-center justify-center bg-transparent focus:outline-none">
                                                        <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 24 24" stroke="none">
                                                            <path fill="currentColor" d="M11,2V4.07C7.38,4.53 4.53,7.38 4.07,11H2V13H4.07C4.53,16.62 7.38,19.47 11,19.93V22H13V19.93C16.62,19.47 19.47,16.62 19.93,13H22V11H19.93C19.47,7.38 16.62,4.53 13,4.07V2M11,6.08V8H13V6.09C15.5,6.5 17.5,8.5 17.92,11H16V13H17.91C17.5,15.5 15.5,17.5 13,17.92V16H11V17.91C8.5,17.5 6.5,15.5 6.08,13H8V11H6.09C6.5,8.5 8.5,6.5 11,6.08M12,11A1,1 0 0,0 11,12A1,1 0 0,0 12,13A1,1 0 0,0 13,12A1,1 0 0,0 12,11Z" />
                                                        </svg>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div v-if="tree.length">
                                        <div class="flex items-center justify-between p-2 bg-gray-900">
                                            <div class="flex font-medium">
                                                <span class="ml-2 uppercase">Path back to root element</span>
                                            </div>
                                        </div>
                                        <div v-for="(item, index) in tree" :index="index" :key="item" class="my-2 px-2 space-y-1 max-h-80 overflow-y-scroll">
                                            <div class="flex-1 flex items-center justify-between border border-black bg-gray-800 truncate">
                                                <div class="flex-1 px-4 py-2 text-xs leading-5 truncate">
                                                    <button @click.stop="selectElement(item, null)" class="font-medium transition ease-in-out duration-150 inline-flex items-center justify-center bg-transparent hover:text-white focus:outline-none">
                                                        <@{{ this.$store.state.content[item].type == 's-wrapper' ? (this.$store.state.content[item].tag ? this.$store.state.content[item].tag : 'div') : this.$store.state.content[item].type.substring(2) }}>
                                                    </button>
                                                </div>
                                                <div class="flex-shrink-0 pr-2">
                                                    <button @click.stop="selectElement(element, null);openEditor({menu: null, selectionMenu: 'settings'});" class="w-8 h-8 inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                        <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20.71,7.04C21.1,6.65 21.1,6 20.71,5.63L18.37,3.29C18,2.9 17.35,2.9 16.96,3.29L15.12,5.12L18.87,8.87M3,17.25V21H6.75L17.81,9.93L14.06,6.18L3,17.25Z" />
                                                        </svg>
                                                    </button>
                                                </div>
                                                <div class="flex-shrink-0 pr-2">
                                                    <button @click.stop="removeElement(element)" class="w-8 h-8 inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                        <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                                        </svg>
                                                    </button>
                                                </div>
                                                <div class="handle cursor-move flex-shrink-0 pr-2">
                                                    <button class="w-8 h-8 cursor-move inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                        <svg class="w-4 h-4" viewBox="0 0 20 20" fill="currentColor"><path d="M7 2a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 2zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 8zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 14zm6-8a2 2 0 1 0-.001-4.001A2 2 0 0 0 13 6zm0 2a2 2 0 1 0 .001 4.001A2 2 0 0 0 13 8zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 13 14z"></path></svg>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </nav>
                        </div>
                    </aside>
                    <aside v-else-if="this.$store.state.editor.navigator" class="hidden sm:block bg-gray-700 text-gray-300 flex-shrink-0 border-l border-black overflow-hidden">
                        <div class="w-64 h-full overflow-x-scroll">
                            <nav class="flex-1 text-xs">
                                <div class="flex items-center justify-between p-2 bg-gray-900">
                                    <div class="flex font-medium">
                                        <svg class="h-4 w-4" viewBox="0 0 21 15">
                                            <path fill="currentColor" d="M15 1H1v3h14V1zM6 6v3h14V6H6zm0 8h14v-3H6v3z"></path>
                                        </svg>
                                        <span class="ml-2 uppercase">Navigator</span>
                                    </div>
                                    <button @click.stop="this.$store.state.editor.navigator = false">
                                        <span class="sr-only">Close panel</span> 
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" aria-hidden="true" class="h-4 w-8 hover:text-white">
                                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path>
                                        </svg>
                                    </button>
                                </div>
                                <div class="px-2 mb-2 space-y-1 max-h-80 overflow-y-scroll">
                                    <div>
                                        <div class="col-span-1 flex">
                                            <div class="flex-1 flex items-center justify-between">
                                                <div class="flex-1 py-2 leading-5 truncate">
                                                    <button @click="this.section = 'header'" :class="{'text-green-600': this.section == 'header'}" class="font-medium inline-flex items-center bg-transparent justify-center focus:outline-none">
                                                        <svg class="w-4 h-4" fill="currentColor" viewBox="0 0 24 24">
                                                            <path d="M6,2H18A2,2 0 0,1 20,4V20A2,2 0 0,1 18,22H6A2,2 0 0,1 4,20V4A2,2 0 0,1 6,2M6,4V8H18V4H6Z" />
                                                        </svg>    
                                                        <span class="ml-3">Header</span>
                                                    </button>
                                                </div>
                                                <div v-show="(!this.$store.state.page.header)" class="flex-shrink-0 pr-2">
                                                    <button @click.stop="this.$store.state.page.header = [];saveBody();" title="Use page header" class="w-8 h-8 inline-flex items-center justify-center rounded-full bg-transparent hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                        <svg class="w-4 h-4" viewBox="0 0 24 24">
                                                            <path fill="currentColor" d="M6,2A2,2 0 0,0 4,4V20A2,2 0 0,0 6,22H18A2,2 0 0,0 20,20V8L14,2H6M6,4H13V9H18V20H6V4M8,12V14H16V12H8M8,16V18H13V16H8Z" />
                                                        </svg>
                                                    </button>
                                                </div>
                                                <div v-show="(this.$store.state.page.header)" class="flex-shrink-0 pr-2">
                                                    <button @click.stop="delete this.$store.state.page.header;saveBody();" title="Use global header" class="w-8 h-8 inline-flex items-center justify-center rounded-full bg-transparent hover:text-white transition ease-in-out duration-150">
                                                        <svg class="w-4 h-4" viewBox="0 0 24 24">
                                                            <path fill="currentColor" d="M17.9,17.39C17.64,16.59 16.89,16 16,16H15V13A1,1 0 0,0 14,12H8V10H10A1,1 0 0,0 11,9V7H13A2,2 0 0,0 15,5V4.59C17.93,5.77 20,8.64 20,12C20,14.08 19.2,15.97 17.9,17.39M11,19.93C7.05,19.44 4,16.08 4,12C4,11.38 4.08,10.78 4.21,10.21L9,15V16A2,2 0 0,0 11,18M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2Z" />
                                                        </svg>
                                                    </button>
                                                </div>
                                                <div class="flex-shrink-0 pr-2">
                                                    <button @click.stop="clearElements('header')" title="Clear header" class="w-8 h-8 inline-flex items-center justify-center rounded-full bg-transparent hover:text-white transition ease-in-out duration-150">
                                                        <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                                        </svg>
                                                    </button>
                                                </div>
                                                <div class="flex-shrink-0 pr-2">
                                                    <button @click.stop="insertElement('s-wrapper', 'header')" title="Insert Element (Ctrl + i)" class="w-8 h-8 inline-flex items-center justify-center hover:text-white focus:outline-none">
                                                        <svg class="w-4 h-4" viewBox="0 0 24 24">
                                                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 4v16m8-8H4" />
                                                        </svg>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <draggable v-if="this.$store.state.page.header" v-model="this.$store.state.page.header" :component-data="{name:'fade'}" item-key="id">
                                            <template #item="{element}" class="col-span-1 flex shadow-sm">
                                                <div class="flex-1 flex items-center justify-between border border-black bg-gray-800 truncate" :class="[{'border border-red-400': typeof this.$store.state.editor != 'undefined' && this.$store.state.editor.currentIndicator == element}]" @mouseover="indicateBlock(element)" @mouseleave="indicateBlock(null)">
                                                    <div class="flex-1 px-4 py-2 text-xs leading-5 truncate">
                                                        <button @click.stop="selectElement(element, 'header')" class="font-medium transition ease-in-out duration-150 inline-flex items-center justify-center rounded-full bg-transparent hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                            <@{{ this.$store.state.content[element].tag }}>
                                                        </button>
                                                    </div>
                                                    <div class="flex-shrink-0 pr-2">
                                                        <button @click.stop="selectElement(element, 'header')" class="w-8 h-8 inline-flex items-center justify-center hover:text-white focus:outline-none">
                                                            <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20.71,7.04C21.1,6.65 21.1,6 20.71,5.63L18.37,3.29C18,2.9 17.35,2.9 16.96,3.29L15.12,5.12L18.87,8.87M3,17.25V21H6.75L17.81,9.93L14.06,6.18L3,17.25Z" />
                                                            </svg>
                                                        </button>
                                                    </div>
                                                    <div class="flex-shrink-0 pr-2">
                                                        <button @click.stop="removeElement(element, 'header')" class="w-8 h-8 inline-flex items-center justify-center hover:text-white focus:outline-none">
                                                            <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                                            </svg>
                                                        </button>
                                                    </div>
                                                    <div class="handle cursor-move flex-shrink-0 pr-2">
                                                        <button class="w-8 h-8 cursor-move inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                            <svg class="w-4 h-4" viewBox="0 0 20 20" fill="currentColor"><path d="M7 2a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 2zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 8zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 14zm6-8a2 2 0 1 0-.001-4.001A2 2 0 0 0 13 6zm0 2a2 2 0 1 0 .001 4.001A2 2 0 0 0 13 8zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 13 14z"></path></svg>
                                                        </button>
                                                    </div>
                                                </div>
                                            </template>
                                        </draggable>
                                        <draggable v-else v-model="this.$store.state.settings.header" :component-data="{name:'fade'}" item-key="id">
                                            <template #item="{element}" class="col-span-1 flex shadow-sm">
                                                <div class="flex-1 flex items-center justify-between border border-black bg-gray-800 truncate" @mouseover="indicateBlock(element)" @mouseleave="indicateBlock(null)">
                                                    <div class="flex-1 px-4 py-2 text-xs leading-5 truncate">
                                                        <button @click.stop="selectElement(element, 'header')" class="font-medium transition ease-in-out duration-150 inline-flex items-center justify-center bg-transparent hover:text-white focus:outline-none ">
                                                            <@{{ this.$store.state.content[element].type == 's-wrapper' ? (this.$store.state.content[element].tag ? this.$store.state.content[element].tag : 'div') : this.$store.state.content[element].type.substring(2) }}>
                                                        </button>
                                                    </div>
                                                    <div class="flex-shrink-0 pr-2">
                                                        <button @click.stop="selectElement(element, 'header')" class="w-8 h-8 inline-flex items-center justify-center hover:text-white focus:outline-none">
                                                            <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20.71,7.04C21.1,6.65 21.1,6 20.71,5.63L18.37,3.29C18,2.9 17.35,2.9 16.96,3.29L15.12,5.12L18.87,8.87M3,17.25V21H6.75L17.81,9.93L14.06,6.18L3,17.25Z" />
                                                            </svg>
                                                        </button>
                                                    </div>
                                                    <div class="flex-shrink-0 pr-2">
                                                        <button @click.stop="removeElement(element, 'header')" class="w-8 h-8 inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                            <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                                            </svg>
                                                        </button>
                                                    </div>
                                                    <div class="handle cursor-move flex-shrink-0 pr-2">
                                                        <button class="w-8 h-8 cursor-move inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                            <svg class="w-4 h-4" viewBox="0 0 20 20" fill="currentColor"><path d="M7 2a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 2zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 8zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 14zm6-8a2 2 0 1 0-.001-4.001A2 2 0 0 0 13 6zm0 2a2 2 0 1 0 .001 4.001A2 2 0 0 0 13 8zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 13 14z"></path></svg>
                                                        </button>
                                                    </div>
                                                </div>
                                            </template>
                                        </draggable>
                                    </div>
                                    <div>
                                        <div class="col-span-1 flex">
                                            <div class="flex-1 flex items-center justify-between">
                                                <div class="flex-1 py-2  leading-5 truncate">
                                                    <button @click="this.section = null;" :class="{'text-green-600': this.section == null}"  class="font-medium inline-flex items-center bg-transparent justify-center focus:outline-none">
                                                        <svg class="w-4 h-4" fill="currentColor" viewBox="0 0 24 24">
                                                            <path d="M6,2H18A2,2 0 0,1 20,4V20A2,2 0 0,1 18,22H6A2,2 0 0,1 4,20V4A2,2 0 0,1 6,2M6,8V16H18V8H6Z" />
                                                        </svg>
                                                        <span class="ml-3">Body</span>
                                                    </button>
                                                </div>
                                                <div v-show="this.$store.state.page.data && this.$store.state.page.data.length" class="flex-shrink-0 pr-2">
                                                    <button @click.stop="clearElements(null)" title="Clear page" class="w-8 h-8 inline-flex items-center justify-center rounded-full bg-transparent focus:outline-none transition ease-in-out duration-150">
                                                        <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                                        </svg>
                                                    </button>
                                                </div>
                                                <div class="flex-shrink-0 pr-2">
                                                    <button @click.stop="insertElement('s-wrapper', null)" title="Insert Element (Ctrl + i)" class="w-8 h-8 inline-flex items-center justify-center hover:text-white focus:outline-none">
                                                        <svg class="w-4 h-4" viewBox="0 0 24 24">
                                                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 4v16m8-8H4" />
                                                        </svg>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <draggable v-if="this.$store.state.page.data" v-model="this.$store.state.page.data" :component-data="{name:'fade'}" item-key="id">
                                            <template #item="{element}" class="col-span-1 flex shadow-sm" :key="element">
                                                <div class="flex-1 flex items-center justify-between border border-black bg-gray-800 truncate" :class="[{'border border-red-400': typeof this.$store.state.editor != 'undefined' && this.$store.state.editor.currentIndicator == element}]" @mouseover="indicateBlock(element)" @mouseleave="indicateBlock(null)">
                                                    <div class="flex-1 px-4 py-2 text-xs leading-5 truncate">
                                                        <button @click.stop="selectElement(element, null)" class="font-medium transition ease-in-out duration-150 inline-flex items-center justify-center bg-transparent hover:text-white focus:outline-none">
                                                            <@{{ this.$store.state.content[element].tag }}>
                                                        </button>
                                                    </div>
                                                    <div class="flex-shrink-0 pr-2">
                                                        <button @click.stop="selectElement(element, null);openEditor({menu: null, selectionMenu: 'settings'});" class="w-8 h-8 inline-flex items-center justify-center hover:text-white focus:outline-none">
                                                            <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20.71,7.04C21.1,6.65 21.1,6 20.71,5.63L18.37,3.29C18,2.9 17.35,2.9 16.96,3.29L15.12,5.12L18.87,8.87M3,17.25V21H6.75L17.81,9.93L14.06,6.18L3,17.25Z" />
                                                            </svg>
                                                        </button>
                                                    </div>
                                                    <div class="flex-shrink-0 pr-2">
                                                        <button @click.stop="removeElement(element)" class="w-8 h-8 inline-flex items-center justify-center hover:text-white focus:outline-none">
                                                            <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                                            </svg>
                                                        </button>
                                                    </div>
                                                    <div class="handle cursor-move flex-shrink-0 pr-2">
                                                        <button class="w-8 h-8 cursor-move inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                            <svg class="w-4 h-4" viewBox="0 0 20 20" fill="currentColor"><path d="M7 2a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 2zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 8zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 14zm6-8a2 2 0 1 0-.001-4.001A2 2 0 0 0 13 6zm0 2a2 2 0 1 0 .001 4.001A2 2 0 0 0 13 8zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 13 14z"></path></svg>
                                                        </button>
                                                    </div>
                                                </div>
                                            </template>
                                        </draggable>
                                    </div>
                                    <div>
                                        <div class="col-span-1 flex">
                                            <div class="flex-1 flex items-center justify-between">
                                                <div class="flex-1 py-2 leading-5 truncate">
                                                    <button @click="this.section = 'footer'" :class="{'text-green-600': this.section == 'footer'}"  class="font-medium inline-flex items-center bg-transparent justify-center focus:outline-none">
                                                        <svg class="w-4 h-4" viewBox="0 0 24 24">
                                                            <path fill="currentColor" d="M6,2H18A2,2 0 0,1 20,4V20A2,2 0 0,1 18,22H6A2,2 0 0,1 4,20V4A2,2 0 0,1 6,2M6,16V20H18V16H6Z" />
                                                        </svg>
                                                        <span class="ml-3">Footer</span>
                                                    </button>
                                                </div>
                                                <div v-show="(!this.$store.state.page.footer)" class="flex-shrink-0 pr-2">
                                                    <button @click.stop="this.$store.state.page.footer = [];saveBody();" title="Use page footer" class="w-8 h-8 inline-flex items-center justify-center rounded-full bg-transparent hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                        <svg class="w-4 h-4" viewBox="0 0 24 24">
                                                            <path fill="currentColor" d="M6,2A2,2 0 0,0 4,4V20A2,2 0 0,0 6,22H18A2,2 0 0,0 20,20V8L14,2H6M6,4H13V9H18V20H6V4M8,12V14H16V12H8M8,16V18H13V16H8Z" />
                                                        </svg>
                                                    </button>
                                                </div>
                                                <div v-show="(this.$store.state.page.footer)" class="flex-shrink-0 pr-2">
                                                    <button @click.stop="delete this.$store.state.page.footer;saveBody();" title="Use global footer" class="w-8 h-8 inline-flex items-center justify-center rounded-full bg-transparent hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                        <svg class="w-4 h-4" viewBox="0 0 24 24">
                                                            <path fill="currentColor" d="M17.9,17.39C17.64,16.59 16.89,16 16,16H15V13A1,1 0 0,0 14,12H8V10H10A1,1 0 0,0 11,9V7H13A2,2 0 0,0 15,5V4.59C17.93,5.77 20,8.64 20,12C20,14.08 19.2,15.97 17.9,17.39M11,19.93C7.05,19.44 4,16.08 4,12C4,11.38 4.08,10.78 4.21,10.21L9,15V16A2,2 0 0,0 11,18M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2Z" />
                                                        </svg>
                                                    </button>
                                                </div>
                                                <div class="flex-shrink-0 pr-2">
                                                    <button @click.stop="clearElements('footer')" title="Clear footer" class="w-8 h-8 inline-flex items-center justify-center rounded-full bg-transparent hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                        <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                                        </svg>
                                                    </button>
                                                </div>
                                                <div class="flex-shrink-0 pr-2">
                                                    <button @click.stop="insertElement('s-wrapper', 'footer')" title="Insert Element (Ctrl + i)" class="w-8 h-8 inline-flex items-center justify-center hover:text-white focus:outline-none">
                                                        <svg class="w-4 h-4" viewBox="0 0 24 24">
                                                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 4v16m8-8H4" />
                                                        </svg>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="grid grid-cols-1">
                                            <draggable v-if="this.$store.state.page.footer" v-model="this.$store.state.page.footer" :component-data="{name:'fade'}" item-key="id">
                                                <template #item="{element}" class="col-span-1 flex shadow-sm">
                                                    <div class="flex-1 flex items-center justify-between border border-black bg-gray-800 truncate" @mouseover="indicateBlock(element)" @mouseleave="indicateBlock(null)">
                                                        <div class="flex-1 px-4 py-2 text-xs leading-5 truncate">
                                                            <button @click.stop="selectElement(element, 'footer')" class="font-medium transition ease-in-out duration-150 inline-flex items-center justify-center bg-transparent hover:text-white focus:outline-none">
                                                                <@{{ this.$store.state.content[element].tag }}>
                                                            </button>
                                                        </div>
                                                        <div class="flex-shrink-0 pr-2">
                                                            <button @click.stop="selectElement(element, 'footer');openEditor()" class="w-8 h-8 inline-flex items-center justify-center hover:text-white focus:outline-none">
                                                                <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20.71,7.04C21.1,6.65 21.1,6 20.71,5.63L18.37,3.29C18,2.9 17.35,2.9 16.96,3.29L15.12,5.12L18.87,8.87M3,17.25V21H6.75L17.81,9.93L14.06,6.18L3,17.25Z" />
                                                                </svg>
                                                            </button>
                                                        </div>
                                                        <div class="flex-shrink-0 pr-2">
                                                            <button @click.stop="removeElement(element, 'footer')" class="w-8 h-8 inline-flex items-center justify-center hover:text-white focus:outline-none">
                                                                <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                                                </svg>
                                                            </button>
                                                        </div>
                                                        <div class="handle cursor-move flex-shrink-0 pr-2">
                                                            <button class="w-8 h-8 cursor-move inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                                <svg class="w-4 h-4" viewBox="0 0 20 20" fill="currentColor"><path d="M7 2a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 2zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 8zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 14zm6-8a2 2 0 1 0-.001-4.001A2 2 0 0 0 13 6zm0 2a2 2 0 1 0 .001 4.001A2 2 0 0 0 13 8zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 13 14z"></path></svg>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </template>
                                            </draggable>
                                            <draggable v-else v-model="this.$store.state.settings.footer" :component-data="{name:'fade'}" item-key="id">
                                                <template #item="{element}" class="col-span-1 flex shadow-sm">
                                                    <div class="flex-1 flex items-center justify-between border border-black bg-gray-800 truncate" @mouseover="indicateBlock(element)" @mouseleave="indicateBlock(null)">
                                                        <div class="flex-1 px-4 py-2 text-xs leading-5 truncate">
                                                            <button @click.stop="selectElement(element, 'footer')" class="font-medium transition ease-in-out duration-150 inline-flex items-center justify-center bg-transparent hover:text-white focus:outline-none">
                                                                <@{{ this.$store.state.content[element].type == 's-wrapper' ? (this.$store.state.content[element].tag ? this.$store.state.content[element].tag : 'div') : this.$store.state.content[element].type.substring(2) }}>
                                                            </button>
                                                        </div>
                                                        <div class="flex-shrink-0 pr-2">
                                                            <button @click.stop="selectElement(element, 'footer');openEditor({menu: null, selectionMenu: 'settings'})" class="w-8 h-8 inline-flex items-center justify-center hover:text-white focus:outline-none">
                                                                <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20.71,7.04C21.1,6.65 21.1,6 20.71,5.63L18.37,3.29C18,2.9 17.35,2.9 16.96,3.29L15.12,5.12L18.87,8.87M3,17.25V21H6.75L17.81,9.93L14.06,6.18L3,17.25Z" />
                                                                </svg>
                                                            </button>
                                                        </div>
                                                        <div class="flex-shrink-0 pr-2">
                                                            <button @click.stop="removeElement(element, 'footer')" class="w-8 h-8 inline-flex items-center justify-center hover:text-white focus:outline-none">
                                                                <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                                                </svg>
                                                            </button>
                                                        </div>
                                                        <div class="handle cursor-move flex-shrink-0 pr-2">
                                                            <button class="w-8 h-8 cursor-move inline-flex items-center justify-center hover:text-white focus:outline-none">
                                                                <svg class="w-4 h-4" viewBox="0 0 20 20" fill="currentColor"><path d="M7 2a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 2zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 8zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 14zm6-8a2 2 0 1 0-.001-4.001A2 2 0 0 0 13 6zm0 2a2 2 0 1 0 .001 4.001A2 2 0 0 0 13 8zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 13 14z"></path></svg>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </template>
                                            </draggable>
                                        </ul>
                                    </div>
                                </div>
                                <div class="flex items-center justify-between p-2 bg-gray-900">
                                    <div class="flex font-medium">
                                        <span class="ml-2 uppercase">Page Attributes</span>
                                    </div>
                                    <button @click.stop="this.$store.state.editor.pageAttributes = !this.$store.state.editor.pageAttributes">
                                        <span class="sr-only">Show/hide Page Attributes</span> 
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 18 14" aria-hidden="true" class="h-4 w-4 mr-2 hover:text-white">
                                            <path fill="currentColor" d="M9.028 1C4.596 1 1 6.94 1 6.94s3.596 6.1 8.028 6.1c4.434 0 8.027-6.1 8.027-6.1S13.462 1 9.028 1zM9 11a4 4 0 01-4-4c0-2.027 1.512-3.683 3.467-3.946A2.48 2.48 0 008 4.5 2.5 2.5 0 0010.5 7a2.49 2.49 0 002.234-1.4c.164.437.266.906.266 1.4a4 4 0 01-4 4z"></path>
                                        </svg>
                                    </button>
                                </div>
                                <div v-show="this.$store.state.editor.pageAttributes" class="px-2 space-y-1 h-40 overflow-y-scroll">
                                    <div class="mt-2" v-for="(value, key) in this.$store.state.page" :key="key">
                                        <div v-if="typeof value != 'object' && typeof value != 'array'" class="border border-black mb-1 flex-1 flex items-center justify-between bg-gray-800 px-2 py-2 text-xs truncate">
                                            <div class="flex-1 text-xs leading-5 truncate">
                                                @{{ key }}: @{{ value }}
                                            </div>
                                            <div v-if="key != 'id' && key != 'slug' && key != 'path'" class="flex-shrink-0">
                                                <button @click.stop="delete this.$store.state.page[key]" class="w-6 h-6 inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                    <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                                    </svg>
                                                </button>
                                            </div>
                                            <div v-if="targetSelectMode" class="flex-shrink-0 pr-2">
                                                <button @click.stop="selectTarget('page', null, key)" class="w-6 h-6 inline-flex items-center justify-center bg-transparent focus:outline-none">
                                                    <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 24 24" stroke="none">
                                                        <path fill="currentColor" d="M11,2V4.07C7.38,4.53 4.53,7.38 4.07,11H2V13H4.07C4.53,16.62 7.38,19.47 11,19.93V22H13V19.93C16.62,19.47 19.47,16.62 19.93,13H22V11H19.93C19.47,7.38 16.62,4.53 13,4.07V2M11,6.08V8H13V6.09C15.5,6.5 17.5,8.5 17.92,11H16V13H17.91C17.5,15.5 15.5,17.5 13,17.92V16H11V17.91C8.5,17.5 6.5,15.5 6.08,13H8V11H6.09C6.5,8.5 8.5,6.5 11,6.08M12,11A1,1 0 0,0 11,12A1,1 0 0,0 12,13A1,1 0 0,0 13,12A1,1 0 0,0 12,11Z" />
                                                    </svg>
                                                </button>
                                            </div>
                                            <div v-show="watchSelectMode || attributeSelectMode" class="flex-shrink-0 pr-2">
                                                <button @click.stop="selectWatch('page',null, key)" class="w-6 h-6 inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" aria-hidden="true" class="w-4 h-4">
                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"></path> <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"></path>
                                                    </svg>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flex items-center justify-between p-2 bg-gray-900">
                                    <div class="flex font-medium">
                                        <span class="ml-2 uppercase">State Attributes</span>
                                    </div>
                                    <button @click.stop="this.$store.state.editor.stateAttributes = !this.$store.state.editor.stateAttributes">
                                        <span class="sr-only">Show/hide State Attributes</span> 
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 18 14" aria-hidden="true" class="h-4 w-4 mr-2 hover:text-white">
                                            <path fill="currentColor" d="M9.028 1C4.596 1 1 6.94 1 6.94s3.596 6.1 8.028 6.1c4.434 0 8.027-6.1 8.027-6.1S13.462 1 9.028 1zM9 11a4 4 0 01-4-4c0-2.027 1.512-3.683 3.467-3.946A2.48 2.48 0 008 4.5 2.5 2.5 0 0010.5 7a2.49 2.49 0 002.234-1.4c.164.437.266.906.266 1.4a4 4 0 01-4 4z"></path>
                                        </svg>
                                    </button>
                                </div>
                                <div v-show="this.$store.state.editor.stateAttributes" class="px-2 space-y-1 h-40 overflow-y-scroll">
                                    <div class="flex">
                                        <div class="border border-black mb-1 flex-1 flex items-center justify-between bg-gray-800 text-xs truncate">
                                            <input v-model="currentKey" class="block bg-gray-800 h-8 w-24 text-xs flex-shrink border-none" placeholder="Key" type="text" autocomplete="off">
                                            <input v-model="currentValue" class="block bg-gray-800 h-8 w-24 text-xs flex-shrink border-none" placeholder="Value" type="text" autocomplete="off">
                                            <button @click.stop="insertKeyValue('state')" class="flex-shrink pr-2">
                                                <svg class="w-4 h-4" viewBox="0 0 24 24"><path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 4v16m8-8H4"></path></svg>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="mt-2" v-for="(value, key) in this.$store.state.state" :key="key">
                                        <div class="border border-black mb-1 flex-1 flex items-center justify-between bg-gray-800 px-2 py-2 text-xs truncate">
                                            <div class="flex-1 text-xs leading-5 truncate">
                                                @{{ key }}: @{{ value }}
                                            </div>
                                            <div class="flex-shrink-0">
                                                <button @click.stop="delete this.$store.state.state[key]" class="w-6 h-6 inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                    <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                                    </svg>
                                                </button>
                                            </div>
                                            <div v-if="targetSelectMode" class="flex-shrink-0 pr-2">
                                                <button @click.stop="selectTarget('state', null, key)" class="w-6 h-6 inline-flex items-center justify-center bg-transparent focus:outline-none">
                                                    <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 24 24" stroke="none">
                                                        <path fill="currentColor" d="M11,2V4.07C7.38,4.53 4.53,7.38 4.07,11H2V13H4.07C4.53,16.62 7.38,19.47 11,19.93V22H13V19.93C16.62,19.47 19.47,16.62 19.93,13H22V11H19.93C19.47,7.38 16.62,4.53 13,4.07V2M11,6.08V8H13V6.09C15.5,6.5 17.5,8.5 17.92,11H16V13H17.91C17.5,15.5 15.5,17.5 13,17.92V16H11V17.91C8.5,17.5 6.5,15.5 6.08,13H8V11H6.09C6.5,8.5 8.5,6.5 11,6.08M12,11A1,1 0 0,0 11,12A1,1 0 0,0 12,13A1,1 0 0,0 13,12A1,1 0 0,0 12,11Z" />
                                                    </svg>
                                                </button>
                                            </div>
                                            <div v-show="watchSelectMode || attributeSelectMode" class="flex-shrink-0 pr-2">
                                                <button @click.stop="selectWatch('state',null, key)" class="w-6 h-6 inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" aria-hidden="true" class="w-4 h-4">
                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"></path> <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"></path>
                                                    </svg>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div v-if="this.$store.state.entity" class="flex items-center justify-between p-2 bg-gray-900">
                                    <div class="flex font-medium">
                                        <span class="ml-2 uppercase">Entity Attributes</span>
                                    </div>
                                    <button @click.stop="this.$store.state.editor.entityAttributes = !this.$store.state.editor.entityAttributes">
                                        <span class="sr-only">Show/hide Entity Attributes</span> 
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 18 14" aria-hidden="true" class="h-4 w-4 mr-2 hover:text-white">
                                            <path fill="currentColor" d="M9.028 1C4.596 1 1 6.94 1 6.94s3.596 6.1 8.028 6.1c4.434 0 8.027-6.1 8.027-6.1S13.462 1 9.028 1zM9 11a4 4 0 01-4-4c0-2.027 1.512-3.683 3.467-3.946A2.48 2.48 0 008 4.5 2.5 2.5 0 0010.5 7a2.49 2.49 0 002.234-1.4c.164.437.266.906.266 1.4a4 4 0 01-4 4z"></path>
                                        </svg>
                                    </button>
                                </div>
                                <div v-show="this.$store.state.entity && this.$store.state.editor.entityAttributes" class="px-2 space-y-1 h-40 overflow-y-scroll">
                                    <div class="mt-2" v-for="(value, key) in this.$store.state.entity" :key="key">
                                        <div class="border border-black mb-1 flex-1 flex items-center justify-between bg-gray-800 px-2 py-2 text-xs truncate">
                                            <div v-if="typeof value != 'object' && typeof value != 'array'" class="flex-1 text-xs leading-5 truncate">
                                                @{{ key }}: @{{ value }}
                                            </div>
                                            <div v-else class="flex-1 text-xs leading-5 truncate">
                                                @{{ key }}
                                            </div>
                                            <div class="flex-shrink-0">
                                                <button @click.stop="delete this.$store.state.entity[key]" class="w-6 h-6 inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                    <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                                    </svg>
                                                </button>
                                            </div>
                                            <div v-if="targetSelectMode" class="flex-shrink-0 pr-2">
                                                <button @click.stop="selectTarget('entity', null, key)" class="w-6 h-6 inline-flex items-center justify-center bg-transparent focus:outline-none">
                                                    <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 24 24" stroke="none">
                                                        <path fill="currentColor" d="M11,2V4.07C7.38,4.53 4.53,7.38 4.07,11H2V13H4.07C4.53,16.62 7.38,19.47 11,19.93V22H13V19.93C16.62,19.47 19.47,16.62 19.93,13H22V11H19.93C19.47,7.38 16.62,4.53 13,4.07V2M11,6.08V8H13V6.09C15.5,6.5 17.5,8.5 17.92,11H16V13H17.91C17.5,15.5 15.5,17.5 13,17.92V16H11V17.91C8.5,17.5 6.5,15.5 6.08,13H8V11H6.09C6.5,8.5 8.5,6.5 11,6.08M12,11A1,1 0 0,0 11,12A1,1 0 0,0 12,13A1,1 0 0,0 13,12A1,1 0 0,0 12,11Z" />
                                                    </svg>
                                                </button>
                                            </div>
                                            <div v-show="watchSelectMode || attributeSelectMode" class="flex-shrink-0 pr-2">
                                                <button @click.stop="selectWatch('entity',null, key)" class="w-6 h-6 inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" aria-hidden="true" class="w-4 h-4">
                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"></path> <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"></path>
                                                    </svg>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </nav>
                        </div>
                    </aside>
                </div>
                <footer class="absolute bottom-0 left-0 right-0 text-xs">
                    <div v-if="this.showEditor" class="fixed z-50 inset-0 overflow-hidden shadow-xl">
                        <div class="absolute inset-0 overflow-hidden">
                            <section class="absolute right-0 max-w-full flex" :class="this.$store.state.editor.editorPosition == 'bottom' ? 'bottom-0 left-0' : 'pl-10 sm:pl-16 inset-y-0'" aria-labelledby="slide-over-heading">
                                <transition
                                    enter-active-class="transform transition ease-in-out duration-500 sm:duration-700"
                                    enter-class="translate-x-full"
                                    enter-to-class="translate-x-0"
                                    leave-active-class="transform transition ease-in-out duration-500 sm:duration-700"
                                    leave-class="otranslate-x-0"
                                    leave-to-class="translate-x-full">
                                    <div :class="this.$store.state.editor.editorPosition == 'bottom' ? 'w-screen' : 'w-64'" :style="this.$store.state.editor.editorPosition == 'bottom' ? 'height: 50vh' : ''">
                                        <div class="h-full flex flex-col bg-gray-700 text-gray-300 border-t border-l border-black">
                                            <div class="flex items-center justify-between bg-gray-900">
                                                <h2 id="slide-over-heading" class="pl-2  uppercase font-medium">
                                                    @{{ this.menu ? this.menu : 'Edit Element (Ctrl + e)' }}
                                                </h2>
                                                <div class="ml-3 h-7 flex items-center mr-2">
                                                    <button v-show="this.$store.state.editor.editorPosition == 'bottom'" @click="this.$store.state.editor.editorPosition = 'right'" class="mr-3 hover:text-white focus:outline-none">
                                                        <span class="sr-only">Editor align bottom</span>
                                                        <svg class="h-4 w-4" viewBox="0 0 24 24" fill="none" stroke="currentColor" aria-hidden="true">
                                                            <path fill="currentColor" d="M6,2H18A2,2 0 0,1 20,4V20A2,2 0 0,1 18,22H6A2,2 0 0,1 4,20V4A2,2 0 0,1 6,2M14,8V16H18V8H14Z" />
                                                        </svg>
                                                    </button>
                                                    <button v-show="this.$store.state.editor.editorPosition == 'right'" @click="this.$store.state.editor.editorPosition = 'bottom'" class="mr-3 rounded-md hover:text-white focus:outline-none">
                                                        <span class="sr-only">Editor align top</span>
                                                        <svg class="h-4 w-4" viewBox="0 0 24 24" fill="none" stroke="currentColor" aria-hidden="true">
                                                            <path fill="currentColor" d="M6,2H18A2,2 0 0,1 20,4V20A2,2 0 0,1 18,22H6A2,2 0 0,1 4,20V4A2,2 0 0,1 6,2M6,16V20H18V16H6Z" />
                                                        </svg>
                                                    </button>
                                                    <button @click="this.showEditor = false" class="hover:text-white focus:outline-none">
                                                        <span class="sr-only">Close panel</span>
                                                        <svg class="h-4 w-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                                                        </svg>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="overflow-y-scroll h-full">
                                                <div v-if="menu == 'Templates'" class="p-2">
                                                    <div class="flex flex-wrap">
                                                        <div class="flex-grow">
                                                            <label class="leading-6 font-medium">Choose a template category</label>
                                                            <select @change="fetchTemplates" v-model="template.type" class="block text-xs bg-gray-800 h-8">
                                                                <option disabled value="">Please select one</option>
                                                                <option value="transitions">Transitions</option>
                                                                <option value="grids">Grids</option>
                                                                <option value="hero">Hero Sections</option>
                                                                <option value="feature">Feature Sections</option>
                                                                <option value="cta">CTA Sections</option>
                                                                <option value="blog">Blog Sections</option>
                                                                <option value="pricing">Pricing Sections</option>
                                                                <option value="header">Header Sections</option>
                                                                <option value="faqs">FAQs</option>
                                                                <option value="newsletter">Newsletter Sections</option>
                                                                <option value="stats">Stats</option>
                                                                <option value="rating">Ratings</option>
                                                                <option value="testimonials">Testimonials</option>
                                                                <option value="card">Card Sections</option>
                                                                <option value="content">Content Sections</option>
                                                                <option value="contact">Contact Sections</option>
                                                                <option value="footer">Footers</option>
                                                                <option value="clouds">Logo Clouds</option>
                                                                <option value="headers">Headers</option>
                                                                <option value="banners">Banners</option>
                                                                <option value="flyout">Flyout Menus</option>
                                                                <option value="inputs">Input Groups</option>
                                                                <option value="select">Select Menus</option>
                                                                <option value="signin">Sign-in and Registration</option>
                                                                <option value="radio">Radio Groups</option>
                                                                <option value="toggles">Toggles</option>
                                                                <option value="panels">Panels</option>
                                                                <option value="alerts">Alerts</option>
                                                                <option value="navbars">Navbars</option>
                                                                <option value="pagination">Pagination</option>
                                                                <option value="tabs">Tabs</option>
                                                                <option value="verticalnav">Vertical Navigation</option>
                                                                <option value="sidenav">Sidebar Navigation</option>
                                                                <option value="breadcrumbs">Breadcrumbs</option>
                                                                <option value="steps">Steps</option>
                                                                <option value="modals">Modals</option>
                                                                <option value="slideovers">Slide-overs</option>
                                                                <option value="notfications">Notifications</option>
                                                                <option value="avatars">Avatars</option>
                                                                <option value="dropdowns">Dropdowns</option>
                                                                <option value="badges">Badges</option>
                                                                <option value="buttons">Buttons</option>
                                                                <option value="buttongroups">Button Groups</option>
                                                                <option value="containers">Containers</option>
                                                                <option value="listcontainers">List Containers</option>
                                                                <option value="media">Media Objects</option>
                                                                <option value="dividers">Dividers</option>
                                                            </select>
                                                            <div class="flex flex-wrap mt-3">
                                                                <a v-for="(template, index) in templates" :key="index" class="flex-initial p-2" style="width:225px" href="#" @click="loadTemplate(template.slug)">
                                                                    <figure v-if="template.image">
                                                                        <div v-if="template.image" class="relative transition transform duration-150 ease-in-out">
                                                                            <img class="w-full h-auto" :src="template.image" alt="">
                                                                            <div class="absolute inset-0 flex items-center justify-center text-center opacity-15 border"></div>
                                                                            <div class="absolute inset-0 bg-white opacity-0 group-hover:opacity-30 transition ease-in-out duration-150"></div>
                                                                        </div>
                                                                        <figcaption class="mt-3">
                                                                            <p class="flex items-baseline text-sm font-medium">
                                                                                <span>@{{ template.name }}</span>
                                                                            </p>
                                                                        </figcaption>
                                                                    </figure>
                                                                    <div v-else class="flex flex-col border border-black p-6 text-center h-full">
                                                                        <dt class="order-2 mt-2 text-sm leading-6 font-medium">
                                                                            @{{ template.name }}
                                                                        </dt>
                                                                        <dd class="order-1 text-md font-mono">
                                                                            No Preview
                                                                        </dd>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div v-if="menu == 'Import'" class="p-2">
                                                    <div class="flex flex-wrap">
                                                        <div class="flex-shink">
                                                            <div class="my-2 mr-6">
                                                                <div class="inline-flex items-center rounded-full p-1 pr-2">
                                                                    <span class="text-lg">HTML Import</span>
                                                                </div>
                                                                <p class="my-2">Copy and paste your HTML into the box below.</p>
                                                                <form v-if="this.$store.state.user.slug" action="/uploadHtml" method="POST">
                                                                    @csrf
                                                                    <input type="hidden" name="currentSelection" v-model="currentSelection">
                                                                    <input type="hidden" name="section" v-model="section">
                                                                    <input type="hidden" name="body" v-model="this.$store.state.page.slug">
                                                                    <textarea v-model="submittedHtml" name="html" class="bg-gray-800 block" cols="24" rows="8" maxLength="10000"></textarea>
                                                                    <button type="submit" class="mt-2 inline-flex items-center px-2 py-2 border border-transparent text-sm font-mono border border-black focus:outline-none focus:ring-2 focus:ring-offset-2">Save</button>
                                                                </form>
                                                                <div v-else>
                                                                    <textarea v-model="submittedHtml" class="bg-gray-800 block" cols="24" rows="8" maxLength="10000"></textarea>
                                                                    <button @click="htmlTree()" class="mt-2 inline-flex items-center px-2 py-2 border border-transparent text-sm font-mono border border-black focus:outline-none focus:ring-2 focus:ring-offset-2">Insert</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div v-show="submittedHtml" class="flex-grow">
                                                            <label for="submittedHtml" class="block font-medium">Preview</label>
                                                            <div id="submittedHtml" class="bg-white" v-html="submittedHtml"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div v-if="menu == 'Gradient'" class="p-2">
                                                    <div class="mt-5">
                                                        <label class="leading-6 font-medium">Select linear gradient direction</label>
                                                        <div class="block items-center">
                                                            <ul class="flex flex-wrap space-x-1">
                                                                <li v-for="(item, index) in attributes.directions" :key="index" class="mt-2">
                                                                    <label :for="prefix + statePrefix + 'bg-gradient-to-' + item" class="block rounded-full cursor-pointer w-8 h-8 from-yellow-400 via-red-500 to-pink-500 hover:border-red-300" :class="[{'border-red-300': this.$store.state.content[this.$store.state.editor.currentSelection].classes && this.$store.state.content[this.$store.state.editor.currentSelection].classes.includes(prefix + statePrefix + 'bg-gradient-to-' + item)}, 'bg-gradient-to-' + item]"></label>
                                                                </li>
                                                            </ul>
                                                            <input v-for="(item, index) in attributes.directions" :key="index" type="checkbox" v-model="this.$store.state.content[this.$store.state.editor.currentSelection].classes" class="hidden" name="color-chooser" :id="prefix + statePrefix + 'bg-gradient-to-' + item" :value="prefix + statePrefix + 'bg-gradient-to-' + item">
                                                        </div>
                                                    </div>
                                                    <div class="mt-5">
                                                        <label class="leading-6 font-medium">Choose gradient starting colour</label>
                                                        <div class="block items-center">
                                                            <ul class="flex flex-wrap space-x-1">
                                                                <li v-for="(item, index) in attributes.colours" :key="index" class="mt-2">
                                                                    <label :for="prefix + statePrefix + 'from-' + item" class="bg-gradient-to-r border border-black rounded-full block w-8 h-8 hover:border-red-300" :class="[{checked: this.$store.state.content[this.$store.state.editor.currentSelection].classes && this.$store.state.content[this.$store.state.editor.currentSelection].classes.includes(prefix + statePrefix + 'from-' + item)}, 'from-' + item]"></label>
                                                                </li>
                                                            </ul>
                                                            <input v-for="(item, index) in attributes.colours" :key="index" type="checkbox" v-model="this.$store.state.content[this.$store.state.editor.currentSelection].classes" class="hidden" name="color-chooser" :id="prefix + statePrefix + 'from-' + item" :value="prefix + statePrefix + 'from-' + item">
                                                        </div>
                                                    </div>
                                                    <div class="mt-5">
                                                        <label class="leading-6 font-medium">Choose gradient link colour (optional)</label>
                                                        <div class="block items-center">
                                                            <ul class="flex flex-wrap space-x-1">
                                                                <li v-for="(item, index) in attributes.colours" :key="index" class="mt-2">
                                                                    <label :for="prefix + statePrefix + 'via-' + item" class="rounded-full border border-black bg-gradient-to-r from-gray-700 to-gray-700 block w-8 h-8 hover:border-red-300" :class="[{checked: this.$store.state.content[this.$store.state.editor.currentSelection].classes && this.$store.state.content[this.$store.state.editor.currentSelection].classes.includes(prefix + statePrefix + 'via-' + item)}, 'via-' + item]"></label>
                                                                </li>
                                                            </ul>
                                                            <input v-for="(item, index) in attributes.colours" :key="index" type="checkbox" v-model="this.$store.state.content[this.$store.state.editor.currentSelection].classes" class="hidden" name="color-chooser" :id="prefix + statePrefix + 'via-' + item" :value="prefix + statePrefix + 'via-' + item">
                                                        </div>
                                                    </div>
                                                    <div class="mt-5">
                                                        <label class="leading-6 font-medium">Choose gradient end colour</label>
                                                        <div class="block items-center">
                                                            <ul class="flex flex-wrap space-x-1">
                                                                <li v-for="(item, index) in attributes.colours" :key="index" class="mt-2">
                                                                    <label :for="prefix + statePrefix + 'to-' + item" class="rounded-full border border-black bg-gradient-to-r from-gray-700 block w-8 h-8 hover:border-gray-300" :class="[{checked: this.$store.state.content[this.$store.state.editor.currentSelection].classes && this.$store.state.content[this.$store.state.editor.currentSelection].classes.includes(settings + 'to-' + item)}, 'to-' + item]"></label>
                                                                </li>
                                                            </ul>
                                                            <input v-for="(item, index) in attributes.colours" :key="index" type="checkbox" v-model="this.$store.state.content[this.$store.state.editor.currentSelection].classes" class="hidden" name="color-chooser" :id="prefix + statePrefix + 'to-' + item" :value="prefix + statePrefix + 'to-' + item">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div v-if="menu == 'Opacity'" class="p-2">
                                                    <div class="mt-5">
                                                        <label class="leading-6 font-medium">Choose Opacity</label>
                                                        <div class="block items-center">
                                                            <ul class="flex flex-wrap space-x-1">
                                                                <li v-for="(item, index) in attributes.opacity" :key="index" class="mt-2">
                                                                    <label :for="prefix + statePrefix + 'opacity-' + item" class="block rounded-full cursor-pointer w-8 h-8 bg-blue-500 hover:border-red-300" :class="[{'border-red-300': this.$store.state.content[this.$store.state.editor.currentSelection].classes && this.$store.state.content[this.$store.state.editor.currentSelection].classes.includes(prefix + statePrefix + 'opacity-' + item)}, 'opacity-' + item]"></label>
                                                                </li>
                                                            </ul>
                                                            <input v-for="(item, index) in attributes.opacity" :key="index" type="checkbox" v-model="this.$store.state.content[this.$store.state.editor.currentSelection].classes" class="hidden" name="color-chooser" :id="prefix + statePrefix + 'opacity-' + item" :value="prefix + statePrefix + 'opacity-' + item">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div v-if="menu == 'Typography'" class="p-2">
                                                    <div class="mt-5">
                                                        <label class="leading-6 font-medium">Line Height</label>
                                                        <div class="block items-center">
                                                            <ul class="flex flex-wrap space-x-1">
                                                                <li v-for="(item, index) in attributes.lineHeight" :key="index" class="mt-2">
                                                                    <label :for="prefix + statePrefix + 'leading-' + item" class="block cursor-pointer" :class="[{'bg-gray-900': this.$store.state.content[this.$store.state.editor.currentSelection].classes && this.$store.state.content[this.$store.state.editor.currentSelection].classes.includes(prefix + statePrefix + 'leading-' + item)}, 'leading-' + item]">Some text...</label>
                                                                </li>
                                                            </ul>
                                                            <input v-for="(item, index) in attributes.lineHeight" :key="index" type="checkbox" v-model="this.$store.state.content[this.$store.state.editor.currentSelection].classes" class="hidden" name="color-chooser" :id="prefix + statePrefix + 'leading-' + item" :value="prefix + statePrefix + 'leading-' + item">
                                                        </div>
                                                    </div>
                                                    <div class="mt-2">
                                                        <label class="leading-6 font-medium">Letter Spacing</label>
                                                        <div class="block items-center">
                                                            <ul class="space-x-1">
                                                                <li v-for="(item, index) in attributes.tracking" :key="index" class="mt-2">
                                                                    <label :for="prefix + statePrefix + 'tracking-' + item" class="block cursor-pointer" :class="[{'bg-gray-900': this.$store.state.content[this.$store.state.editor.currentSelection].classes && this.$store.state.content[this.$store.state.editor.currentSelection].classes.includes(prefix + statePrefix + 'tracking-' + item)}, 'tracking-' + item]">The quick brown fox jumps over the lazy dog.</label>
                                                                </li>
                                                            </ul>
                                                            <input v-for="(item, index) in attributes.tracking" :key="index" type="checkbox" v-model="this.$store.state.content[this.$store.state.editor.currentSelection].classes" class="hidden" name="color-chooser" :id="prefix + statePrefix + 'tracking-' + item" :value="prefix + statePrefix + 'tracking-' + item">
                                                        </div>
                                                    </div>
                                                    <div class="mt-2">
                                                        <label class="leading-6 font-medium">Text Overflow</label>
                                                        <div class="block items-center">
                                                            <ul class="flex flex-wrap space-x-1">
                                                                <li v-for="(item, index) in attributes.textOverflow" :key="index" class="mt-2">
                                                                    <label :for="prefix + statePrefix + item.class" class="block cursor-pointer p-2" :class="[{'bg-gray-900': this.$store.state.content[this.$store.state.editor.currentSelection].classes && this.$store.state.content[this.$store.state.editor.currentSelection].classes.includes(prefix + statePrefix + item.class)}]">@{{ item.name }}</label>
                                                                </li>
                                                            </ul>
                                                            <input v-for="(item, index) in attributes.textOverflow" :key="index" type="checkbox" v-model="this.$store.state.content[this.$store.state.editor.currentSelection].classes" class="hidden" name="color-chooser" :id="prefix + statePrefix + item.class" :value="prefix + statePrefix + item.class">
                                                        </div>
                                                    </div>
                                                    <div class="mt-2">
                                                        <label class="leading-6 font-medium">List Style Type</label>
                                                        <div class="block items-center">
                                                            <ul class="flex flex-wrap space-x-1">
                                                                <li v-for="(item, index) in attributes.listStyle" :key="index" class="mt-2">
                                                                    <label :for="prefix + statePrefix + 'list-' + item.class" class="block cursor-pointer p-2" :class="[{'bg-gray-900': this.$store.state.content[this.$store.state.editor.currentSelection].classes && this.$store.state.content[this.$store.state.editor.currentSelection].classes.includes(prefix + statePrefix + 'list-' + item.class)}]">@{{ item.name }}</label>
                                                                </li>
                                                            </ul>
                                                            <input v-for="(item, index) in attributes.listStyle" :key="index" type="checkbox" v-model="this.$store.state.content[this.$store.state.editor.currentSelection].classes" class="hidden" name="color-chooser" :id="prefix + statePrefix + 'list-' + item.class" :value="prefix + statePrefix + 'list-' + item.class">
                                                        </div>
                                                    </div>
                                                    <div class="mt-2">
                                                        <label class="leading-6 font-medium">List Style Position</label>
                                                        <div class="block items-center">
                                                            <ul class="flex flex-wrap space-x-1">
                                                                <li v-for="(item, index) in attributes.listStylePosition" :key="index" class="mt-2">
                                                                    <label :for="prefix + statePrefix + 'list-' + item.class" class="block cursor-pointer p-2" :class="[{'bg-gray-900': this.$store.state.content[this.$store.state.editor.currentSelection].classes && this.$store.state.content[this.$store.state.editor.currentSelection].classes.includes(prefix + statePrefix + 'list-' + item.class)}]">@{{ item.name }}</label>
                                                                </li>
                                                            </ul>
                                                            <input v-for="(item, index) in attributes.listStylePosition" :key="index" type="checkbox" v-model="this.$store.state.content[this.$store.state.editor.currentSelection].classes" class="hidden" name="color-chooser" :id="prefix + statePrefix + 'list-' + item.class" :value="prefix + statePrefix + 'list-' + item.class">
                                                        </div>
                                                    </div>
                                                    <div class="mt-2">
                                                        <label class="leading-6 font-medium">Numeric Variant</label>
                                                        <div class="block items-center">
                                                            <ul class="flex flex-wrap space-x-1">
                                                                <li v-for="(item, index) in attributes.numericVariant" :key="index" class="mt-2">
                                                                    <label :for="prefix + statePrefix + item.class" class="block cursor-pointer p-2" :class="[{'bg-gray-900': this.$store.state.content[this.$store.state.editor.currentSelection].classes && this.$store.state.content[this.$store.state.editor.currentSelection].classes.includes(prefix + statePrefix + item.class)}]">@{{ item.name }}</label>
                                                                </li>
                                                            </ul>
                                                            <input v-for="(item, index) in attributes.numericVariant" :key="index" type="checkbox" v-model="this.$store.state.content[this.$store.state.editor.currentSelection].classes" class="hidden" name="color-chooser" :id="prefix + statePrefix + item.class" :value="prefix + statePrefix + item.class">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div v-if="menu == 'Fonts'" class="p-2">
                                                    <label for="source" class="leading-6 font-medium">Select font</label>
                                                    <select @change="fontSelection" class="block bg-gray-800 h-8 text-xs">
                                                        <option value="">No font</option>
                                                        <option v-for="font in attributes.families" :value="font.name">
                                                            @{{ font.name }}
                                                        </option>
                                                    </select>
                                                    <button v-if="fonts.length == 0" type="button" @click="loadFonts" class="mt-2">
                                                        Load all fonts?
                                                    </button>
                                                    <div v-if="fonts && fonts.length" class="mt-5">
                                                        <label for="source" class="leading-6 font-medium">Page fonts</label>
                                                        <ul class="mt-2 w-56 flex flex-wrap w-full">
                                                            <li v-for="(item, index) in fonts" :key="index" class="flex-1">
                                                                <label :for="'font-effect-' + item" :class="[{checked: this.$store.state.content[this.$store.state.editor.currentSelection].classes && this.$store.state.content[this.$store.state.editor.currentSelection].classes.includes(prefix + statePrefix + 'font-effect-' + item)}, 'font-effect-' + item]" class="inline-block w-56">@{{ item }}</label>
                                                            </li>
                                                        </ul>
                                                        <input v-for="(item, index) in classes.effects" :key="index" type="checkbox" v-model="this.$store.state.content[this.$store.state.editor.currentSelection].classes" class="hidden" name="color-chooser" :id="'font-effect-' + item" :value="'font-effect-' + item">
                                                    </div>
                                                    <div class="mt-5">
                                                        <label for="source" class="leading-6 font-medium">Add effect</label>
                                                        <ul class="mt-2 w-56">
                                                            <li v-for="(item, index) in classes.effects" :key="index">
                                                                <label :for="'font-effect-' + item" class="text-lg mt-2"  :class="[{'bg-gray-800': this.$store.state.content[this.$store.state.editor.currentSelection].classes && this.$store.state.content[this.$store.state.editor.currentSelection].classes.includes(prefix + statePrefix + 'font-effect-' + item)}, 'font-effect-' + item]" class="inline-block w-56">@{{ item }}</label>
                                                            </li>
                                                        </ul>
                                                        <input v-for="(item, index) in classes.effects" :key="index" type="checkbox" v-model="this.$store.state.content[this.$store.state.editor.currentSelection].classes" class="hidden" name="color-chooser" :id="'font-effect-' + item" :value="'font-effect-' + item">
                                                    </div>
                                                </div>
                                                <div v-if="menu == 'Page'">
                                                    <div class="block">
                                                        <nav class="flex items-center divide-x divide-gray-800 border-b border-gray-800" aria-label="Tabs">
                                                            <button @click="this.pageSelectionMenu = 'seo'" :class="this.pageSelectionMenu == 'seo' ? 'bg-gray-800' : 'text-gray-300 hover:text-white hover:bg-gray-600'" class="flex flex-1 items-center justify-center h-10 font-medium text-sm focus:outline-none">
                                                                <svg class="h-4 w-4" viewBox="0 0 24 24">
                                                                <path fill="currentColor" d="M12,15.5A3.5,3.5 0 0,1 8.5,12A3.5,3.5 0 0,1 12,8.5A3.5,3.5 0 0,1 15.5,12A3.5,3.5 0 0,1 12,15.5M19.43,12.97C19.47,12.65 19.5,12.33 19.5,12C19.5,11.67 19.47,11.34 19.43,11L21.54,9.37C21.73,9.22 21.78,8.95 21.66,8.73L19.66,5.27C19.54,5.05 19.27,4.96 19.05,5.05L16.56,6.05C16.04,5.66 15.5,5.32 14.87,5.07L14.5,2.42C14.46,2.18 14.25,2 14,2H10C9.75,2 9.54,2.18 9.5,2.42L9.13,5.07C8.5,5.32 7.96,5.66 7.44,6.05L4.95,5.05C4.73,4.96 4.46,5.05 4.34,5.27L2.34,8.73C2.21,8.95 2.27,9.22 2.46,9.37L4.57,11C4.53,11.34 4.5,11.67 4.5,12C4.5,12.33 4.53,12.65 4.57,12.97L2.46,14.63C2.27,14.78 2.21,15.05 2.34,15.27L4.34,18.73C4.46,18.95 4.73,19.03 4.95,18.95L7.44,17.94C7.96,18.34 8.5,18.68 9.13,18.93L9.5,21.58C9.54,21.82 9.75,22 10,22H14C14.25,22 14.46,21.82 14.5,21.58L14.87,18.93C15.5,18.67 16.04,18.34 16.56,17.94L19.05,18.95C19.27,19.03 19.54,18.95 19.66,18.73L21.66,15.27C21.78,15.05 21.73,14.78 21.54,14.63L19.43,12.97Z" />
                                                                </svg>
                                                            </button>
                                                            <button @click="this.pageSelectionMenu = 'datasources'" :class="this.pageSelectionMenu == 'datasources' ? 'bg-gray-800' : 'text-gray-300 hover:text-white hover:bg-gray-600'" class="flex flex-1 items-center justify-center h-10 font-medium text-sm focus:outline-none">
                                                                <svg class="h-4 w-4" viewBox="0 0 24 24">
                                                                    <path fill="currentColor" d="M12,3C7.58,3 4,4.79 4,7C4,9.21 7.58,11 12,11C16.42,11 20,9.21 20,7C20,4.79 16.42,3 12,3M4,9V12C4,14.21 7.58,16 12,16C16.42,16 20,14.21 20,12V9C20,11.21 16.42,13 12,13C7.58,13 4,11.21 4,9M4,14V17C4,19.21 7.58,21 12,21C16.42,21 20,19.21 20,17V14C20,16.21 16.42,18 12,18C7.58,18 4,16.21 4,14Z" />
                                                                </svg>
                                                            </button>
                                                            <button @click="this.pageSelectionMenu = 'authorisation'" :class="this.pageSelectionMenu == 'authorisation' ? 'bg-gray-800' : 'text-gray-300 hover:text-white hover:bg-gray-600'" class="flex flex-1 items-center justify-center h-10 font-medium text-sm focus:outline-none">
                                                                <svg class="h-4 w-4" viewBox="0 0 24 24">
                                                                    <path fill="currentColor" d="M12,8A1,1 0 0,1 13,9A1,1 0 0,1 12,10A1,1 0 0,1 11,9A1,1 0 0,1 12,8M21,11C21,16.55 17.16,21.74 12,23C6.84,21.74 3,16.55 3,11V5L12,1L21,5V11M12,6A3,3 0 0,0 9,9C9,10.31 9.83,11.42 11,11.83V18H13V16H15V14H13V11.83C14.17,11.42 15,10.31 15,9A3,3 0 0,0 12,6Z" />
                                                                </svg>
                                                            </button>
                                                            <button @click="this.pageSelectionMenu = 'events'" :class="this.pageSelectionMenu == 'variables' ? 'bg-gray-800' : 'text-gray-300 hover:text-white hover:bg-gray-600'" class="flex flex-1 items-center justify-center h-10 font-medium text-sm focus:outline-none">
                                                                <svg class="h-4 w-4" viewBox="0 0 24 24">
                                                                    <path fill="currentColor" d="M11.5,20L16.36,10.27H13V4L8,13.73H11.5V20M12,2C14.75,2 17.1,3 19.05,4.95C21,6.9 22,9.25 22,12C22,14.75 21,17.1 19.05,19.05C17.1,21 14.75,22 12,22C9.25,22 6.9,21 4.95,19.05C3,17.1 2,14.75 2,12C2,9.25 3,6.9 4.95,4.95C6.9,3 9.25,2 12,2Z" />
                                                                </svg>
                                                            </button>
                                                            <button @click="this.pageSelectionMenu = 'inspector'" :class="this.pageSelectionMenu == 'inspector' ? 'bg-gray-800' : 'text-gray-300 hover:text-white hover:bg-gray-600'" class="flex flex-1 items-center justify-center h-10 font-medium text-sm focus:outline-none">
                                                                <svg class="h-4 w-4" viewBox="0 0 24 24">
                                                                    <path fill="currentColor" d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" />
                                                                </svg>
                                                            </button>
                                                        </nav>
                                                    </div>
                                                    <div class="p-2">
                                                        <div v-show="pageSelectionMenu == 'seo'">
                                                            <div class="flex flex-wrap">
                                                                <div class="flex-grow">
                                                                    <h3 class="text-base font-medium mt-3">Page Settings</h3>
                                                                    <div class="mt-2">
                                                                        <label for="path" class="block font-medium">Name</label>
                                                                        <input @input="saveBody" v-model="this.$store.state.page.name" class="block bg-gray-800 h-8" placeholder="Untitled webpage" type="text" autocomplete="off">
                                                                    </div>
                                                                    <div class="mt-2">
                                                                        <label for="path" class="block font-medium">Path</label>
                                                                        <input @input="saveBody" type="text" v-model="this.$store.state.page.path" name="path" id="path" autocomplete="off" class="block bg-gray-800 h-8">
                                                                    </div>
                                                                    <div class="mt-2">
                                                                        <label class="block font-medium">Render page on server</label>
                                                                        <input type="checkbox" class="block focus:outline-none focus:border-none focus:shadow-none h-4 w-4 text-blue-600 border-gray-300 rounded" disabled>
                                                                    </div>
                                                                </div>
                                                                <div class="flex-grow">
                                                                    <h3 class="text-base font-medium mt-3">Includes</h3>
                                                                    <div class="mt-2">
                                                                        <div>
                                                                            <label class="mt-2 block font-medium">External Scripts</label>
                                                                            <input type="text" v-model="script" name="script" placeholder="e.g. https://js.stripe.com/v3/" class="block bg-gray-800 h-8">
                                                                            <button @click="addScript">Add</button>
                                                                        </div>
                                                                        <div class="mt-2" v-for="(script, index) in this.$store.state.page.scripts">
                                                                            <span class="block">@{{ script.src }}</span><label>Async<input type="checkbox" v-model="this.$store.state.page.scripts[index].async" class="ml-2 inline-block focus:outline-none focus:border-none focus:shadow-none h-4 w-4 text-blue-600 border-gray-300 rounded"></label><label class="ml-2">Defer<input type="checkbox" v-model="this.$store.state.page.scripts[index].defer" class="ml-2 inline-block focus:outline-none focus:border-none focus:shadow-none h-4 w-4 text-blue-600 border-gray-300 rounded"></label><a class="ml-2 hover:underline" href="#" @click="removeScript(index)">Remove<svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" /></svg></a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mt-2">
                                                                        <div>
                                                                            <label class="mt-2 block font-medium">External Links</label>
                                                                            <input type="text" v-model="link" name="link" placeholder="e.g. https://yourwebsite.com/style.css" class="block bg-gray-800 h-8">
                                                                            <button @click="addLink">Add</button>
                                                                        </div>
                                                                        <div class="mt-2" v-for="(link, index) in this.$store.state.page.links">
                                                                            <span>@{{ link.href }}</span><a class="ml-2 hover:underline" href="#" @click="removeLink(index)">Remove</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="flex-grow">
                                                                    <h3 class="text-base font-medium mt-3">SEO</h3>
                                                                    <div class="mt-2">
                                                                        <label for="meta_title" class="block font-medium">Meta Title</label>
                                                                        <input type="text" v-model="this.$store.state.page.meta_title" name="meta_title" id="meta_title" autocomplete="off" class="block bg-gray-800 h-8">
                                                                    </div>
                                                                    <div class="mt-2">
                                                                        <label for="meta_description" class="block font-medium">Meta Description</label>
                                                                        <textarea v-model="this.$store.state.page.meta_description" id="meta_description" name="meta_description" rows="3" class="block bg-gray-800 h-8"></textarea>
                                                                    </div>
                                                                    <p class="mt-2 text-xs">Search Engine Preview:</p>
                                                                    <div class="bg-white text-black mt-2 p-2">
                                                                        <p v-if="this.$store.state.page.path" class="inline-block text-sm">@{{ (this.$store.state.settings.domain ? this.$store.state.settings.domain : '') + (this.$store.state.page.path != '/' ? '/' : '') + this.$store.state.page.path}}</p>
                                                                        <h1 v-if="this.$store.state.page.meta_title" class="text-blue-600 text-lg">@{{ this.$store.state.page.meta_title }}</h1>
                                                                        <p v-if="this.$store.state.page.meta_description" class="text-sm">@{{ this.$store.state.page.meta_description }}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div v-show="pageSelectionMenu == 'datasources'">
                                                            <div class="flex justify-between flex-wrap mt-2">
                                                                <div>
                                                                    <svg class="h-8 w-8 text-gray-300" viewBox="0 0 24 24">
                                                                        <path fill="currentColor" d="M4,5C2.89,5 2,5.89 2,7V17C2,18.11 2.89,19 4,19H20C21.11,19 22,18.11 22,17V7C22,5.89 21.11,5 20,5H4M4.5,7A1,1 0 0,1 5.5,8A1,1 0 0,1 4.5,9A1,1 0 0,1 3.5,8A1,1 0 0,1 4.5,7M7,7H20V17H7V7M8,8V16H11V8H8M12,8V16H15V8H12M16,8V16H19V8H16M9,9H10V10H9V9M13,9H14V10H13V9M17,9H18V10H17V9Z" />
                                                                    </svg>
                                                                    <div>
                                                                        <span>Fetch and display data using our CMS or</span>
                                                                        <span class="flex items-center">
                                                                            <a href="/cms" target="_blank" class="mr-1">manage datasources</a>
                                                                            <svg class="h-3 w-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" aria-hidden="true">
                                                                                <path fill="currentColor" d="M14,3V5H17.59L7.76,14.83L9.17,16.24L19,6.41V10H21V3M19,19H5V5H12V3H5C3.89,3 3,3.9 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V12H19V19Z" />
                                                                            </svg>
                                                                        </span>
                                                                    </div>
                                                                    <div class="mt-2">
                                                                        <label for="source" class="my-4 leading-6 font-medium">Page type</label>
                                                                        <select @change="saveBody" id="entity" v-model="this.$store.state.page.entityType" class="block bg-gray-800 h-8 text-xs">
                                                                            <option value="">Default</option>
                                                                            <option value="singleton">Singleton</option>
                                                                            <option value="collection">Collection</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="mt-2">
                                                                        <div class="flex-1 min-w-0 max-w-sm">
                                                                            <label for="search" class="sr-only">Search</label>
                                                                            <div class="relative">
                                                                                <label for="source" class="leading-6 font-medium">Attach Datasources</label>
                                                                                <o-autocomplete
                                                                                    class="my-3 stellify"
                                                                                    :data="searchData"
                                                                                    placeholder="Search datasources"
                                                                                    field="name"
                                                                                    @input="getAsyncData"
                                                                                    @select="selection">
                                                                                </o-autocomplete>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div v-if="this.$store.state.page.sources && this.$store.state.page.sources.length" class="">
                                                                        <label for="source" class="leading-6 font-medium">Attached Sources</label>
                                                                        <ul class="mt-2 space-y-10 md:space-y-0 max-w-sm">
                                                                            <li v-for="source in this.$store.state.page.sources" class="border cursor-pointer mt-3 flex items-center justify-between">
                                                                                <div class="flex-1 flex items-center justify-between border border-black bg-gray-800 truncate">
                                                                                    <div class="flex-1 px-4 py-2 text-xs leading-5 truncate">
                                                                                        <button @click.stop="selectBlock(item, index)" class="font-medium transition ease-in-out duration-150 inline-flex items-center justify-center bg-transparent hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                                                            @{{ this.$store.state.content[source].name }}
                                                                                        </button>
                                                                                    </div>    
                                                                                    <div class="flex-shrink-0 pr-2">
                                                                                        <button @click.stop="removeBodySource(source)" class="w-8 h-8 inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" class="h-4 w-4">                         
                                                                                                <path d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                                                                            </svg>
                                                                                        </button>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div class="pr-2">
                                                                    <svg class="h-8 w-8 text-gray-300" viewBox="0 0 24 24">
                                                                        <path fill="currentColor" d="M7 7H5A2 2 0 0 0 3 9V17H5V13H7V17H9V9A2 2 0 0 0 7 7M7 11H5V9H7M14 7H10V17H12V13H14A2 2 0 0 0 16 11V9A2 2 0 0 0 14 7M14 11H12V9H14M20 9V15H21V17H17V15H18V9H17V7H21V9Z" />
                                                                    </svg>
                                                                    <p>Build requests that fetch data from API endpoints for access on page load.</p>
                                                                    <div v-if="this.$store.state.page.requests" class="mt-2 max-w-sm" v-if="this.$store.state.content[this.currentSelection][event]">
                                                                        <label for="trigger" class="leading-6 font-medium">Attached requests <a @click="this.$store.state.page.requests.push({url: 'New request'});saveBody();" class="font-normal underline">Add new?</a></label>
                                                                        <div v-if="this.$store.state.page.requests && this.$store.state.page.requests.length">
                                                                            <draggable v-model="this.$store.state.page.requests" @end="saveBody" class="mt-2" :component-data="{name:'fade'}" item-key="id">
                                                                                <template #item="{element, index}" class="col-span-1 flex shadow-sm">
                                                                                    <div class="col-span-1 flex shadow-sm">
                                                                                        <div class="flex-1 flex items-center justify-between border border-black bg-gray-800 truncate">
                                                                                            <div class="flex-1">
                                                                                                <div class="flex-1 px-4 py-2 text-xs leading-5 truncate">
                                                                                                    <button @click="request = element;requestIndex = index;selectionMenu = 'requestParameters';" class="font-medium transition ease-in-out duration-150 inline-flex items-center justify-center bg-transparent hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                                                                        @{{ element.url }}
                                                                                                    </button>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="flex-shrink-0 pr-2">
                                                                                                <button @click="this.$store.state.page.requests.splice(index, 1);saveBody();" class="w-8 h-8 inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                                                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" class="w-4 h-4">
                                                                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path>
                                                                                                    </svg>
                                                                                                </button>
                                                                                            </div>
                                                                                            <div class="handle cursor-move flex-shrink-0 pr-2">
                                                                                                <button class="w-8 h-8 cursor-move inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                                                                    <svg class="w-4 h-4" viewBox="0 0 20 20" fill="currentColor"><path d="M7 2a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 2zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 8zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 14zm6-8a2 2 0 1 0-.001-4.001A2 2 0 0 0 13 6zm0 2a2 2 0 1 0 .001 4.001A2 2 0 0 0 13 8zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 13 14z"></path></svg>
                                                                                                </button>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </template>
                                                                            </draggable>
                                                                        </div>
                                                                    </div>
                                                                    <div v-else>
                                                                        <button @click.stop="this.$store.state.page.requests = [];saveBody();" type="submit" class="mt-2 inline-flex items-center px-2 py-2 border border-transparent text-sm font-mono border border-black focus:outline-none focus:ring-2 focus:ring-offset-2">Create request</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div v-show="pageSelectionMenu == 'authorisation'">
                                                            <div class="mt-2">
                                                                <label class="leading-6 font-medium">Users must be logged in to view this page</label>
                                                                <input type="checkbox" v-model="this.$store.state.page.auth" class="block focus:outline-none focus:border-none focus:shadow-none h-4 w-4 text-blue-600 border-gray-300 rounded">
                                                            </div>
                                                            <div class="mt-2">
                                                                <label for="cacheDriver" class="block font-medium">Apply a policy</label>
                                                                <select class="bg-gray-800 h-8 block">
                                                                    <option disabled="disabled" value="">Please select one</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div v-show="pageSelectionMenu == 'events'">
                                                            <div class="flex flex-wrap">
                                                                <div class="flex-grow">
                                                                    <p class="font-medium text-sm">Input Events</p>
                                                                    <p>Attach events and trigger commands.</p>
                                                                    <div class="mt-2">
                                                                        <label for="trigger" class="leading-6 font-medium">Event</label>
                                                                        <select id="trigger" v-model="event" class="text-xs bg-gray-800 h-8 block">
                                                                            <option value="">None</option>
                                                                            <option value="beforeMount">Before Mount</option>
                                                                            <option value="mounted">Mounted</option>
                                                                            <option value="onDestroy">On Destroy</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="mt-5" v-if="event">
                                                                        <label for="trigger" class="leading-6 font-medium">Attach a command</label>
                                                                        <div class="flex items-center justify-between bg-gray-800  border-t border-black">
                                                                            <div class="flex-shrink animate-pulse">
                                                                                <svg class="h-4 text-green-400" viewBox="0 0 24 24">
                                                                                    <path fill="currentColor" d="M8.59,16.58L13.17,12L8.59,7.41L10,6L16,12L10,18L8.59,16.58Z" />
                                                                                </svg>
                                                                            </div>
                                                                            <input @keyup.enter="runCommand" @keydown="consoleKeydown" v-model="currentCommand" style="box-shadow: none !important;" placeholder="Type a command, then hit ⏎, cycle commands using ⇧ ⇩" class="bg-gray-800 text-gray-300 flex-1 font-mono text-xs pl-2 border-none shadow-none focus:outline-none w-full">
                                                                            <div class="flex-shrink">
                                                                                <a href="#" @click.stop="addCommand('page')" class="inline-flex items-center justify-center" style="height: 2.5em; width: 2.5em;">
                                                                                    <span class="sr-only">Add command</span>
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true" class="h-4 w-4 text-gray-300 hover:text-white">
                                                                                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 4v16m8-8H4"></path>
                                                                                    </svg>
                                                                                </a>
                                                                            </div>
                                                                            <div class="flex-shrink">
                                                                                <a v-if="attributeSelectMode" @click="attributeSelectMode = false;" href="#" class="inline-flex items-center justify-center" style="height: 2.5em; width: 2.5em;">
                                                                                    <span class="sr-only">Exit insert attribute node</span>
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true" class="h-4 w-4 text-gray-300 hover:text-white">
                                                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13.875 18.825A10.05 10.05 0 0112 19c-4.478 0-8.268-2.943-9.543-7a9.97 9.97 0 011.563-3.029m5.858.908a3 3 0 114.243 4.243M9.878 9.878l4.242 4.242M9.88 9.88l-3.29-3.29m7.532 7.532l3.29 3.29M3 3l3.59 3.59m0 0A9.953 9.953 0 0112 5c4.478 0 8.268 2.943 9.543 7a10.025 10.025 0 01-4.132 5.411m0 0L21 21" />
                                                                                    </svg>
                                                                                </a>
                                                                                <a v-else @click="attributeSelect" href="#" class="inline-flex items-center justify-center" style="height: 2.5em; width: 2.5em;">
                                                                                    <span class="sr-only">Enter insert attribute mode</span>
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true" class="h-4 w-4 text-gray-300 hover:text-white">
                                                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"></path> <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"></path>
                                                                                    </svg>
                                                                                </a>
                                                                            </div>
                                                                            <div class="flex-shrink">
                                                                                <a href="#" @click.stop="currentCommand = ''" class="inline-flex items-center justify-center" style="height: 2.5em; width: 2.5em;">
                                                                                    <span class="sr-only">Clear console</span>
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true" class="h-4 w-4 text-gray-300 hover:text-white">
                                                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path>
                                                                                    </svg>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mt-5 max-w-sm" v-if="this.$store.state.page[event] && this.$store.state.page[event].length">
                                                                        <label for="trigger" class="leading-6 font-medium">Attached event triggers</label>
                                                                        <draggable v-model="this.$store.state.page[event]" @end="save" class="mt-2" :component-data="{name:'fade'}" item-key="id">
                                                                            <template #item="{element, index}" class="col-span-1 flex shadow-sm">
                                                                                <div class="col-span-1 flex shadow-sm">
                                                                                    <div class="flex-1 flex items-center justify-between border border-black bg-gray-800 truncate">
                                                                                        <div class="flex-1 px-4 py-2 text-xs leading-5 truncate">
                                                                                            <button v-if="typeof this.$store.state.page[element] != 'undefined'"  @click="eventTrigger = element;eventIndex = index;selectionMenu = 'eventArguments';" class="font-medium transition ease-in-out duration-150 inline-flex items-center justify-center bg-transparent hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                                                                @{{ element }}
                                                                                            </button>
                                                                                            <span v-else>
                                                                                                @{{ element }}
                                                                                            </span>
                                                                                        </div>
                                                                                        <div class="flex-shrink-0 pr-2">
                                                                                            <button @click="removeEvent(index, 'page')" class="w-8 h-8 inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" class="w-4 h-4">
                                                                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path>
                                                                                                </svg>
                                                                                            </button>
                                                                                        </div>
                                                                                        <div class="handle cursor-move flex-shrink-0 pr-2">
                                                                                            <button class="w-8 h-8 cursor-move inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                                                                <svg class="w-4 h-4" viewBox="0 0 20 20" fill="currentColor"><path d="M7 2a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 2zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 8zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 14zm6-8a2 2 0 1 0-.001-4.001A2 2 0 0 0 13 6zm0 2a2 2 0 1 0 .001 4.001A2 2 0 0 0 13 8zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 13 14z"></path></svg>
                                                                                            </button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </template>
                                                                        </draggable>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div v-show="pageSelectionMenu == 'inspector'">
                                                            <pre class="scrollbar-none overflow-x-auto p-6 text-sm leading-snug language-html text-white bg-black bg-opacity-75">
                                                                <code style="white-space:pre;">
                                                                    @{{ JSON.stringify(this.$store.state.page, undefined, 4).replace('{', '').replace('}', '') }}
                                                                </code>
                                                            </pre>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div v-if="menu == 'Editor'" class="p-2">
                                                    <div>
                                                        <label class="leading-6 font-medium">Select element on insert</label>
                                                        <input type="checkbox" v-model="this.$store.state.editor.selectOnInsert" class="block focus:outline-none focus:border-none focus:shadow-none h-4 w-4 text-blue-600 border-gray-300 rounded">
                                                    </div>
                                                    <div class="mt-3">
                                                        <label class="leading-6 font-medium">Allow selection using canvas</label>
                                                        <input type="checkbox" v-model="this.$store.state.editor.selectUsingCanvas" class="block focus:outline-none focus:border-none focus:shadow-none h-4 w-4 text-blue-600 border-gray-300 rounded">
                                                    </div>
                                                    <div class="mt-3">
                                                        <label class="leading-6 font-medium">Set canvas colour</label>
                                                        <input type="color" v-model="this.$store.state.editor.editorColor" class="block focus:outline-none focus:border-none focus:shadow-none h-4 w-4 text-blue-600 border-gray-300 rounded">
                                                    </div>
                                                    <div class="mt-3">
                                                        <label class="leading-6 font-medium">Display guidelines</label>
                                                        <input type="checkbox" v-model="this.$store.state.editor.guidelines" class="block focus:outline-none focus:border-none focus:shadow-none h-4 w-4 text-blue-600 border-gray-300 rounded">
                                                    </div>
                                                </div>
                                                <div v-if="menu == 'Commit History'" class="p-2">
                                                    <div class="mt-2">
                                                        <label for="currentBranch" class="block font-medium">Current Branch</label>
                                                        <input class="block text-xs bg-gray-800 h-8" type="text" id="currentBranch" autocomplete="off" value="master" disabled>
                                                    </div>
                                                </div>
                                                <div v-if="menu == 'Key Mappings'" class="p-2">
                                                    <table class="s-prose">
                                                        <tr>
                                                            <td>
                                                                <kbd>ctrl</kbd><kbd>e</kbd>
                                                            </td>
                                                            <td>
                                                                Toggle edit menu display
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <kbd>ctrl</kbd><kbd>n</kbd>
                                                            </td>
                                                            <td>
                                                                Toggle navigator menu display
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <kbd>ctrl</kbd><kbd>w</kbd>
                                                            </td>
                                                            <td>
                                                                Toggle component menu display
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <kbd>ctrl</kbd><kbd>u</kbd>
                                                            </td>
                                                            <td>
                                                                Toggle toolbar menu display
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <kbd>ctrl</kbd><kbd>d</kbd>
                                                            </td>
                                                            <td>
                                                                Toggle classbar display
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <kbd>ctrl</kbd><kbd>o</kbd>
                                                            </td>
                                                            <td>
                                                                Toggle select element on insert
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <kbd>ctrl</kbd><kbd>g</kbd>
                                                            </td>
                                                            <td>
                                                                Toggle guidelines
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <kbd>ctrl</kbd><kbd>i</kbd>
                                                            </td>
                                                            <td>
                                                                Insert element
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <kbd>ctrl</kbd><kbd>r</kbd>
                                                            </td>
                                                            <td>
                                                                Remove selected element
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <kbd>ctrl</kbd><kbd>l</kbd>
                                                            </td>
                                                            <td>
                                                                Select parent element
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <kbd>ctrl</kbd><kbd>l</kbd>
                                                            </td>
                                                            <td>
                                                                Copy element
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <kbd>ctrl</kbd><kbd>l</kbd>
                                                            </td>
                                                            <td>
                                                                Paste element
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <kbd>ctrl</kbd><kbd>x</kbd>
                                                            </td>
                                                            <td>
                                                                Cut element
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <kbd>ctrl</kbd><kbd>q</kbd>
                                                            </td>
                                                            <td>
                                                                Return to top level
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <kbd>ctrl</kbd><kbd>t</kbd>
                                                            </td>
                                                            <td>
                                                                Cycle elements
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <kbd>ctrl</kbd><kbd>y</kbd>
                                                            </td>
                                                            <td>
                                                                Toggle sections
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <kbd>ctrl</kbd><kbd>s</kbd>
                                                            </td>
                                                            <td>
                                                                Select current element
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div v-if="menu == 'Animation'">
                                                    <div class="relative flex flex-wrap items-center bg-gray-900">
                                                        <button @click="newKeyframe()" title="New Animation" type="button" class="cursor-pointer relative inline-flex items-center text-gray-300 hover:text-white px-2 py-2 font-medium hover:bg-gray-800 focus:outline-none">
                                                            <span class="sr-only">New Animation</span> 
                                                            New keyframe
                                                        </button>
                                                        <button v-if="this.$store.state.content[this.$store.state.editor.currentSelection].keyframes" @click="deleteAnimation()" title="Remove Keyframe" type="button" class="cursor-pointer relative inline-flex items-center text-gray-300 hover:text-white px-2 py-2 font-medium hover:bg-gray-800 focus:outline-none">
                                                            <span class="sr-only">Delete Animation</span> 
                                                            Delete Animation
                                                        </button>
                                                        <button v-if="this.$store.state.content[this.$store.state.editor.currentSelection].keyframes" @click="removeKeyframe()" title="Remove Keyframe" type="button" class="cursor-pointer relative inline-flex items-center text-gray-300 hover:text-white px-2 py-2 font-medium hover:bg-gray-800 focus:outline-none">
                                                            <span class="sr-only">Remove selected keyframe</span> 
                                                            Remove selected keyframe
                                                        </button>
                                                        <div v-if="this.$store.state.content[this.$store.state.editor.currentSelection].keyframes">
                                                            <div class="flex">
                                                                <div class="relative flex items-stretch flex-grow focus-within:z-10">
                                                                    <input v-model="currentAttribute" placeholder="Enter attribute name" type="text" name="attribute" id="attribute" class="block bg-gray-800 h-8 text-sm">
                                                                </div>
                                                                <button @click="addAttribute()" type="button" title="Add Attribute" class="relative inline-flex items-center font-medium px-2 py-2 border-t border-r border-b h-8 border-gray-500 hover:bg-gray-800 focus:outline-none">
                                                                    <span class="sr-only">Add  new attribute</span> 
                                                                    Add new attribute
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="block mt-5 relative p-2" v-if="this.$store.state.content[this.$store.state.editor.currentSelection].keyframes">
                                                        <div class="flex flex-wrap">
                                                            <div class="flex-grow">
                                                                <p class="font-medium text-base">Keyframes</p>
                                                                <label class="block text-xs font-medium">Select Keyframe</label>
                                                                <div class="mt-2 flex items-center">
                                                                    <button v-for="(keyframe, index) in this.$store.state.content[this.$store.state.editor.currentSelection].keyframes" :key="index"  @click="selectKeyframe(index)" type="button" :class="currentKeyframe == index ? 'bg-gray-900' : null" class="border-gray-300 text-gray-300 relative inline-flex items-center px-4 py-2 border text-sm font-medium">@{{ index }}</button>
                                                                </div>
                                                                <div class="my-6">
                                                                    <label label="Easing" class="block text-xs font-medium mt-2">Attributes</label>
                                                                    <p v-for="(attribute, index) in this.$store.state.content[this.$store.state.editor.currentSelection].keyframes[currentKeyframe]" :key="index">
                                                                        <label label="Easing" class="block text-xs font-medium mt-2">@{{ index }}: </label>
                                                                        <input type="text" v-model="this.$store.state.content[this.$store.state.editor.currentSelection].keyframes[currentKeyframe][index]" class="inline-block bg-gray-800 h-8" /> 
                                                                        <button @click="removeAttribute(index)" class="inline-flex hover:text-gray-500 focus:outline-none">
                                                                            <span class="sr-only">Remove attribute</span>
                                                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                                                            </svg>
                                                                        </button>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="flex-grow mb-2">
                                                                <p class="font-medium text-base">Quick Attributes</p>
                                                                <button class="block" @click="addAttribute('transform', 'rotate(0)');save();">transform: rotate(0)</button>
                                                                <button class="block" @click="addAttribute('transform', 'rotate(360deg)');save();">transform: rotate(360deg)</button>
                                                                <button class="block" @click="addAttribute('transform', 'scale(0)');save();">transform: scale(0)</button>
                                                                <button class="block" @click="addAttribute('transform', 'scale(1)');save();">transform: scale(1)</button>
                                                                <button class="block" @click="addAttribute('opacity', '0');save();">opacity: 0</button>
                                                                <button class="block" @click="addAttribute('opacity', '1');save();">opacity: 1</button>
                                                                <button class="block" @click="addAttribute('filter', 'blur(0)');save();">filter: blur(0)</button>
                                                                <button class="block" @click="addAttribute('filter', 'blur(100px)');save();">filter: blur(100px)</button>
                                                                <button class="block" @click="addAttribute('backgroundColor', 'green');save();">backgroundColor: green</button>
                                                                <button class="block" @click="addAttribute('backgroundColor', 'red');save();">backgroundColor:red</button>
                                                            </div>
                                                            <div class="flex-grow" v-if="this.$store.state.content[this.$store.state.editor.currentSelection].timing">
                                                                <p class="font-medium text-base">Timing Properties</p>
                                                                <label class="block text-xs font-medium mt-2">Duration</label>
                                                                <input @input="save" v-model.number="this.$store.state.content[this.$store.state.editor.currentSelection].timing.duration" type="number" class="mt-2 block bg-gray-800 h-8 inline-block" /><span>Milliseconds</span>
                                                                <label class="block text-xs font-medium mt-2">Easing</label>
                                                                <select @change="save" v-model="this.$store.state.content[this.$store.state.editor.currentSelection].timing.easing" class="my-2 block bg-gray-800 text-sm h-10 inline-block">
                                                                    <option value="linear">Linear</option>
                                                                    <option value="ease">Ease</option> 
                                                                    <option value="ease-in">Ease-in</option>
                                                                    <option value="ease-in-out">Ease-in-out</option>
                                                                    <option value="ease-out">Ease-out</option>
                                                                    <option value="cubic-bezier(0.42, 0, 0.58, 1)">Cubic-bezier</option> 
                                                                </select>
                                                                <label class="block text-xs font-medium mt-2">Delay</label>
                                                                <input @input="save" v-model.number="this.$store.state.content[this.$store.state.editor.currentSelection].timing.delay" type="number" class="mt-2 block bg-gray-800 h-8 inline-block" /><span>Milliseconds</span>
                                                                <label class="block text-xs font-medium mt-2">Direction</label>
                                                                <select @change="save" v-model="this.$store.state.content[this.$store.state.editor.currentSelection].timing.direction" class="my-2 block bg-gray-800 text-sm h-10 inline-block">
                                                                    <option value="normal">Normal</option>
                                                                    <option value="reverse">Reverse</option> 
                                                                    <option value="alternate">Alternate</option>
                                                                    <option value="alternate-reverse">Reverse</option>
                                                                </select>
                                                                <label class="block text-xs font-medium">Fill</label>
                                                                <select @change="save" v-model="this.$store.state.content[this.$store.state.editor.currentSelection].timing.fill" class="my-2 block bg-gray-800 text-sm h-10 inline-block">
                                                                    <option value="none">None</option>
                                                                    <option value="forwards">Forwards</option> 
                                                                    <option value="backwards">Backwards</option>
                                                                </select>
                                                                <label class="block text-xs font-medium mt-2">Iteration Start</label>
                                                                <input @input="save" v-model.number="this.$store.state.content[this.$store.state.editor.currentSelection].timing.iterationStart" type="number" class="mt-2 block bg-gray-800 h-8 inline-block" /><span>Milliseconds</span>
                                                                <label class="block text-xs font-medium mt-2">Iterations</label>
                                                                <select @change="save" v-model="this.$store.state.content[this.$store.state.editor.currentSelection].timing.iterations" class="my-2 block bg-gray-800 text-sm h-10 inline-block">
                                                                    <option value="Infinity">Infinity</option>
                                                                    <option value="1">1</option> 
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                </select>
                                                                <label class="block text-xs font-medium">Composite</label>
                                                                <select @change="save" v-model="this.$store.state.content[this.$store.state.editor.currentSelection].timing.composite" class="my-2 block bg-gray-800 text-sm h-10 inline-block">
                                                                    <option value="add">Add</option>
                                                                    <option value="accumulate">Accumulate</option>
                                                                    <option value="replace">Replace</option>
                                                                </select>
                                                                <label class="block text-xs font-medium">Iteration Composite</label>
                                                                <select @change="save" v-model="this.$store.state.content[this.$store.state.editor.currentSelection].timing.iterationComposite" class="my-2 block bg-gray-800 text-sm h-10 inline-block">
                                                                    <option value="accumulate">Accumulate</option>
                                                                    <option value="replace">Replace</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div v-if="menu == null">
                                                    <div class="block">
                                                        <nav class="flex items-center divide-x divide-gray-800 border-b border-gray-800" aria-label="Tabs">
                                                            <button @click="this.selectionMenu = 'settings'" :class="this.selectionMenu == 'settings' ? 'bg-gray-800' : 'text-gray-300 hover:text-white hover:bg-gray-600'" class="flex flex-1 items-center justify-center h-10 font-medium text-sm focus:outline-none">
                                                                <svg class="h-4 w-4" viewBox="0 0 24 24">
                                                                    <path fill="currentColor" d="M12,15.5A3.5,3.5 0 0,1 8.5,12A3.5,3.5 0 0,1 12,8.5A3.5,3.5 0 0,1 15.5,12A3.5,3.5 0 0,1 12,15.5M19.43,12.97C19.47,12.65 19.5,12.33 19.5,12C19.5,11.67 19.47,11.34 19.43,11L21.54,9.37C21.73,9.22 21.78,8.95 21.66,8.73L19.66,5.27C19.54,5.05 19.27,4.96 19.05,5.05L16.56,6.05C16.04,5.66 15.5,5.32 14.87,5.07L14.5,2.42C14.46,2.18 14.25,2 14,2H10C9.75,2 9.54,2.18 9.5,2.42L9.13,5.07C8.5,5.32 7.96,5.66 7.44,6.05L4.95,5.05C4.73,4.96 4.46,5.05 4.34,5.27L2.34,8.73C2.21,8.95 2.27,9.22 2.46,9.37L4.57,11C4.53,11.34 4.5,11.67 4.5,12C4.5,12.33 4.53,12.65 4.57,12.97L2.46,14.63C2.27,14.78 2.21,15.05 2.34,15.27L4.34,18.73C4.46,18.95 4.73,19.03 4.95,18.95L7.44,17.94C7.96,18.34 8.5,18.68 9.13,18.93L9.5,21.58C9.54,21.82 9.75,22 10,22H14C14.25,22 14.46,21.82 14.5,21.58L14.87,18.93C15.5,18.67 16.04,18.34 16.56,17.94L19.05,18.95C19.27,19.03 19.54,18.95 19.66,18.73L21.66,15.27C21.78,15.05 21.73,14.78 21.54,14.63L19.43,12.97Z" />
                                                                </svg>
                                                            </button>
                                                            <button @click="this.selectionMenu = 'styles'" :class="this.selectionMenu == 'styles' ? 'bg-gray-800' : 'text-gray-300 hover:text-white hover:bg-gray-600'" class="flex flex-1 items-center justify-center h-10 font-medium text-sm focus:outline-none">
                                                                <svg class="h-4 w-4" viewBox="0 0 24 24">
                                                                    <path fill="currentColor" d="M20.71,4.63L19.37,3.29C19,2.9 18.35,2.9 17.96,3.29L9,12.25L11.75,15L20.71,6.04C21.1,5.65 21.1,5 20.71,4.63M7,14A3,3 0 0,0 4,17C4,18.31 2.84,19 2,19C2.92,20.22 4.5,21 6,21A4,4 0 0,0 10,17A3,3 0 0,0 7,14Z" />
                                                                </svg>
                                                            </button>
                                                            <!-- <button @click="this.selectionMenu = 'template'" :class="this.selectionMenu == 'template' ? 'bg-gray-800' : 'text-gray-300 hover:text-white hover:bg-gray-600'" class="flex flex-1 items-center justify-center h-10 font-medium text-sm focus:outline-none">
                                                                <svg class="h-4 w-4" viewBox="0 0 24 24">
                                                                    <path fill="currentColor" d="M4 5a1 1 0 011-1h14a1 1 0 011 1v2a1 1 0 01-1 1H5a1 1 0 01-1-1V5zM4 13a1 1 0 011-1h6a1 1 0 011 1v6a1 1 0 01-1 1H5a1 1 0 01-1-1v-6zM16 13a1 1 0 011-1h2a1 1 0 011 1v6a1 1 0 01-1 1h-2a1 1 0 01-1-1v-6z" />
                                                                </svg>
                                                            </button> -->
                                                            <button @click="this.selectionMenu = 'conditional'" :class="this.selectionMenu == 'conditional' ? 'bg-gray-800' : 'text-gray-300 hover:text-white hover:bg-gray-600'" class="flex flex-1 items-center justify-center h-10 font-medium text-sm focus:outline-none">
                                                                <svg class="h-4 w-4" viewBox="0 0 24 24">
                                                                    <path fill="currentColor" d="M12 2C11.5 2 11 2.19 10.59 2.59L2.59 10.59C1.8 11.37 1.8 12.63 2.59 13.41L10.59 21.41C11.37 22.2 12.63 22.2 13.41 21.41L21.41 13.41C22.2 12.63 22.2 11.37 21.41 10.59L13.41 2.59C13 2.19 12.5 2 12 2M12 6.95C14.7 7.06 15.87 9.78 14.28 11.81C13.86 12.31 13.19 12.64 12.85 13.07C12.5 13.5 12.5 14 12.5 14.5H11C11 13.65 11 12.94 11.35 12.44C11.68 11.94 12.35 11.64 12.77 11.31C14 10.18 13.68 8.59 12 8.46C11.18 8.46 10.5 9.13 10.5 9.97H9C9 8.3 10.35 6.95 12 6.95M11 15.5H12.5V17H11V15.5Z" />
                                                                </svg>
                                                            </button>
                                                            <button @click="this.selectionMenu = 'data'" :class="this.selectionMenu == 'data' ? 'bg-gray-800' : 'text-gray-300 hover:text-white hover:bg-gray-600'" class="flex flex-1 items-center justify-center h-10 font-medium text-sm focus:outline-none">
                                                                <svg class="h-4 w-4" viewBox="0 0 24 24">
                                                                    <path fill="currentColor" d="M12,3C7.58,3 4,4.79 4,7C4,9.21 7.58,11 12,11C16.42,11 20,9.21 20,7C20,4.79 16.42,3 12,3M4,9V12C4,14.21 7.58,16 12,16C16.42,16 20,14.21 20,12V9C20,11.21 16.42,13 12,13C7.58,13 4,11.21 4,9M4,14V17C4,19.21 7.58,21 12,21C16.42,21 20,19.21 20,17V14C20,16.21 16.42,18 12,18C7.58,18 4,16.21 4,14Z" />
                                                                </svg>
                                                            </button>
                                                            <button v-if="this.currentSelection && this.$store.state.content[this.currentSelection].type == 's-input'" @click="this.selectionMenu = 'events'" :class="this.selectionMenu == 'events' ? 'bg-gray-800' : 'text-gray-300 hover:text-white hover:bg-gray-600'" class="flex flex-1 items-center justify-center h-10 font-medium text-sm focus:outline-none">
                                                                <svg class="h-4 w-4" viewBox="0 0 24 24">
                                                                    <path fill="currentColor" d="M11.5,20L16.36,10.27H13V4L8,13.73H11.5V20M12,2C14.75,2 17.1,3 19.05,4.95C21,6.9 22,9.25 22,12C22,14.75 21,17.1 19.05,19.05C17.1,21 14.75,22 12,22C9.25,22 6.9,21 4.95,19.05C3,17.1 2,14.75 2,12C2,9.25 3,6.9 4.95,4.95C6.9,3 9.25,2 12,2Z" />
                                                                </svg>
                                                            </button>
                                                            <button @click="this.selectionMenu = 'inspector'" :class="this.selectionMenu == 'inspector' ? 'bg-gray-800' : 'text-gray-300 hover:text-white hover:bg-gray-600'" class="flex flex-1 items-center justify-center h-10 font-medium text-sm focus:outline-none">
                                                                <svg class="h-4 w-4" viewBox="0 0 24 24">
                                                                    <path fill="currentColor" d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" />
                                                                </svg>
                                                            </button>
                                                        </nav>
                                                    </div>
                                                    <div class="p-2">
                                                        <div v-if="this.selectionMenu == 'settings'">
                                                            <component
                                                                v-for="(item, index) in this.$store.state.profile['general'][this.$store.state.content[this.currentSelection].type].attributes" 
                                                                :key="index"
                                                                is="s-control"
                                                                :opts="item">
                                                            </component>
                                                        </div>
                                                        <div v-if="this.selectionMenu == 'styles'">
                                                            <component
                                                                v-for="(item, index) in this.$store.state.profile['general'][this.$store.state.content[this.currentSelection].type].styles" 
                                                                :key="index"
                                                                is="s-control"
                                                                :opts="item">
                                                            </component>
                                                            <nav v-if="this.$store.state.content[this.currentSelection].backgroundGradient" class="flex space-x-4" aria-label="Tabs">
                                                                <a v-for="(gradient, gradientIndex) in this.$store.state.content[this.currentSelection].backgroundGradient" :key="gradientIndex" class="text-gray-500 hover:text-gray-700 px-3 py-2 font-medium text-sm rounded-md">
                                                                    @{{ 'Gradient ' + (gradientIndex + 1).toString() }}
                                                                </a>
                                                            </nav>
                                                            <div v-if="this.$store.state.content[this.currentSelection].backgroundGradient">
                                                                <div v-for="(gradient, gradientIndex) in this.$store.state.content[this.currentSelection].backgroundGradient" :key="gradientIndex" :label="'Gradient ' + (gradientIndex + 1).toString()">
                                                                    <label v-if="gradient.type != 'radial'" label="Gradient Angle">
                                                                        <input type="range" v-model="this.$store.state.content[this.currentSelection].backgroundGradient[gradientIndex].backgroundLinearGradientAngle" id="volume" name="gradient_angle" min="0" max="360">
                                                                    </label>
                                                                    <label label="Gradient Angle">
                                                                        <select placeholder="Angle Measure" v-model="this.$store.state.content[this.currentSelection].backgroundGradient[gradientIndex].backgroundLinearGradientAngleMeasure" class="mt-1 block w-full pl-3 pr-10 py-2 border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md">
                                                                            <option value="deg">Degrees</option>
                                                                            <option value="rad">Radians</option>
                                                                            <option value="turn">Turns</option> 
                                                                        </select>
                                                                    </label>
                                                                    <div v-for="(colourStop, colourStopIndex) in gradient.colourStops" :key="colourStopIndex">
                                                                        <label label="Gradient Colour Stops">
                                                                            <input type="color" v-model="this.$store.state.content[this.currentSelection].backgroundGradient[gradientIndex].colourStops[colourStopIndex].colour" />
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div v-show="this.selectionMenu == 'data'">
                                                            <div class="mt-2 flex items-center space-x-2">
                                                                <a href="/cms" target="_blank" class="font-medium">Manage Datasources</a>
                                                                <svg class="h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" aria-hidden="true">
                                                                    <path fill="currentColor" d="M14,3V5H17.59L7.76,14.83L9.17,16.24L19,6.41V10H21V3M19,19H5V5H12V3H5C3.89,3 3,3.9 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V12H19V19Z" />
                                                                </svg>
                                                            </div>
                                                            <div v-if="this.$store.state.page.sources" class="mt-5">
                                                                <p class="font-medium text-sm">Source</p>
                                                                <label for="source" class="leading-6 font-medium">Attach Source</label>
                                                                <select id="source" @change="setSource" v-model="this.$store.state.content[this.currentSelection].source" class="block bg-gray-800 h-8 text-xs">
                                                                    <option value="">None</option>
                                                                    <option v-for="option in this.$store.state.page.sources" :value="option">
                                                                        @{{ this.$store.state.content[option].name }}
                                                                    </option>
                                                                </select>
                                                            </div>
                                                            <div v-else>
                                                                You must attach a datasource to the page.
                                                            </div>
                                                            <div v-if="this.source" class="mt-5">
                                                                <label for="source" class="leading-6 font-medium">Select attribute</label>
                                                                <select @change="save" id="source" v-model="this.$store.state.content[this.currentSelection].fieldName" class="block bg-gray-800 h-8 text-xs">
                                                                    <option value="field">Text</option>
                                                                    <option value="nameField">Name</option>
                                                                    <option value="valueField">Value</option>
                                                                    <option value="hrefField">Href (Hyperlink)</option>
                                                                    <option value="classField">Class</option>
                                                                    <option value="idField">ID</option>
                                                                    <option value="tagField">Tag</option>
                                                                    <option value="typeField">Type</option>
                                                                    <option value="valueField">Value</option>
                                                                    <option value="srcField">Image Source</option>
                                                                    <option value="dField">Definition field (SVG)</option>
                                                                    <option value="forField">For</option>
                                                                    <option value="labelField">Label</option>
                                                                    <option value="widthField">Width</option>
                                                                    <option value="heightField">Height</option>
                                                                    <option value="fillField">Fill</option>
                                                                    <option value="strokeField">Stroke</option>
                                                                </select>
                                                            </div>
                                                            <div v-if="this.source" class="mt-5">
                                                                <label for="source" class="leading-6 font-medium">Select source field</label>
                                                                <select @change="save" id="source" v-model="this.$store.state.content[this.currentSelection][this.$store.state.content[this.currentSelection].fieldName ? this.$store.state.content[this.currentSelection].fieldName : 'field']" class="block bg-gray-800 h-8 text-xs">
                                                                    <option value="">None</option>
                                                                    <option v-for="option in this.$store.state.content[this.source].fields" :value="option.field">
                                                                        @{{ option.label }}
                                                                    </option>
                                                                </select>
                                                            </div>
                                                            <!-- <div v-if="typeof this.$store.state.content[this.$store.state.content[this.currentSelection].source].persist != 'undefined'" class="mt-5">
                                                                <label for="persist" class="leading-6 font-medium">Persist changes?</label>
                                                                <select @change="save" id="persist" v-model="this.$store.state.content[this.$store.state.content[this.currentSelection].source].persist" class="block bg-gray-800 h-8 text-xs">
                                                                    <option value="">None</option>
                                                                    <option value="storage">Storage</option>
                                                                    <option value="session">Session</option>
                                                                    <option value="local">Local</option>
                                                                </select>
                                                            </div> -->
                                                            <div v-if="this.source" class="mt-5">
                                                                <label for="perPage" class="leading-6 font-medium">Results per page</label>
                                                                <input v-model="this.$store.state.content[this.currentSelection].perPage" type="number" id="perPage" class="block bg-gray-800 h-8 text-xs">
                                                            </div>
                                                            <div v-if="this.$store.state.content[source]" class="mt-5 text-black">
                                                                <table class="min-w-full overflow-x-scroll divide-y divide-gray-200">
                                                                    <thead v-if="this.$store.state.content[source].fields">
                                                                        <tr>
                                                                            <th class="w-8 bg-gray-900"></th>
                                                                            <th
                                                                            v-for="(column, index) in this.$store.state.content[source].fields"
                                                                            :key="index + 'header'"
                                                                            class="px-6 py-3 text-left text-xs font-medium bg-gray-900 text-gray-400 uppercase tracking-wider">
                                                                            @{{ column.label }}
                                                                            </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody v-if="this.$store.state.content[source].data">
                                                                        <tr v-for="(row, index) in this.$store.state.content[source].data" :key="index" :class="index % 2 ? 'bg-white' : 'bg-gray-50'" class="overflow-x-auto">
                                                                            <td class="w-8 p-2">
                                                                                <div class="relative flex justify-end items-center">
                                                                                    <input type="checkbox" :value="row" v-model="this.$store.state.content[this.currentSelection].sourceData" class="focus:ring-indigo-500 h-4 w-4 border-gray-300 rounded">
                                                                                </div>
                                                                            </td>
                                                                            <td v-for="(column, colindex) in this.$store.state.content[source].fields" :key="index + ':' + colindex" style="max-width: 100px;" class="overflow-auto px-6 py-4 whitespace-nowrap text-sm font-medium">
                                                                                <span v-if="this.$store.state.content[row] && typeof this.$store.state.content[row][column.field] == 'Array'">
                                                                                <ul>
                                                                                    <li v-for="(listitem, index) in this.$store.state.content[row][column.field]">@{{ listitem }}</li>
                                                                                </ul>
                                                                                </span>
                                                                                <span v-else>
                                                                                @{{ this.$store.state.content[row] ? this.$store.state.content[row][column.field] : '' }}
                                                                                </span>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div v-show="this.selectionMenu == 'conditional'">
                                                            <div class="mt-5">
                                                                <label for="trigger" class="leading-6 font-medium">Add conditional rendering logic</label>
                                                                <div class="flex items-center justify-between bg-gray-800  border-t border-black">
                                                                    <div class="flex-shrink animate-pulse">
                                                                        <svg class="h-4 text-green-400" viewBox="0 0 24 24">
                                                                            <path fill="currentColor" d="M8.59,16.58L13.17,12L8.59,7.41L10,6L16,12L10,18L8.59,16.58Z" />
                                                                        </svg>
                                                                    </div>
                                                                    <input @keydown="consoleKeydown" v-model="currentCommand" style="box-shadow: none !important;" placeholder="Type a command, then hit ⏎, cycle commands using ⇧ ⇩" class="bg-gray-800 text-gray-300 flex-1 font-mono text-xs pl-2 border-none shadow-none focus:outline-none w-full">
                                                                    <div class="flex-shrink">
                                                                        <a href="#" @click.stop="addConditionaRenderingLogic" class="inline-flex items-center justify-center" style="height: 2.5em; width: 2.5em;">
                                                                            <span class="sr-only">Add conditional rendering logic</span>
                                                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true" class="h-4 w-4 text-gray-300 hover:text-white">
                                                                                <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 4v16m8-8H4"></path>
                                                                            </svg>
                                                                        </a>
                                                                    </div>
                                                                    <div class="flex-shrink">
                                                                        <a v-if="attributeSelectMode" @click="attributeSelectMode = false;" href="#" class="inline-flex items-center justify-center" style="height: 2.5em; width: 2.5em;">
                                                                            <span class="sr-only">Exit insert attribute node</span>
                                                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true" class="h-4 w-4 text-gray-300 hover:text-white">
                                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13.875 18.825A10.05 10.05 0 0112 19c-4.478 0-8.268-2.943-9.543-7a9.97 9.97 0 011.563-3.029m5.858.908a3 3 0 114.243 4.243M9.878 9.878l4.242 4.242M9.88 9.88l-3.29-3.29m7.532 7.532l3.29 3.29M3 3l3.59 3.59m0 0A9.953 9.953 0 0112 5c4.478 0 8.268 2.943 9.543 7a10.025 10.025 0 01-4.132 5.411m0 0L21 21" />
                                                                            </svg>
                                                                        </a>
                                                                        <a v-else @click="attributeSelect" href="#" class="inline-flex items-center justify-center" style="height: 2.5em; width: 2.5em;">
                                                                            <span class="sr-only">Enter insert attribute mode</span>
                                                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true" class="h-4 w-4 text-gray-300 hover:text-white">
                                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"></path> <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"></path>
                                                                            </svg>
                                                                        </a>
                                                                    </div>
                                                                    <div class="flex-shrink">
                                                                        <a href="#" @click.stop="currentCommand = ''" class="inline-flex items-center justify-center" style="height: 2.5em; width: 2.5em;">
                                                                            <span class="sr-only">Clear console</span>
                                                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true" class="h-4 w-4 text-gray-300 hover:text-white">
                                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path>
                                                                            </svg>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="mt-5 max-w-sm" v-if="this.$store.state.content[this.currentSelection]['conditionalLogic']">
                                                                <label for="trigger" class="leading-6 font-medium">Rendering conditions</label>
                                                                <draggable v-model="this.$store.state.content[this.currentSelection]['conditionalLogic']" @end="save" class="mt-2" :component-data="{name:'fade'}" item-key="id">
                                                                    <template #item="{element, index}" class="col-span-1 flex shadow-sm">
                                                                        <div class="col-span-1 flex shadow-sm">
                                                                            <div class="flex-1 flex items-center justify-between border border-black bg-gray-800 truncate">
                                                                                <div class="flex flex-1 px-4 py-2 text-xs leading-5 truncate" v-if="typeof this.$store.state.content[this.currentSelection][element] != 'undefined'">
                                                                                    <button @click="eventTrigger = element;eventIndex = index;selectionMenu = 'eventArguments';" class="font-medium transition ease-in-out duration-150 inline-flex items-center justify-center bg-transparent hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                                                        @{{ element }}
                                                                                    </button>
                                                                                    <div class="flex-shrink-0 pr-2">
                                                                                        <button @click="eventTrigger = element;eventIndex = index;selectionMenu = 'eventArguments';" class="w-8 h-8 inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                                                            (arg)
                                                                                        </button>
                                                                                    </div>
                                                                                </div>
                                                                                <div v-else class="flex-1 px-4 py-2 text-xs leading-5 truncate">
                                                                                    @{{ element }}
                                                                                </div>
                                                                                <div class="flex-shrink-0 pr-2">
                                                                                    <button @click="removeEvent(index, 'condition')" class="w-8 h-8 inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" class="w-4 h-4">
                                                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path>
                                                                                        </svg>
                                                                                    </button>
                                                                                </div>
                                                                                <div class="handle cursor-move flex-shrink-0 pr-2">
                                                                                    <button class="w-8 h-8 cursor-move inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                                                        <svg class="w-4 h-4" viewBox="0 0 20 20" fill="currentColor"><path d="M7 2a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 2zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 8zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 14zm6-8a2 2 0 1 0-.001-4.001A2 2 0 0 0 13 6zm0 2a2 2 0 1 0 .001 4.001A2 2 0 0 0 13 8zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 13 14z"></path></svg>
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </template>
                                                                </draggable>
                                                            </div>
                                                        </div>
                                                        <!-- <div v-show="this.selectionMenu == 'template'">
                                                            Store your selection as a Template:
                                                            <div class="mt-5">
                                                                <label for="templateName" class="leading-6 font-medium">Give your template a name</label>
                                                                <input v-model="template.name" class="block bg-gray-800 h-8" type="text" id="templateName" placeholder="e.g. Transparent Card">
                                                            </div>
                                                            <div class="mt-5">
                                                                <label for="templateName" class="leading-6 font-medium">Choose a type</label>
                                                                <select v-model="template.type" class="block bg-gray-800 h-8 text-xs">
                                                                    <option disabled value="">Please select one</option>
                                                                    <option value="transitions">Transitions</option>
                                                                    <option value="grids">Grids</option>
                                                                    <option value="hero">Hero Sections</option>
                                                                    <option value="feature">Feature Sections</option>
                                                                    <option value="cta">CTA Sections</option>
                                                                    <option value="blog">Blog Sections</option>
                                                                    <option value="pricing">Pricing Sections</option>
                                                                    <option value="header">Header Sections</option>
                                                                    <option value="faqs">FAQs</option>
                                                                    <option value="newsletter">Newsletter Sections</option>
                                                                    <option value="stats">Stats</option>
                                                                    <option value="this.$store.state.content">Content Sections</option>
                                                                    <option value="rating">Ratings</option>
                                                                    <option value="testimonials">Testimonials</option>
                                                                    <option value="card">Card Sections</option>
                                                                    <option value="contact">Contact Sections</option>
                                                                    <option value="footer">Footers</option>
                                                                    <option value="clouds">Logo Clouds</option>
                                                                    <option value="headers">Headers</option>
                                                                    <option value="banners">Banners</option>
                                                                    <option value="flyout">Flyout Menus</option>
                                                                    <option value="inputs">Input Groups</option>
                                                                    <option value="select">Select Menus</option>
                                                                    <option value="signin">Sign-in and Registration</option>
                                                                    <option value="radio">Radio Groups</option>
                                                                    <option value="toggles">Toggles</option>
                                                                    <option value="panels">Panels</option>
                                                                    <option value="alerts">Alerts</option>
                                                                    <option value="navbars">Navbars</option>
                                                                    <option value="pagination">Pagination</option>
                                                                    <option value="tabs">Tabs</option>
                                                                    <option value="verticalnav">Vertical Navigation</option>
                                                                    <option value="sidenav">Sidebar Navigation</option>
                                                                    <option value="breadcrumbs">Breadcrumbs</option>
                                                                    <option value="steps">Steps</option>
                                                                    <option value="modals">Modals</option>
                                                                    <option value="slideovers">Slide-overs</option>
                                                                    <option value="notfications">Notifications</option>
                                                                    <option value="avatars">Avatars</option>
                                                                    <option value="dropdowns">Dropdowns</option>
                                                                    <option value="badges">Badges</option>
                                                                    <option value="buttons">Buttons</option>
                                                                    <option value="buttongroups">Button Groups</option>
                                                                    <option value="containers">Containers</option>
                                                                    <option value="listcontainers">List Containers</option>
                                                                    <option value="media">Media Objects</option>
                                                                    <option value="dividers">Dividers</option>
                                                                </select>
                                                            </div>
                                                            <button type="button" @click="storeAsTemplate" class="mt-5 inline-flex items-center px-4 py-2 border border-transparent text-sm font-mono border border-black focus:outline-none focus:ring-2 focus:ring-offset-2">
                                                                Save as Template
                                                            </button>
                                                        </div> -->
                                                        <div v-show="this.selectionMenu == 'events' && this.currentSelection && this.$store.state.content[this.currentSelection].type == 's-input'">
                                                            <div class="mt-2">
                                                                <p class="font-medium text-sm">Input Events</p>
                                                                <p>Attach events and trigger commands.</p>
                                                                <div class="mt-2">
                                                                    <label for="trigger" class="leading-6 font-medium">Event</label>
                                                                    <select id="trigger" v-model="event" class="text-xs bg-gray-800 h-8 block">
                                                                        <option value="">None</option>
                                                                        <option value="onClick">On Click</option>
                                                                        <option value="onDblClick">On Double Click</option>
                                                                        <option value="onChange">On Change</option>
                                                                        <option value="onInput">On Input</option>
                                                                        <option value="onFocus">On Focus</option>
                                                                        <option value="onBlur">On Blur</option>
                                                                        <option value="onKeyUp">On Key Up</option>
                                                                        <option value="onKeyDown">On Key Down</option>
                                                                        <option value="onMouseover">On Mouse Enter</option>
                                                                        <option value="onMouseout">On Mouse Leave</option>
                                                                        <option value="onDrag">On Drag</option>
                                                                        <option value="onRelease">On Release</option>
                                                                    </select>
                                                                </div>
                                                                <div v-if="event == 'onKeyUp' || event == 'onKeyDown'" class="mt-5">
                                                                    <label class="text-xs leading-6 font-medium">Keycode</label>
                                                                    <select id="trigger" v-model="this.$store.state.content[this.currentSelection].keyCode" class="text-xs bg-gray-800 h-8 block">
                                                                        <option value="">None</option>
                                                                        <option value="13">Enter</option>
                                                                    </select>
                                                                </div>
                                                                <div class="mt-5" v-if="event">
                                                                    <label for="trigger" class="leading-6 font-medium">Attach a command</label>
                                                                    <div class="flex items-center justify-between bg-gray-800  border-t border-black">
                                                                        <div class="flex-shrink animate-pulse">
                                                                            <svg class="h-4 text-green-400" viewBox="0 0 24 24">
                                                                                <path fill="currentColor" d="M8.59,16.58L13.17,12L8.59,7.41L10,6L16,12L10,18L8.59,16.58Z" />
                                                                            </svg>
                                                                        </div>
                                                                        <input @keyup.enter="runCommand" @keydown="consoleKeydown" v-model="currentCommand" style="box-shadow: none !important;" placeholder="Type a command, then hit ⏎, cycle commands using ⇧ ⇩" class="bg-gray-800 text-gray-300 flex-1 font-mono text-xs pl-2 border-none shadow-none focus:outline-none w-full">
                                                                        <div class="flex-shrink">
                                                                            <a href="#" @click.stop="addCommand" class="inline-flex items-center justify-center" style="height: 2.5em; width: 2.5em;">
                                                                                <span class="sr-only">Add command</span>
                                                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true" class="h-4 w-4 text-gray-300 hover:text-white">
                                                                                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 4v16m8-8H4"></path>
                                                                                </svg>
                                                                            </a>
                                                                        </div>
                                                                        <div class="flex-shrink">
                                                                            <a v-if="attributeSelectMode" @click="attributeSelectMode = false;" href="#" class="inline-flex items-center justify-center" style="height: 2.5em; width: 2.5em;">
                                                                                <span class="sr-only">Exit insert attribute node</span>
                                                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true" class="h-4 w-4 text-gray-300 hover:text-white">
                                                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13.875 18.825A10.05 10.05 0 0112 19c-4.478 0-8.268-2.943-9.543-7a9.97 9.97 0 011.563-3.029m5.858.908a3 3 0 114.243 4.243M9.878 9.878l4.242 4.242M9.88 9.88l-3.29-3.29m7.532 7.532l3.29 3.29M3 3l3.59 3.59m0 0A9.953 9.953 0 0112 5c4.478 0 8.268 2.943 9.543 7a10.025 10.025 0 01-4.132 5.411m0 0L21 21" />
                                                                                </svg>
                                                                            </a>
                                                                            <a v-else @click="attributeSelect" href="#" class="inline-flex items-center justify-center" style="height: 2.5em; width: 2.5em;">
                                                                                <span class="sr-only">Enter insert attribute mode</span>
                                                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true" class="h-4 w-4 text-gray-300 hover:text-white">
                                                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"></path> <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"></path>
                                                                                </svg>
                                                                            </a>
                                                                        </div>
                                                                        <div class="flex-shrink">
                                                                            <a href="#" @click.stop="currentCommand = ''" class="inline-flex items-center justify-center" style="height: 2.5em; width: 2.5em;">
                                                                                <span class="sr-only">Clear console</span>
                                                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true" class="h-4 w-4 text-gray-300 hover:text-white">
                                                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path>
                                                                                </svg>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="mt-5" v-if="event">
                                                                    <label for="trigger" class="leading-6 font-medium">Attach a method</label>
                                                                    <div class="flex">
                                                                        <select id="trigger" v-model="eventTrigger" v-model="eventTrigger" class="text-xs bg-gray-800 h-8 block">
                                                                            <option value="">None</option>
                                                                            <option v-for="(method, index) in this.$store.state.profile.methods" :value="method.value">
                                                                                @{{ method.name }}
                                                                            </option>
                                                                        </select>
                                                                        <button @click="addMethod" type="button" class="relative inline-flex items-center font-medium px-2 py-2 bg-gray-500 hover:bg-gray-800 focus:outline-none">Add</button>
                                                                    </div>
                                                                </div>
                                                                <div class="mt-5 max-w-sm" v-if="this.$store.state.content[this.currentSelection][event]">
                                                                    <label for="trigger" class="leading-6 font-medium">Attached event triggers</label>
                                                                    <draggable v-model="this.$store.state.content[this.currentSelection][event]" @end="save" class="mt-2" :component-data="{name:'fade'}" item-key="id">
                                                                        <template #item="{element, index}" class="col-span-1 flex shadow-sm">
                                                                            <div class="col-span-1 flex shadow-sm">
                                                                                <div class="flex-1 flex items-center justify-between border border-black bg-gray-800 truncate">
                                                                                    <div class="flex flex-1 px-4 py-2 text-xs leading-5 truncate" v-if="typeof this.$store.state.content[this.currentSelection][element] != 'undefined'">
                                                                                        <button @click="eventTrigger = element;eventIndex = index;selectionMenu = 'eventArguments';" class="font-medium transition ease-in-out duration-150 inline-flex items-center justify-center bg-transparent hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                                                            @{{ element }}
                                                                                        </button>
                                                                                        <div class="flex-shrink-0 pr-2">
                                                                                            <button @click="eventTrigger = element;eventIndex = index;selectionMenu = 'eventArguments';" class="w-8 h-8 inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                                                                (arg)
                                                                                            </button>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div v-else class="flex-1 px-4 py-2 text-xs leading-5 truncate">
                                                                                        @{{ element }}
                                                                                    </div>
                                                                                    <div class="flex-shrink-0 pr-2">
                                                                                        <button @click="removeEvent(index, null)" class="w-8 h-8 inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" class="w-4 h-4">
                                                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path>
                                                                                            </svg>
                                                                                        </button>
                                                                                    </div>
                                                                                    <div class="handle cursor-move flex-shrink-0 pr-2">
                                                                                        <button class="w-8 h-8 cursor-move inline-flex items-center justify-center hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                                                            <svg class="w-4 h-4" viewBox="0 0 20 20" fill="currentColor"><path d="M7 2a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 2zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 8zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 14zm6-8a2 2 0 1 0-.001-4.001A2 2 0 0 0 13 6zm0 2a2 2 0 1 0 .001 4.001A2 2 0 0 0 13 8zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 13 14z"></path></svg>
                                                                                        </button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </template>
                                                                    </draggable>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div v-show="this.selectionMenu == 'eventArguments'">
                                                            <button @click="selectionMenu = 'events';" class="block font-medium hover:text-white focus:outline-none transition ease-in-out duration-150">
                                                                ← Return to events
                                                            </button>
                                                            <div v-if="eventIndex != null && this.$store.state.content[this.currentSelection][eventTrigger]">
                                                                <div class="mt-5">
                                                                    <label class="text-lg font-medium">@{{ eventTrigger }}(
                                                                        <span v-for="(arg, index) in this.$store.state.profile.methods[eventTrigger].arguments" :key="index">
                                                                            <a v-if="arg == 'Target'" href="#" @click="targetSelect" class="text-blue-300 hover:underline">@{{ arg + (index < this.$store.state.profile.methods[eventTrigger].arguments.length - 1 ? ', ' : '') }}</a>
                                                                            <a v-else-if="arg == 'Object'" href="#" @click="objectSelect" class="text-blue-300 hover:underline">@{{ arg + ' (Optional)' + (index < this.$store.state.profile.methods[eventTrigger].arguments.length - 1 ? ', ' : '') }}</a>
                                                                            <span v-else class="hover:underline">@{{ arg + (index < this.$store.state.profile.methods[eventTrigger].arguments.length - 1 ? ', ' : '') }}</span>
                                                                        </span> )
                                                                    </label>
                                                                </div>
                                                                <div v-for="(param, index) in this.$store.state.profile.methods[eventTrigger].parameters" :key="index">
                                                                    <div class="mt-5">
                                                                        <label class="text-xs font-medium">@{{ param.name }}</label>
                                                                        <div v-if="param.type == 'variable'" class="mt-2">
                                                                            <label for="variableType" class="my-4 leading-6 font-medium">Type</label>
                                                                            <select id="variableType" v-model="variableType" class="block bg-gray-800 h-8 text-xs">
                                                                                <option value="string">String</option>
                                                                                <option value="array">Array</option>
                                                                                <option value="boolean">Boolean</option>
                                                                                <option value="number">Number</option>
                                                                            </select>
                                                                        </div>
                                                                        <input v-if="param.type == 'text' || variableType == 'string'" type="text" v-model="this.$store.state.content[this.currentSelection][eventTrigger][eventIndex][param.name]" class="bg-gray-800 h-8 block">
                                                                        <input v-else-if="param.type == 'number' || variableType == 'number'" type="number" v-model="this.$store.state.content[this.currentSelection][eventTrigger][eventIndex][param.name]" class="bg-gray-800 h-8 block">
                                                                        <input v-else-if="param.type == 'array' || variableType == 'array'" type="button" @click="this.$store.state.content[this.currentSelection][eventTrigger][eventIndex][param.name] = []" value="Set to empty array" class="bg-gray-800 inline-flex items-center px-2 py-2 border border-transparent text-xs font-mono border border-black focus:outline-none focus:ring-2 focus:ring-offset-2">
                                                                        <input v-if="param.type == 'checkbox' || variableType == 'boolean'" type="checkbox" v-model="this.$store.state.content[this.currentSelection][eventTrigger][eventIndex][param.name]" class="block focus:outline-none focus:border-none focus:shadow-none h-4 w-4 text-blue-600 border-gray-300 rounded">
                                                                        <textarea v-if="param.type == 'textarea'" class="bg-gray-800 h-8 block" v-model="this.$store.state.content[this.currentSelection][eventTrigger][eventIndex][param.name]"></textarea>
                                                                        <p v-else>@{{ this.$store.state.content[this.currentSelection][eventTrigger][eventIndex][param.name] }}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div v-show="this.selectionMenu == 'inspector'">
                                                            <button type="button" @click="this.$store.commit('clearElementAttributes')" class="my-5 inline-flex items-center px-4 py-2 border border-black ">
                                                                Clear/ Reset Attributes
                                                            </button>
                                                            <pre class="scrollbar-none overflow-x-auto p-6 text-sm leading-snug language-html text-white bg-black bg-opacity-75">
                                                                <code style="white-space:pre;">
                                                                    @{{ JSON.stringify(this.$store.state.content[this.currentSelection], undefined, 4).replace('{', '').replace('}', '') }}
                                                                </code>
                                                            </pre>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div v-show="this.selectionMenu == 'requestParameters' && menu == 'Page' && pageSelectionMenu == 'datasources'">
                                                    <div class="p-2" v-if="requestIndex != null">
                                                        <div class="mt-5">
                                                            <label for="method" class="leading-6 font-medium">Select method</label>
                                                            <select @change="saveBody" id="method" v-model="this.$store.state.page.requests[requestIndex].method" class="block bg-gray-800 h-8 text-xs">
                                                                <option value="get">GET</option>
                                                                <option value="post">POST</option>
                                                                <option value="delete">DELETE</option>
                                                                <option value="put">PUT</option>
                                                                <option value="patch">PATCH</option>
                                                            </select>
                                                        </div>
                                                        <div class="mt-5">
                                                            <label for="url" class="leading-6 font-medium">URL</label>
                                                            <input @input="saveBody" v-model="this.$store.state.page.requests[requestIndex].url" type="text" id="url" class="block bg-gray-800 h-8 w-64 text-xs">
                                                        </div>
                                                        <div class="mt-5">
                                                            <label for="query" class="leading-6 font-medium">Query</label>
                                                            <input @input="saveBody" v-model="this.$store.state.page.requests[requestIndex].query" type="text" id="query" class="block bg-gray-800 h-8 w-64 text-xs">
                                                        </div>
                                                        <div class="mt-5">
                                                            <label for="requestname" class="leading-6 font-medium">Name</label>
                                                            <input @input="saveBody" v-model="this.$store.state.page.requests[requestIndex].name" type="text" id="requestname" class="block bg-gray-800 h-8 text-xs">
                                                        </div>
                                                        <div class="mt-5">
                                                            <label for="key" class="leading-6 font-medium">Key</label>
                                                            <input @input="saveBody" v-model="this.$store.state.page.requests[requestIndex].key" type="text" id="key" class="block bg-gray-800 h-8 text-xs">
                                                        </div>
                                                        <div class="mt-5">
                                                            <label for="username" class="leading-6 font-medium">Username</label>
                                                            <input @input="saveBody" v-model="this.$store.state.page.requests[requestIndex].username" type="text" id="username" class="block bg-gray-800 h-8 text-xs">
                                                        </div>
                                                        <div class="mt-5">
                                                            <label for="password" class="leading-6 font-medium">Password</label>
                                                            <input @input="saveBody" v-model="this.$store.state.page.requests[requestIndex].password" type="text" id="password" class="block bg-gray-800 h-8 text-xs">
                                                        </div>
                                                        <div class="mt-5">
                                                            <label for="token" class="leading-6 font-medium">Token</label>
                                                            <input @input="saveBody" v-model="this.$store.state.page.requests[requestIndex].token" type="text" id="token" class="block bg-gray-800 h-8 text-xs">
                                                        </div>
                                                        <div class="mt-5">
                                                            <label for="timeout" class="leading-6 font-medium">Timeout</label>
                                                            <input @input="saveBody" v-model="this.$store.state.page.requests[requestIndex].timeout" type="number" id="timeout" class="block bg-gray-800 h-8 text-xs">
                                                        </div>
                                                        <div class="mt-5">
                                                            <label for="retryamount" class="leading-6 font-medium">Retry Amount</label>
                                                            <input @input="saveBody" v-model="this.$store.state.page.requests[requestIndex].retryamount" type="number" id="retryamount" class="block bg-gray-800 h-8 text-xs">
                                                        </div>
                                                        <div class="mt-5">
                                                            <label for="retrydelay" class="leading-6 font-medium">Retry Delay</label>
                                                            <input @input="saveBody" v-model="this.$store.state.page.requests[requestIndex].retrydelay" type="number" id="retrydelay" class="block bg-gray-800 h-8 text-xs">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </transition>
                            </section>
                        </div>
                    </div>
                </footer>
            </div>
            @else
            <s-app></s-app>
            @endif
        </div>
        @if (empty($editMode) && !empty($settings['pwa']))
        <script>
        if ("serviceWorker" in navigator) {
            navigator.serviceWorker
            .register("sw.js", {
                scope: "./"
            })
            .then(function(reg) {
                console.log(
                "Service worker has been registered for scope:" + reg.scope
                );
            });
        }
        </script>
        @endif
        @if(!empty($body) && $body->render == 0 || !empty($editMode))
        <script defer type="text/javascript" src="/js/general{{$editMode}}.js"></script>
        <script>
            window.App = {!! json_encode([
                'content' => !empty($content) ? $content : [],
                'settings' => !empty($settings) ? $settings : [],
                'body' => !empty($body) ? $body : [],
                'user' => !empty($user) ? $user : [],
                'users' => !empty($users) ? $users : [],
                'profile' => !empty($profile) ? $profile : [],
                'entity' => !empty($entity) ? $entity : [],
                'editor' => !empty($editor) ? $editor : [],
                'session' => Session::all(),
                'templates' => !empty($templates) ? $templates : [],
                'stripepublic' => !empty($stripePublicKey) ? $stripePublicKey : '',
                'state' => !empty($state) ? $state : []
            ]) !!};
        </script>
        @else

        @endif
    </body>
</html>