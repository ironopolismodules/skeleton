<div class="inner-page without-banner">   
    <div class="page-section">
        <div class="container">
            <div class="page-content">
                <h1>This is Embarrasing!</h1>
                <div class="page-paragraph">
                    @if (!empty($error))
                        <p>{{ $error }}</p>
                    @else
                        <p>Page not found</p>
                    @endif
                </div>
            </div>            
        </div>
    </div>
</div>
