@extends('layouts.app')
@section('content')
<div class="relative bg-gray-100">
  <main>
    <div v-if="user.site_id" class="lg:grid lg:grid-cols-12">

      <div v-show="displayMobileMenu" class="fixed inset-0 flex z-40 lg:hidden" role="dialog" aria-modal="true">

        <transition
          enter-active-class="transition-opacity ease-linear duration-300"
          enter-from-class="opacity-0"
          enter-to-class="opacity-100"
          leave-active-class="transition-opacity ease-linear duration-300"
          leave-from-class="opacity-100"
          leave-to-class="opacity-0">
          <div class="fixed inset-0 bg-gray-600 bg-opacity-75" aria-hidden="true"></div>
        </transition>

        <transition
          enter-active-class="transition ease-in-out duration-300 transform"
          enter-from-class="-translate-x-full"
          enter-to-class="translate-x-0"
          leave-active-class="transition ease-in-out duration-300 transform"
          leave-from-class="translate-x-0"
          leave-to-class="-translate-x-full">
          <div class="relative flex-1 flex flex-col max-w-xs w-full pt-5 pb-4 bg-white">
            <transition
              enter-active-class="ease-in-out duration-300"
              enter-from-class="opacity-0"
              enter-to-class="opacity-100"
              leave-active-class="ease-in-out duration-300"
              leave-from-class="opacity-100"
              leave-to-class="opacity-0">
              <div class="absolute top-0 right-0 -mr-12 pt-2">
                <button @click="displayMobileMenu = false" type="button" class="ml-1 flex items-center justify-center h-10 w-10 rounded-full focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white">
                  <span class="sr-only">Close sidebar</span>
                  <!-- Heroicon name: outline/x -->
                  <svg class="h-6 w-6 text-white" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                  </svg>
                </button>
              </div>
            </transition>

            <div class="flex-shrink-0  px-4">
              <button class="flex items-center" @click="displayCategory('')">
                <img class="group-hover:flex-shrink-0 -ml-1 mr-3 h-4 w-4 rounded-full" src="https://stellifysoftware.s3.eu-west-2.amazonaws.com/stellisoft-logo.jpg" />
                <span class="uppercase truncate">
                  Application Control
                </span>
              </button>
            </div>
            
            <div class="mt-5 flex-1 h-0 overflow-y-auto">
              <nav class="px-2">
                <div class="space-y-1">
                  <nav class="space-y-1 text-sm">
                    <a href="#" @click="displayCategory('project')" class="hover:hover:bg-gray-50 group rounded-md px-3 py-2 flex items-center font-medium">
                      <svg class="group-hover:flex-shrink-0 -ml-1 mr-3 h-4 w-4" viewBox="0 0 24 24">
                          <path fill="currentColor" d="M6 2C4.89 2 4 2.9 4 4V20C4 21.11 4.89 22 6 22H12V20H6V4H13V9H18V12H20V8L14 2M18 14C17.87 14 17.76 14.09 17.74 14.21L17.55 15.53C17.25 15.66 16.96 15.82 16.7 16L15.46 15.5C15.35 15.5 15.22 15.5 15.15 15.63L14.15 17.36C14.09 17.47 14.11 17.6 14.21 17.68L15.27 18.5C15.25 18.67 15.24 18.83 15.24 19C15.24 19.17 15.25 19.33 15.27 19.5L14.21 20.32C14.12 20.4 14.09 20.53 14.15 20.64L15.15 22.37C15.21 22.5 15.34 22.5 15.46 22.5L16.7 22C16.96 22.18 17.24 22.35 17.55 22.47L17.74 23.79C17.76 23.91 17.86 24 18 24H20C20.11 24 20.22 23.91 20.24 23.79L20.43 22.47C20.73 22.34 21 22.18 21.27 22L22.5 22.5C22.63 22.5 22.76 22.5 22.83 22.37L23.83 20.64C23.89 20.53 23.86 20.4 23.77 20.32L22.7 19.5C22.72 19.33 22.74 19.17 22.74 19C22.74 18.83 22.73 18.67 22.7 18.5L23.76 17.68C23.85 17.6 23.88 17.47 23.82 17.36L22.82 15.63C22.76 15.5 22.63 15.5 22.5 15.5L21.27 16C21 15.82 20.73 15.65 20.42 15.53L20.23 14.21C20.22 14.09 20.11 14 20 14M19 17.5C19.83 17.5 20.5 18.17 20.5 19C20.5 19.83 19.83 20.5 19 20.5C18.16 20.5 17.5 19.83 17.5 19C17.5 18.17 18.17 17.5 19 17.5Z" />
                      </svg>
                      <span class="truncate">
                        Project
                      </span>
                    </a>

                    <a href="#" @click="displayCategory('project', 'settings')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
                      <span class="truncate ml-7">
                        Settings
                      </span>
                    </a>

                    <a href="#" @click="displayCategory('project', 'pages')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
                      <span class="truncate ml-7">
                        Pages
                      </span>
                    </a>

                    <a href="/cms" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
                      <span class="truncate ml-7">
                        CMS
                      </span>
                    </a>

                    <a href="/hosting-plans" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
                      <span class="truncate ml-7">
                        Hosting
                      </span>
                    </a>

                    <a href="#" @click="displayCategory('application')" class="hover:hover:bg-gray-50 group rounded-md px-3 py-2 flex items-center font-medium">
                      <svg class="group-hover:flex-shrink-0 -ml-1 mr-3 h-4 w-4" viewBox="0 0 24 24">
                          <path fill="currentColor" d="M21.7 18.6V17.6L22.8 16.8C22.9 16.7 23 16.6 22.9 16.5L21.9 14.8C21.9 14.7 21.7 14.7 21.6 14.7L20.4 15.2C20.1 15 19.8 14.8 19.5 14.7L19.3 13.4C19.3 13.3 19.2 13.2 19.1 13.2H17.1C16.9 13.2 16.8 13.3 16.8 13.4L16.6 14.7C16.3 14.9 16.1 15 15.8 15.2L14.6 14.7C14.5 14.7 14.4 14.7 14.3 14.8L13.3 16.5C13.3 16.6 13.3 16.7 13.4 16.8L14.5 17.6V18.6L13.4 19.4C13.3 19.5 13.2 19.6 13.3 19.7L14.3 21.4C14.4 21.5 14.5 21.5 14.6 21.5L15.8 21C16 21.2 16.3 21.4 16.6 21.5L16.8 22.8C16.9 22.9 17 23 17.1 23H19.1C19.2 23 19.3 22.9 19.3 22.8L19.5 21.5C19.8 21.3 20 21.2 20.3 21L21.5 21.4C21.6 21.4 21.7 21.4 21.8 21.3L22.8 19.6C22.9 19.5 22.9 19.4 22.8 19.4L21.7 18.6M18 19.5C17.2 19.5 16.5 18.8 16.5 18S17.2 16.5 18 16.5 19.5 17.2 19.5 18 18.8 19.5 18 19.5M12.3 22H3C1.9 22 1 21.1 1 20V4C1 2.9 1.9 2 3 2H21C22.1 2 23 2.9 23 4V13.1C22.4 12.5 21.7 12 21 11.7V6H3V20H11.3C11.5 20.7 11.8 21.4 12.3 22Z" />
                      </svg>
                      <span class="truncate">
                        Application
                      </span>
                    </a>

                    <a href="#" @click="displayCategory('application', 'general')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
                      <span class="truncate ml-7">
                        General
                      </span>
                    </a>

                    <a href="#" @click="displayCategory('application', 'seo')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
                      <span class="truncate ml-7">
                        SEO
                      </span>
                    </a>

                    <a href="#" @click="displayCategory('application', 'includes')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
                      <span class="truncate ml-7">
                        Includes
                      </span>
                    </a>

                    <a href="#" @click="displayCategory('application', 'routing')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
                      <span class="truncate ml-7">
                        Routing
                      </span>
                    </a>

                    <a href="#" @click="displayCategory('application', 'environment')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
                      <span class="truncate ml-7">
                        Environment Variables
                      </span>
                    </a>

                    <a href="#" @click="displayCategory('application', 'localisation')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
                      <span class="truncate ml-7">
                        Localisation
                      </span>
                    </a>

                    <a href="#" @click="displayCategory('security')" class="hover:hover:bg-gray-50 group rounded-md px-3 py-2 flex items-center font-medium">
                      <svg class="group-hover:flex-shrink-0 -ml-1 mr-3 h-4 w-4" viewBox="0 0 24 24">
                        <path fill="currentColor" d="M12,12H19C18.47,16.11 15.72,19.78 12,20.92V12H5V6.3L12,3.19M12,1L3,5V11C3,16.55 6.84,21.73 12,23C17.16,21.73 21,16.55 21,11V5L12,1Z" />
                      </svg>
                      <span class="truncate">
                        Security
                      </span>
                    </a>

                    <a href="#" @click="displayCategory('security', 'authentication')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
                      <span class="truncate ml-7">
                        Authentication
                      </span>
                    </a>

                    <a href="#" @click="displayCategory('security', 'encryption')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
                      <span class="truncate ml-7">
                        Encryption
                      </span>
                    </a>

                    <!-- <a href="#" @click="displayCategory('server')" class="hover:hover:bg-gray-50 group rounded-md px-3 py-2 flex items-center font-medium">
                      <svg class="group-hover:flex-shrink-0 -ml-1 mr-3 h-4 w-4" viewBox="0 0 24 24">
                          <path fill="currentColor" d="M4,1H20A1,1 0 0,1 21,2V6A1,1 0 0,1 20,7H4A1,1 0 0,1 3,6V2A1,1 0 0,1 4,1M4,9H20A1,1 0 0,1 21,10V14A1,1 0 0,1 20,15H4A1,1 0 0,1 3,14V10A1,1 0 0,1 4,9M4,17H20A1,1 0 0,1 21,18V22A1,1 0 0,1 20,23H4A1,1 0 0,1 3,22V18A1,1 0 0,1 4,17M9,5H10V3H9V5M9,13H10V11H9V13M9,21H10V19H9V21M5,3V5H7V3H5M5,11V13H7V11H5M5,19V21H7V19H5Z" />
                      </svg>
                      <span class="truncate">
                        Server
                      </span>
                    </a>

                    <a href="#" @click="displayCategory('server', 'rateLimiting')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
                      <span class="truncate ml-7">
                        Robots.txt
                      </span>
                    </a>

                    <a href="#" @click="displayCategory('server', 'rateLimiting')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
                      <span class="truncate ml-7">
                        Sitemap.xml
                      </span>
                    </a>

                    <a href="#" @click="displayCategory('server', 'dns')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
                      <span class="truncate ml-7">
                        DNS
                      </span>
                    </a>

                    <a href="#" @click="displayCategory('server', 'rateLimiting')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
                      <span class="truncate ml-7">
                        Rate Limiting
                      </span>
                    </a>

                    <a href="#" @click="displayCategory('server', 'taskScheduling')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
                      <span class="truncate ml-7">
                        Task scheduling
                      </span>
                    </a>

                    <a href="#" @click="displayCategory('server', 'loadBalancing')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
                      <span class="truncate ml-7">
                        Load balancing
                      </span>
                    </a> -->

                    <a href="#" @click="displayCategory('storage')" @click="" class="hover:hover:bg-gray-50 group rounded-md px-3 py-2 flex items-center font-medium">
                      <svg class="group-hover:flex-shrink-0 -ml-1 mr-3 h-4 w-4" viewBox="0 0 24 24">
                          <path fill="currentColor" d="M12,3C7.58,3 4,4.79 4,7C4,9.21 7.58,11 12,11C16.42,11 20,9.21 20,7C20,4.79 16.42,3 12,3M4,9V12C4,14.21 7.58,16 12,16C16.42,16 20,14.21 20,12V9C20,11.21 16.42,13 12,13C7.58,13 4,11.21 4,9M4,14V17C4,19.21 7.58,21 12,21C16.42,21 20,19.21 20,17V14C20,16.21 16.42,18 12,18C7.58,18 4,16.21 4,14Z" />
                      </svg>
                      <span class="truncate">
                        Storage
                      </span>
                    </a>

                    <a href="#" @click="displayCategory('storage', 'connect')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
                      <span class="truncate ml-7">
                        Connect Database
                      </span>
                    </a>

                    <a href="#" @click="displayCategory('storage', 'cloud')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
                      <span class="truncate ml-7">
                        Connect Cloud Storage
                      </span>
                    </a>

                    <a href="#" @click="displayCategory('storage', 'cache')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
                      <span class="truncate ml-7">
                        Configure Cache
                      </span>
                    </a>

                    <a href="#" @click="displayCategory('mail')" class="hover:hover:bg-gray-50 group rounded-md px-3 py-2 flex items-center font-medium">
                      <svg class="group-hover:flex-shrink-0 -ml-1 mr-3 h-4 w-4" viewBox="0 0 24 24">
                        <path fill="currentColor" d="M12,15C12.81,15 13.5,14.7 14.11,14.11C14.7,13.5 15,12.81 15,12C15,11.19 14.7,10.5 14.11,9.89C13.5,9.3 12.81,9 12,9C11.19,9 10.5,9.3 9.89,9.89C9.3,10.5 9,11.19 9,12C9,12.81 9.3,13.5 9.89,14.11C10.5,14.7 11.19,15 12,15M12,2C14.75,2 17.1,3 19.05,4.95C21,6.9 22,9.25 22,12V13.45C22,14.45 21.65,15.3 21,16C20.3,16.67 19.5,17 18.5,17C17.3,17 16.31,16.5 15.56,15.5C14.56,16.5 13.38,17 12,17C10.63,17 9.45,16.5 8.46,15.54C7.5,14.55 7,13.38 7,12C7,10.63 7.5,9.45 8.46,8.46C9.45,7.5 10.63,7 12,7C13.38,7 14.55,7.5 15.54,8.46C16.5,9.45 17,10.63 17,12V13.45C17,13.86 17.16,14.22 17.46,14.53C17.76,14.84 18.11,15 18.5,15C18.92,15 19.27,14.84 19.57,14.53C19.87,14.22 20,13.86 20,13.45V12C20,9.81 19.23,7.93 17.65,6.35C16.07,4.77 14.19,4 12,4C9.81,4 7.93,4.77 6.35,6.35C4.77,7.93 4,9.81 4,12C4,14.19 4.77,16.07 6.35,17.65C7.93,19.23 9.81,20 12,20H17V22H12C9.25,22 6.9,21 4.95,19.05C3,17.1 2,14.75 2,12C2,9.25 3,6.9 4.95,4.95C6.9,3 9.25,2 12,2Z" />
                      </svg>
                      <span class="truncate">
                        Mail & Notifications
                      </span>
                    </a>

                    <a href="#" @click="displayCategory('mail', 'provider')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
                      <span class="truncate ml-7">
                        Choose Provider
                      </span>
                    </a>

                    <a href="#" @click="displayCategory('mail', 'mailSettings')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
                      <span class="truncate ml-7">
                        Configure
                      </span>
                    </a>

                    <a href="#" @click="displayCategory('testing')" class="hover:hover:bg-gray-50 group rounded-md px-3 py-2 flex items-center font-medium">
                      <!-- Heroicon name: outline/cog -->
                      <svg class="group-hover:flex-shrink-0 -ml-1 mr-3 h-4 w-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="none" aria-hidden="true">
                        <path fill="currentColor" d="M7,2V4H8V18A4,4 0 0,0 12,22A4,4 0 0,0 16,18V4H17V2H7M11,16C10.4,16 10,15.6 10,15C10,14.4 10.4,14 11,14C11.6,14 12,14.4 12,15C12,15.6 11.6,16 11,16M13,12C12.4,12 12,11.6 12,11C12,10.4 12.4,10 13,10C13.6,10 14,10.4 14,11C14,11.6 13.6,12 13,12M14,7H10V4H14V7Z" />
                      </svg>
                      <span class="truncate">
                        Testing
                      </span>
                    </a>

                    <a href="#" @click="displayCategory('testing', 'debugging')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
                      <span class="truncate ml-7">
                        Debugging
                      </span>
                    </a>

                    <a href="#" @click="displayCategory('testing', 'logs')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
                      <span class="truncate ml-7">
                        Error logs
                      </span>
                    </a>

                    <!-- <a href="#" @click="displayCategory('billing')" class="bg-gray-50 hover:bg-white group rounded-md px-3 py-2 flex items-center font-medium" aria-current="page">
                      <svg class="flex-shrink-0 -ml-1 mr-3 h-4 w-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 10h18M7 15h1m4 0h1m-7 4h12a3 3 0 003-3V8a3 3 0 00-3-3H6a3 3 0 00-3 3v8a3 3 0 003 3z" />
                      </svg>
                      <span class="truncate">
                        Subscriptions &amp; Billing
                      </span>
                    </a> -->

                    <a href="#" @click="displayCategory('api')" class="hover:hover:bg-gray-50 group rounded-md px-3 py-2 flex items-center font-medium">
                      <svg class="group-hover:flex-shrink-0 -ml-1 mr-3 h-4 w-4" viewBox="0 0 24 24">
                        <path fill="currentColor" d="M7 7H5A2 2 0 0 0 3 9V17H5V13H7V17H9V9A2 2 0 0 0 7 7M7 11H5V9H7M14 7H10V17H12V13H14A2 2 0 0 0 16 11V9A2 2 0 0 0 14 7M14 11H12V9H14M20 9V15H21V17H17V15H18V9H17V7H21V9Z" />
                      </svg>
                      <span class="truncate">
                        API
                      </span>
                    </a>

                    <a href="#" @click="displayCategory('testing', 'logs')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
                      <span class="truncate ml-7">
                        Access
                      </span>
                    </a>

                    <a href="#" @click="displayCategory('testing', 'logs')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
                      <span class="truncate ml-7">
                        Endpoints
                      </span>
                    </a>
                  </nav>
                </div>
              </nav>
            </div>
          </div>
        </transition>

        <div class="flex-shrink-0 w-14" aria-hidden="true">
          <!-- Dummy element to force sidebar to shrink to fit close icon -->
        </div>
      </div>

      <aside class="hidden lg:block bg-gray-50 border-r border-gray-200 py-4 lg:col-span-2">
        <nav class="space-y-1 text-sm">
          <a href="#" @click="displayCategory('')" class="hover:hover:bg-gray-50 group rounded-md px-3 py-2 flex items-center font-medium">
            <img class="group-hover:flex-shrink-0 -ml-1 mr-3 h-4 w-4 rounded-full" src="https://stellifysoftware.s3.eu-west-2.amazonaws.com/stellisoft-logo.jpg" />
            <span class="uppercase truncate">
              Application Control
            </span>
          </a>

          <a href="#" @click="displayCategory('project')" class="hover:hover:bg-gray-50 group rounded-md px-3 py-2 flex items-center font-medium">
            <svg class="group-hover:flex-shrink-0 -ml-1 mr-3 h-4 w-4" viewBox="0 0 24 24">
                <path fill="currentColor" d="M6 2C4.89 2 4 2.9 4 4V20C4 21.11 4.89 22 6 22H12V20H6V4H13V9H18V12H20V8L14 2M18 14C17.87 14 17.76 14.09 17.74 14.21L17.55 15.53C17.25 15.66 16.96 15.82 16.7 16L15.46 15.5C15.35 15.5 15.22 15.5 15.15 15.63L14.15 17.36C14.09 17.47 14.11 17.6 14.21 17.68L15.27 18.5C15.25 18.67 15.24 18.83 15.24 19C15.24 19.17 15.25 19.33 15.27 19.5L14.21 20.32C14.12 20.4 14.09 20.53 14.15 20.64L15.15 22.37C15.21 22.5 15.34 22.5 15.46 22.5L16.7 22C16.96 22.18 17.24 22.35 17.55 22.47L17.74 23.79C17.76 23.91 17.86 24 18 24H20C20.11 24 20.22 23.91 20.24 23.79L20.43 22.47C20.73 22.34 21 22.18 21.27 22L22.5 22.5C22.63 22.5 22.76 22.5 22.83 22.37L23.83 20.64C23.89 20.53 23.86 20.4 23.77 20.32L22.7 19.5C22.72 19.33 22.74 19.17 22.74 19C22.74 18.83 22.73 18.67 22.7 18.5L23.76 17.68C23.85 17.6 23.88 17.47 23.82 17.36L22.82 15.63C22.76 15.5 22.63 15.5 22.5 15.5L21.27 16C21 15.82 20.73 15.65 20.42 15.53L20.23 14.21C20.22 14.09 20.11 14 20 14M19 17.5C19.83 17.5 20.5 18.17 20.5 19C20.5 19.83 19.83 20.5 19 20.5C18.16 20.5 17.5 19.83 17.5 19C17.5 18.17 18.17 17.5 19 17.5Z" />
            </svg>
            <span class="truncate">
              Project
            </span>
          </a>

          <a href="#" @click="displayCategory('project', 'settings')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
            <span class="truncate ml-7">
              Settings
            </span>
          </a>

          <a href="#" @click="displayCategory('project', 'pages')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
            <span class="truncate ml-7">
              Pages
            </span>
          </a>

          <a href="/cms" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
            <span class="truncate ml-7">
              CMS
            </span>
          </a>

          <a href="/hosting-plans" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
            <span class="truncate ml-7">
              Hosting
            </span>
          </a>

          <a href="#" @click="displayCategory('application')" class="hover:hover:bg-gray-50 group rounded-md px-3 py-2 flex items-center font-medium">
            <svg class="group-hover:flex-shrink-0 -ml-1 mr-3 h-4 w-4" viewBox="0 0 24 24">
                <path fill="currentColor" d="M21.7 18.6V17.6L22.8 16.8C22.9 16.7 23 16.6 22.9 16.5L21.9 14.8C21.9 14.7 21.7 14.7 21.6 14.7L20.4 15.2C20.1 15 19.8 14.8 19.5 14.7L19.3 13.4C19.3 13.3 19.2 13.2 19.1 13.2H17.1C16.9 13.2 16.8 13.3 16.8 13.4L16.6 14.7C16.3 14.9 16.1 15 15.8 15.2L14.6 14.7C14.5 14.7 14.4 14.7 14.3 14.8L13.3 16.5C13.3 16.6 13.3 16.7 13.4 16.8L14.5 17.6V18.6L13.4 19.4C13.3 19.5 13.2 19.6 13.3 19.7L14.3 21.4C14.4 21.5 14.5 21.5 14.6 21.5L15.8 21C16 21.2 16.3 21.4 16.6 21.5L16.8 22.8C16.9 22.9 17 23 17.1 23H19.1C19.2 23 19.3 22.9 19.3 22.8L19.5 21.5C19.8 21.3 20 21.2 20.3 21L21.5 21.4C21.6 21.4 21.7 21.4 21.8 21.3L22.8 19.6C22.9 19.5 22.9 19.4 22.8 19.4L21.7 18.6M18 19.5C17.2 19.5 16.5 18.8 16.5 18S17.2 16.5 18 16.5 19.5 17.2 19.5 18 18.8 19.5 18 19.5M12.3 22H3C1.9 22 1 21.1 1 20V4C1 2.9 1.9 2 3 2H21C22.1 2 23 2.9 23 4V13.1C22.4 12.5 21.7 12 21 11.7V6H3V20H11.3C11.5 20.7 11.8 21.4 12.3 22Z" />
            </svg>
            <span class="truncate">
              Application
            </span>
          </a>

          <a href="#" @click="displayCategory('application', 'general')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
            <span class="truncate ml-7">
              General
            </span>
          </a>

          <a href="#" @click="displayCategory('application', 'seo')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
            <span class="truncate ml-7">
              SEO
            </span>
          </a>

          <a href="#" @click="displayCategory('application', 'includes')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
            <span class="truncate ml-7">
              Includes
            </span>
          </a>

          <a href="#" @click="displayCategory('application', 'routing')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
            <span class="truncate ml-7">
              Routing
            </span>
          </a>

          <a href="#" @click="displayCategory('application', 'environment')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
            <span class="truncate ml-7">
              Environment Variables
            </span>
          </a>

          <a href="#" @click="displayCategory('application', 'localisation')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
            <span class="truncate ml-7">
              Localisation
            </span>
          </a>

          <a href="#" @click="displayCategory('security')" class="hover:hover:bg-gray-50 group rounded-md px-3 py-2 flex items-center font-medium">
            <svg class="group-hover:flex-shrink-0 -ml-1 mr-3 h-4 w-4" viewBox="0 0 24 24">
              <path fill="currentColor" d="M12,12H19C18.47,16.11 15.72,19.78 12,20.92V12H5V6.3L12,3.19M12,1L3,5V11C3,16.55 6.84,21.73 12,23C17.16,21.73 21,16.55 21,11V5L12,1Z" />
            </svg>
            <span class="truncate">
              Security
            </span>
          </a>

          <a href="#" @click="displayCategory('security', 'authentication')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
            <span class="truncate ml-7">
              Authentication
            </span>
          </a>

          <a href="#" @click="displayCategory('security', 'encryption')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
            <span class="truncate ml-7">
              Encryption
            </span>
          </a>

          <!-- <a href="#" @click="displayCategory('server')" class="hover:hover:bg-gray-50 group rounded-md px-3 py-2 flex items-center font-medium">
            <svg class="group-hover:flex-shrink-0 -ml-1 mr-3 h-4 w-4" viewBox="0 0 24 24">
                <path fill="currentColor" d="M4,1H20A1,1 0 0,1 21,2V6A1,1 0 0,1 20,7H4A1,1 0 0,1 3,6V2A1,1 0 0,1 4,1M4,9H20A1,1 0 0,1 21,10V14A1,1 0 0,1 20,15H4A1,1 0 0,1 3,14V10A1,1 0 0,1 4,9M4,17H20A1,1 0 0,1 21,18V22A1,1 0 0,1 20,23H4A1,1 0 0,1 3,22V18A1,1 0 0,1 4,17M9,5H10V3H9V5M9,13H10V11H9V13M9,21H10V19H9V21M5,3V5H7V3H5M5,11V13H7V11H5M5,19V21H7V19H5Z" />
            </svg>
            <span class="truncate">
              Server
            </span>
          </a>

          <a href="#" @click="displayCategory('server', 'rateLimiting')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
            <span class="truncate ml-7">
              Robots.txt
            </span>
          </a>

          <a href="#" @click="displayCategory('server', 'rateLimiting')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
            <span class="truncate ml-7">
              Sitemap.xml
            </span>
          </a>

          <a href="#" @click="displayCategory('server', 'dns')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
            <span class="truncate ml-7">
              DNS
            </span>
          </a>

          <a href="#" @click="displayCategory('server', 'rateLimiting')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
            <span class="truncate ml-7">
              Rate Limiting
            </span>
          </a>

          <a href="#" @click="displayCategory('server', 'taskScheduling')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
            <span class="truncate ml-7">
              Task scheduling
            </span>
          </a>

          <a href="#" @click="displayCategory('server', 'loadBalancing')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
            <span class="truncate ml-7">
              Load balancing
            </span>
          </a> -->

          <a href="#" @click="displayCategory('storage')" @click="" class="hover:hover:bg-gray-50 group rounded-md px-3 py-2 flex items-center font-medium">
            <svg class="group-hover:flex-shrink-0 -ml-1 mr-3 h-4 w-4" viewBox="0 0 24 24">
                <path fill="currentColor" d="M12,3C7.58,3 4,4.79 4,7C4,9.21 7.58,11 12,11C16.42,11 20,9.21 20,7C20,4.79 16.42,3 12,3M4,9V12C4,14.21 7.58,16 12,16C16.42,16 20,14.21 20,12V9C20,11.21 16.42,13 12,13C7.58,13 4,11.21 4,9M4,14V17C4,19.21 7.58,21 12,21C16.42,21 20,19.21 20,17V14C20,16.21 16.42,18 12,18C7.58,18 4,16.21 4,14Z" />
            </svg>
            <span class="truncate">
              Storage
            </span>
          </a>

          <a href="#" @click="displayCategory('storage', 'connect')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
            <span class="truncate ml-7">
              Connect Database
            </span>
          </a>

          <a href="#" @click="displayCategory('storage', 'cloud')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
            <span class="truncate ml-7">
              Connect Cloud Storage
            </span>
          </a>

          <a href="#" @click="displayCategory('storage', 'cache')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
            <span class="truncate ml-7">
              Configure Cache
            </span>
          </a>

          <a href="#" @click="displayCategory('mail')" class="hover:hover:bg-gray-50 group rounded-md px-3 py-2 flex items-center font-medium">
            <svg class="group-hover:flex-shrink-0 -ml-1 mr-3 h-4 w-4" viewBox="0 0 24 24">
              <path fill="currentColor" d="M12,15C12.81,15 13.5,14.7 14.11,14.11C14.7,13.5 15,12.81 15,12C15,11.19 14.7,10.5 14.11,9.89C13.5,9.3 12.81,9 12,9C11.19,9 10.5,9.3 9.89,9.89C9.3,10.5 9,11.19 9,12C9,12.81 9.3,13.5 9.89,14.11C10.5,14.7 11.19,15 12,15M12,2C14.75,2 17.1,3 19.05,4.95C21,6.9 22,9.25 22,12V13.45C22,14.45 21.65,15.3 21,16C20.3,16.67 19.5,17 18.5,17C17.3,17 16.31,16.5 15.56,15.5C14.56,16.5 13.38,17 12,17C10.63,17 9.45,16.5 8.46,15.54C7.5,14.55 7,13.38 7,12C7,10.63 7.5,9.45 8.46,8.46C9.45,7.5 10.63,7 12,7C13.38,7 14.55,7.5 15.54,8.46C16.5,9.45 17,10.63 17,12V13.45C17,13.86 17.16,14.22 17.46,14.53C17.76,14.84 18.11,15 18.5,15C18.92,15 19.27,14.84 19.57,14.53C19.87,14.22 20,13.86 20,13.45V12C20,9.81 19.23,7.93 17.65,6.35C16.07,4.77 14.19,4 12,4C9.81,4 7.93,4.77 6.35,6.35C4.77,7.93 4,9.81 4,12C4,14.19 4.77,16.07 6.35,17.65C7.93,19.23 9.81,20 12,20H17V22H12C9.25,22 6.9,21 4.95,19.05C3,17.1 2,14.75 2,12C2,9.25 3,6.9 4.95,4.95C6.9,3 9.25,2 12,2Z" />
            </svg>
            <span class="truncate">
              Mail & Notifications
            </span>
          </a>

          <a href="#" @click="displayCategory('mail', 'provider')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
            <span class="truncate ml-7">
              Choose Provider
            </span>
          </a>

          <a href="#" @click="displayCategory('mail', 'mailSettings')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
            <span class="truncate ml-7">
              Configure
            </span>
          </a>

          <a href="#" @click="displayCategory('testing')" class="hover:hover:bg-gray-50 group rounded-md px-3 py-2 flex items-center font-medium">
            <!-- Heroicon name: outline/cog -->
            <svg class="group-hover:flex-shrink-0 -ml-1 mr-3 h-4 w-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="none" aria-hidden="true">
              <path fill="currentColor" d="M7,2V4H8V18A4,4 0 0,0 12,22A4,4 0 0,0 16,18V4H17V2H7M11,16C10.4,16 10,15.6 10,15C10,14.4 10.4,14 11,14C11.6,14 12,14.4 12,15C12,15.6 11.6,16 11,16M13,12C12.4,12 12,11.6 12,11C12,10.4 12.4,10 13,10C13.6,10 14,10.4 14,11C14,11.6 13.6,12 13,12M14,7H10V4H14V7Z" />
            </svg>
            <span class="truncate">
              Testing
            </span>
          </a>

          <a href="#" @click="displayCategory('testing', 'debugging')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
            <span class="truncate ml-7">
              Debugging
            </span>
          </a>

          <a href="#" @click="displayCategory('testing', 'logs')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
            <span class="truncate ml-7">
              Error logs
            </span>
          </a>

          <!-- <a href="#" @click="displayCategory('billing')" class="bg-gray-50 hover:bg-white group rounded-md px-3 py-2 flex items-center font-medium" aria-current="page">
            <svg class="flex-shrink-0 -ml-1 mr-3 h-4 w-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 10h18M7 15h1m4 0h1m-7 4h12a3 3 0 003-3V8a3 3 0 00-3-3H6a3 3 0 00-3 3v8a3 3 0 003 3z" />
            </svg>
            <span class="truncate">
              Subscriptions &amp; Billing
            </span>
          </a> -->

          <a href="#" @click="displayCategory('api')" class="hover:hover:bg-gray-50 group rounded-md px-3 py-2 flex items-center font-medium">
            <svg class="group-hover:flex-shrink-0 -ml-1 mr-3 h-4 w-4" viewBox="0 0 24 24">
              <path fill="currentColor" d="M7 7H5A2 2 0 0 0 3 9V17H5V13H7V17H9V9A2 2 0 0 0 7 7M7 11H5V9H7M14 7H10V17H12V13H14A2 2 0 0 0 16 11V9A2 2 0 0 0 14 7M14 11H12V9H14M20 9V15H21V17H17V15H18V9H17V7H21V9Z" />
            </svg>
            <span class="truncate">
              API
            </span>
          </a>

          <a href="#" @click="displayCategory('testing', 'logs')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
            <span class="truncate ml-7">
              Access
            </span>
          </a>

          <a href="#" @click="displayCategory('testing', 'logs')" class="hover:hover:bg-gray-50 group rounded-md px-3 flex items-center font-light">
            <span class="truncate ml-7">
              Endpoints
            </span>
          </a>
        </nav>
      </aside>

      <div class="lg:px-0 lg:col-span-10">

        <div v-if="category == ''" class="text-gray-300 text-xs">
          <div class="flex-1 flex items-stretch overflow-hidden">
            <main class=" text-black flex-1 overflow-y-auto">
              <div>
                <div class="px-4 sm:px-6 lg:max-w-6xl lg:mx-auto lg:px-8">
                  <div class="py-6 md:flex md:items-center md:justify-between lg:border-t lg:border-gray-200">
                    <div class="flex-1 min-w-0">
                      <!-- Profile -->
                      <div class="flex items-center">
                        <img class="hidden h-16 w-16 rounded-full sm:block" src="https://stellifysoftware.s3.eu-west-2.amazonaws.com/stellisoft-logo.jpg" alt="">
                        <div>
                          <div class="flex items-center">
                            <img class="h-16 w-16 rounded-full sm:hidden" src="https://stellifysoftware.s3.eu-west-2.amazonaws.com/stellisoft-logo.jpg" alt="">
                            <h1 class="ml-3 text-2xl font-bold leading-7 text-gray-900 sm:leading-9 sm:truncate">
                              @{{ settings.name ? settings.name : 'New Project' }}
                            </h1>
                          </div>
                          <p class="mt-6 flex flex-col sm:ml-3 sm:mt-1 sm:flex-row sm:flex-wrap">Project ID: @{{ settings.site_id }}</p>
                        </div>
                      </div>
                    </div>
                    <div v-if="userData.projects && userData.projects.length > 1" class="mt-6 flex space-x-3 md:mt-0 md:ml-4">
                      <div>
                        <label for="combobox" class="block text-sm font-medium text-gray-700">Switch project</label>
                        <div class="relative mt-1">
                          <input id="combobox" type="text" class="w-full rounded-md border border-gray-300 bg-white py-2 pl-3 pr-12 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-1 focus:ring-indigo-500 sm:text-sm" role="combobox" aria-controls="options" aria-expanded="false">
                          <button type="button" class="absolute inset-y-0 right-0 flex items-center rounded-r-md px-2 focus:outline-none">
                            <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                              <path fill-rule="evenodd" d="M10 3a1 1 0 01.707.293l3 3a1 1 0 01-1.414 1.414L10 5.414 7.707 7.707a1 1 0 01-1.414-1.414l3-3A1 1 0 0110 3zm-3.707 9.293a1 1 0 011.414 0L10 14.586l2.293-2.293a1 1 0 011.414 1.414l-3 3a1 1 0 01-1.414 0l-3-3a1 1 0 010-1.414z" clip-rule="evenodd" />
                            </svg>
                          </button>

                          <ul class="absolute z-10 mt-1 max-h-60 w-full overflow-auto bg-white text-base ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm" id="options" role="listbox">
                            <li  v-for="(site, index) in userData.projects" :key="index" :class="site == settings.site_id ? 'text-white bg-black' : 'text-gray-900'" class="relative cursor-default select-none py-2 pl-3 pr-9" id="option-0" role="option" tabindex="-1">
                              <span class="block truncate" :class="site == settings.site_id ? 'font-semibold' : ''">@{{ siteData[site].name }}</span>
                              <span :class="site == settings.site_id ? 'text-white' : 'text-white'" class="absolute inset-y-0 right-0 flex items-center pr-4">
                                <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                                </svg>
                              </span>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="mt-1 flex flex-col sm:flex-row sm:flex-wrap sm:mt-0 sm:space-x-8">
                    <div  class="mt-2 flex items-center text-sm">
                      <svg class="flex-shrink-0 mr-1.5 h-5 w-5" x-description="Heroicon name: solid/briefcase" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" aria-hidden="true">
                        <path fill="currentColor" d="M12,9A3,3 0 0,0 9,12A3,3 0 0,0 12,15A3,3 0 0,0 15,12A3,3 0 0,0 12,9M12,17A5,5 0 0,1 7,12A5,5 0 0,1 12,7A5,5 0 0,1 17,12A5,5 0 0,1 12,17M12,4.5C7,4.5 2.73,7.61 1,12C2.73,16.39 7,19.5 12,19.5C17,19.5 21.27,16.39 23,12C21.27,7.61 17,4.5 12,4.5Z" />
                      </svg>
                      <span v-show="settings.privacy == 1 || typeof settings.privacy == 'undefined'">Everyone</span>
                      <span v-show="settings.privacy == 2">Team</span>
                      <span v-show="settings.privacy == 3">Only you</span>
                    </div>
                    <div class="mt-2 flex items-center text-sm">
                      <svg class="flex-shrink-0 mr-1.5 h-5 w-5" x-description="Heroicon name: solid/briefcase" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" aria-hidden="true">
                        <path fill="currentColor" d="M13,14C9.64,14 8.54,15.35 8.18,16.24C9.25,16.7 10,17.76 10,19A3,3 0 0,1 7,22A3,3 0 0,1 4,19C4,17.69 4.83,16.58 6,16.17V7.83C4.83,7.42 4,6.31 4,5A3,3 0 0,1 7,2A3,3 0 0,1 10,5C10,6.31 9.17,7.42 8,7.83V13.12C8.88,12.47 10.16,12 12,12C14.67,12 15.56,10.66 15.85,9.77C14.77,9.32 14,8.25 14,7A3,3 0 0,1 17,4A3,3 0 0,1 20,7C20,8.34 19.12,9.5 17.91,9.86C17.65,11.29 16.68,14 13,14M7,18A1,1 0 0,0 6,19A1,1 0 0,0 7,20A1,1 0 0,0 8,19A1,1 0 0,0 7,18M7,4A1,1 0 0,0 6,5A1,1 0 0,0 7,6A1,1 0 0,0 8,5A1,1 0 0,0 7,4M17,6A1,1 0 0,0 16,7A1,1 0 0,0 17,8A1,1 0 0,0 18,7A1,1 0 0,0 17,6Z" />
                      </svg>
                      0 Branches
                    </div>
                    <div class="mt-2 flex items-center text-sm">
                      <svg class="flex-shrink-0 mr-1.5 h-5 w-5" x-description="Heroicon name: solid/location-marker" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" aria-hidden="true">
                        <path fill="currentColor" d="M4,5C2.89,5 2,5.89 2,7V17C2,18.11 2.89,19 4,19H20C21.11,19 22,18.11 22,17V7C22,5.89 21.11,5 20,5H4M4.5,7A1,1 0 0,1 5.5,8A1,1 0 0,1 4.5,9A1,1 0 0,1 3.5,8A1,1 0 0,1 4.5,7M7,7H20V17H7V7M8,8V16H11V8H8M12,8V16H15V8H12M16,8V16H19V8H16M9,9H10V10H9V9M13,9H14V10H13V9M17,9H18V10H17V9Z" />
                      </svg>
                      0 MB
                    </div>
                    <div class="mt-2 flex items-center text-sm">
                      <svg class="flex-shrink-0 mr-1.5 h-5 w-5" x-description="Heroicon name: solid/currency-dollar" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" aria-hidden="true">
                        <path fill="currentColor" d="M21.41 11.58L12.41 2.58A2 2 0 0 0 11 2H4A2 2 0 0 0 2 4V11A2 2 0 0 0 2.59 12.42L11.59 21.42A2 2 0 0 0 13 22A2 2 0 0 0 14.41 21.41L21.41 14.41A2 2 0 0 0 22 13A2 2 0 0 0 21.41 11.58M13 20L4 11V4H11L20 13M6.5 5A1.5 1.5 0 1 1 5 6.5A1.5 1.5 0 0 1 6.5 5Z" />
                      </svg>
                      0 Tags
                    </div>
                    <div class="mt-2 flex items-center text-sm">
                      <svg class="flex-shrink-0 mr-1.5 h-5 w-5" x-description="Heroicon name: solid/calendar" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" aria-hidden="true">
                        <path fill="currentColor" d="M17,12C17,14.42 15.28,16.44 13,16.9V21H11V16.9C8.72,16.44 7,14.42 7,12C7,9.58 8.72,7.56 11,7.1V3H13V7.1C15.28,7.56 17,9.58 17,12M12,9A3,3 0 0,0 9,12A3,3 0 0,0 12,15A3,3 0 0,0 15,12A3,3 0 0,0 12,9Z" />
                      </svg>
                      0 Commits
                    </div>
                  </div>
                </div>
              </div>
              <div class="pt-8 max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                <div class="flex">
                  <div class="ml-6 bg-gray-100 p-0.5 rounded-lg flex items-center sm:hidden">
                    <button type="button" class="p-1.5 rounded-md text-gray-400 hover:bg-white hover:shadow-sm focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                      <svg class="h-5 w-5" x-description="Heroicon name: solid/view-list" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                        <path fill-rule="evenodd" d="M3 4a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zm0 4a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zm0 4a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zm0 4a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd"></path>
                      </svg>
                      <span class="sr-only">Use list view</span>
                    </button>
                    <button type="button" class="ml-0.5 bg-white p-1.5 rounded-md shadow-sm text-gray-400 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                      <svg class="h-5 w-5" x-description="Heroicon name: solid/view-grid" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                        <path d="M5 3a2 2 0 00-2 2v2a2 2 0 002 2h2a2 2 0 002-2V5a2 2 0 00-2-2H5zM5 11a2 2 0 00-2 2v2a2 2 0 002 2h2a2 2 0 002-2v-2a2 2 0 00-2-2H5zM11 5a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2V5zM11 13a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2v-2z"></path>
                      </svg>
                      <span class="sr-only">Use grid view</span>
                    </button>
                  </div>
                </div>

                <!-- Tabs -->
                <div class="mt-3 sm:mt-2">
                  <div class="sm:hidden">
                    <label for="tabs" class="sr-only">Select a tab</label>
                    <!-- Use an "onChange" listener to redirect the user to the selected tab URL. -->
                    <select id="tabs" name="tabs" class="block w-full pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md">
                      <option selected="">Web pages</option>
                      <option>Templates</option>
                    </select>
                  </div>
                  <div class="hidden sm:block">
                    <div class="flex items-center border-b border-gray-200">
                      <nav class="flex-1 -mb-px flex space-x-6 xl:space-x-8" aria-label="Tabs">
                        
                          <a href="#" aria-current="page" class="border-black text-black whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm" x-state:on="Current" x-state:off="Default" x-state-description="Current: &quot;border-indigo-500 text-indigo-600&quot;, Default: &quot;border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300&quot;">
                            Web pages
                          </a>
                        
                          <a href="#" class="border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300 whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm" x-state-description="undefined: &quot;border-indigo-500 text-indigo-600&quot;, undefined: &quot;border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300&quot;">
                            Templates
                          </a>
                        
                      </nav>
                      <div class="hidden ml-6 bg-gray-100 p-0.5 rounded-lg items-center sm:flex">
                        <button type="button" class="p-1.5 rounded-md text-gray-400 hover:bg-white hover:shadow-sm focus:outline-none focus:ring-2 focus:ring-inset focus:black">
                          <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                            <path fill-rule="evenodd" d="M3 4a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zm0 4a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zm0 4a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zm0 4a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd"></path>
                          </svg>
                          <span class="sr-only">Use list view</span>
                        </button>
                        <button type="button" class="ml-0.5 bg-white p-1.5 rounded-md shadow-sm text-gray-400 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-black">
                          <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                            <path d="M5 3a2 2 0 00-2 2v2a2 2 0 002 2h2a2 2 0 002-2V5a2 2 0 00-2-2H5zM5 11a2 2 0 00-2 2v2a2 2 0 002 2h2a2 2 0 002-2v-2a2 2 0 00-2-2H5zM11 5a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2V5zM11 13a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2v-2z"></path>
                          </svg>
                          <span class="sr-only">Use grid view</span>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Web pages -->
                <section class="mt-8 pb-16" aria-labelledby="gallery-heading">
                  <h2 id="gallery-heading" class="sr-only">Recently viewed</h2>
                  <ul role="list" class="grid grid-cols-2 gap-x-4 gap-y-8 sm:grid-cols-3 sm:gap-x-6 md:grid-cols-4 lg:grid-cols-3 xl:grid-cols-4 xl:gap-x-8">
                    
                      <li class="relative">
                        <form action="/createPage" method="POST">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <div class="ring-2 ring-offset-2 ring-black group block w-full aspect-w-10 aspect-h-7 rounded-lg bg-gray-100 overflow-hidden">
                            <button type="submit" class="flex justify-center items-center p-3 ">
                              <svg class="h-10 w-10 text-black" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                <path fill-rule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clip-rule="evenodd"></path>
                              </svg>
                            </button>
                          </div>
                          <p class="mt-2 block text-sm font-medium text-gray-900 truncate pointer-events-none">INSERT BLANK PAGE</p>
                        </form>
                      </li>
                    
                      <li v-for="(body, index) in bodies" :key="index" class="relative">
                        <a :href="body.path != '/' ? '/' + body.path + '?edit' : '/?edit'">
                          <div style="background-image: url('https://d3e54v103j8qbb.cloudfront.net/img/placeholder.4daa7c5fe0.png')" class="bg-contain ring-2 ring-offset-2 ring-offset-gray-100 ring-black group block w-full aspect-w-10 aspect-h-7 rounded-lg bg-gray-100 overflow-hidden">
                            <button type="button" class="absolute inset-0 focus:outline-none">
                              <span class="sr-only">@{{ body.path }}</span>
                            </button>
                          </div>
                          <p class="mt-2 block text-sm font-medium text-gray-900 truncate uppercase pointer-events-none">@{{ body.path }}</p>
                        </a>
                      </li>
                  </ul>
                </section>


              </div>
            </main>

            <!-- Details sidebar -->
            <aside class="bg-white text-black hidden w-96 p-8 overflow-y-auto lg:block">
              <div class="pb-16 space-y-6">
                <div>
                  <div class="flex items-start justify-between">
                    <div>
                      <h2 class="text-lg font-medium"><span class="sr-only">Details for </span>About</h2>
                      <div class="mt-2 flex items-center justify-between">
                        <textarea class="outline-none border-none p-0" cols="30" rows="3" v-model="settings.description" class="text-sm text-gray-500 italic" placeholder="Add a project description."></textarea>
                      </div>
                    </div>
                    <button type="button" class="ml-4 rounded-full h-8 w-8 flex items-center justify-center hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-indigo-500">
                      <svg class="h-6 w-6" x-description="Heroicon name: outline/heart" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="none" aria-hidden="true">
                        <path fill="currentColor" d="M12,8A4,4 0 0,1 16,12A4,4 0 0,1 12,16A4,4 0 0,1 8,12A4,4 0 0,1 12,8M12,10A2,2 0 0,0 10,12A2,2 0 0,0 12,14A2,2 0 0,0 14,12A2,2 0 0,0 12,10M10,22C9.75,22 9.54,21.82 9.5,21.58L9.13,18.93C8.5,18.68 7.96,18.34 7.44,17.94L4.95,18.95C4.73,19.03 4.46,18.95 4.34,18.73L2.34,15.27C2.21,15.05 2.27,14.78 2.46,14.63L4.57,12.97L4.5,12L4.57,11L2.46,9.37C2.27,9.22 2.21,8.95 2.34,8.73L4.34,5.27C4.46,5.05 4.73,4.96 4.95,5.05L7.44,6.05C7.96,5.66 8.5,5.32 9.13,5.07L9.5,2.42C9.54,2.18 9.75,2 10,2H14C14.25,2 14.46,2.18 14.5,2.42L14.87,5.07C15.5,5.32 16.04,5.66 16.56,6.05L19.05,5.05C19.27,4.96 19.54,5.05 19.66,5.27L21.66,8.73C21.79,8.95 21.73,9.22 21.54,9.37L19.43,11L19.5,12L19.43,13L21.54,14.63C21.73,14.78 21.79,15.05 21.66,15.27L19.66,18.73C19.54,18.95 19.27,19.04 19.05,18.95L16.56,17.95C16.04,18.34 15.5,18.68 14.87,18.93L14.5,21.58C14.46,21.82 14.25,22 14,22H10M11.25,4L10.88,6.61C9.68,6.86 8.62,7.5 7.85,8.39L5.44,7.35L4.69,8.65L6.8,10.2C6.4,11.37 6.4,12.64 6.8,13.8L4.68,15.36L5.43,16.66L7.86,15.62C8.63,16.5 9.68,17.14 10.87,17.38L11.24,20H12.76L13.13,17.39C14.32,17.14 15.37,16.5 16.14,15.62L18.57,16.66L19.32,15.36L17.2,13.81C17.6,12.64 17.6,11.37 17.2,10.2L19.31,8.65L18.56,7.35L16.15,8.39C15.38,7.5 14.32,6.86 13.12,6.62L12.75,4H11.25Z" />
                      </svg>
                      <span class="sr-only">Settings</span>
                    </button>
                  </div>
                </div>
                <div>
                  <h3 class="font-medium">System Information</h3>
                  <dl class="mt-2 border-t border-b border-black text-black divide-y divide-gray-200">
                    
                      <div class="py-3 flex justify-between text-sm font-medium">
                        <dt>Front-end</dt>
                        <dd class="mt-3 flex items-center text-sm font-medium sm:mt-0"><svg xmlns="http://www.w3.org/2000/svg" class="flex-shrink-0 mr-1.5 h-5 w-5 text-green-400" version="1.1" viewBox="0 0 261.76 226.69"><g transform="matrix(1.3333 0 0 -1.3333 -76.311 313.34)"><g transform="translate(178.06 235.01)"><path d="m0 0-22.669-39.264-22.669 39.264h-75.491l98.16-170.02 98.16 170.02z" fill="#41b883"></path></g><g transform="translate(178.06 235.01)"><path d="m0 0-22.669-39.264-22.669 39.264h-36.227l58.896-102.01 58.896 102.01z" fill="#34495e"></path></g></g></svg> Vue (v3) </dd>
                      </div>
                    
                      <div class="py-3 flex justify-between text-sm font-medium">
                        <dt>Back-end</dt>
                        <dd class="flex items-center text-sm font-medium"><svg class="flex-shrink-0 mr-1.5 h-5 w-5 text-red-400" x-description="Heroicon name: solid/office-building" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" aria-hidden="true"><path fill="currentColor" d="M21.7 6.53C21.71 6.55 21.71 6.58 21.71 6.61V10.9C21.71 11 21.65 11.12 21.56 11.17L17.95 13.25V17.36C17.95 17.47 17.9 17.57 17.8 17.63L10.28 21.96C10.26 21.97 10.24 22 10.22 22L10.2 22C10.15 22 10.09 22 10.04 22C10.03 22 10 22 10 22C10 22 10 21.97 9.96 21.96L2.44 17.63C2.35 17.58 2.29 17.47 2.29 17.36V4.5C2.29 4.45 2.29 4.42 2.3 4.4C2.3 4.39 2.31 4.38 2.31 4.37C2.31 4.35 2.32 4.34 2.33 4.32C2.33 4.31 2.34 4.3 2.35 4.29C2.36 4.28 2.37 4.27 2.38 4.26C2.39 4.25 2.4 4.24 2.41 4.23C2.42 4.22 2.43 4.21 2.44 4.21L6.2 2.04C6.3 2 6.42 2 6.5 2.04L10.28 4.21H10.28C10.29 4.22 10.3 4.22 10.31 4.23C10.32 4.24 10.33 4.25 10.34 4.26C10.35 4.27 10.36 4.28 10.37 4.29C10.38 4.3 10.39 4.31 10.39 4.32C10.4 4.34 10.41 4.35 10.41 4.37C10.41 4.38 10.42 4.39 10.42 4.4C10.43 4.43 10.43 4.45 10.43 4.5V12.5L13.57 10.72V6.61C13.57 6.58 13.57 6.55 13.58 6.53L13.59 6.5C13.59 6.5 13.6 6.47 13.61 6.45C13.61 6.44 13.62 6.43 13.63 6.42C13.64 6.41 13.65 6.4 13.66 6.39C13.67 6.38 13.68 6.37 13.69 6.36C13.7 6.35 13.71 6.34 13.72 6.34L17.5 4.17C17.58 4.11 17.7 4.11 17.8 4.17L21.56 6.34C21.57 6.34 21.58 6.35 21.59 6.36L21.62 6.39C21.63 6.4 21.64 6.41 21.65 6.42C21.66 6.43 21.66 6.44 21.67 6.45C21.68 6.47 21.68 6.5 21.69 6.5C21.7 6.5 21.7 6.5 21.7 6.53M21.09 10.72V7.15L17.95 8.95V12.5L21.09 10.72M17.33 17.18V13.6L10.43 17.54V21.15L17.33 17.18M2.91 5V17.18L9.81 21.15V17.54L6.21 15.5L6.2 15.5L6.2 15.5C6.19 15.5 6.18 15.5 6.17 15.47C6.16 15.47 6.15 15.46 6.14 15.45V15.45C6.13 15.44 6.12 15.43 6.11 15.42C6.1 15.41 6.1 15.4 6.09 15.39V15.39C6.08 15.37 6.08 15.36 6.07 15.35C6.07 15.33 6.06 15.32 6.06 15.31C6.05 15.3 6.05 15.28 6.05 15.27C6.05 15.25 6.05 15.24 6.05 15.23V6.82L2.91 5M6.36 2.68L3.23 4.5L6.36 6.28L9.5 4.5L6.36 2.68M9.81 12.88V5L6.67 6.82V14.69L9.81 12.88M17.64 4.8L14.5 6.61L17.64 8.41L20.77 6.61L17.64 4.8M17.33 8.95L14.19 7.15V10.72L17.33 12.5V8.95M10.12 17L17 13.06L13.88 11.26L7 15.23L10.12 17Z"></path></svg> Laravel (v8) </dd>
                      </div>
                    
                      <div class="py-3 flex justify-between text-sm font-medium">
                        <dt>Cloud Provider</dt>
                        <dd class="flex items-center text-sm font-medium"><svg class="flex-shrink-0 mr-1.5 h-5 w-5 text-blue-400" x-description="Heroicon name: solid/office-building" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" aria-hidden="true"><path fill="currentColor" d="M6 12H2C2 6.5 6.5 2 12 2C17.5 2 22 6.5 22 12C22 17.5 17.5 22 12 22V18H8V14H12V18C15.32 18 18 15.31 18 12C18 8.69 15.31 6 12 6C8.69 6 6 8.69 6 12M8 18V21H5V18H8M3 16H5V18H3V16Z" /></svg> Digital Ocean </dd>
                      </div>
                    
                      <div class="py-3 flex justify-between text-sm font-medium">
                        <dt>Memory</dt>
                        <dd>1GB</dd>
                      </div>
                    
                      <div class="py-3 flex justify-between text-sm font-medium">
                        <dt>Storage</dt>
                        <dd>512MB</dd>
                      </div>
                    
                  </dl>
                </div>
                <div class="text-black">
                  <h3 class="font-medium">Collaborators</h3>
                  <ul role="list" class="mt-2 border-t border-b border-black divide-y divide-gray-200">
                    <li class="py-2 flex justify-between items-center">
                      <button type="button" class="group -ml-1 p-1 rounded-md flex items-center focus:outline-none focus:ring-2 focus:ring-indigo-500">
                        <span class="w-8 h-8 rounded-full border-2 border-dashed border-black flex items-center justify-center">
                          <svg class="h-5 w-5" x-description="Heroicon name: solid/plus-sm" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                            <path fill-rule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clip-rule="evenodd"></path>
                          </svg>
                        </span>
                        <span class="ml-4 text-sm font-medium">Add collaborator</span>
                      </button>
                    </li>
                  </ul>
                </div>
              </div>
            </aside>
          </div>
        </div>

        <div v-if="category == 'project'" class="text-gray-300 text-xs">
          <section>
            <div class="border border-black sm:overflow-hidden">
              <div class="flex items-center justify-between bg-gray-900">
                <h2 class="p-1 uppercase font-medium">Project</h2>
              </div>
              <div @click="displayCategory('project', 'settings')" class="cursor-pointer flex items-center justify-between p-2 bg-gray-900">
                <div class="flex items-center justify-center">
                  <svg aria-hidden="true" focusable="false" width="16" height="16" viewBox="0 0 16 16" class="text-gray-300 block">
                    <path d="M4 6l3 .01h2L12 6l-4 4-4-4z" fill="currentColor"></path>
                  </svg> 
                  <span class="ml-2 uppercase">Settings</span>
                </div>
              </div>
              <div v-if="subcategory == 'settings'" class="bg-gray-700 py-2 px-4 sm:p-6">
              <div class="space-y-5">
                  <div>
                      <label for="projectName" class="block font-medium">Project Name</label>
                      <input type="text" v-model="settings.name" name="projectName" id="projectName" autocomplete="off" class="block bg-gray-800 h-8 text-xs">
                  </div>
                </div>
              </div>
              <div @click="displayCategory('project', 'pages')" class="cursor-pointer flex items-center justify-between p-2 bg-gray-900">
                <div class="flex items-center justify-center">
                  <svg aria-hidden="true" focusable="false" width="16" height="16" viewBox="0 0 16 16" class="text-gray-300 block">
                    <path d="M4 6l3 .01h2L12 6l-4 4-4-4z" fill="currentColor"></path>
                  </svg> 
                  <span class="ml-2 uppercase">Pages</span>
                </div>
              </div>
              <div v-if="subcategory == 'pages'" class="bg-gray-700">
                <div class="flex items-center justify-between p-2 bg-gray-900">
                  <form action="/createPage" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="bg-gray-700 inline-flex items-center px-4 py-2 border border-black text-xs font-medium font-mono">New page</button>
                  </form>
                </div>
                <div>
                  <div class="hiddensm:block">
                    <div class="align-middle inline-block min-w-full">
                      <table class="min-w-full">
                        <thead>
                          <tr>
                            <th class="px-6 py-1 text-left text-xs font-medium uppercase tracking-wider">
                              <span class="lg:pl-2">Page</span>
                            </th>
                            <th class="px-6 py-1 text-left text-xs font-medium uppercase tracking-wider">
                              Contributors
                            </th>
                            <th class="hidden md:table-cell px-6 py-1 text-right text-xs font-medium uppercase tracking-wider">
                              Last updated
                            </th>
                            <th class="pr-6 py-1 text-right text-xs font-medium uppercase tracking-wider">
                              Actions
                            </th>
                          </tr>
                        </thead>
                        <tbody class="bg-white divide-y divide-gray-100">
                          <tr v-for="(body, index) in bodies" :key="index">
                            <td class="px-6 py-3 max-w-0 w-full whitespace-nowrap text-sm font-medium text-gray-900">
                              <div class="flex items-center space-x-3 lg:pl-2">
                                <a :href="body.path != '/' ? '/' + body.path + '?edit' : '/?edit'" class="truncate hover:text-gray-600">
                                  <span>
                                  @{{ body.path }}
                                  </span>
                                </a>
                              </div>
                            </td>
                            <td class="px-6 py-3 text-sm text-gray-500 font-medium">
                              <div class="flex items-center space-x-2">
                                <div class="flex flex-shrink-0 -space-x-1">
                                  <img title="You" class="max-w-none h-6 w-6 rounded-full ring-2 ring-white" :src="user.profileImage ? user.profileImage : 'https://stellifysoftware.s3.eu-west-2.amazonaws.com/stellisoft-logo.jpg'" :alt="user.name">
                                </div>
                                <!-- <span class="flex-shrink-0 text-xs leading-5 font-medium">+8</span> -->
                              </div>
                            </td>
                            <td class="hidden md:table-cell px-6 py-3 whitespace-nowrap text-sm text-gray-500 text-right">
                            @{{ new Date(body.created_at).toLocaleString() }}

                            </td>
                            <td class="px-6 py-3 whitespace-nowrap text-right text-sm font-medium">
                              <div class="flex items-center text-gray-900">
                                <form action="deletePage" method="POST">
                                  <input type="hidden" name="_token" :value="csrf">
                                  <input type="hidden" name="path" :value="body.path">
                                  <button class="p-1" title="Delete">
                                    <svg class="h-5 w-5" viewBox="0 0 24 24">
                                      <path fill="currentColor" d="M19,4H15.5L14.5,3H9.5L8.5,4H5V6H19M6,19A2,2 0 0,0 8,21H16A2,2 0 0,0 18,19V7H6V19Z" />
                                    </svg>
                                  </button>
                                </form>
                                <a :href="body.path" class="p-1" title="Preview">
                                  <svg viewBox="0 0 18 14" class="h-5 w-5">
                                    <path fill="currentColor" d="M9.028 1C4.596 1 1 6.94 1 6.94s3.596 6.1 8.028 6.1c4.434 0 8.027-6.1 8.027-6.1S13.462 1 9.028 1zM9 11a4 4 0 01-4-4c0-2.027 1.512-3.683 3.467-3.946A2.48 2.48 0 008 4.5 2.5 2.5 0 0010.5 7a2.49 2.49 0 002.234-1.4c.164.437.266.906.266 1.4a4 4 0 01-4 4z"></path>
                                  </svg>
                                </a>
                              </div>
                            </td>
                          </tr>

                          <!-- More projects... -->
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>  
          </section>
        </div>

        <div v-if="category == 'application'" class="text-gray-300 text-xs">
          <section>
            <div class="border border-black sm:overflow-hidden">
              <div class="flex items-center justify-between bg-gray-900">
                <h2 class="p-1 uppercase font-medium">Application</h2>
              </div>
              <div @click="displayCategory('application', 'general')" class="cursor-pointer flex items-center justify-between p-2 bg-gray-900">
                <div class="flex items-center justify-center">
                  <svg aria-hidden="true" focusable="false" width="16" height="16" viewBox="0 0 16 16" class="text-gray-300 block">
                    <path d="M4 6l3 .01h2L12 6l-4 4-4-4z" fill="currentColor"></path>
                  </svg> 
                  <span class="ml-2 uppercase">General</span>
                </div>
              </div>
              <div v-if="subcategory == 'general'" class="bg-gray-700 py-2 px-4 sm:p-6">
                <div class="space-y-5">
                  <div class="mt-2">
                        <label for="appName" class="block font-medium">App name</label>
                        <input type="text" v-model="settings.appName" name="appName" id="appName" autocomplete="off" class="block bg-gray-800 h-8 text-xs">
                    </div>
                  <div>
                    <label class="text-xs leading-6 font-medium">Render all views on server</label> 
                    <input type="checkbox" disabled="disabled" class="block focus:outline-none focus:border-none focus:shadow-none h-4 w-4 text-blue-600 border-gray-300 rounded">
                  </div>
                  <div>
                      <label class="block font-medium">Upload Favicon</label>
                      <input type="file" class="p-2 block">
                  </div>
                  <div>
                    <label class="text-xs leading-6 font-medium">Activate PWA</label> 
                    <input type="checkbox" v-model="pwa" class="block focus:outline-none focus:border-none focus:shadow-none h-4 w-4 text-blue-600 border-gray-300 rounded">
                  </div>
                  <div v-if="pwa" class="space-y-5">
                    <div class="mt-2">
                        <label for="shortName" class="block font-medium">Short app name</label>
                        <input type="text" v-model="settings.shortName" name="shortName" id="shortName" autocomplete="off" class="block bg-gray-800 h-8 text-xs">
                    </div>
                    <div class="mt-2">
                        <label for="backgroundColor" class="block font-medium">Background color</label>
                        <input type="color" v-model="settings.backgroundColor" name="backgroundColor" id="backgroundColor" autocomplete="off" class="block bg-gray-800 h-8 text-xs">
                    </div>
                    <div class="mt-2">
                        <label for="themeColor" class="block font-medium">Theme color</label>
                        <input type="color" v-model="settings.themeColor" name="themeColor" id="themeColor" autocomplete="off" class="block bg-gray-800 h-8 text-xs">
                    </div>
                    <div>
                      <label for="displayMode" class="block font-medium">Select display mode</label>
                      <select class="bg-gray-800 h-8 block text-xs" name="displayMode" id="displayMode" v-model="settings.displayMode">
                        <option disabled="disabled" value="">Please select one</option>
                        <option value="browser" selected>Browser</option>
                        <option value="standalone">Standalone</option>
                        <option value="minimal">Minimal UI</option>
                        <option value="fullscreen">Fullscreen</option>
                      </select>
                    </div>
                    <div>
                      <label for="orientation" class="block font-medium">Select orientation</label>
                      <select class="bg-gray-800 h-8 block text-xs" name="orientation" id="orientation" v-model="settings.orientation">
                        <option disabled="disabled" value="">Please select one</option>
                        <option value="any" selected>Any</option>
                        <option value="portrait" selected>Portrait</option>
                        <option value="landscape">Landscape</option>
                      </select>
                    </div>
                    <div class="mt-2">
                        <label for="applicationScope" class="block font-medium">Application Scope</label>
                        <input type="color" v-model="settings.applicationScope" name="applicationScope" id="applicationScope" autocomplete="off" class="block bg-gray-800 h-8 text-xs">
                    </div>
                    <div class="mt-2">
                        <label for="startUrl" class="block font-medium">Start URL</label>
                        <input type="color" v-model="settings.startUrl" name="startUrl" id="startUrl" autocomplete="off" class="block bg-gray-800 h-8 text-xs">
                    </div>
                    <div>
                        <label class="block font-medium">Generate Icons</label>
                        <p class="text-xs">The Web App Manifest allows for specifying icons of varying sizes. Upload a 512x512 image for the icon and we'll generate the remaining sizes.</p>
                        <input type="file" class="p-2 block">
                    </div>
                  </div>
                </div>
              </div>
              <div @click="displayCategory('application', 'seo')" class="cursor-pointer flex items-center justify-between p-2 bg-gray-900">
                <div class="flex items-center justify-center">
                  <svg aria-hidden="true" focusable="false" width="16" height="16" viewBox="0 0 16 16" class="text-gray-300 block">
                    <path d="M4 6l3 .01h2L12 6l-4 4-4-4z" fill="currentColor"></path>
                  </svg> 
                  <span class="ml-2 uppercase">SEO</span>
                </div>
              </div>
              <div v-if="subcategory == 'seo'" class="bg-gray-700 py-2 px-4 sm:p-6">
                <div class="space-y-5">
                  <div class="mt-2">
                      <label for="domain" class="block font-medium">Domain</label>
                      <input type="text" v-model="settings.domain" name="domain" id="domain" autocomplete="off" class="block bg-gray-800 h-8 text-xs">
                  </div>
                  <div class="mt-2">
                      <label for="meta_title" class="block font-medium">Meta Title</label>
                      <input type="text" v-model="settings.meta_title" name="meta_title" id="meta_title" autocomplete="off" class="block bg-gray-800 h-8 text-xs">
                  </div>
                  <div class="mt-2">
                      <label for="meta_description" class="block font-medium">Meta Description</label>
                      <textarea v-model="settings.meta_description" id="meta_description" name="meta_description" rows="3" class="block bg-gray-800 text-xs"></textarea>
                  </div>
                  <div v-if="settings.meta_title || settings.meta_description">
                    <p class="mt-2 text-sm">Preview:</p>
                    <div class="mt-2 p-2 bg-white text-black">
                        <p v-if="settings.domain" class="inline-block text-base">@{{ (settings.domain ? settings.domain : '') }}</p>
                        <a href="#" v-if="settings.meta_title" class="block text-lg hover:underline text-blue-700">@{{ settings.meta_title }}</a>
                        <p v-if="settings.meta_description" class="text-sm">@{{ settings.meta_description }}</p>
                    </div>
                  </div>
                </div>
              </div>
              <div @click="displayCategory('application', 'includes')" class="cursor-pointer flex items-center justify-between p-2 bg-gray-900">
                <div class="flex items-center justify-center">
                  <svg aria-hidden="true" focusable="false" width="16" height="16" viewBox="0 0 16 16" class="text-gray-300 block">
                    <path d="M4 6l3 .01h2L12 6l-4 4-4-4z" fill="currentColor"></path>
                  </svg> 
                  <span class="ml-2 uppercase">Includes</span>
                </div>
              </div>
              <div v-if="subcategory == 'includes'" class="bg-gray-700 py-2 px-4 sm:p-6">
                <div class="space-y-5">
                  <div>
                    <div>
                      <label class="block font-medium">External Scripts</label>
                      <input type="text" v-model="script" name="script" placeholder="e.g. https://js.stripe.com/v3/" class="block bg-gray-800 h-8">
                      <button @click="addScript">Add</button>
                    </div>
                    <div class="mt-2" v-for="(script, index) in settings.scripts">
                        <span class="block">@{{ script.src }}</span>
                        <label>Async<input type="checkbox" v-model="settings.scripts[index].async" class="ml-2 inline-block focus:outline-none focus:border-none focus:shadow-none h-4 w-4 text-blue-600 border-gray-300 rounded"></label>
                        <label class="ml-2">Defer<input type="checkbox" v-model="settings.scripts[index].defer" class="ml-2 inline-block focus:outline-none focus:border-none focus:shadow-none h-4 w-4 text-blue-600 border-gray-300 rounded"></label>
                        <a class="ml-2 hover:underline" href="#" @click="removeScript(index)">Remove<svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" /></svg></a>
                    </div>
                  </div>
                  <div>
                      <div>
                        <label class="block font-medium">External Links</label>
                        <input type="text" v-model="link" name="link" placeholder="e.g. https://yourwebsite.com/style.css" class="block bg-gray-800 h-8">
                        <button @click="addLink">Add</button>
                      </div>
                      <div class="mt-2" v-for="(link, index) in settings.links">
                          <span>@{{ link.href }}</span>
                          <a class="ml-2 hover:underline" href="#" @click="removeLink(index)">Remove</a>
                      </div>
                  </div>
                  <!-- <label class="block font-medium">Inline Script (max. 10000 characters)</label> -->
                  <!-- <prism-editor class="my-editor" v-model="code" :highlight="highlighter" line-numbers :readonly="!(user.plan > 1)"></prism-editor> -->
                </div>
              </div>
              <div @click="displayCategory('application', 'routing')" class="cursor-pointer flex items-center justify-between p-2 bg-gray-900">
                <div class="flex items-center justify-center">
                  <svg aria-hidden="true" focusable="false" width="16" height="16" viewBox="0 0 16 16" class="text-gray-300 block">
                    <path d="M4 6l3 .01h2L12 6l-4 4-4-4z" fill="currentColor"></path>
                  </svg> 
                  <span class="ml-2 uppercase">Routing</span>
                </div>
              </div>
              <div v-if="subcategory == 'routing'" class="bg-gray-700 py-6 px-4 sm:p-6">
                <div class="space-y-5">
                  <div>
                    <label for="errorRedirect" class="block font-medium">404 error redirect path</label>
                    <input type="text" v-model="settings.errorRedirect"  name="errorRedirect" id="errorRedirect" class="bg-gray-800 h-8 block">
                  </div>
                  <div>
                    <label for="unauthenticatedRedirect" class="block font-medium">Unauthenticated redirect path</label>
                    <input type="text" v-model="settings.unauthenticatedRedirect"  name="unauthenticatedRedirect" id="unauthenticatedRedirect" class="bg-gray-800 h-8 block">
                  </div>
                  <div>
                    <label for="loginRedirect" class="block font-medium">Login redirect path</label>
                    <input type="text" v-model="settings.loginRedirect"  name="loginRedirect" id="loginRedirect" class="bg-gray-800 h-8 block">
                  </div>
                  <div>
                    <label for="registerRedirect" class="block font-medium">Registration redirect path</label>
                    <input type="text" v-model="settings.registerRedirect"  name="registerRedirect" id="registerRedirect" class="bg-gray-800 h-8 block">
                  </div>
                  <div>
                    <label for="passwordResetRedirect" class="block font-medium">Password reset redirect path</label>
                    <input type="text" v-model="settings.passwordResetRedirect"  name="passwordResetRedirect" id="passwordResetRedirect" class="bg-gray-800 h-8 block">
                  </div>
                </div>
              </div>
              <div @click="displayCategory('application', 'environment')" class="cursor-pointer flex items-center justify-between p-2 bg-gray-900">
                <div class="flex items-center justify-center">
                  <svg aria-hidden="true" focusable="false" width="16" height="16" viewBox="0 0 16 16" class="text-gray-300 block">
                    <path d="M4 6l3 .01h2L12 6l-4 4-4-4z" fill="currentColor"></path>
                  </svg> 
                  <span class="ml-2 uppercase">Environment Variables</span>
                </div>
              </div>
              <div v-if="subcategory == 'environment'" class="bg-gray-700 py-2 px-4 sm:p-6">
                <pre class="scrollbar-none overflow-x-auto p-6 text-sm leading-snug language-html text-white bg-black bg-opacity-75">
                    <code style="white-space:pre;">
                        @{{ JSON.stringify(settings, undefined, 4).replace('{', '').replace('}', '') }}
                    </code>
                </pre>
              </div>
              <div @click="displayCategory('application', 'localisation')" class="cursor-pointer flex items-center justify-between p-2 bg-gray-900">
                <div class="flex items-center justify-center">
                  <svg aria-hidden="true" focusable="false" width="16" height="16" viewBox="0 0 16 16" class="text-gray-300 block">
                    <path d="M4 6l3 .01h2L12 6l-4 4-4-4z" fill="currentColor"></path>
                  </svg> 
                  <span class="ml-2 uppercase">Localisation</span>
                </div>
              </div>
              <div v-if="subcategory == 'localisation'" class="bg-gray-700 py-2 px-4 sm:p-6">
                Please upgrade to a paid plan.
              </div>
            </div>
          </section>
        </div>

        <div v-if="category == 'security'" class="text-gray-300 text-xs">
          <section>
            <div class="border border-black sm:overflow-hidden">
              <div class="flex items-center justify-between bg-gray-900">
                <h2 class="p-1 uppercase font-medium">Security</h2>
              </div>
              <div @click="displayCategory('application', 'authentication')" class="cursor-pointer flex items-center justify-between p-2 bg-gray-900">
                <div class="flex items-center justify-center">
                  <svg aria-hidden="true" focusable="false" width="16" height="16" viewBox="0 0 16 16" class="text-gray-300 block">
                    <path d="M4 6l3 .01h2L12 6l-4 4-4-4z" fill="currentColor"></path>
                  </svg> 
                  <span class="ml-2 uppercase">Authentication</span>
                </div>
              </div>
              <div v-if="subcategory == 'authentication'" class="bg-gray-700 py-2 px-4 sm:p-6">
                <div class="space-y-5">
                  <a class="text-xs">Please note: Protecting specific routes is an action you carry out within the editor.</a>
                  <div>
                    <label class="text-xs leading-6 font-medium">Activate Autheticated Routes</label> <a class="text-xs cursor-pointer" @click="displayCategory('application', 'routing')">(Click here if you are wanting to redirect authenticated users).</a>
                    <input type="checkbox" disabled="disabled" class="block focus:outline-none focus:border-none focus:shadow-none h-4 w-4 text-blue-600 border-gray-300 rounded">
                  </div>
                  <div>
                    <label class="text-xs leading-6 font-medium">Activate Email Verification</label> 
                    <input type="checkbox" disabled="disabled" class="block focus:outline-none focus:border-none focus:shadow-none h-4 w-4 text-blue-600 border-gray-300 rounded">
                  </div>
                </div>
              </div>
              <div @click="displayCategory('application', 'encryption')" class="cursor-pointer flex items-center justify-between p-2 bg-gray-900">
                <div class="flex items-center justify-center">
                  <svg aria-hidden="true" focusable="false" width="16" height="16" viewBox="0 0 16 16" class="text-gray-300 block">
                    <path d="M4 6l3 .01h2L12 6l-4 4-4-4z" fill="currentColor"></path>
                  </svg> 
                  <span class="ml-2 uppercase">Encryption</span>
                </div>
              </div>
              <div v-if="subcategory == 'encryption'" class="bg-gray-700 py-2 px-4 sm:p-6">
                <div class="space-y-5">
                  <div>
                    <label class="text-xs leading-6 font-medium">Activate TSL</label>
                    <input type="checkbox" disabled="disabled" class="block focus:outline-none focus:border-none focus:shadow-none h-4 w-4 text-blue-600 border-gray-300 rounded">
                  </div>
                </div>
              </div>
            </div>
          </section> 
        </div>

        <div v-if="category == 'storage'" class="text-gray-300 text-xs">
          <section>
            <div class="border border-black sm:overflow-hidden">
              <div class="flex items-center justify-between bg-gray-900">
                <h2 class="p-1 uppercase font-medium">Storage</h2>
              </div>
              <div @click="displayCategory('storage', 'connect')" class="cursor-pointer flex items-center justify-between p-2 bg-gray-900">
                <div class="flex items-center justify-center">
                  <svg aria-hidden="true" focusable="false" width="16" height="16" viewBox="0 0 16 16" class="text-gray-300 block">
                    <path d="M4 6l3 .01h2L12 6l-4 4-4-4z" fill="currentColor"></path>
                  </svg> 
                  <span class="ml-2 uppercase">Connect Database</span>
                </div>
              </div>
              <div v-if="subcategory == 'connect'" class="bg-gray-700 py-2 px-4 sm:p-6">
                <div class="space-y-5">
                  <div>
                    <label for="host" class="block font-medium">Host</label>
                    <input type="text" name="host" id="host" class="bg-gray-800 h-8 block">
                  </div>
                  <div>
                    <label for="port" class="flex items-center font-medium">
                      <span>Port</span>
                      <svg class="ml-1 flex-shrink-0 h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                        <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-8-3a1 1 0 00-.867.5 1 1 0 11-1.731-1A3 3 0 0113 8a3.001 3.001 0 01-2 2.83V11a1 1 0 11-2 0v-1a1 1 0 011-1 1 1 0 100-2zm0 8a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd" />
                      </svg>
                    </label>
                    <input type="text" name="port" id="port" autocomplete="cc-csc" class="bg-gray-800 h-8 block">
                  </div>
                  <div>
                    <label for="database" class="block font-medium">Database</label>
                    <input type="text" name="database" id="database" autocomplete="cc-family-name" class="bg-gray-800 h-8 block">
                  </div>
                  <div>
                    <label for="username" class="block font-medium">Username</label>
                    <input type="text" name="username" id="username" class="bg-gray-800 h-8 block">
                  </div>
                  <div>
                    <label for="password" class="block font-medium">Password</label>
                    <input type="password" name="password" id="password" class="bg-gray-800 h-8 block" placeholder="">
                  </div>
                </div>
              </div>
              <div @click="displayCategory('storage', 'cloud')" class="cursor-pointer flex items-center justify-between p-2 bg-gray-900">
                <div class="flex items-center justify-center">
                  <svg aria-hidden="true" focusable="false" width="16" height="16" viewBox="0 0 16 16" class="text-gray-300 block">
                    <path d="M4 6l3 .01h2L12 6l-4 4-4-4z" fill="currentColor"></path>
                  </svg> 
                  <span class="ml-2 uppercase">Connect Cloud Storage</span>
                </div>
              </div>
              <div v-if="subcategory == 'cloud'" class="bg-gray-700 py-2 px-4 sm:p-6">
                <div class="space-y-5">
                  <div>
                    <label for="url" class="block font-medium">URL</label>
                    <input type="text" name="url" id="url" class="bg-gray-800 h-8 block">
                  </div>
                  <div>
                    <label for="region" class="flex items-center font-medium">
                      <span>Region</span>
                      <svg class="ml-1 flex-shrink-0 h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                        <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-8-3a1 1 0 00-.867.5 1 1 0 11-1.731-1A3 3 0 0113 8a3.001 3.001 0 01-2 2.83V11a1 1 0 11-2 0v-1a1 1 0 011-1 1 1 0 100-2zm0 8a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd" />
                      </svg>
                    </label>
                    <input type="text" name="region" id="region" autocomplete="cc-csc" class="bg-gray-800 h-8 block">
                  </div>
                  <div>
                    <label for="bucket" class="block font-medium">Bucket name/id</label>
                    <input type="text" name="bucket" id="bucket" autocomplete="cc-family-name" class="bg-gray-800 h-8 block">
                  </div>
                  <div>
                    <label for="key" class="block font-medium">Key</label>
                    <input type="text" name="key" id="key" class="bg-gray-800 h-8 block">
                  </div>
                  <div>
                    <label for="secret" class="block font-medium">Secret</label>
                    <input type="password" name="secret" id="secret" class="bg-gray-800 h-8 block" placeholder="">
                  </div>
                </div>
              </div>
              <div @click="displayCategory('storage', 'cache')" class="cursor-pointer flex items-center justify-between p-2 bg-gray-900">
                <div class="flex items-center justify-center">
                  <svg aria-hidden="true" focusable="false" width="16" height="16" viewBox="0 0 16 16" class="text-gray-300 block">
                    <path d="M4 6l3 .01h2L12 6l-4 4-4-4z" fill="currentColor"></path>
                  </svg> 
                  <span class="ml-2 uppercase">Cache</span>
                </div>
              </div>
              <div v-if="subcategory == 'cache'" class="bg-gray-700 py-2 px-4 sm:p-6">
                <div class="space-y-5">
                  <label for="cacheDriver" class="block font-medium">Select cache driver</label>
                  <select class="bg-gray-800 h-8 block text-xs" name="cacheDriver" id="cacheDriver" v-model="settings.cacheDriver">
                    <option disabled="disabled" value="">Please select one</option>
                    <option value="file" selected>File</option>
                    <option value="memcached">Memcached</option>
                    <option value="redis">Redis</option>
                  </select>
                  <button type="submit" class="mt-5 inline-flex items-center px-4 py-2 border border-transparent font-mono border border-black focus:outline-none focus:ring-2 focus:ring-offset-2" disabled>
                  Clear Cache
                  </button>
                </div>
              </div>
            </div>
          </section>
        </div>

        <div v-if="category == 'mail'" class="text-gray-300 text-xs">
          <section>
            <div class="border border-black sm:overflow-hidden">
              <div class="flex items-center justify-between bg-gray-900">
                <h2 class="p-1 uppercase font-medium">Mail</h2>
              </div>
              <div>
                <div @click="displayCategory('mail', 'provider')" class="cursor-pointer flex items-center justify-between p-2 bg-gray-900">
                  <div class="flex items-center justify-center">
                    <svg aria-hidden="true" focusable="false" width="16" height="16" viewBox="0 0 16 16" class="text-gray-300 block">
                      <path d="M4 6l3 .01h2L12 6l-4 4-4-4z" fill="currentColor"></path>
                    </svg> 
                    <span class="ml-2 uppercase">Provider</span>
                  </div>
                </div>
                <div v-if="subcategory == 'provider'" class="bg-gray-700 py-6 px-4 sm:p-6">
                  <div class="space-y-5">
                    <div class="col-span-4 sm:col-span-1">
                      <label for="host" class="block font-medium">Host</label>
                      <input type="text" name="host" id="host" class="bg-gray-800 h-8 block">
                    </div>

                    <div>
                      <label for="port" class="flex items-center font-medium">
                        <span>Port</span>
                        <svg class="ml-1 flex-shrink-0 h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                          <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-8-3a1 1 0 00-.867.5 1 1 0 11-1.731-1A3 3 0 0113 8a3.001 3.001 0 01-2 2.83V11a1 1 0 11-2 0v-1a1 1 0 011-1 1 1 0 100-2zm0 8a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd" />
                        </svg>
                      </label>
                      <input type="text" name="port" id="port" autocomplete="cc-csc" class="bg-gray-800 h-8 block">
                    </div>

                    <div class="col-span-4 sm:col-span-1">
                      <label for="mailDriver" class="block font-medium">Driver</label>
                      <select class="bg-gray-800 h-8 block text-xs"  name="mailDriver" id="mailDriver">
                        <option disabled="disabled" value="">Please select one</option>
                        <option value="smtp">smtp</option>
                        <option value="mail">Mail</option>
                        <option value="sendmail">Sendmail</option>
                        <option value="mailgun">Mailgun</option>
                        <option value="mandrill">Mandrill</option>
                        <option value="log">Log</option></select>
                    </div>

                    <div class="col-span-4 sm:col-span-1">
                      <label for="encryption" class="block font-medium">Encryption</label>
                      <select class="bg-gray-800 h-8 block text-xs"  name="encryption" id="encryption">
                        <option disabled="disabled" value="">Please select one</option>
                        <option value="tls" selected>TLS</option>
                      </select>
                    </div>

                    <div class="col-span-4 sm:col-span-2">
                      <label for="username" class="block font-medium">Username</label>
                      <input type="text" name="username" id="username" class="bg-gray-800 h-8 block">
                    </div>

                    <div class="col-span-4 sm:col-span-2">
                      <label for="password" class="block font-medium">Password</label>
                      <input type="password" name="password" id="password" class="bg-gray-800 h-8 block" placeholder="">
                    </div>
                  </div>
                </div>
              </div>
              <div>
                <div @click="displayCategory('mail', 'mailSettings')" class="cursor-pointer flex items-center justify-between p-2 bg-gray-900">
                  <div class="flex items-center justify-center">
                    <svg aria-hidden="true" focusable="false" width="16" height="16" viewBox="0 0 16 16" class="text-gray-300 block">
                      <path d="M4 6l3 .01h2L12 6l-4 4-4-4z" fill="currentColor"></path>
                    </svg> 
                    <span class="ml-2 uppercase">Configure</span>
                  </div>
                </div>
                <div v-if="subcategory == 'mailSettings'" class="bg-gray-700 py-6 px-4 sm:p-6">
                  <div class="space-y-5">
                    <div class="col-span-4 sm:col-span-2">
                      <label for="mailfromaddress" class="block font-medium">Mail From Address</label>
                      <input type="text" name="mailfromaddress" id="mailfromaddress" placeholder="e.g. support@stellify.com" class="bg-gray-800 h-8 block">
                    </div>

                    <div class="col-span-4 sm:col-span-2">
                      <label for="mailfromname" class="block font-medium">Mail From Name</label>
                      <input type="text" name="mailfromname" id="mailfromname" placeholder="e.g. Stellify" class="bg-gray-800 h-8 block">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>

        <div v-if="category == 'testing'" class="text-gray-300 text-xs">
          <section>
            <div class="border border-black sm:overflow-hidden">
              <div class="flex items-center justify-between bg-gray-900">
                <h2 class="p-1 uppercase font-medium">Testing</h2>
              </div>
              <div @click="displayCategory('testing', 'debugging')" class="cursor-pointer flex items-center justify-between p-2 bg-gray-900">
                <div class="flex items-center justify-center">
                  <svg aria-hidden="true" focusable="false" width="16" height="16" viewBox="0 0 16 16" class="text-gray-300 block">
                    <path d="M4 6l3 .01h2L12 6l-4 4-4-4z" fill="currentColor"></path>
                  </svg> 
                  <span class="ml-2 uppercase">Debugging</span>
                </div>
              </div>
              <div v-if="subcategory == 'debugging'" class="bg-gray-700 py-2 px-4 sm:p-6">
                <div class="space-y-5">
                  <div>
                    <label class="text-xs leading-6 font-medium">Activate debugging</label>
                    <input type="checkbox" v-model="settings.debugging" class="block focus:outline-none focus:border-none focus:shadow-none h-4 w-4 text-blue-600 border-gray-300 rounded">
                  </div>
                </div>
              </div>
              <div @click="displayCategory('testing', 'logs')" class="cursor-pointer flex items-center justify-between p-2 bg-gray-900">
                <div class="flex items-center justify-center">
                  <svg aria-hidden="true" focusable="false" width="16" height="16" viewBox="0 0 16 16" class="text-gray-300 block">
                    <path d="M4 6l3 .01h2L12 6l-4 4-4-4z" fill="currentColor"></path>
                  </svg> 
                  <span class="ml-2 uppercase">Logs</span>
                </div>
              </div>
              <div v-if="subcategory == 'logs'" class="bg-gray-700 py-2 px-4 sm:p-6">
                <div class="space-y-5">
                  <div>
                    <label class="text-xs leading-6 font-medium">Activate logs</label>
                    <input type="checkbox" v-model="settings.logs" class="block focus:outline-none focus:border-none focus:shadow-none h-4 w-4 text-blue-600 border-gray-300 rounded">
                  </div>
                </div>
              </div>
            </div>
          </section> 
        </div>

        <div v-if="category == 'api'" class="text-gray-300 text-xs">
          <section>
            <div class="border border-black sm:overflow-hidden">
              <div class="flex items-center justify-between bg-gray-900">
                <h2 class="p-1 uppercase font-medium">API</h2>
              </div>

            </div>
          </section> 
        </div>

      </div>

    </div>
    <div v-else>
      <div class="max-w-lg mx-auto pt-10 pb-12 px-4 lg:pb-16">
        <form action="/createProject" method="POST">
          @csrf
          <div class="space-y-6">
            <div>
              <h1 class="text-lg leading-6 font-medium text-gray-900">
                Create a new project
              </h1>
              <p class="mt-1 text-sm text-gray-500">
                Let’s get started by filling in the information below to create your new project.
              </p>
            </div>

            <div>
              <label for="project-name" class="block text-sm font-medium text-gray-700">
                Project Name
              </label>
              <div class="mt-1">
                <input type="text" v-model="settings.name" name="name" id="name" class="block w-full shadow-sm focus:ring-sky-500 focus:border-sky-500 sm:text-sm border-gray-300 rounded-md" placeholder="Project Nero" required>
              </div>
            </div>

            <div>
              <label for="description" class="block text-sm font-medium text-gray-700">
                Description
              </label>
              <div class="mt-1">
                <textarea id="description" v-model="settings.description" name="description" rows="3" class="block w-full shadow-sm focus:ring-sky-500 focus:border-sky-500 sm:text-sm border border-gray-300 rounded-md"></textarea>
              </div>
            </div>

            <fieldset>
              <legend class="text-sm font-medium text-gray-900">
                Privacy
              </legend>

              <div class="mt-1 bg-white rounded-md shadow-sm -space-y-px">
                <label class="rounded-tl-md rounded-tr-md relative border p-4 flex cursor-pointer focus:outline-none">
                  <input type="radio" v-model="settings.privacy" name="privacy" value="1" class="h-4 w-4 mt-0.5 cursor-pointer text-sky-600 border-gray-300 focus:ring-sky-500" aria-labelledby="privacy-setting-0-label" aria-describedby="privacy-setting-0-description">
                  <div class="ml-3 flex flex-col">
                    <span id="privacy-setting-0-label" class="block text-sm font-medium">
                      Public access
                    </span>
                    <span id="privacy-setting-0-description" class="block text-sm">
                      This project would be available to anyone who has the link
                    </span>
                  </div>
                </label>

                <label class="relative border p-4 flex cursor-pointer focus:outline-none">
                  <input type="radio" v-model="settings.privacy" name="privacy" value="2" class="h-4 w-4 mt-0.5 cursor-pointer text-sky-600 border-gray-300 focus:ring-sky-500" aria-labelledby="privacy-setting-1-label" aria-describedby="privacy-setting-1-description">
                  <div class="ml-3 flex flex-col">
                    <span id="privacy-setting-1-label" class="block text-sm font-medium">
                      Private to Project Members
                    </span>
                    <span id="privacy-setting-1-description" class="block text-sm">
                      Only members of this project would be able to access
                    </span>
                  </div>
                </label>

                <label class="rounded-bl-md rounded-br-md relative border p-4 flex cursor-pointer focus:outline-none">
                  <input type="radio" v-model="settings.privacy" name="privacy" value="3" class="h-4 w-4 mt-0.5 cursor-pointer text-sky-600 border-gray-300 focus:ring-sky-500" aria-labelledby="privacy-setting-2-label" aria-describedby="privacy-setting-2-description">
                  <div class="ml-3 flex flex-col">
                    <span id="privacy-setting-2-label" class="block text-sm font-medium">
                      Private to you
                    </span>
                    <span id="privacy-setting-2-description" class="block text-sm">
                      You are the only one able to access this project
                    </span>
                  </div>
                </label>
              </div>
            </fieldset>

            <div class="flex justify-end">
              <!-- <button v-if="user.projects && user.projects.length()" type="button" class="bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-sky-500">
                Cancel
              </button> -->
              <button type="submit" class="ml-3 inline-flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-gray-900 hover:bg-gray-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-sky-500">
                Create this project
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </main>
</div>
@endsection