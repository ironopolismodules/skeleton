@extends('layouts.app')

@section('content')
<div v-cloak>
    <nav class="bg-white shadow">
        <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
            <div class="flex justify-between h-16">
            <div class="flex">
                <div class="-ml-2 mr-2 flex items-center md:hidden">
                <!-- Mobile menu button -->
                <button type="button" class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500" aria-controls="mobile-menu" aria-expanded="false">
                    <span class="sr-only">Open main menu</span>
                    <!--
                    Icon when menu is closed.

                    Heroicon name: outline/menu

                    Menu open: "hidden", Menu closed: "block"
                    -->
                    <svg class="block h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                    </svg>
                    <!--
                    Icon when menu is open.

                    Heroicon name: outline/x

                    Menu open: "block", Menu closed: "hidden"
                    -->
                    <svg class="hidden h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </button>
                </div>
                <div class="flex-shrink-0 flex items-center">
                    <a href="/dashboard" class="inline-flex justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out" aria-label="Main menu" aria-expanded="false">
                        <svg class="from-green-400 to-pink-500 h-12 w-12" viewBox="0 0 24 24">
                            <defs>
                                <linearGradient id="tw-gradient" x2="1" y2="1">
                                    <stop offset="0%" stop-color="var(--tw-gradient-from)" />
                                    <stop offset="100%" stop-color="var(--tw-gradient-to)" />
                                </linearGradient>
                            </defs>
                            <path fill="url(#tw-gradient)" d="M12,2A10,10 0 0,1 22,12A10,10 0 0,1 12,22A10,10 0 0,1 2,12A10,10 0 0,1 12,2M9,7V17H11V13H14V11H11V9H15V7H9Z" />
                        </svg>
                    </a>
                </div>
                <div class="hidden md:ml-6 md:flex md:space-x-8">
                <!-- Current: "border-indigo-500 text-gray-900", Default: "border-transparent text-gray-500 hover:border-gray-300 hover:text-gray-700" -->
                <a href="#" class="border-indigo-500 text-gray-900 inline-flex items-center px-1 pt-1 border-b-2 text-sm font-medium">
                    Day
                </a>
                <a href="#" class="border-transparent text-gray-500 hover:border-gray-300 hover:text-gray-700 inline-flex items-center px-1 pt-1 border-b-2 text-sm font-medium">
                    Week
                </a>
                <a href="#" class="border-transparent text-gray-500 hover:border-gray-300 hover:text-gray-700 inline-flex items-center px-1 pt-1 border-b-2 text-sm font-medium">
                    Month
                </a>
                <a href="#" class="border-transparent text-gray-500 hover:border-gray-300 hover:text-gray-700 inline-flex items-center px-1 pt-1 border-b-2 text-sm font-medium">
                    Year
                </a>
                </div>
            </div>
            <div class="flex items-center">
                <div v-if="currentFlow" class="flex-shrink-0">
                    <button type="button" @click="addTask" class="relative inline-flex items-center px-4 py-2 border border-transparent text-xs font-medium rounded-md text-white bg-indigo-600 shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        <!-- Heroicon name: solid/plus -->
                        <svg class="-ml-1 mr-2 h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                        <path fill-rule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clip-rule="evenodd" />
                        </svg>
                        <span>New task</span>
                    </button>
                </div>
                <div class="flex-shrink-0">
                    <button type="button" @click="addFlow" class="ml-3 relative inline-flex items-center px-4 py-2 border border-transparent text-xs font-medium rounded-md text-white bg-indigo-600 shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        <!-- Heroicon name: solid/plus -->
                        <svg class="-ml-1 mr-2 h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                        <path fill-rule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clip-rule="evenodd" />
                        </svg>
                        <span>New Flow</span>
                    </button>
                </div>
                <div class="hidden md:ml-4 md:flex-shrink-0 md:flex md:items-center">
                    <button @click="toggleMode" class="bg-white p-1 rounded-full text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        <span class="sr-only">View mode</span>
                        <svg v-if="settings.editMode" xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z" />
                        </svg>
                        <svg v-else xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
                        </svg>
                    </button>

                    <div class="ml-3 relative">
                        <div>
                            <button aria-haspopup="true" aria-expanded="true" @click.stop="openEditor('Settings')" class="bg-white p-1 rounded-full text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                <span class="sr-only">Open Page Settings</span> 
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z" />
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </nav>

    <div class="flex overflow-hidden bg-white">
        <div class="hidden lg:flex flex-col lg:flex-shrink-0 border-r border-gray-200 justify-center w-8">
            <div class="h-10 flex items-center justify-center cursor-pointer" @click="displayCurrentTime = !displayCurrentTime">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" :class="displayCurrentTime ? 'text-black' : 'text-gray-400'" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z" />
                </svg>
            </div>
            <div v-for="(flow, flowIndex) in flows" :key="flowIndex" @click.stop="selectFlow(content[flow].slug)" class="truncate cursor-pointer flex text-xs items-center justify-center" style="height: 130px;writing-mode: vertical-rl;text-orientation: mixed;" :class="{'bg-green-500': content[flow].slug == currentFlow}">
                @{{ content[flow].name }}
            </div>
        </div>
        <div class="flex-1 relative" ref="timeline">
            <div v-show="displayCurrentTime" class="absolute border-r border-red-800 h-full top-0" :style="{width: ((timelineWidth/timelineSeconds) * getAdjustedSecondsElapsed()) + 'px'}"></div>

            <div class="border-t border-gray-200">
                <div v-for="time in timeline" :key="time" :style="{width: (timelineWidth/timelineSeconds) * 3600 + 'px !important'}" class="h-10 inline-block py-3 border-b border-gray-200 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    <span class="lg:pl-2">@{{ time }}</span>
                </div>
            </div>

            <div class="border-t border-gray-200">
                <div v-for="day in days" :key="day" :style="{width: (timelineWidth/7) + 'px !important'}" class="h-10 inline-block py-3 border-b border-gray-200 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    <span class="lg:pl-2">@{{ day }}</span>
                </div>
            </div>

            <div class="border-t border-gray-200 flex">
                <div v-for="(weeks, weeksIndex) in weeksInSelectedMonth" :key="weeksIndex" class="h-10 flex flex-1 justify-around py-3 border-b border-gray-200 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    <div v-for="(week, weekIndex) in weeks" :key="weekIndex" class="lg:pl-2 w-6 flex items-center justify-center">@{{ dayInitials[week.getDay()] }}</div>
                </div>
            </div>
            <div class="border-t border-gray-200 flex">
                <div v-for="(weeks, weeksIndex) in weeksInSelectedMonth" :key="weeksIndex" class="h-10 flex flex-1 justify-around py-3 border-b border-gray-200 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    <div v-for="(week, weekIndex) in weeks" :key="weekIndex" class="lg:pl-2 w-6 flex items-center justify-center">@{{ week.getDate() }}</div>
                </div>
            </div>




            <div class="flex flex-col overflow-hidden bg-white">
                <div v-for="(flow, flowIndex) in flows" :key="flowIndex" class="flex flex-row min-w-0 flex-1 overflow-hidden w-full" style="min-height: 130px;height: 130px;" :class="content[flow].classes">
                    <!-- <div v-for="(task, taskIndex) in content[flow].tasks" :key="taskIndex" @click="selectTask(content[task].slug)" :style="{width: ((timelineWidth/timelineSeconds) * (content[task].duration)) + 'px'}" class="relative rounded-lg border border-gray-300 bg-white px-6 py-5 shadow-sm flex items-center space-x-3 hover:border-gray-400 focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500" :class="content[task].classes">
                        <div class="flex-1 min-w-0">
                            <a href="#" class="focus:outline-none">
                                <p class="text-sm font-medium truncate">

                                </p>
                            </a>
                        </div>
                    </div> -->
                    <component
                        :style="{width: ((timelineWidth/timelineSeconds) * (content[task].duration)) + 'px'}"
                        v-for="(task, taskIndex) in content[flow].tasks"
                        :key="taskIndex"
                        :content="content"
                        :settings="settings"
                        :is="content[task].type"
                        :opts="content[task]">
                    </component>
                </div>
            </div>
        </div>
    </div>

    <div v-if="showEditor" class="fixed z-50 inset-0 overflow-hidden">
        <div class="absolute inset-0 overflow-hidden">
            <section class="absolute right-0 max-w-full flex" :class="editorPosition == 'bottom' ? 'bottom-0 left-0' : 'pl-10 sm:pl-16 inset-y-0'" aria-labelledby="slide-over-heading">
                <transition
                    enter-active-class="transform transition ease-in-out duration-500 sm:duration-700"
                    enter-class="translate-x-full"
                    enter-to-class="translate-x-0"
                    leave-active-class="transform transition ease-in-out duration-500 sm:duration-700"
                    leave-class="otranslate-x-0"
                    leave-to-class="translate-x-full">
                    <div class="w-screen" :class="editorPosition == 'bottom' ? '' : 'max-w-lg'" :style="editorPosition == 'bottom' ? 'height: 50vh' : ''">
                        <div class="h-full flex flex-col py-6 bg-white shadow-xl overflow-y-scroll">
                            <div class="px-4 sm:px-6">
                                <div class="flex items-start justify-between mb-3">
                                    <h2 id="slide-over-heading" class="text-lg font-medium text-gray-900">
                                        @{{ menu ? menu : 'Editor' }}
                                    </h2>
                                    <div class="ml-3 h-7 flex items-center">
                                        <button v-show="editorPosition == 'bottom'" @click="toggleEditorPosition" class="mr-3 bg-white rounded-md text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                            <span class="sr-only">Editor align bottom</span>
                                            <svg class="h-6 w-6" viewBox="0 0 24 24" fill="none" stroke="currentColor" aria-hidden="true">
                                                <path fill="currentColor" d="M6,2H18A2,2 0 0,1 20,4V20A2,2 0 0,1 18,22H6A2,2 0 0,1 4,20V4A2,2 0 0,1 6,2M14,8V16H18V8H14Z" />
                                            </svg>
                                        </button>
                                        <button v-show="editorPosition == 'right'" @click="toggleEditorPosition" class="mr-3 bg-white rounded-md text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                            <span class="sr-only">Editior align top</span>
                                            <svg class="h-6 w-6" viewBox="0 0 24 24" fill="none" stroke="currentColor" aria-hidden="true">
                                                <path fill="currentColor" d="M6,2H18A2,2 0 0,1 20,4V20A2,2 0 0,1 18,22H6A2,2 0 0,1 4,20V4A2,2 0 0,1 6,2M6,16V20H18V16H6Z" />
                                            </svg>
                                        </button>
                                        <button @click="closeEditor" class="bg-white rounded-md text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                            <span class="sr-only">Close panel</span>
                                            <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                                <div v-if="menu == 'Settings'">
                                    <div class="mt-5">
                                        <label for="source" class="text-base leading-6 font-medium text-gray-900">Select start time</label>
                                        <select v-model="range[0]" class="mt-1 block w-full bg-white border border-gray-300 rounded-md shadow-sm py-2 px-3 sm:max-w-xs focus:outline-none focus:ring-gray-900 focus:border-gray-900 sm:text-sm" @change="calculateTimeline">
                                            <option v-for="(time, index) in times" :value="index">
                                                @{{ time }}
                                            </option>
                                        </select>
                                    </div>
                                    <div class="mt-5">
                                        <label for="source" class="text-base leading-6 font-medium text-gray-900">Select finish time</label>
                                        <select v-model="range[1]" class="mt-1 block w-full bg-white border border-gray-300 rounded-md shadow-sm py-2 px-3 sm:max-w-xs focus:outline-none focus:ring-gray-900 focus:border-gray-900 sm:text-sm" @change="calculateTimeline">
                                            <option v-for="(time, index) in times" :value="index">
                                                @{{ time }}
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div v-if="menu == 'Flow'" class="h-full divide-y divide-gray-200 flex flex-col bg-white">
                                    <div class="flex-1 h-0 overflow-y-auto">
                                        <div class="flex-1 flex flex-col justify-between">
                                            <div class="px-4 divide-y divide-gray-200 sm:px-6">
                                                <div class="space-y-6 pt-6 pb-5">
                                                    <div>
                                                        <label for="project_name" class="block text-sm font-medium text-gray-900">
                                                        Flow name
                                                        </label>
                                                        <div class="mt-1">
                                                            <input type="text" v-model="content[currentFlow].name" name="project_name" id="project_name" class="block w-full shadow-sm sm:text-sm focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded-md">
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <label for="description" class="block text-sm font-medium text-gray-900">
                                                        Description
                                                        </label>
                                                        <div class="mt-1">
                                                        <textarea id="description" v-model="content[currentFlow].description" name="description" rows="4" class="block w-full shadow-sm sm:text-sm focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded-md"></textarea>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <h3 class="text-sm font-medium text-gray-900">
                                                        Team Members
                                                        </h3>
                                                        <div class="mt-2">
                                                            <div class="flex space-x-2">
                                                                <a href="#" class="rounded-full hover:opacity-75">
                                                                <img class="inline-block h-8 w-8 rounded-full" src="https://stellifysoftware.s3.eu-west-2.amazonaws.com/matthewAndersonAvatar.png" alt="Matt Anderson">
                                                                </a>

                                                                <button type="button" class="flex-shrink-0 bg-white inline-flex h-8 w-8 items-center justify-center rounded-full border-2 border-dashed border-gray-200 text-gray-400 hover:text-gray-500 hover:border-gray-300 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                                                <span class="sr-only">Add team member</span>
                                                                <!-- Heroicon name: solid/plus -->
                                                                <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                                                    <path fill-rule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clip-rule="evenodd" />
                                                                </svg>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <fieldset>
                                                        <legend class="text-sm font-medium text-gray-900">
                                                        Privacy
                                                        </legend>
                                                        <div class="mt-2 space-y-5">
                                                            <div class="relative flex items-start">
                                                                <div class="absolute flex items-center h-5">
                                                                <input id="privacy_public" name="privacy" aria-describedby="privacy_public_description" type="radio" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300" checked>
                                                                </div>
                                                                <div class="pl-7 text-sm">
                                                                <label for="privacy_public" class="font-medium text-gray-900">
                                                                    Public access
                                                                </label>
                                                                <p id="privacy_public_description" class="text-gray-500">
                                                                    Everyone with the link will see this project.
                                                                </p>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="relative flex items-start">
                                                                <div class="absolute flex items-center h-5">
                                                                    <input id="privacy_private-to-project" name="privacy" aria-describedby="privacy_private-to-project_description" type="radio" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300">
                                                                </div>
                                                                <div class="pl-7 text-sm">
                                                                    <label for="privacy_private-to-project" class="font-medium text-gray-900">
                                                                    Private to project members
                                                                    </label>
                                                                    <p id="privacy_private-to-project_description" class="text-gray-500">
                                                                    Only members of this project would be able to access.
                                                                    </p>
                                                                </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="relative flex items-start">
                                                                    <div class="absolute flex items-center h-5">
                                                                        <input id="privacy_private" name="privacy" aria-describedby="privacy_private-to-project_description" type="radio" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300">
                                                                    </div>
                                                                    <div class="pl-7 text-sm">
                                                                        <label for="privacy_private" class="font-medium text-gray-900">
                                                                        Private to you
                                                                        </label>
                                                                        <p id="privacy_private_description" class="text-gray-500">
                                                                        You are the only one able to access this project.
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                    <div class="mt-5">
                                                        <label class="text-sm font-medium text-gray-900">Choose Background colour</label>
                                                        <div class="mt-2 block items-center">
                                                            <ul class="color-swatches flex flex-wrap space-x-1">
                                                                <li v-for="(item, index) in attributes.colours" :key="index" class="mt-2">
                                                                    <label :for="item" class="shadow-md block w-8 h-8 rounded-md hover:border-gray-300" :class="[{checked: content[currentFlow].classes && content[currentFlow].classes.includes('bg-' + item)}, 'bg-' + item]"></label>
                                                                </li>
                                                            </ul>
                                                            <input v-for="(item, index) in attributes.colours" :key="index" @input="save" type="checkbox" v-model="content[currentFlow].classes" class="hidden" name="color-chooser" :id="item" :value="'bg-' + item">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div v-if="menu == 'Task' && currentSelection">
                                    <div class="hidden sm:block">
                                        <div class="border-b border-gray-200">
                                            <nav class="-mb-px flex space-x-8" aria-label="Tabs">
                                                <a href="#" @click="selectionMenu = 'attributes'" :class="selectionMenu == 'attributes' ? 'border-indigo-500 text-indigo-600' : 'border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300'" class="whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm">
                                                    Task Information
                                                </a>
                                                <a href="#" @click="selectionMenu = 'styles'" :class="selectionMenu == 'styles' ? 'border-indigo-500 text-indigo-600' : 'border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300'" class="whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm">
                                                    Styles
                                                </a>
                                                <a href="#" @click="selectionMenu = 'data'" :class="selectionMenu == 'data' ? 'border-indigo-500 text-indigo-600' : 'border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300'" class="whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm">
                                                    Data
                                                </a>
                                                <a href="#" @click="selectionMenu = 'inspector'" :class="selectionMenu == 'inspector' ? 'border-indigo-500 text-indigo-600' : 'border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300'" class="whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm">
                                                    Inspector
                                                </a>
                                            </nav>
                                        </div>
                                    </div>
                                    <div v-show="selectionMenu == 'attributes'">
                                        <div class="mt-5">
                                            <label for="duration" class="text-base leading-6 font-medium text-gray-900">Duration: @{{ (content[currentSelection]['duration'] / 60)  + ' Minutes'}}</label>
                                            <input @change="setDuration" type="number" id="duration" placeholder="Enter new task duration in minutes" class="p-2 block w-full shadow-md focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border border-gray-300 rounded-md" />
                                        </div>
                                        <component
                                            v-for="(item, index) in profile[content[currentSelection].type].attributes" 
                                            :key="index"
                                            is="s-inputadmin"
                                            :content="content"
                                            :settings="settings"
                                            :source="currentSelection"
                                            :opts="item">
                                        </component>
                                    </div>
                                    <div v-show="selectionMenu == 'styles'">
                                        <div class="mt-5">
                                            <label class="text-base leading-6 font-medium text-gray-900">Choose Background colour</label>
                                            <div class="block items-center">
                                                <ul class="color-swatches flex flex-wrap space-x-1">
                                                    <li v-for="(item, index) in attributes.colours" :key="index" class="mt-2">
                                                        <label :for="item" class="shadow-md block w-8 h-8 rounded-md hover:border-gray-300" :class="[{checked: content[currentSelection].classes && content[currentSelection].classes.includes('bg-' + item)}, 'bg-' + item]"></label>
                                                    </li>
                                                </ul>
                                                <input v-for="(item, index) in attributes.colours" :key="index" @input="save" type="checkbox" v-model="content[currentSelection].classes" class="hidden" name="color-chooser" :id="item" :value="'bg-' + item">
                                            </div>
                                        </div>
                                        <div class="mt-5">
                                            <label class="text-base leading-6 font-medium text-gray-900">Choose Text colour</label>
                                            <div class="block items-center">
                                                <ul class="color-swatches flex flex-wrap space-x-1">
                                                    <li v-for="(item, index) in attributes.colours" :key="index" class="mt-2">
                                                        <label :for="'text-' + item" class="shadow-md block w-8 h-8 rounded-md hover:border-gray-300" :class="[{checked: content[currentSelection].classes && content[currentSelection].classes.includes('text-' + item)}, 'bg-' + item]"></label>
                                                    </li>
                                                </ul>
                                            </div>
                                            <input v-for="(item, index) in attributes.colours" :key="index" @input="save" type="checkbox" v-model="content[currentSelection].classes" class="hidden" name="color-chooser" :id="'text-' + item" :value="'text-' + item">
                                        </div>
                                        <div class="mt-5">
                                            <label class="text-base leading-6 font-medium text-gray-900">Choose border thickness</label>
                                            <div class="block items-center">
                                                <ul class="p-3 color-swatches flex flex-wrap space-x-1">
                                                    <li v-for="(item, index) in attributes.borders" :key="index" class="mt-2" :class="{'ml-1': index == 0}">
                                                        <label :for="'border' + item" class="block h-8 w-8 border-gray-900 bg-gray-200" :class="[{checked: content[currentSelection].classes && content[currentSelection].classes.includes('border' + item)}, 'border' + item]"></label>
                                                    </li>
                                                </ul>
                                            </div>
                                            <input v-for="(item, index) in attributes.borders" :key="index + item" @input="save" type="checkbox" v-model="content[currentSelection].classes" class="hidden" name="color-chooser" :id="'border' + item" :value="'border' + item">
                                        </div>
                                        <div class="mt-5">
                                            <label class="text-base leading-6 font-medium text-gray-900">Choose border colour</label>
                                            <div class="block items-center">
                                                <ul class="color-swatches flex flex-wrap space-x-1">
                                                    <li v-for="(item, index) in attributes.colours" :key="index" class="mt-2">
                                                        <label :for="'border-' + item" class="shadow-md block w-8 h-8 rounded-md hover:border-gray-300" :class="[{checked: content[currentSelection].classes && content[currentSelection].classes.includes('border-' + item)}, 'bg-' + item]"></label>
                                                    </li>
                                                </ul>
                                            </div>
                                            <input v-for="(item, index) in attributes.colours" :key="index + item" @input="save" type="checkbox" v-model="content[currentSelection].classes" class="hidden" name="color-chooser" :id="'border-' + item" :value="'border-' + item">
                                        </div>
                                        <div class="mt-5">
                                            <label class="text-base leading-6 font-medium text-gray-900">Choose border style</label>
                                            <div class="block items-center">
                                                <ul class="color-swatches flex flex-col space-y-1">
                                                    <li v-for="(item, index) in attributes.borderStyles" :key="index" class="flex-grow">
                                                        <label :for="'border-' + item" class="border-2 block w-full h-4 border-black hover:border-gray-300" :class="[{checked: content[currentSelection].classes && content[currentSelection].classes.includes('border-' + item)}, 'border-' + item]"></label>
                                                    </li>
                                                </ul>
                                            </div>
                                            <input v-for="(item, index) in attributes.borderStyles" :key="index + item" @input="save" type="checkbox" v-model="content[currentSelection].classes" class="hidden" name="color-chooser" :id="'border-' + item" :value="'border-' + item">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </transition>
            </section>
        </div>
    </div>

    <section v-if="showTask" class="fixed inset-0 overflow-hidden" aria-labelledby="slide-over-title" role="dialog" aria-modal="true">
        <div class="absolute inset-0 overflow-hidden">
            <div class="absolute inset-0" aria-hidden="true"></div>
                <div class="absolute inset-y-0 right-0 pl-10 max-w-full flex sm:pl-16">
                <transition
                    enter-active-class="transform transition ease-in-out duration-500 sm:duration-700"
                    enter-class="translate-x-full"
                    enter-to-class="translate-x-0"
                    leave-active-class="transform transition ease-in-out duration-500 sm:duration-700"
                    leave-class="otranslate-x-0"
                    leave-to-class="translate-x-full">
                    <div class="w-screen max-w-2xl">
                        <form class="h-full flex flex-col bg-white shadow-xl overflow-y-scroll">
                        <div class="flex-1">
                            <!-- Header -->
                            <div class="px-4 py-6 bg-gray-50 sm:px-6">
                            <div class="flex items-start justify-between space-x-3">
                                <div class="space-y-1">
                                    <h2 class="text-lg font-medium text-gray-900" id="slide-over-title">
                                        @{{ content[currentSelection].name }}
                                    </h2>
                                </div>
                                <div class="h-7 flex items-center">
                                <button @click="showTask = false;" type="button" class="bg-white rounded-md text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-indigo-500">
                                    <span class="sr-only">Close panel</span>
                                    <!-- Heroicon name: outline/x -->
                                    <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                                    </svg>
                                </button>
                                </div>
                            </div>
                            </div>

                            <!-- Divider container -->
                            <div class="py-6 space-y-6 sm:py-0 sm:space-y-0 sm:divide-y sm:divide-gray-200">
                            <!-- Project name -->
                            <div class="space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5">
                                <div>
                                <label for="project_name" class="block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2">
                                    Status
                                </label>
                                </div>
                                <div class="sm:col-span-2">
                                    <select id="location" name="location" class="mt-1 block w-full pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md">
                                        <option>Not started</option>
                                        <option>Parked</option>
                                        <option>Pending</option>
                                        <option>Complete</option>
                                        <option>Complete pending review</option>
                                    </select>
                                </div>
                            </div>

                            <!-- Project description -->
                            <div v-if="content[currentSelection].description" class="space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5">
                                <div>
                                <label for="project_description" class="block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2">
                                    Description
                                </label>
                                </div>
                                <div class="sm:col-span-2">
                                    @{{ content[currentSelection].description }}
                                </div>
                            </div>

                            <!-- Team members -->
                            <div class="space-y-2 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-center sm:px-6 sm:py-5">
                                <div>
                                <h3 class="text-sm font-medium text-gray-900">
                                    Team Members
                                </h3>
                                </div>
                                <div class="sm:col-span-2">
                                <div class="flex space-x-2">
                                    <a href="#" class="flex-shrink-0 rounded-full hover:opacity-75">
                                    <img class="inline-block h-8 w-8 rounded-full" src="https://stellifysoftware.s3.eu-west-2.amazonaws.com/matthewAndersonAvatar.png" alt="Matt Anderson">
                                    </a>

                                    <button type="button" class="flex-shrink-0 bg-white inline-flex h-8 w-8 items-center justify-center rounded-full border-2 border-dashed border-gray-200 text-gray-400 hover:text-gray-500 hover:border-gray-300 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                    <span class="sr-only">Add team member</span>
                                    <!-- Heroicon name: solid/plus -->
                                    <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                        <path fill-rule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clip-rule="evenodd" />
                                    </svg>
                                    </button>
                                </div>
                                </div>
                            </div>

                            <!-- Privacy -->
                            <fieldset>
                                <div class="space-y-2 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:px-6 sm:py-5">
                                <div>
                                    <legend class="text-sm font-medium text-gray-900">
                                    Privacy
                                    </legend>
                                </div>
                                <div class="space-y-5 sm:col-span-2">
                                    <div class="space-y-5 sm:mt-0">
                                    <div class="relative flex items-start">
                                        <div class="absolute flex items-center h-5">
                                        <input id="public_access" name="privacy" aria-describedby="public_access_description" type="radio" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300" checked>
                                        </div>
                                        <div class="pl-7 text-sm">
                                        <label for="public_access" class="font-medium text-gray-900">
                                            Public access
                                        </label>
                                        <p id="public_access_description" class="text-gray-500">
                                            Everyone with the link will see this project
                                        </p>
                                        </div>
                                    </div>
                                    <div class="relative flex items-start">
                                        <div class="absolute flex items-center h-5">
                                        <input id="restricted_access" name="privacy" aria-describedby="restricted_access_description" type="radio" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300">
                                        </div>
                                        <div class="pl-7 text-sm">
                                        <label for="restricted_access" class="font-medium text-gray-900">
                                            Private to Project Members
                                        </label>
                                        <p id="restricted_access_description" class="text-gray-500">
                                            Only members of this project would be able to access
                                        </p>
                                        </div>
                                    </div>
                                    <div class="relative flex items-start">
                                        <div class="absolute flex items-center h-5">
                                        <input id="private_access" name="privacy" aria-describedby="private_access_description" type="radio" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300">
                                        </div>
                                        <div class="pl-7 text-sm">
                                        <label for="private_access" class="font-medium text-gray-900">
                                            Private to you
                                        </label>
                                        <p id="private_access_description" class="text-gray-500">
                                            You are the only one able to access this project
                                        </p>
                                        </div>
                                    </div>
                                    </div>
                                    <hr class="border-gray-200">
                                    <div class="flex flex-col space-between space-y-4 sm:flex-row sm:items-center sm:space-between sm:space-y-0">
                                    <div class="flex-1">
                                        <a href="#" class="group flex items-center text-sm text-indigo-600 hover:text-indigo-900 font-medium space-x-2.5">
                                        <!-- Heroicon name: solid/link -->
                                        <svg class="h-5 w-5 text-indigo-500 group-hover:text-indigo-900" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                            <path fill-rule="evenodd" d="M12.586 4.586a2 2 0 112.828 2.828l-3 3a2 2 0 01-2.828 0 1 1 0 00-1.414 1.414 4 4 0 005.656 0l3-3a4 4 0 00-5.656-5.656l-1.5 1.5a1 1 0 101.414 1.414l1.5-1.5zm-5 5a2 2 0 012.828 0 1 1 0 101.414-1.414 4 4 0 00-5.656 0l-3 3a4 4 0 105.656 5.656l1.5-1.5a1 1 0 10-1.414-1.414l-1.5 1.5a2 2 0 11-2.828-2.828l3-3z" clip-rule="evenodd" />
                                        </svg>
                                        <span>
                                            Copy link
                                        </span>
                                        </a>
                                    </div>
                                    <div>
                                        <a href="#" class="group flex items-center text-sm text-gray-500 hover:text-gray-900 space-x-2.5">
                                        <!-- Heroicon name: solid/question-mark-circle -->
                                        <svg class="h-5 w-5 text-gray-400 group-hover:text-gray-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                            <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-8-3a1 1 0 00-.867.5 1 1 0 11-1.731-1A3 3 0 0113 8a3.001 3.001 0 01-2 2.83V11a1 1 0 11-2 0v-1a1 1 0 011-1 1 1 0 100-2zm0 8a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd" />
                                        </svg>
                                        <span>
                                            Learn more about sharing
                                        </span>
                                        </a>
                                    </div>
                                    </div>
                                </div>
                                </div>
                            </fieldset>
                            </div>
                        </div>

                        <!-- Action buttons -->
                        <div class="flex-shrink-0 px-4 border-t border-gray-200 py-5 sm:px-6">
                            <div class="space-x-3 flex justify-end">
                            <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500">
                                Delete task?
                            </button>
                            </div>
                        </div>
                        </form>
                    </div>
                </transition>
            </div>
        </div>
    </section>

    <div v-cloak v-show="!user.slug && openWelcome" class="fixed z-10 inset-0 overflow-y-auto" aria-labelledby="modal-title" role="dialog" aria-modal="true">
        <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
            <div class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" aria-hidden="true"></div>
            <!-- This element is to trick the browser into centering the modal contents. -->
            <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>

            <div class="relative inline-block align-bottom bg-white rounded-lg px-4 pt-5 pb-4 text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-sm sm:w-full sm:p-6">
                <div aria-hidden="true" class="absolute inset-0 -mt-72 sm:-mt-32 md:mt-0">
                    <svg class="absolute inset-0 h-full w-full" preserveAspectRatio="xMidYMid slice" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 1463 360">
                    <path class="text-rose-400 text-opacity-40" fill="currentColor" d="M-82.673 72l1761.849 472.086-134.327 501.315-1761.85-472.086z"></path>
                    <path class="text-rose-600 text-opacity-40" fill="currentColor" d="M-217.088 544.086L1544.761 72l134.327 501.316-1761.849 472.086z"></path>
                    </svg>
                </div>
                <div class="relative">
                    <div class="mx-auto flex items-center justify-center h-18 w-18">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-12 w-12 from-yellow-400 via-red-500 to-pink-500" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <defs>
                                <linearGradient id="tw-gradient" x2="1" y2="1">
                                    <stop offset="0%" stop-color="var(--tw-gradient-from)" />
                                    <stop offset="100%" stop-color="var(--tw-gradient-to)" />
                                <linearGradient>
                            </defs>
                            <path stroke="url(#tw-gradient)" fill="url(#tw-gradient)" d="M16.5,21C13.5,21 12.31,16.76 11.05,12.28C10.14,9.04 9,5 7.5,5C4.11,5 4,11.93 4,12H2C2,11.63 2.06,3 7.5,3C10.5,3 11.71,7.25 12.97,11.74C13.83,14.8 15,19 16.5,19C19.94,19 20.03,12.07 20.03,12H22.03C22.03,12.37 21.97,21 16.5,21Z" />
                        </svg>
                    </div>
                    <div class="mt-3 text-center sm:mt-5">
                        <h3 class="text-lg leading-6 font-medium text-gray-900" id="modal-title">
                            Introducing: Flows
                        </h3>
                        <div class="mt-2">
                            <p class="text-sm text-gray-500">
                                A modern project management tool that automates tasks and supercharges your workflows.
                            </p>
                        </div>
                    </div>
                    <div class="mt-5 sm:mt-6">
                        <button @click="storeSession('openFlowWelcome', false);openWelcome = false;" class="inline-flex justify-center w-full rounded-md border border-transparent shadow-sm px-4 py-2 bg-indigo-600 text-base font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:text-sm">
                            <span>Get started</span>
                            <svg class="ml-3 h-5 w-5 flex-shrink-0 text-white" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 7l5 5m0 0l-5 5m5-5H6" />
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div v-cloak v-show="user.slug && !openWelcome && flows.length == 0" class="fixed z-10 inset-0 overflow-y-auto" aria-labelledby="modal-title" role="dialog" aria-modal="true">
        <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
            <div class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" aria-hidden="true"></div>
            <!-- This element is to trick the browser into centering the modal contents. -->
            <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>

            <div class="relative inline-block align-bottom bg-white rounded-lg px-4 pt-5 pb-4 text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-sm sm:w-full sm:p-6">
                <div aria-hidden="true" class="absolute inset-0 -mt-72 sm:-mt-32 md:mt-0">
                    <svg class="absolute inset-0 h-full w-full" preserveAspectRatio="xMidYMid slice" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 1463 360">
                    <path class="text-rose-400 text-opacity-40" fill="currentColor" d="M-82.673 72l1761.849 472.086-134.327 501.315-1761.85-472.086z"></path>
                    <path class="text-rose-600 text-opacity-40" fill="currentColor" d="M-217.088 544.086L1544.761 72l134.327 501.316-1761.849 472.086z"></path>
                    </svg>
                </div>
                <div class="relative">
                    <div class="mx-auto flex items-center justify-center h-18 w-18">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-12 w-12 from-yellow-400 via-red-500 to-pink-500" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <defs>
                                <linearGradient id="tw-gradient" x2="1" y2="1">
                                    <stop offset="0%" stop-color="var(--tw-gradient-from)" />
                                    <stop offset="100%" stop-color="var(--tw-gradient-to)" />
                                <linearGradient>
                            </defs>
                            <path stroke="url(#tw-gradient)" fill="url(#tw-gradient)" d="M16.5,21C13.5,21 12.31,16.76 11.05,12.28C10.14,9.04 9,5 7.5,5C4.11,5 4,11.93 4,12H2C2,11.63 2.06,3 7.5,3C10.5,3 11.71,7.25 12.97,11.74C13.83,14.8 15,19 16.5,19C19.94,19 20.03,12.07 20.03,12H22.03C22.03,12.37 21.97,21 16.5,21Z" />
                        </svg>
                    </div>
                    <div class="mt-3 text-center sm:mt-5">
                        <h3 class="text-lg leading-6 font-medium text-gray-900" id="modal-title">
                            Create a Flow
                        </h3>
                        <div class="mt-4">
                            <fieldset>
                                <legend class="sr-only">
                                    Flow selection
                                </legend>
                                <div class="bg-white rounded-md -space-y-px">
                                    <!-- Checked: "bg-indigo-50 border-indigo-200 z-10", Not Checked: "border-gray-200" -->
                                    <label class="border-gray-200 rounded-tl-md rounded-tr-md relative border p-4 flex cursor-pointer">
                                        <input type="radio" name="privacy_setting" value="Public access" class="h-4 w-4 mt-0.5 cursor-pointer text-indigo-600 border-gray-300 focus:ring-indigo-500" aria-labelledby="privacy-setting-0-label" aria-describedby="privacy-setting-0-description">
                                        <div class="ml-3 flex flex-col">
                                            <!-- Checked: "text-indigo-900", Not Checked: "text-gray-900" -->
                                            <span id="privacy-setting-0-label" class="text-gray-900 block text-sm font-medium">
                                            Techincal support flow
                                            </span>
                                        </div>
                                    </label>
                                    <label class="border-gray-200 relative border p-4 flex cursor-pointer">
                                        <input type="radio" name="privacy_setting" value="Private to Project Members" class="h-4 w-4 mt-0.5 cursor-pointer text-indigo-600 border-gray-300 focus:ring-indigo-500" aria-labelledby="privacy-setting-1-label" aria-describedby="privacy-setting-1-description">
                                        <div class="ml-3 flex flex-col">
                                            <!-- Checked: "text-indigo-900", Not Checked: "text-gray-900" -->
                                            <span id="privacy-setting-1-label" class="text-gray-900 block text-sm font-medium">
                                            Development project flow
                                            </span>
                                        </div>
                                    </label>
                                    <label class="border-gray-200 rounded-bl-md rounded-br-md relative border p-4 flex cursor-pointer">
                                        <input type="radio" name="privacy_setting" value="Private to you" class="h-4 w-4 mt-0.5 cursor-pointer text-indigo-600 border-gray-300 focus:ring-indigo-500" aria-labelledby="privacy-setting-2-label" aria-describedby="privacy-setting-2-description">
                                        <div class="ml-3 flex flex-col">
                                            <!-- Checked: "text-indigo-900", Not Checked: "text-gray-900" -->
                                            <span id="privacy-setting-2-label" class="text-gray-900 block text-sm font-medium">
                                            Project management flow
                                            </span>
                                        </div>
                                    </label>
                                    <label class="border-gray-200 rounded-bl-md rounded-br-md relative border p-4 flex cursor-pointer">
                                        <input type="radio" name="privacy_setting" value="Private to you" class="h-4 w-4 mt-0.5 cursor-pointer text-indigo-600 border-gray-300 focus:ring-indigo-500" aria-labelledby="privacy-setting-2-label" aria-describedby="privacy-setting-2-description">
                                        <div class="ml-3 flex flex-col">
                                            <!-- Checked: "text-indigo-900", Not Checked: "text-gray-900" -->
                                            <span id="privacy-setting-2-label" class="text-gray-900 block text-sm font-medium">
                                            Website management flow
                                            </span>
                                        </div>
                                    </label>
                                    <label class="border-gray-200 rounded-bl-md rounded-br-md relative border p-4 flex cursor-pointer">
                                        <input type="radio" name="privacy_setting" value="Private to you" class="h-4 w-4 mt-0.5 cursor-pointer text-indigo-600 border-gray-300 focus:ring-indigo-500" aria-labelledby="privacy-setting-2-label" aria-describedby="privacy-setting-2-description">
                                        <div class="ml-3 flex flex-col">
                                            <!-- Checked: "text-indigo-900", Not Checked: "text-gray-900" -->
                                            <span id="privacy-setting-2-label" class="text-gray-900 block text-sm font-medium">
                                            Marketing campaign flow
                                            </span>
                                        </div>
                                    </label>
                                    <label class="border-gray-200 rounded-bl-md rounded-br-md relative border p-4 flex cursor-pointer">
                                        <input type="radio" name="privacy_setting" value="Private to you" class="h-4 w-4 mt-0.5 cursor-pointer text-indigo-600 border-gray-300 focus:ring-indigo-500" aria-labelledby="privacy-setting-2-label" aria-describedby="privacy-setting-2-description">
                                        <div class="ml-3 flex flex-col">
                                            <!-- Checked: "text-indigo-900", Not Checked: "text-gray-900" -->
                                            <span id="privacy-setting-2-label" class="text-gray-900 block text-sm font-medium">
                                            Scale operations flow
                                            </span>
                                        </div>
                                    </label>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="mt-5 sm:mt-6">
                        <button @click="storeSession('openFlowWelcome', false);openWelcome = false;" class="inline-flex justify-center w-full rounded-md border border-transparent shadow-sm px-4 py-2 bg-indigo-600 text-base font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:text-sm">
                            <span>Get started</span>
                            <svg class="ml-3 h-5 w-5 flex-shrink-0 text-white" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 7l5 5m0 0l-5 5m5-5H6" />
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
