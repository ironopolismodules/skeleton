<svg class="{{ !empty($opts->classes) ? implode(" ", $opts->classes) : '' }}"
    xmlns="http://www.w3.org/2000/svg"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    @if (!empty($opts->viewBox))
    viewBox="{{ $opts->viewBox }}"
    @endif
    @if (!empty($opts->height))
    height="{{ $opts->height }}"
    @endif
    @if (!empty($opts->width))
    width="{{ $opts->width }}"
    @endif
    @if (!empty($opts->fill))
    fill="{{ $opts->fill }}"
    @endif
    @if (!empty($opts->preserveAspectRatio))
    preserveAspectRatio="{{ $opts->preserveAspectRatio }}"
    @endif
    @if (!empty($opts->role))
    role="{{ $opts->role }}"
    @endif
    >
    {!! !empty($opts->path) ? $opts->path : '' !!}
</svg>