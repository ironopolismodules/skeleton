<{{ !empty($opts->tag) ? $opts->tag : 'div' }} {{ !empty($opts->src) ? 'src=' . $opts->src . '' : '' }} {{ !empty($opts->href) ? 'href=' . $opts->href . '' : '' }} {{ !empty($opts->target) ? 'target=' . $opts->target . '' : '' }} class="{{ !empty($opts->classes) ? implode(" ", $opts->classes) : '' }}">
    {{ !empty($opts->text) ? $opts->text : '' }}
    @if (!empty($opts->data))
    @foreach($opts->data as $data)
    <x-dynamic-component :component="substr($content[$data]->type, 2)" :is="$content[$data]->type" :opts="$content[$data]" :settings="$settings" :content="$content" />
    @endforeach
    @endif
</{{ !empty($opts->tag) ? $opts->tag : 'div' }}>