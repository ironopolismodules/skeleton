<?php

namespace Ironopolis\Skeleton;

use Illuminate\Database\Eloquent\Model;

class Templateblocks extends Model
{
    protected $fillable = [
        'user_id', 'slug', 'data', 'typeIndex'
    ];
}