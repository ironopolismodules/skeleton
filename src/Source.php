<?php

namespace Ironopolis\Skeleton;

use Illuminate\Database\Eloquent\Model;

class Source extends Model
{
  protected $fillable = [
    'site_id', 'name', 'slug', 'data'
  ];
}
