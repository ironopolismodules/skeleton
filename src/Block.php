<?php

namespace Ironopolis\Skeleton;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

class Block extends Model
{
    //use SoftDeletes;
//    // This breaks JSON save in DB
//     protected $casts = [
//         'data' => 'array',
//     ];
    protected $fillable = [
        'user_id', 'slug', 'data', 'typeIndex'
    ];

    protected $hidden = array('pivot');

    public function bodies()
    {
        return $this->belongsToMany('Ironopolis\Skeleton\Body');
    }

    public function types()
    {
        return $this->belongsToMany('Ironopolis\Skeleton\Type');
    }

    public function globals()
    {
        return $this->types(4);
    }
}
