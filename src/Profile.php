<?php

namespace Ironopolis\Skeleton;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
  protected $fillable = [
    'site_id', 'data'
  ];
}
