<?php

namespace Ironopolis\Skeleton;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Template extends Model
{
    //use SoftDeletes;
//    // This breaks JSON save in DB
//     protected $casts = [
//         'data' => 'array',
//     ];
    protected $fillable = [
        'user_id', 'site_id', 'name', 'slug', 'type', 'data', 'image'
    ];

}