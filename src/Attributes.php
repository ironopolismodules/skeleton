<?php

namespace Ironopolis\Skeleton;

use Illuminate\Database\Eloquent\Model;

class EntityAttributes extends Model
{
  protected $fillable = [
    'slug', 'entity_id', 'attribute_group_id', 'attribute_id', 'collection_id', 'field', 'value'
  ];
}
