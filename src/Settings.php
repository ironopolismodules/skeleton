<?php

namespace Ironopolis\Skeleton;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
  protected $fillable = [
    'site_id', 'data', 'active_domain'
  ];
}
