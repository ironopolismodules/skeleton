<?php

namespace Ironopolis\Skeleton\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Ironopolis\Skeleton\Settings;
use Illuminate\Support\Facades\Auth;

class ShopifyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $userid;
    private $settings;
    private $userSettings;
    private $token;
    private $apiKey;
    private $sharedSecret;
    private $storeName;

    public function __construct() {
        $this->middleware(function ($request, $next) {
            //$this->userid = Auth::id();
            $this->userid = 0;
            $this->userSettings = Settings::select()
                ->where('user_id', $this->userid)
                ->first();
            if (!empty($this->userSettings)) {
                $this->settings = json_decode($this->userSettings->data, true);
            }
            $this->apiKey = env('SHOPIFY_KEY');
            $this->sharedSecret = env('SHOPIFY_SECRET');
            $this->token = !empty($this->settings['shopifyToken']) ? $this->settings['shopifyToken'] : null;
            $this->storeName = !empty($this->settings['shopifyStoreName']) ? $this->settings['shopifyStoreName'] : null;
            return $next($request);
        });
    }

    public function installApp() {
        if (empty($this->token)) {
            // Set variables for our request
            $shop = $_GET['shop'];
            $extractStoreName = explode(".", $_GET['shop']);
            $this->storeName = $extractStoreName[0];
            $scopes = "read_content,write_content,read_products,write_products,read_customers,write_customers,read_orders,write_orders,read_draft_orders,write_draft_orders,read_inventory,write_inventory,read_locations,read_fulfillments,write_fulfillments,read_assigned_fulfillment_orders,write_assigned_fulfillment_orders,read_merchant_managed_fulfillment_orders,write_merchant_managed_fulfillment_orders,read_third_party_fulfillment_orders,write_third_party_fulfillment_orders,read_shipping,write_shipping,read_analytics,read_checkouts,write_checkouts,read_reports,write_reports,read_price_rules,write_price_rules,read_discounts,write_discounts,read_marketing_events,write_marketing_events,read_resource_feedbacks,write_resource_feedbacks,read_shopify_payments_payouts,read_shopify_payments_disputes,read_translations,write_translations,read_locales,write_locales";
            $redirect_uri = "http://3957d92a6e1a.ngrok.io/yaketyShopify/generateToken";
            // Build install/approval URL to redirect to
            $install_url = "https://" . $shop . "/admin/oauth/authorize?client_id=" . $this->apiKey . "&scope=" . $scopes . "&redirect_uri=" . urlencode($redirect_uri);
            // Redirect
            header("Location: " . $install_url);
        } else {
            //display dashboard
            $requests = $_GET;
            $hmac = $_GET['hmac'];
            $serializeArray = serialize($requests);
            $requests = array_diff_key($requests, array('hmac' => ''));
            ksort($requests);

            $this->products();
        }
    }

    public function generateToken() {
        // Set variables for our request
        $params = $_GET; // Retrieve all request parameters
        $hmac = $_GET['hmac']; // Retrieve HMAC request parameter

        $params = array_diff_key($params, array('hmac' => '')); // Remove hmac from params
        ksort($params); // Sort params lexographically
        $computed_hmac = hash_hmac('sha256', http_build_query($params), $this->sharedSecret);

        // Use hmac data to check that the response is from Shopify or not
        if (hash_equals($hmac, $computed_hmac)) {
            // Set variables for our request
            $query = array(
                "client_id" => $this->apiKey, // Your API key
                "client_secret" => $this->sharedSecret, // Your app credentials (secret key)
                "code" => $params['code'] // Grab the access key from the URL
            );
            // Generate access token URL
            $access_token_url = "https://" . $params['shop'] . "/admin/oauth/access_token";
            // Configure curl client and execute request
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $access_token_url);
            curl_setopt($ch, CURLOPT_POST, count($query));
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($query));
            $result = curl_exec($ch);
            curl_close($ch);
            // Store the access token
            $result = json_decode($result, true);
            $access_token = $result['access_token'];
            // Store key in the database
            $userSettings = Settings::select()
                ->where('user_id', $this->userid)
                ->first();
            $this->settings = json_decode($userSettings->data, true);
            $this->settings['shopifyToken'] = $access_token;
            $this->settings['shopifyStoreName'] = $this->storeName;
            $userSettings->data = json_encode($this->settings);
            $userSettings->save();
            dd($access_token, $userSettings);

        } else {
            // Someone is trying to be shady!
            die('This request is NOT from Shopify!');
        }
    }

    public function products() {
        // Run API call to get products
        $products = $this->shopify_call($this->token, $this->storeName, "/admin/products.json", array(), 'GET');
        // Convert product JSON information into an array
        $products = json_decode($products['response'], TRUE);

        dd($products);
    }

    public function shopify_call($token, $shop, $api_endpoint, $query = array(), $method = 'GET', $request_headers = array()) {
    
        // Build URL
        $url = "https://" . $shop . ".myshopify.com" . $api_endpoint;
        if (!is_null($query) && in_array($method, array('GET', 	'DELETE'))) $url = $url . "?" . http_build_query($query);
    
        // Configure cURL
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, TRUE);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($curl, CURLOPT_MAXREDIRS, 3);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        // curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 3);
        // curl_setopt($curl, CURLOPT_SSLVERSION, 3);
        curl_setopt($curl, CURLOPT_USERAGENT, 'My New Shopify App v.1');
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    
        // Setup headers
        $request_headers[] = "";
        if (!is_null($token)) $request_headers[] = "X-Shopify-Access-Token: " . $token;
        curl_setopt($curl, CURLOPT_HTTPHEADER, $request_headers);
    
        if ($method != 'GET' && in_array($method, array('POST', 'PUT'))) {
            if (is_array($query)) $query = http_build_query($query);
            curl_setopt ($curl, CURLOPT_POSTFIELDS, $query);
        }
        
        // Send request to Shopify and capture any errors
        $response = curl_exec($curl);
        $error_number = curl_errno($curl);
        $error_message = curl_error($curl);
    
        // Close cURL to be nice
        curl_close($curl);
    
        // Return an error is cURL has a problem
        if ($error_number) {
            return $error_message;
        } else {
    
            // No error, return Shopify's response by parsing out the body and the headers
            $response = preg_split("/\r\n\r\n|\n\n|\r\r/", $response, 2);
    
            // Convert headers into an array
            $headers = array();
            $header_data = explode("\n",$response[0]);
            $headers['status'] = $header_data[0]; // Does not contain a key, have to explicitly set
            array_shift($header_data); // Remove status, we've already set it above
            foreach($header_data as $part) {
                $h = explode(":", $part);
                $headers[trim($h[0])] = trim($h[1]);
            }
    
            // Return headers and Shopify's response
            return array('headers' => $headers, 'response' => $response[1]);
    
        }
    }
}