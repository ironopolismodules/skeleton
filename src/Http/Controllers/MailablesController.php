<?php

namespace Ironopolis\Skeleton\Http\Controllers;

use App\Http\Controllers\Controller;
use Ironopolis\Skeleton\Mail\Enquiry;
use Illuminate\Support\Facades\Mail;

class MailablesController extends Controller
{
    public function index() {
        Mail::to(env('MAIL_FROM_ADDRESS'))->send(new Enquiry);
        return response()->json(['response'=>'Thank you we will be in touch shortly.']);
    }
}
