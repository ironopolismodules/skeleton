<?php

namespace Ironopolis\Skeleton\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use App\User;
use Ramsey\Uuid\Uuid;
use Ironopolis\Skeleton\State;
use Ironopolis\Skeleton\Body;
use Ironopolis\Skeleton\Block;
use Ironopolis\Skeleton\Script;
use Ironopolis\Skeleton\Settings;
use Ironopolis\Skeleton\Source;
use Ironopolis\Skeleton\Record;
use Ironopolis\Skeleton\Profile;
use Ironopolis\Skeleton\Template;
use Illuminate\Support\Str;
use Ironopolis\Skeleton\AttributeGroups;
use Ironopolis\Skeleton\Attributes;
use Carbon\Carbon;
//use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;
use Laravel\Cashier\Exceptions\IncompletePayment;
use function GuzzleHttp\json_decode;

class PagesController extends Controller
{
    private $body;
    private $data;
    private $userid;
    private $user;
    private $users;
    private $currency;
    private $locale;
    private $siteid;
    private $siteOwner;
    private $root;
    private $filters;
    private $path;
    private $fullPath;
    private $state;
    private $entity;
    private $profile;
    private $scripts;
    private $templates;
    private $settings;
    private $fonts;
    private $editMode;

    public function __construct() {
        $this->middleware(function ($request, $next) {
            $this->root = $request->root();
            $this->fullPath = $request->path();
            $this->path = explode('/', $this->fullPath);
            $this->url = $request->url();
            $this->userid = null;
            $this->user = Auth::user();
            $this->userData = null;
            $this->users = [];
            $this->data = [];
            $this->scripts = null;
            $this->currencies = ['en' => ['ISO' => 'GBP', 'symbol' => '£']];
            $this->locale = 'en';
            $this->state = ["lastClicked" => null, "lastPressed" => null];
            $this->siteid = null;
            $this->entity = [];
            $this->editor = [];
            $this->siteOwner = null;
            $this->profile = null;
            $this->project = null;
            $this->settings = [];
            $this->editMode = $request->has('edit');
            $this->filters = $request->query('filters');
            $this->deviceMode = $request->has('device');
            $this->js = [];
            $this->app = '';
            if (!empty($this->user)) {
                $expr = '/(?<=\s|^)[a-z]/i';
                preg_match_all($expr, $this->user->name, $matches);
                $result = implode('', $matches[0]);
                $result = strtoupper($result);
                $this->user->initials = $result;
            }
            // $current = Carbon::now();
            // $current = new Carbon();
            // $today = Carbon::today();

            // $month = '2015-01';
            // $start = Carbon::parse($month)->startOfMonth();
            // $end = Carbon::parse($month)->endOfMonth();

            // $dates = [];
            // while ($start->lte($end)) {
            //     $dates[] = $start->copy();
            //     $start->addDay();
            // }
            
            //initial check is multisite setting?
            // $languages = ['en','de'];
            // if (!empty($uriArray[0]) && in_array($uriArray[0], $languages)) {
            //     $this->siteid = array_search($uriArray[0], $languages);
            //     if (!empty($uriArray[1])) {
            //         $this->path = $uriArray[1];
            //     } else {
            //         $this->path = '/';
            //     }
            // }
            if (empty($this->path[0])) {
                $this->path = ['/'];
            }  

            //check if we have a request for a live site and we're going to use the site owner to retrieve the settings for this site and lock it down
            // $this->site = Settings::where('active_domain', $this->remove_http($this->root))->first();
            $this->site = config('settings');
            if (empty($this->user) && !empty($this->site->active_domain)) {
                $this->siteid = $this->site->site_id;
                $this->settings = json_decode($this->site->data, true);
                $this->settings['locked'] = true;
                $this->body = Body::where(['path' => $this->path[0], 'site_id' => $this->siteid])->orWhere(['slug' => $this->path[0]])->first();
                if (!empty($this->body) && $this->body->searchable == 0) {
                    abort(403, 'Unauthorized action. The project owner may have set this page as private.');
                }
            } else {
                //we don't have an incoming request from a live site
                if (!empty($this->user)) {
                    $this->siteid = !empty($this->user) ? $this->user->site_id : 0;
                    $this->userid = Auth::id();
                }
                $this->body = Body::where(['path' => $this->path[0], 'site_id' => $this->siteid])->orWhere(['slug' => $this->path[0]])->first();
                if (empty($this->body)) {
                    $this->body = Body::where(['path' => $this->path[0], 'site_id' => '*'])->first();
                    if (!empty($this->body)) {
                        $settings = Settings::select('data')
                            ->where('site_id', '*')
                            ->first();
                        $this->settings = json_decode($settings->data, true);
                        $this->settings['locked'] = true;
                    }
                }
                if (!empty($this->body)) {
                    $this->siteid = $this->body->site_id;
                    $settings = Settings::select('data')
                        ->where('site_id', $this->siteid)
                        ->first();
                    if (!empty($settings)) {
                        $this->settings = json_decode($settings->data, true);
                    }
                    $this->settings['locked'] = true;
                    $this->settings['editMode'] = false;
                    if (!empty($this->user) && $this->siteid == $this->user->site_id) {
                        $this->settings['locked'] = false;
                    }
                } else {
                    $settings = Settings::select('data')
                        ->where('site_id', $this->siteid)
                        ->first();
                    if (!empty($settings)) {
                        $this->settings = json_decode($settings->data, true);
                    }
                }
            }
            if (!empty($this->user)) {
                $userData = State::select('data')
                    ->where('slug', $this->user->slug)
                    ->first();
                if (!empty($this->user->site_id)) {
                    $this->userData['site_id'] = $this->user->site_id;
                }
                if (!empty($userData)) {
                    $this->userData = json_decode($userData->data, true);
                }
                $this->state['username'] = $this->user->name;
                $this->state['user'] = $this->user->slug;
            }
            if ($this->editMode) {
                $this->settings['editMode'] = $this->editMode;
                $this->editor = [
                    'showComponents' => true,
                    'selectOnInsert' => false,
                    'currentIndicator' => null,
                    'editorPosition' => 'bottom', 
                    'classbar' => true, 
                    'toolbar' => true, 
                    'navigator' => true
                ];
                $profiles = Profile::select('data')
                    ->where(['slug' => 'general'])
                    ->orWhere(['slug' => 'methods'])
                    ->get();
                if (!empty($profiles)) {
                    $key = 'general';
                    foreach($profiles as $profile) {
                        $this->profile[$key] = json_decode($profile->data, true);
                        $key = 'methods';
                    }
                }
            } else {
                $profiles = Profile::select('data')
                    ->where(['slug' => 'methods'])
                    ->get();
                if (!empty($profiles)) {
                    foreach($profiles as $profile) {
                        $this->profile = json_decode($profile->data, true);
                    }
                }
            }
            // is the user subscribed? If so, remove lock
            //if (!empty($this->user) && $this->userid != 0 && $this->user->subscribedToPlan('price_1H4koyL2EQC83td8KO8OalYa', 'main')) {
                //case statement? Need to have different options depening on tier for clients
                //$this->settings['locked'] = false;
            //}
            if (!empty($this->body)) {
                if ($this->body->loggedin && empty($this->user)) {
                    return redirect('/login');
                }
                $data = json_decode($this->body->data);
                if (!empty($data)) {
                    foreach($data as $dataItem => $dataValue) {
                        $this->body[$dataItem] = $dataValue;
                    }
                }
                //fetch state
                // if (!empty($this->body->variables)) {
                //     foreach($this->body->variables as $variable) {
                //         $this->state[$variable->name] = $variable->default;
                //     }
                // }
                if (!empty($data->sources)) {
                    $sources = Source::whereIn('slug', $data->sources)->get();
                    foreach($sources as $source) {
                        $sourceData = json_decode($source->data, true);
                        //we fetch the dataitself further down so that additional records can be fetched for use by entities
                        $this->data[$source->slug] = json_decode($source->data);
                    }
                }
                if (!empty($this->path) && is_array($this->path) && count($this->path) > 1) {
                    $path = $this->path;
                    unset($path[0]);
                    $path = implode('/', $path);
                    if ($this->body->entityType == 'singleton') {
                        $entity = Record::where('path', $path)->first();
                        if (!empty($entity)) {
                            $entityData = json_decode($entity->data, true);
                            $this->entity = $entityData;
                        }
                    } else if ($this->body->entityType == 'collection') {

                    }
                }
                if (!empty($this->body->datasources)) {
                    $collection = Collection::where('path', $path)->first();
                    if (!empty($collection)) {
                        $collectionData = json_decode($collection->data, true);
                        $this->entity = $collectionData;
                        if (!empty($this->entity['breadcrumbs'])) {
                            if (!empty($sourceData['data'])) {
                                $sourceData['data'] = array_merge($sourceData['data'], $this->entity['breadcrumbs']);
                            } else {
                                $sourceData['data'] = $this->entity['breadcrumbs'];
                            }
                        }
                        //fetch all of a type
                        // if (!empty($collectionData['all'])) {
                        //     $entityAttributes = EntityAttributes::all();
                        // } else {
                        //     if (!empty($this->filters)) {
                        //         $this->filters = explode(',', $this->filters);
                        //         $entityAttributes = EntityAttributes::where('collection_id', $collection['slug'])->whereIn('attribute_id', $this->filters)->get();
                        //     } else {
                        //         $entityAttributes = EntityAttributes::where('collection_id', $collection['slug'])->get();
                        //     }
                        // }
                        //we have entity ids?
                        // if (!empty($entityAttributes)) {
                        //     $entityIds = $entityAttributes->unique('entity_id')->pluck('entity_id');
                        //     if (!empty($this->filters)) {
                        //         $entityAttributes = EntityAttributes::whereIn('entity_id', $entityIds)->get();
                        //     }
                        //     $entities = Entity::whereIn('slug', $entityIds)->get();
                        //     if (!empty($entities)) {
                        //         foreach($entities as $entity) {
                        //             $this->data[$entity->slug] = json_decode($entity->data);
                        //         }
                        //         $this->entity['entities'] = $entityIds->toArray();
                        //     }
                        //     //fetch attribute groups and corresponding attributes
                        //     $attributeGroupIds = $entityAttributes->unique('attribute_group_id')->pluck('attribute_group_id');
                        //     $attributeIds = $entityAttributes->unique('attribute_id')->pluck('attribute_id');
                        //     $attributeGroups = AttributeGroups::whereIn('slug', $attributeGroupIds)->get();
                        //     //get required data to page
                        //     foreach($attributeGroups as $attributeGroup) {
                        //         $attributeGroupData = json_decode($attributeGroup['data'], true);
                        //         $this->data[$attributeGroup['slug']] = $attributeGroupData;
                        //         if (!empty($attributeGroupData['data'])) {
                        //             $attributes = Attribute::whereIn('slug', $attributeIds)->get();
                        //             foreach ($attributes as $attribute) {
                        //                 if (!empty($attribute['data'])) {
                        //                     $this->data[$attribute->slug] = json_decode($attribute['data']);
                        //                 }
                        //             }
                        //         }
                        //     }
                        //     //dd($attributeGroups);
                        //     //define structure of data for display
                        //     $groupedAttributes = $entityAttributes->groupBy('attribute_group_id');
                        //     if (!empty($attributeGroups)) {
                        //         $this->entity['attribute_groups'] = $attributeGroupIds->toArray();
                        //         //$this->entity['attributes'] = $attributeIds->toArray();
                        //         foreach($groupedAttributes as $attributeGroupId => $attributeGroupAttributes) {
                        //             if (!empty($this->data[$attributeGroupId]['name'])) {
                        //                 //at some point will need to order the attributes using a definition
                        //                 $this->entity[$this->data[$attributeGroupId]['name']] = $attributeGroupAttributes->unique('attribute_id')->pluck('attribute_id')->toArray();
                        //             }
                        //         }
                        //     }
                        //     //dd($this->entity);
                        // }
                    }
                }
                //dd($path);
                if ($this->body->entityType == 'single') {
                    $entity = Record::where('path', $path)->first();
                    if (!empty($entity)) {
                        $entityData = json_decode($entity->data, true);
                        $this->entity = $entityData;
                        // $entityAttributes = EntityAttributes::where('entity_id', $entityData['slug'])->get();
                        // if (!empty($entityData['images'])) {
                        //     $images = Image::where('entity_id', $entityData['slug'])->get();
                        //     foreach($images as $index => $image) {
                        //         $imageData = json_decode($image->data, true);
                        //         $this->entity['image' . $index] = $imageData['src'];
                        //         $this->data[$image->slug] = $imageData;
                        //     }
                        // }
                        //children are independent navigable linked products NOT variants of one product.
                        // if (!empty($entityData['children'])) {
                        //     foreach($entityData['children'] as $index => $child) {
                        //         $child = Entity::where('slug', $child)->first();
                        //         $childData = json_decode($child->data, true);
                        //         $this->data[$child->slug] = $childData;
                        //         if (!empty($childData['images'])) {
                        //             $split = explode('-', $childData['slug']);
                        //             $childImages = Image::where('entity_id', $childData['slug'])->get();
                        //             foreach($childImages as $index => $childImage) {
                        //                 $childImageData = json_decode($childImage->data, true);
                        //                 $this->data[$childImage->slug] = $childImageData;
                        //             }
                        //         }
                        //     }
                        // }
                        // if (!empty($entityData['profile'])) {
                        //     $entityProfile = Profile::where('slug', $entityData['profile'])->first();
                        //     $profileData = json_decode($entityProfile->data, true);
                        //     if (!empty($profileData['attribute_groups'])) {
                        //         $attributeGroups = AttributeGroups::whereIn('slug', $profileData['attribute_groups'])->get();
                        //         //create an empty attribute key field
                        //         $this->entity['attributeKey'] = '';
                        //         //get required data to page
                        //         foreach($attributeGroups as $index => $attributeGroup) {
                        //             $attributeGroupData = json_decode($attributeGroup['data'], true);
                        //             $this->entity['attributeGroup' . $index] = $attributeGroupData['name'];
                        //             $this->entity['attributeGroups'][] = $attributeGroupData['name'];
                        //             $this->entity['attributeGroupIds'][] = $attributeGroupData['slug'];
                        //             $this->data[$attributeGroup['slug']] = $attributeGroupData;
                        //             if (!empty($profileData[$attributeGroup['slug']])) {
                        //                 $attributes = Attribute::whereIn('slug', $profileData[$attributeGroup['slug']])->get();
                        //                 foreach ($attributes as $attribute) {
                        //                     $this->entity[$attributeGroup['name']][] = $attribute->slug;
                        //                     if (!empty($attribute['data'])) {
                        //                         $this->data[$attribute->slug] = json_decode($attribute['data']);
                        //                     }
                        //                 }
                        //             }
                        //         }
                        //     }
                        // }
                    }
                }
                if ($this->body->entityType == 'cart') {
                    if (!empty($this->userData['basket'])) {
                        $entities = Entity::whereIn('slug', $this->userData['basket'])->get();
                        $this->entity['basket'] = [];
                        $subtotal = 0;
                        $shipping = 500/100;
                        foreach($entities as $entity) {
                            $entityData = json_decode($entity->data, true);
                            $this->data[$entity->slug] = $entityData;
                            $this->data[$entity->slug]['price'] = ($entity->price / 100);
                            $this->data[$entity->slug]['currencySymbol'] = $this->currencies[$this->locale]['symbol'];
                            $this->entity['basket'][] = $entity->slug;
                            $subtotal += $entity->price / 100;
                        }
                        $this->entity['subtotal'] = $this->currencies[$this->locale]['symbol'] . number_format($subtotal, 2);
                        $this->entity['shipping'] = $this->currencies[$this->locale]['symbol'] . number_format($shipping, 2);
                        $vat = ($subtotal / 120) * 100;
                        $this->entity['vat'] = $this->currencies[$this->locale]['symbol'] . number_format($subtotal - $vat, 2);
                        $this->entity['total'] = $this->currencies[$this->locale]['symbol'] . number_format($subtotal + $shipping, 2);
                    }
                }
                if (!empty($this->body->requests)) {
                    $this->fetchData($request->query());
                }
                //dd($this->entity);
                //display body within a body
                if (!empty($this->body->body)) {
                    //dynamic body section inclusion
                    $body = Body::where('path', $this->path[count($this->path) - 1])->first();
                    if (!empty($body)) {
                        $bodyData = json_decode($body->data, true);
                        if (!empty($bodyData['sources'])) {
                            if (empty($data->sources)) {
                                $data->sources = [];
                            }
                            $data->sources = array_merge($bodyData['sources'], $data->sources);
                        }
                        $this->data['view'] = $bodyData;
                        if (!empty($bodyData['blocks'])) {
                            $blocks = Block::whereIn('slug', $bodyData['blocks'])->get();
                            foreach($blocks as $block) {
                                $this->data[$block->slug] = json_decode($block->data);
                            }
                        }
                    }
                }
                if (!empty($sourceData['data']) && empty($sourceData->dynamic)) {
                    $fields = Record::whereIn('slug', $sourceData['data'])->get();
                    foreach($fields as $field) {
                        $this->data[$field->slug] = json_decode($field->data);
                    }
                }
                //static body section inclusion
                if (!empty($data->bodies)) {
                    foreach($data->bodies as $body) {
                        $body = Body::where('slug', $body)->first();
                        $bodyData = json_decode($body->data, true);
                        $this->data[$bodyData['slug']] = $bodyData;
                        if (!empty($bodyData['blocks'])) {
                            $blocks = Block::whereIn('slug', $bodyData['blocks'])->get();
                            foreach($blocks as $block) {
                                $this->data[$block->slug] = json_decode($block->data);
                            }
                        }
                    }
                }
                if (!empty($data->data)) {
                    $layouts = Block::whereIn('slug', $data->data)->get();
                    foreach($layouts as $layout) {
                        $this->data[$layout->slug] = json_decode($layout->data);
                        if (!empty($this->data[$layout->slug]->fetchFields)) {
                            $fetchedFields = $this->fetchFields($this->data[$layout->slug]->fetchFields);
                            //dd($fetchedFields);
                            if (!empty($fetchedFields)) {
                                $fetchedFieldsData = json_decode($fetchedFields->data);
                                foreach($fetchedFieldsData as $fetchedFieldKey => $fetchedFieldItem) {
                                    $this->data[$layout->slug]->{$fetchedFieldKey} = $fetchedFieldItem;
                                }
                            }
                        }
                        // if (!empty($this->data[$layout->slug]->fetchContent)) {
                        //     //need to implement limits here also
                        //     foreach($this->data[$layout->slug]->fetchContent as $method) {
                        //         $limit = 1;
                        //         $fetchedContent = $this->fetchContent($method, $limit);
                        //     }
                        // }

                        // if (!empty($this->data[$layout->slug]->fetchMethod)) {
                        //     $method = $this->data[$layout->slug]->fetchMethod;
                        //     $this->{'\Ironopolis\Skeleton\Http\Controllers\AppController\\'.$method}();
                        //     dd('halt');
                        // }
                        if (!empty($this->data[$layout->slug]->fetchData)) {
                            //need to implement pagination here also
                            foreach($this->data[$layout->slug]->fetchData as $method) {
                                if (empty($this->data[$layout->slug]->data)) {
                                    $this->data[$layout->slug]->data = [];
                                }
                                $fetchedData = $this->fetchData($method);
                                if (!empty($fetchedData)) {
                                    //loop fetched data to create content blocks...and asign slugs to this current block
                                    foreach($fetchedData as $fetchedDataKey => $fetchedDataItem) {
                                        if (!empty($fetchedDataItem->data)) {
                                            $this->data[$fetchedDataItem->slug] = json_decode($fetchedDataItem->data);
                                        } else {
                                            $this->data[$fetchedDataItem->slug] = $fetchedDataItem;
                                        }
                                        if (!empty($this->data[$fetchedDataItem->slug]->user_id) || $this->data[$fetchedDataItem->slug]->user_id == 0) {
                                            //fetch basic user details in for presentational purposes
                                            array_push($this->users, $this->data[$fetchedDataItem->slug]->user_id); 
                                        }
                                        //push on to data array of this block
                                        array_push($this->data[$layout->slug]->data, $fetchedDataItem->slug);
                                    }
                                    if (!empty($this->data[$layout->slug]->fetchOverrideContent)) {
                                        $fetchedOverrideContent = $this->fetchOverrideContent($this->data[$layout->slug]->fetchOverrideContent, $this->data[$layout->slug]->data);
                                        if (!empty($fetchedOverrideContent)) {
                                            foreach($fetchedOverrideContent as $fetchedOverrideContentKey => $fetchedOverrideContentItem) {
                                                if (!is_array($this->data[$fetchedOverrideContentItem->slug]) && !is_array($fetchedOverrideContentItem)) {
                                                    $this->data[$fetchedOverrideContentItem->slug] = array_merge($this->data[$fetchedOverrideContentItem->slug]->toArray(), $fetchedOverrideContentItem->toArray());
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (!empty($this->settings['sources'])) {
                    $sources = Source::whereIn('slug', $this->settings['sources'])->get();
                    foreach($sources as $source) {
                        $sourceData = json_decode($source->data, true);
                        if (empty($sourceData->dynamic)) {
                            $fields = Record::whereIn('slug', $sourceData['data'])->get();
                            foreach($fields as $field) {
                                $this->data[$field->slug] = json_decode($field->data);
                            }

                        }
                        $this->data[$source->slug] = json_decode($source->data);
                    }
                }
                if (!empty($data->blocks)) {
                    $blocks = Block::whereIn('slug', $data->blocks)->get();
                    foreach($blocks as $block) {
                        $this->data[$block->slug] = json_decode($block->data);
                    }
                    if (!empty($this->site)) {
                        $siteBlocks = Block::whereIn('slug', $data->blocks)->get();
                        foreach($siteBlocks as $siteBlock) {
                            $this->data[$siteBlock->slug] = json_decode($siteBlock->data);
                        }
                    }
                }
                if (!empty($data->formFields)) {
                    $formSubmissions = Formsubmissions::where('user_id', $this->userid)->whereIn('slug', $data->formFields)->get();
                    foreach($formSubmissions as $formSubmission) {
                        $formSubmissionData = json_decode($formSubmission->data);
                        foreach($formSubmissionData as $formSubmissionDataKey => $formSubmissionDataItem) {
                            if (!empty($this->data[$formSubmission->slug]) && $formSubmissionDataKey == 'value') {
                                $this->data[$formSubmission->slug]->{$formSubmissionDataKey} = $formSubmissionDataItem;
                            }
                        }
                    }
                }
                if (!empty($this->settings['bodies'])) {
                    $globalBodies = Body::whereIn('slug', $this->settings['bodies'])->get();
                    foreach($globalBodies as $globalBody) {
                        $this->data[$globalBody->slug] = $globalBody;
                        $globalBodyData = json_decode($globalBody->data);
                        foreach($globalBodyData as $globalBodyDataKey => $globalBodyDataItem) {
                            $this->data[$globalBody->slug][$globalBodyDataKey] = $globalBodyDataItem;
                        }
                    }
                }
                // if (!empty($this->settings['blocks'])) {
                //     $globalBlocks = Block::whereIn('slug', $this->settings['blocks'])->get();
                //     foreach($globalBlocks as $globalBlock) {
                //         $this->data[$globalBlock->slug] = $globalBlock;
                //         if (!empty($globalBlock->data)) {
                //             $globalBlockData = json_decode($globalBlock->data);
                //             foreach($globalBlockData as $globalBlockDataKey => $globalBlockDataItem) {
                //                 $this->data[$globalBlock->slug][$globalBlockDataKey] = $globalBlockDataItem;
                //             }
                //         }
                //     }
                // }
                if (!empty($this->settings['blocks'])) {
                    $globalBlocks = Block::whereIn('slug', $this->settings['blocks'])->get();
                    foreach($globalBlocks as $globalBlock) {
                        $this->data[$globalBlock->slug] = json_decode($globalBlock->data);
                        // if (!empty($globalBlock->data)) {
                        //     $globalBlockData = json_decode($globalBlock->data);
                        //     foreach($globalBlockData as $globalBlockDataKey => $globalBlockDataItem) {
                        //         $this->data[$globalBlock->slug][$globalBlockDataKey] = $globalBlockDataItem;
                        //     }
                        // }
                    }
                }
                //Get users associated with blocks
                //perhaps look at flag to bypass this when not required
                $this->users = array_unique($this->users);
                $this->users = User::whereIn('id', $this->users)->get();
                if (!empty($this->users)) {
                    foreach ($this->users as $user) {
                        //$this->data[$user->slug] = $user;
                        // if (!empty($user->data)) {
                        //     $userBlockData = json_decode($user->data);
                        //     foreach($userBlockData as $userBlockDataKey => $userBlockDataItem) {
                        //         $this->data[$user->slug][$userBlockDataKey] = $userBlockDataItem;
                        //     }
                        // }
                        if (!empty($user->data)) {
                            $this->data[$user->slug] = json_decode($user->data);
                        }
                    }
                }
                // if (empty($this->settings['primaryImagePath'])) {
                //     $this->settings['primaryImagePath'] = 'https://stellifysoftware.s3.eu-west-2.amazonaws.com';
                // }
                if (!empty($this->settings['fonts'])) {
                    foreach($this->settings['fonts'] as $font) {
                        $this->fonts[] = $font;
                    }
                }
                if (!empty($this->body['fonts'])) {
                    foreach($this->body['fonts'] as $font) {
                        $this->fonts[] = $font;
                    }
                }


                $this->data['test'] = ['colour' => 'bg-green-400'];
            }
            return $next($request);
        });
    }

    protected function subId($id) {
        $split = explode('-', $id);
        return $split[0];
    }

    protected function remove_http($url) {
        $disallowed = array('http://www.', 'https://www.', 'http://', 'https://');
        foreach($disallowed as $d) {
           if (strpos($url, $d) === 0) {
              return str_replace($d, '', $url);
           }
        }
        return $url;
    }

    public function index(Request $request) {
        //dd($this->body, $this->user);
        // if ($this->body->subscribed && $this->user->subscribed('main') == false) {
        //     return redirect('/register');
        // }
        $errors = \Session::get('errors');
        if (!empty($errors)) {
            $this->state['errors'] = $errors->all();
        }
        //include user details in settings
        return view('skeleton::page', [
            "editMode" => $this->editMode || $this->deviceMode ? 'edit' : '',
            "deviceMode" => $this->deviceMode,
            "app" => !empty($this->app) ? $this->app : '',
            "fonts" => !empty($this->fonts) ? $this->fonts : null,
            "body" => !empty($this->body) ? $this->body : null,
            "editor" => !empty($this->editor) ? $this->editor : null,
            "content" => !empty($this->data) ? $this->data : null,
            "scripts" => !empty($this->scripts) ? $this->scripts : null,
            "entity" => !empty($this->entity) ? $this->entity : null,
            "user" => !empty($this->userData) ? $this->userData : null,
            "users" => !empty($this->users) ? $this->users : null,
            "state" => !empty($this->state) ? $this->state : null,
            "settings" => $this->settings,
            "profile" => $this->profile ? $this->profile : null,
            "templates" => $this->templates ? $this->templates : null,
            "stripePublicKey" => env('STRIPE_KEY'),
            "googleauthredirect" => env('GOOGLEOAUTHREDIRECT'),
            "dev" => env('APP_DEBUG')
        ]);
    }

    // public function product($product, Request $request) {
    //     $testUrl = 'https://api.playground.klarna.com';
    //     $apiEndpoint = \Klarna\Rest\Transport\ConnectorInterface::NA_TEST_BASE_URL;
    //     $connector = \Klarna\Rest\Transport\GuzzleConnector::create(
    //         'PN01867_a6f62242c43a',
    //         'pwRJyBqqb2G3shcr',
    //         $apiEndpoint
    //     );
    //     try {
    //         $buttonsApi = new \Klarna\Rest\InstantShopping\ButtonKeys($connector);
    //         $data = [
    //             'merchant_name' => 'John Doe',
    //             'merchant_urls' => [
    //               'place_order' => 'https://example.com/place-callback',
    //               'push' => 'https://example.com/push-callback',
    //               'confirmation' => 'https://example.com/confirmation-callback',
    //               'terms' => 'https://example.com/terms-callback',
    //               'notification' => 'https://example.com/notification-callback',
    //               'update' => 'https://example.com/update-callback',
    //             ]
    //         ];
    //         $button = $buttonsApi->create($data);
    //     } catch (Exception $e) {
    //         echo 'Caught exception: ' . $e->getMessage() . "\n";
    //     }
    //     return view('skeleton::page', [
    //         'buttonKey' => $button['button_key'],
    //         "app" => !empty($this->app) ? $this->app : '',
    //         "jsfiles" => !empty($this->js) ? $this->js : ['app'],
    //         "fonts" => !empty($this->fonts) ? $this->fonts : null,
    //         "body" => !empty($this->body) ? $this->body : null,
    //         "content" => !empty($this->data) ? $this->data : null,
    //         "graph" => !empty($this->graph) ? $this->graph : null,
    //         "settings" => $this->settings,
    //         "products" => !empty($this->products) ? $this->products : null,
    //         "dev" => env('APP_DEBUG')
    //     ]);
    // }

    // public function klarnaCheckout(Request $request) {
    //     if (!empty($items = $request->input())) {
    //         $products = Entity::whereIn('product_id',array_keys($items))->get();
    //     }
    //     $testUrl = 'https://api.playground.klarna.com';
    //     $apiEndpoint = \Klarna\Rest\Transport\ConnectorInterface::NA_TEST_BASE_URL;
    //     $connector = \Klarna\Rest\Transport\GuzzleConnector::create(
    //         'PN01867_a6f62242c43a',
    //         'pwRJyBqqb2G3shcr',
    //         $apiEndpoint
    //     );
    //     $orderLines = [];
    //     $orderTotal = 0;
    //     if (!empty($products)) {
    //         foreach($products as $product) {
    //             $orderLine = [];
    //             $orderLine['type'] = empty($product['type']) ? 'physical' : 'electronic';
    //             $orderLine['reference'] = $product['product_id'];
    //             $orderLine['name'] = $product['product_name'];
    //             $orderLine['quantity'] = $items[$product['product_id']];
    //             $orderLine['quantity_unit'] = $product['quantity_unit'];
    //             $orderLine['unit_price'] = $product['price'] - $product['adjustment'];
    //             $orderLine['tax_rate'] = 0;
    //             $orderLine['total_amount'] = ($product['price'] - $product['adjustment']) * $items[$product['product_id']];
    //             $orderLine['total_tax_amount'] = 0;
    //             array_push($orderLines, $orderLine);
    //             $orderTotal += ($product['price'] - $product['adjustment']) * $items[$product['product_id']];
    //         }
    //     }
    //     $order = [
    //         "purchase_country" => "gb",
    //         "purchase_currency" => "gbp",
    //         "locale" => "en-gb",
    //         "order_amount" => $orderTotal,
    //         "order_tax_amount" => 0,
    //         "order_lines" => $orderLines,
    //         "merchant_urls" => [
    //             "terms" => "https://yakety.co.uk/terms",
    //             "cancellation_terms" => "https://yakety.co.uk/terms",
    //             "checkout" => "https://yakety.co.uk/checkout",
    //             "confirmation" => "https://yakety.co.uk/confirmation",
        
    //             // Callbacks
    //             "push" => "https://www.example.com/api/push",
    //             "validation" => "https://www.example.com/api/validation",
    //             "shipping_option_update" => "https://www.example.com/api/shipment",
    //             "address_update" => "https://www.example.com/api/address",
    //             "notification" => "https://www.example.com/api/pending",
    //             "country_change" => "https://www.example.com/api/country"
    //         ]
    //     ];
    //     //dd($order);
    //     try {
    //         $checkout = new \Klarna\Rest\Checkout\Order($connector);
    //         $checkout->create($order);
        
    //         // Store checkout order id
    //         $orderId = $checkout->getId();
    //     } catch (Exception $e) {
    //         echo 'Caught exception: ' . $e->getMessage() . "\n";
    //     }
    //     return $checkout['html_snippet'];
    // }

    public function manageData(Request $request) {
        return view('skeleton::manageData', [
            "user" => $this->user,
            "settings" => $this->settings,
            "path" => "cms",
            "message" => !empty($message) ? $message : null
        ]);
    }

    public function addDays() {
        $now = Carbon::now();
        return $now->addDays(1)->toDateString();
    }

    public function makeRequest($requestData, $queryString, $body) {
        if (!empty($requestData->headers)) {
            $request = Http::withHeaders($requestData->headers);
        } else {
            $request = Http::withHeaders([]);
        }
        if (!empty($requestData->username) && !empty($requestData->password)) {
            $request->withBasicAuth($requestData->username, $requestData->password);
        }
        if (!empty($requestData->token)) {
            $request->withToken($requestData->token);
        }
        if (!empty($requestData->timeout)) {
            $request->timeout($requestData->timeout);
        }
        if (!empty($requestData->retryamount) && !empty($requestData->retrydelay)) {
            $request->retry($requestData->retryamount, $requestData->retrydelay);
        }
        if (!empty($requestData->method) && $requestData->method == 'POST') {
            $response = $request->post($requestData->url, $body);
        } else {
            $response = $request->get($requestData->url . $queryString);
        }
        //dd($response->json());
        if ($response->ok()) {
            if (!empty($requestData->name)) {
                $this->entity[$requestData->name] = [];
            }
            if (!empty($requestData->key)) {
                foreach($response->json()[$requestData->key] as $key => $item) {
                    $uuid = Uuid::uuid4()->toString();
                    if (!empty($requestData->name)) {
                        $this->entity[$requestData->name][] = $uuid;
                        $this->data[$uuid] = $item;
                    } else {
                        $this->entity[$key] = $item;
                    }
                }
            } else {
                foreach($response->json() as $key => $item) {
                    $uuid = Uuid::uuid4()->toString();
                    if (!empty($requestData->name)) {
                        $this->entity[$requestData->name][] = $uuid;
                        $this->data[$uuid] = $item;
                    } else {
                        $this->entity[$key] = $item;
                    }
                }
            }
        }
        //else
        //log error for users to view rather than return error
    }
    
    public function fetchData($query) {
        if (!empty($this->body->requests)) {
            foreach ($this->body->requests as $requestData) {
                //inject query params from request into the query string of the API request
                if (!empty($query) && !empty($requestData->url)) {
                    preg_match_all('/\$(.*?)\$/', $requestData->url, $matches);
                    if (!empty($matches)) {
                        foreach($matches[0] as $index => $match) {
                            if (!empty($query[$matches[1][$index]])) {
                                $requestData->url = str_replace($match, $query[$matches[1][$index]], $requestData->url);
                            }
                        }
                    }
                }
                $queryString = '';
                if (!empty($query) && !empty($requestData->query)) {
                    preg_match_all('/\$(.*?)\$/', $requestData->query, $matches);
                    if (!empty($matches)) {
                        foreach($matches[0] as $index => $match) {
                            if (!empty($query[$matches[1][$index]])) {
                                $queryString = str_replace($match, $query[$matches[1][$index]], $requestData->query);
                            }
                        }
                    }
                    $queryString = '?' . $queryString ;
                }
                $this->makeRequest($requestData, $queryString, null);
            }
        }
    }

    public function manageMedia(Request $request) {
        return view('skeleton::manageMedia', [
            "user" => $this->user,
            "settings" => $this->settings,
            "message" => !empty($message) ? $message : null
        ]);
    }

    public function control(Request $request) {
        if (!empty($this->user) && !empty($this->siteid)) {
            $bodies = Body::where('site_id', $this->siteid)->get();
            if ($this->user->data) {
                $data = json_decode($this->user->data);
                if (!empty($data->projects) && count($data->projects) > 1) {
                    $sites = Settings::whereIn('site_id', $data->projects)->get();
                    if (!empty($sites)) {
                        $siteData = [];
                        foreach($sites as $site) {
                            $siteData[$site->site_id] = json_decode($site->data);
                        }
                    }
                }
            }
            return view('skeleton::control', [
                "user" => $this->user,
                "userData" => !empty($this->user->data) ? json_decode($this->user->data) : [],
                "bodies" => !empty($bodies) ? $bodies : [],
                "siteData" => !empty($siteData) ? $siteData : [],
                "settings" => $this->settings,
                "message" => !empty($message) ? $message : null,
                "path" => 'control'
            ]);
        }
        return redirect('/');
    }

    public function flows(Request $request) {
        return view('skeleton::flows', [
            "path" => 'flows',
            "profile" => $this->profile,
            "user" => $this->user,
            "settings" => $this->settings
        ]);
    }

    public function sequences(Request $request) {
        return view('skeleton::sequences', [
            "path" => 'sequences',
            "body" => !empty($this->body) ? $this->body : null,
            "profile" => $this->profile,
            "user" => $this->user,
            "users" => !empty($this->users) ? $this->users : null,
            "content" => $this->data,
            "settings" => $this->settings
        ]);
    }

    public function account(Request $request) {
        $expr = '/(?<=\s|^)[a-z]/i';
        preg_match_all($expr, $this->user->name, $matches);
        $result = implode('', $matches[0]);
        $result = strtoupper($result);
        $this->user->initials = $result;
        $invoices = $this->user->invoices();
        $subscription = null;
        $previous = false;
        if (!empty($this->user) && $this->user->subscribedToPlan('main')) {
            $subscription = $this->user->subscription('main');
            $previous = $this->user->subscription('main')->cancelled();
        }
        return view('account', [
            "user" => $this->user,
            "route" => 'account',
            "stripe" => true,
            "settings" => $this->settings,
            "previous" => $previous,
            "invoices" => $invoices,
            "subscription" => $subscription,
            "plan" => !empty($subscription->stripe_plan) ? $subscription->stripe_plan : null,
            "stripePublicKey" => env('STRIPE_KEY')
        ]);
    }

    public function checkout(Request $request) {
        return view('checkout', [
            "user" => $this->user,
            "message" => !empty($message) ? $message : null,
            "stripe" => true,
            "settings" => $this->settings,
            "stripePublicKey" => env('STRIPE_KEY')
        ]);
    }

    public function subscription(Request $request) {
        return view('update-payment-method', [
            'intent' => $this->user->createSetupIntent()
        ]);
    }

    public function createSubscription(Request $request) {
        $inputData = $this->validate($request, array(
            'paymentMethodId' => 'required',
            'priceId' => 'required'
        ));
        try {
            $subscription = $this->user->newSubscription('main', $inputData['priceId'])->create($inputData['paymentMethodId'], [
                'email' => $this->user->email
            ]);
        } catch (IncompletePayment $exception) {
            return redirect()->route(
                'cashier.payment',
                [$exception->payment->id, 'redirect' => route('/')]
            );
        }
        return response()->json($subscription);
    }

    public function activateSubscription(Request $request) {
        if (!empty($this->user) && $this->user->subscribedToPlan('main')) {
            //check if user has re-subscribed?
            $uuid = $this->activateAccount();
            return response()->json([
                'uuid' => $uuid
            ]);
        }
    }

    public function cancelSubscription(Request $request) {
        if ($request->input('cancelSubscriptionImmediately')) {
            $this->user->subscription('main')->cancelNow();
        } else {
            $this->user->subscription('main')->cancel();
        }
        return \Redirect::back()->with('message', 'Subscription Cancelled.');
    }

    public function swapPlan(Request $request) {
        $inputData = $this->validate($request, array(
            'pricing_plan' => 'required'
        ));
        $this->user->subscription('main')->swap($inputData['pricing_plan']);
        return \Redirect::back()->with('message', 'You have successfully switched plans.');
    }

    public function resumeSubscription(Request $request) {
        $this->user->subscription('main')->resume();
        return redirect('/account')->with('message', 'Subscription resumed.');
    }

    public function updatePaymentMethod(Request $request) {
        $inputData = $this->validate($request, array(
            'paymentMethodId' => 'required',
            'customerId' => 'required'
        ));
        $user = $this->user->updateDefaultPaymentMethod($inputData['paymentMethodId']);
        return response()->json($user);
    }

    public function deletePaymentMethod(Request $request) {
        if ($user->hasDefaultPaymentMethod()) {
            $user->deletePaymentMethods();
        }
        return \Redirect::back()->with('message', 'You have successfully removed your credit card details.');
    }

    public function pricing(Request $request) {
        return view('pricing', [
            "user" => $this->user,
            "message" => !empty($message) ? $message : null
        ]);
    }

    public function hostingPlans(Request $request) {
        return view('hostingPlans', [
            "user" => $this->user,
            "message" => !empty($message) ? $message : null
        ]);
    }

    public function hostingCheckout(Request $request) {
        return view('hostingCheckout', [
            "user" => $this->user,
            "stripe" => true,
            "stripePublicKey" => env('STRIPE_KEY'),
            "message" => !empty($message) ? $message : null
        ]);
    }

    public function business(Request $request) {
        return view('business', [
            "user" => $this->user,
            "message" => !empty($message) ? $message : null
        ]);
    }

    public function support(Request $request) {
        $this->scripts = Script::where('slug', '71f774b0-ecbb-40cc-a2ba-c85f2e5997a7')->get();
        return view('support', [
            "user" => $this->user,
            "scripts" => !empty($this->scripts) ? $this->scripts : null,
            "message" => !empty($message) ? $message : null
        ]);
    }

    // public function account(Request $request) {
    //     $paymentMethod = $this->user->defaultPaymentMethod();
    //     if ($this->user->subscribedToPlan('plan_GdBqIpRz8KYzrI', 'main')) {
    //         $plan = 'plan_GdBqrQfkcXNOzX';
    //     } else {
    //         $plan = 'plan_GdBqIpRz8KYzrI';
    //     }
    //     return view('account', [
    //         "plan" => $plan,
    //         "grace" => $this->user->subscription('main')->onGracePeriod(),
    //         "subscribed" => $this->user->subscribed('main'),
    //         "stripePublicKey" => env('STRIPE_KEY'),
    //         "defaultPaymentMethod" => $paymentMethod->card->last4,
    //         "user" => $this->user,
    //         "debug" => env('APP_DEBUG'),
    //         "redirect" => "/settings/updatePaymentMethod",
    //         "intent" => $this->user->createSetupIntent(),
    //         "invoices" => $this->user->invoices(),
    //         "message" => !empty($message) ? $message : null
    //     ]);
    // }

    public function selectPackage(Request $request) {
        $inputData = $this->validate($request, array(
            'package' => 'required'
        ));
        $product = Product::where('name', $inputData['package'])->first();
        if (!empty($product)) {
            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            $session = \Stripe\Checkout\Session::create([
                'customer_email' => $this->user->email,
                'payment_method_types' => ['card'],
                'line_items' => [[
                    'name' => $product->name,
                    'description' => $product->description,
                    'images' => [$product->image],
                    'amount' => $product->amount,
                    'currency' => $product->currency,
                    'quantity' => $product->quantity,
                ]],
                'success_url' => 'https://stellisoft.co.uk/control?session_id={CHECKOUT_SESSION_ID}',
                'cancel_url' => 'https://stellisoft.co.uk/control',
            ]);
            return view('billing', [
                "user" => Auth::user(),
                "session" => $session,
                "stripekey" => env('STRIPE_KEY')
            ]); 
        }
    }

    public function paymentReceived() {
        // Set your secret key: remember to change this to your live secret key in production
        // See your keys here: https://dashboard.stripe.com/account/apikeys
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        // You can find your endpoint's secret in your webhook settings
        $endpoint_secret = env('ENDPOINT_SECRET');

        $payload = @file_get_contents('php://input');
        $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
        $event = null;

        try {
            $event = \Stripe\Webhook::constructEvent(
                $payload, $sig_header, $endpoint_secret
            );
        } catch(\UnexpectedValueException $e) {
            // Invalid payload
            http_response_code(400);
            exit();
        } catch(\Stripe\Exception\SignatureVerificationException $e) {
            // Invalid signature
            http_response_code(400);
            exit();
        }
        // Handle the checkout.session.completed event
        if ($event->type == 'checkout.session.completed') {
            $session = $event->data->object;
            $customer = $session->customer;
            $user = User::where('stripe_id', $customer)
                    ->orWhere('email', $session->customer_email)
                    ->first();
            if (!empty($user) && !empty($session)) {
                $user->type = $session->display_items[0]->custom->name;
                $user->save();
            }
            // Fulfill the purchase...
            //\Log::info($session->display_items[0]->custom->name);
            //handle_checkout_session($session);
        }

        http_response_code(200);
    }
}
