<?php

namespace Ironopolis\Skeleton\Http\Controllers;
use App\Http\Controllers\Controller;
use App\User;
use Ironopolis\Skeleton\UserData;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use Ironopolis\Skeleton\Settings;
use Ironopolis\Skeleton\Source;
use Ironopolis\Skeleton\Collection;
use Ironopolis\Skeleton\AttributeGroups;
use Ironopolis\Skeleton\Attribute;
use Ironopolis\Skeleton\Record;
use Ironopolis\Skeleton\EntityAttributes;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use function GuzzleHttp\json_decode;
use function GuzzleHttp\json_encode;


class CmsController extends Controller
{
    protected $taxonomy;
    private $userid;
    private $returnObject;
    private $user;
    private $data;
    private $siteid;

    public function __construct() {
        $this->middleware(function ($request, $next) {
            $this->userid = Auth::id();
            $this->user = Auth::user();
            $this->data = [];
            $this->returnObject = [
                'content' => [],
                'body' => [],
                'blocks' => [],
                'bodyBlocks' => [],
                'target' => null
            ];
            if (!empty($this->user)) {
                $this->siteid = $this->user->site_id;
                if (!empty($this->user->data)) {
                    $this->user->data = json_decode($this->user->data);
                }
            }
            return $next($request);
        });
    }

    public function fetchSources(Request $request) {
        $inputData = $this->validate($request, array(
            'source' => 'required'
        ));
        if (!empty($this->user)) {
            $sources = Source::select()
                ->where('name', 'LIKE', '%'.$inputData['source'].'%')
                ->get();
        } else {
            $sources = Source::select()
                ->where('name', 'LIKE', '%'.$inputData['source'].'%')
                ->where('site_id', $this->siteid)
                ->orWhere('site_id',  '*')
                ->get();
        }
        $decoded = [];
        foreach ($sources as $source) {
            $data = json_decode($source['data'], true);
            $data['created_at'] = $source['created_at'];
            $data['updated_at'] = $source['updated_at'];
            $decoded[] = $data;
        }
        return $decoded;
    }

    public function demoFetchSources(Request $request) {
        $inputData = $this->validate($request, array(
            'source' => 'required'
        ));
        $sources = Source::select()
            ->where('name', 'LIKE', '%'.$inputData['source'].'%')
            ->where('site_id', '*')
            ->get();
        if (!empty($sources)) {
            $decoded = [];
            foreach ($sources as $source) {
                $decoded[] = json_decode($source['data']);
            }
            return $decoded;
        }
        return [];
    }

    public function deleteSource(Request $request) {
        $inputData = $this->validate($request, array(
            'source' => 'required'
        ));
        $source = Source::where(['slug' => $inputData['source'], 'site_id' => $this->siteid])->first();
        if (!empty($source)) {
            $sourceData = json_decode($source->data, true);
            $records = Record::whereIn('slug', $sourceData['data'])
                ->where('user_id', $this->userid)
                ->delete(); 
            $source->delete();
            return response()->json(['response' => 'Source deleted']);
        }
        return response()->json(['response' => 'You do not have permission to delete this source.']);
    }

    public function fetchAllSources() {
        $sources = Source::select('data')
            ->where('site_id', $this->siteid)
            ->get();;
        if (!empty($sources)) {
            return $sources;
        }
        return [];
    }

    public function fetchFields(Request $request) {
        $inputData = $this->validate($request, array(
            'source' => 'required'
        ));
        $returnData = [];
        $source = Source::select('data')
            ->where('slug', $inputData['source'])
            ->where('site_id', $this->siteid)
            ->first();
        $sourceData = json_decode($source->data, true);
        if (!empty($source)) {
            $returnData['source'] = $sourceData;
            $fields = Record::whereIn('slug', $sourceData['data'])->get();
            $returnData['records'] = $fields;
            if (!empty($fields)) {
                return $returnData;
            }
            return [];
        }
    }

    public function fetchRecords(Request $request) {
        $inputData = $this->validate($request, array(
            'slug' => 'required'
        ));
        $source = Source::where(['slug' => $inputData['slug']])->first();
        $sourceData = json_decode($source->data, true);
        if (!empty($sourceData['data'])) {
            $records = Record::whereIn('slug', $sourceData['data'])->get();
            if (!empty($records)) {
                return $records;
            }
        }
        return [];
    }

    public function fetchFilterables(Request $request) {
        $inputData = $this->validate($request, array(
            'slug' => 'required'
        ));
        $filterables = EntityAttributes::select('collection_id', 'attribute_id', 'attribute_group_id', 'field', 'value')
            ->where('entity_id', $inputData['slug'])->get();
        if (!empty($filterables->pluck('collection_id'))) {
            $collections = Collection::whereIn('slug', $filterables->pluck('collection_id'))->get();
            $mappedCollections = [];
            foreach($collections as $collection) {
                $mappedCollections[$collection->slug] = $collection->name;
            }
        }
        if (!empty($filterables->pluck('attribute_id'))) {
            $attributes = Attribute::whereIn('slug', $filterables->pluck('attribute_id'))->get();
            $mappedAttributes = [];
            foreach($attributes as $attribute) {
                $mappedAttributes[$attribute->slug] = $attribute->name;
            }
        }
        if (!empty($filterables->pluck('attribute_group_id'))) {
            $attributeGroups = AttributeGroups::whereIn('slug', $filterables->pluck('attribute_group_id'))->get();
            $mappedAttributeGroups = [];
            foreach($attributeGroups as $attributeGroup) {
                $mappedAttributeGroups[$attributeGroup->slug] = $attributeGroup->name;
            }
        }
        if (!empty($filterables)) {
            $mappedFilterables = [];
            foreach($filterables as $index => $filterable) {
                $mappedFilterables[$index]['collection_id'] = $mappedCollections[$filterable['collection_id']];
                $mappedFilterables[$index]['attribute_id'] = $mappedAttributes[$filterable['attribute_id']];
                $mappedFilterables[$index]['attribute_group_id'] = $mappedAttributeGroups[$filterable['attribute_group_id']];
                $mappedFilterables[$index]['price'] = $filterable['price'];
            }
        }
        return $mappedFilterables;
    }

    public function fetchCollections(Request $request) {
        $collections = Collection::where('site_id', $this->siteid)->get();
        return $collections;
    }

    public function saveFilterables(Request $request) {
        EntityAttributes::updateOrCreate(
            ['entity_id' => $request->input('entity_id')],
            [
                'name' => $request->input('name'),
                'price' => $request->input('price')
            ]
        );
        return response()->json(['response' => 'Saved']);
    }

    public function createSource(Request $request) {
        $inputData = $this->validate($request, array(
            'type' => ''
        ));
        $uuid = Uuid::uuid4();
        $object = ['name' => 'New source', 'slug' => $uuid, 'fields' => []];
        if (!empty($inputData['type'])) {
            $profile = \Ironopolis\Skeleton\Profile::where('slug', $inputData['type'])->first();
            if (!empty($profile)) {
                $profileDecoded = json_decode($profile->data, true);
                $object['fields'] = $profileDecoded['fields'];
            }
        }
        $source = Source::updateOrCreate(
            ['slug' => $uuid],
            [
                'site_id' => $this->siteid, 
                'name' => 'New source',
                'slug' => $uuid,
                'data' => json_encode($object)
            ]
        );
        return $source;
    }

    public function saveSource(Request $request) {
        $inputData = $this->validate($request, array(
            'slug' => 'required',
            'name' => 'required'
        ));
        $source = Source::where(['slug' => $inputData['slug'], 'site_id' => $this->siteid])->first();
        if (!empty($source)) {
            Source::updateOrCreate(
                ['slug' => $inputData['slug'], 'site_id' => $this->siteid],
                ['name' => $inputData['name'], 'data' => json_encode($request->input())]
            );
            return response()->json(['response' => 'Saved']);
        }
        return response()->json(['response' => 'You do not have permission to edit this source.']);
    }

    public function testFetchRecords(Request $request) {
        $inputData = $this->validate($request, array(
            'slug' => 'required'
        ));
        $source = Source::where('slug', $inputData['slug'])->first();
        $sourceData = json_decode($source->data, true);
        $records = Record::whereIn('slug', $sourceData['data'])->get();
        if (!empty($records)) {
            return $records;
        }
        return [];
    }

    public function saveRecord(Request $request) {
        $inputData = $this->validate($request, array(
            'slug' => 'required',
            'record' => 'required'
        ));
        $record = ['slug' => $inputData['slug'], 'user_id' => $this->userid];
        if (!empty($inputData['record']['path'])) {
            $data['path'] = $inputData['record']['path'];
            $inputData['record']['slug'] = $inputData['slug'];
        }
        $data['data'] = json_encode($inputData['record']);
        Record::updateOrCreate(
            $record,
            $data
        );
        return response()->json(['response' => 'Saved']);
    }

    public function addRecord(Request $request) {
        $inputData = $this->validate($request, array(
            'slug' => 'required',
            'record' => 'required'
        ));
        $uuid = Uuid::uuid4();
        $source = Source::where(['slug' => $inputData['slug'], 'site_id' => $this->siteid])->first();
        if (!empty($source)) {
            $sourceData = json_decode($source->data, true);
            if (empty($sourceData['data'])) {
                $sourceData['data'] = [];
            }
            array_push($sourceData['data'], $uuid);
            $sourceData['total'] = count($sourceData['data']);
            Record::updateOrCreate(
                ['slug' => $uuid, 'user_id' => $this->userid],
                ['data' => json_encode($inputData['record'])]
            );
            Source::updateOrCreate(
                ['slug' => $inputData['slug'], 'site_id' => $this->siteid],
                ['data' => json_encode($sourceData)]
            );
            return response()->json(['response' => 'Saved']);
        }
        return response()->json(['response' => 'You do not have permission to add a record to this source.']);
    }

    public function deleteRecord(Request $request) {
        $inputData = $this->validate($request, array(
            'source' => 'required',
            'record' => 'required'
        ));
        $source = Source::where(['slug' => $inputData['source'], 'site_id' => $this->siteid])->first();
        if (!empty($source->data)) {
            $sourceDataArray = json_decode($source->data, true);
            $pos = array_search($inputData['record'], $sourceDataArray['data']);
            array_splice($sourceDataArray['data'], $pos, 1);
            $source->data = json_encode($sourceDataArray);
            $source->save();
            $record = Record::where('slug', $inputData['record'])
                ->where('user_id', $this->userid)
                ->delete(); 
            return response()->json(['response' => 'Record deleted']);
        }
    }
}
