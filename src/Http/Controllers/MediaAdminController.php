<?php

namespace Ironopolis\Skeleton\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Ironopolis\Skeleton\Block;

class MediaAdminController extends Controller
{
  public function store(Request $request) {
    $data = $request->except(['files']);
    $files = $request->file('files');
    if (!empty($files)) {
      foreach($files as $index => $file) {
        $ext = $file->guessClientExtension();
        $data['files'][] = $file->storeAs(null, $request->input('slug').'_'.$index.'.'.$ext, 's3');
      }
    } else if (!empty($existing = $request->input('files'))) {
      $data['files'] = $request->input('files');
    }
    $data['classes'] = json_decode($data['classes']);
    $block = [
      'meta' => json_encode($data),
      'slug' => $request->slug,
      'type' => 's-image',
      'title' => $request->title,
      'subtitle' => $request->subtitle,
      'content' => $request->content
    ];
    $block = Block::updateOrCreate(
      ['slug' => $request->slug],
      $block
    );
    if ($block) {
        return response()->json(['response'=>'Data was successfully added']);
    }
    return response()->json(['response'=>'There was a problem storing your selection']);
  }
}