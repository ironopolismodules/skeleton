<?php

namespace Ironopolis\Skeleton\Http\Controllers;

use App\Http\Controllers\Controller;
use Ironopolis\Skeleton\Collection;
use Ironopolis\Skeleton\Entity;
use Ironopolis\Skeleton\EntityAttributes;
use Ironopolis\Skeleton\AttributeGroups;
use Ironopolis\Skeleton\Profile;
use Ironopolis\Skeleton\Template;
use Ironopolis\Skeleton\Attribute;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    private $entity;
    private $data;
    private $filters;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware(function ($request, $next) {
            $this->data = [];
            $this->siteid = null;
            $this->entity = [];
            $this->filters = [];
            return $next($request);
        });
    }

    public function fetchEntities(Request $request) {
        $inputData = $this->validate($request, array(
            'path' => 'required'
        ));
        if (!empty($request->input('filters'))) {
            $this->filters = $request->input('filters');
        }
        $path = explode('/', $inputData['path']);
        unset($path[0]);
        unset($path[1]);
        $path = implode('/', $path);
        $collection = Collection::where('path', $path)->first();
        if (!empty($collection)) {
            $collectionData = json_decode($collection->data, true);
            $this->entity = $collectionData;
            if (!empty($this->entity['breadcrumbs'])) {
                if (!empty($sourceData['data'])) {
                    $sourceData['data'] = array_merge($sourceData['data'], $this->entity['breadcrumbs']);
                } else {
                    $sourceData['data'] = $this->entity['breadcrumbs'];
                }
            }
            //fetch all of a type
            if (!empty($collectionData['all'])) {
                $entityAttributes = EntityAttributes::all();
            } else {
                if (!empty($this->filters)) {
                    $this->filters = explode(',', $this->filters);
                    $entityAttributes = EntityAttributes::where('collection_id', $collection['slug'])->whereIn('attribute_id', $this->filters)->get();
                } else {
                    $entityAttributes = EntityAttributes::where('collection_id', $collection['slug'])->get();
                }
            }
            //we have entity ids?
            if (!empty($entityAttributes)) {
                $entityIds = $entityAttributes->unique('entity_id')->pluck('entity_id');
                if (!empty($this->filters)) {
                    $entityAttributes = EntityAttributes::whereIn('entity_id', $entityIds)->get();
                }
                $entities = Entity::whereIn('slug', $entityIds)->get();
                if (!empty($entities)) {
                    foreach($entities as $entity) {
                        $this->data[$entity->slug] = json_decode($entity->data);
                    }
                    $this->entity['entities'] = $entityIds->toArray();
                }
                //fetch attribute groups and corresponding attributes
                $attributeGroupIds = $entityAttributes->unique('attribute_group_id')->pluck('attribute_group_id');
                $attributeIds = $entityAttributes->unique('attribute_id')->pluck('attribute_id');
                $attributeGroups = AttributeGroups::whereIn('slug', $attributeGroupIds)->get();
                //get required data to page
                foreach($attributeGroups as $attributeGroup) {
                    $attributeGroupData = json_decode($attributeGroup['data'], true);
                    $this->data[$attributeGroup['slug']] = $attributeGroupData;
                    if (!empty($attributeGroupData['data'])) {
                        $attributes = Attribute::whereIn('slug', $attributeIds)->get();
                        foreach ($attributes as $attribute) {
                            if (!empty($attribute['data'])) {
                                $this->data[$attribute->slug] = json_decode($attribute['data']);
                            }
                        }
                    }
                }
                //dd($attributeGroups);
                //define structure of data for display
                $groupedAttributes = $entityAttributes->groupBy('attribute_group_id');
                if (!empty($attributeGroups)) {
                    $this->entity['attribute_groups'] = $attributeGroupIds->toArray();
                    //$this->entity['attributes'] = $attributeIds->toArray();
                    foreach($groupedAttributes as $attributeGroupId => $attributeGroupAttributes) {
                        if (!empty($this->data[$attributeGroupId]['name'])) {
                            //at some point will need to order the attributes using a definition
                            $this->entity[$this->data[$attributeGroupId]['name']] = $attributeGroupAttributes->unique('attribute_id')->pluck('attribute_id')->toArray();
                        }
                    }
                }
            }
        }
        $returnObject = [];
        $returnObject['entity'] = $this->entity;
        $returnObject['data'] = $this->data;
        return $returnObject;
    }

    public function fetchClasses(Request $request) {
        $profile = Profile::select('data')
                ->where('slug', 'tailwind')
                ->first();
        if (!empty($profile)) {
            $this->profile = json_decode($profile->data, true);
        }
        return $this->profile;
    }

    public function fetchComponent(Request $request) {
        $profile = Profile::select('data')
                ->where('slug', $request->input('category'))
                ->first();
        if (!empty($profile)) {
            $this->profile = json_decode($profile->data, true);
        }
        return $this->profile;
    }

    public function fetchTemplates(Request $request) {
        $templates = Template::where(['type' => $request->input('type'), 'user_id' => 0])->get();
        $this->userid = 1;
        if (!empty($this->userid)) {
            $userTemplates = Template::where(['type' => $request->input('type'), 'user_id' => $this->userid])->get();
            if (!empty($userTemplates)) {
                $templates = $templates->merge($userTemplates);
            }
        }
        return $templates;
    }
}
