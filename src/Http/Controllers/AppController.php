<?php

namespace Ironopolis\Skeleton\Http\Controllers;
use Symfony\Component\DomCrawler\Crawler;
use App\Http\Controllers\Controller;
use App\User;
use Ironopolis\Skeleton\State;
use Illuminate\Http\Request;
use Ironopolis\Skeleton\Block;
use Ironopolis\Skeleton\Body;
use Ramsey\Uuid\Uuid;
use Ironopolis\Skeleton\Template;
use Ironopolis\Skeleton\TemplateBlocks;
use Ironopolis\Skeleton\FormSubmissions;
use Ironopolis\Skeleton\Submissions;
use Ironopolis\Skeleton\Settings;
use Ironopolis\Skeleton\Form;
use Ironopolis\Skeleton\Profile;
use Ironopolis\Skeleton\Image;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use function GuzzleHttp\json_decode;
use function GuzzleHttp\json_encode;


class AppController extends Controller
{
    protected $taxonomy;
    private $userid;
    private $returnObject;
    private $user;
    private $data;
    private $siteid;

    public function __construct() {
        $this->middleware(function ($request, $next) {
            $this->userid = Auth::id();
            $this->user = Auth::user();
            $this->data = [];
            $this->returnObject = [
                'root' => null,
                'page' => null,
                'content' => [],
                'body' => [],
                'bodyData' => [],
                'blocks' => [],
                'bodyBlocks' => [],
                'target' => null
            ];
            if (!empty($this->user)) {
                $this->siteid = $this->user->site_id;
                if (!empty($this->user->data)) {
                    $this->user->data = json_decode($this->user->data);
                }
            }
            return $next($request);
        });
    }

    public function fetchAllImages() {
        $files = \Storage::disk('s3')->files();
        //dd($files);
        return $files;
    }

    public function pasteBlock(Request $request) {
        $inputData = $this->validate($request, array(
            'item' => 'required',
            'body' => '',
            'key' => '',
            'selection' => ''
        ));
        $returnObject = [];
        $block = Block::where('slug', $inputData['item'])->first();
        if (!empty($block)) {
            if (!empty($inputData['key'])) {
                $settings = Settings::select('data')->where('site_id', $this->siteid)->first();
                $settingsData = json_decode($settings->data, true);
            } else {
                if (!empty($inputData['selection'])) {
                    $targetBlock = Block::where(['slug' => $inputData['selection']])->first();
                    $targetBlockData = json_decode($targetBlock->data, true);
                }
                $body = Body::where(['slug' => $inputData['body']])->first();
                $bodyData = json_decode($body->data, true);
            }
            $blockData = json_decode($block->data);
            if (!empty($blockData->data)) {
                $newBlockSlug = Uuid::uuid4();
                //structure return object
                $this->returnObject['history'] = [];
                $blockData->slug = $newBlockSlug;
                $blockData->parent = $inputData['selection'];
                $this->returnObject['content'][$newBlockSlug] = $blockData;
                //$this->returnObject['history'][$templateData->root] = $newBlockSlug;
                array_push($this->returnObject['blocks'], $newBlockSlug);
                $this->returnObject['root'] = $newBlockSlug;
                if (!empty($blockData->data)) {
                    foreach($blockData->data as $slug) {
                        $this->duplicateBlock($slug, $newBlockSlug);
                    }
                }
                //dd($this->returnObject);
                $content = [];
                //loop and add all blocks in one go
                //dd($this->returnObject['content']);
                $blockTypes = [
                    's-wrapper',
                    's-svg',
                    's-path',
                    's-carousel',
                    's-transition',
                    's-media',
                    's-table',
                    's-date',
                    's-form',
                    's-input',
                    's-iframe',
                    's-shape',
                    's-source'
                ];
                foreach($this->returnObject['content'] as $item) {
                    array_push($content, $item);
                    $typeIndex = array_search($item->type, $blockTypes);
                    Block::updateOrCreate(
                        ['slug' => $item->slug],
                        [
                            'user_id' => $this->userid, 
                            'slug' => $item->slug,
                            'typeIndex' => $typeIndex,
                            'data' => json_encode($item)
                        ]
                    );
                }
                //flatten content to array
                $this->returnObject['content'] = $content;
                if (!empty($inputData['key'])) {
                    //save new root item to body
                    if (empty($settingsData['blocks'])) {
                        $settingsData['blocks'] = [];
                    }
                    $settingsData['blocks'] = array_merge($this->returnObject['blocks'], $settingsData['blocks']);
                    if (empty($settingsData[$inputData['key']])) {
                        $settingsData[$inputData['key']] = [];
                    }
                    array_push($settingsData[$inputData['key']], $this->returnObject['root']);
                    //add to settings
                    Settings::updateOrCreate(
                        ['site_id' => $this->siteid],
                        ['data' => json_encode($settingsData)]
                    );
                } else {
                    if (!empty($inputData['selection'])) {
                        if (empty($targetBlockData['data'])) {
                            $targetBlockData['data'] = [];
                        }
                        array_push($targetBlockData['data'], $this->returnObject['root']);
                        Block::updateOrCreate(
                            ['slug' => $inputData['selection']],
                            [
                                'user_id' => $this->userid, 
                                'slug' => $inputData['selection'],
                                'data' => json_encode($targetBlockData)
                            ]
                        );
                    } else {
                        if (empty($bodyData['data'])) {
                            $bodyData['data'] = [];
                        }
                        array_push($bodyData['data'], $this->returnObject['root']);
                    }
                    //save new root item to body
                    if (empty($bodyData['blocks'])) {
                        $bodyData['blocks'] = [];
                    }
                    $bodyData['blocks'] = array_merge($this->returnObject['blocks'], $bodyData['blocks']);
                    Body::updateOrCreate(
                        ['slug' => $inputData['body']],
                        ['data' => json_encode($bodyData)]
                    );
                }
                return $this->returnObject;
            }
        }
        return [];
    }

    public function duplicatePage(Request $request) {
        return redirect('/developer-plans');
    }

    public function duplicateBlock($block, $target) {
        $block = block::where(['slug' => $block])->first(); 
        if (!empty($block->data)) {
            $blockData = json_decode($block->data);
            $newBlockSlug = Uuid::uuid4();
            $blockData->slug = $newBlockSlug;
            $blockData->parent = $target;
            $offset = array_search($block, $this->returnObject['content'][$target]->data);
            array_splice($this->returnObject['content'][$target]->data, $offset, 1);
            array_push($this->returnObject['blocks'], $newBlockSlug);
            array_push($this->returnObject['content'][$target]->data, $newBlockSlug);
            $this->returnObject['content'][$newBlockSlug] = $blockData;
            if (!empty($blockData->data)) {
                foreach($blockData->data as $slug) {
                    $this->duplicateBlock($slug, $newBlockSlug);
                }
            }
        }
        return;
    }

    public function sitemap() {
        $base_url = env('APP_URL');
        $bodies = Body::where(['searchable' => true, 'user_id' => 0])->get();
        $sitemap_start = '<?xml version="1.0" encoding="UTF-8"?>
        <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        $sitemap_end = '</urlset>';
        $date = date('Y-m-d');
        foreach ($bodies as $body) {
        //   $slash = '/';
        //   if ($body->path == '/') {
        //     $slash = '';
        //     $body->path = '';
        //   }
          $sitemap_start = $sitemap_start . "
          <url>
            <loc>{$base_url}{$body->path}</loc>
            <lastmod>{$date}</lastmod>
          </url>
          ";
        }
        $sitemap_start = $sitemap_start . $sitemap_end;
        print_r($sitemap_start);exit;
    }

    public function parseElement($element, $parentUuid) {
        $children = $element->children();
        foreach ($children as $index => $childElement) {
            if (empty($this->returnObject['content'][$parentUuid]['data']) || !is_array($this->returnObject['content'][$parentUuid]['data'])) {
                $this->returnObject['content'][$parentUuid]['data'] = [];
            }
            $uuid = $this->createElement($childElement, $parentUuid);
            array_push($this->returnObject['content'][$parentUuid]['data'], $uuid);
            $hasElement = false;
            foreach ($childElement->childNodes as $index => $nestedChildElement) {
                if ($nestedChildElement->nodeType == 1) {
                    $hasElement = true;
                }
            }
            if ($hasElement) {
                $newcrawler = new \Symfony\Component\DomCrawler\Crawler($childElement);
                $this->parseElement($newcrawler, $uuid);
            }
        }
    }

    public function sanitiseHtml($html) {
        $html = str_replace(array("\n", "\t", "\r"), '', $html);
        $search = array(
            '/\>[^\S ]+/s',     // strip whitespaces after tags, except space
            '/[^\S ]+\</s',     // strip whitespaces before tags, except space
            '/(\s)+/s',         // shorten multiple whitespace sequences
            '/<!--(.|\s)*?-->/' // Remove HTML comments
        );
        $replace = array(
            '>',
            '<',
            '\\1',
            ''
        );
        $html = preg_replace($search, $replace, $html);
        $html = str_replace('> ', '>', $html);
        $html = str_replace(' <', '<', $html);
        return $html;
    }

    public function uploadHtml(Request $request) {
        $this->returnObject['page'] = $request->input('body');
        $sanitised = $this->sanitiseHtml($request->input('html'));
        $crawler = new \Symfony\Component\DomCrawler\Crawler($sanitised);
        $rootChildren = $crawler->filter('body')->children();
        foreach ($rootChildren as $index => $childElement) {
            if ($childElement->nodeType == 1) {
                $uuid = $this->createElement($childElement, null);
                array_push($this->returnObject['bodyData'], $uuid);
                if ($childElement->hasChildNodes()) {
                    $this->parseElement($rootChildren, $uuid);
                }
            }
        }
        $body = $this->saveElementTree();
        return redirect('/'.$body->path.'?edit'); 
    }

    public function saveElementTree() {
        $body = Body::where(['slug' => $this->returnObject['page'], 'site_id' => $this->siteid])->first();
        $bodyData = json_decode($body->data, true);
        if (empty($bodyData['data'])) {
            $bodyData['data'] = [];
        }
        $bodyData['data'] = array_merge($bodyData['data'], $this->returnObject['bodyData']);
        if (empty($bodyData['blocks'])) {
            $bodyData['blocks'] = [];
        }
        $bodyData['blocks'] = array_merge($bodyData['blocks'], $this->returnObject['bodyBlocks']);
        //dd('halt', $this->returnObject, $bodyData);
        Body::updateOrCreate(
            ['slug' => $this->returnObject['page']],
            ['data' => json_encode($bodyData)]
        );
        foreach($this->returnObject['content'] as $item) {
            Block::updateOrCreate(
                ['slug' => $item['slug']],
                [
                    'user_id' => $this->userid, 
                    'slug' => $item['slug'],
                    'data' => json_encode($item)
                ]
            );
        }
        return $body;
    }

    public function createElement($element, $parent) {
        $node = [];
        $node['slug'] = Uuid::uuid4()->toString();
        $node['tag'] = $element->tagName;
        if ($node['tag'] == 'input') {
            $node['type'] = 's-input';
        } else {
            $node['type'] = 's-wrapper';
        }
        if ($element->getAttribute('class')) {
            $node['classes'] = explode(' ', $element->getAttribute('class'));
        }
        $text = '';
        if ($element->hasChildNodes()) {
            foreach ($element->childNodes as $index => $childElement) {
                if ($childElement->nodeType === 3) {
                    $text = $childElement->textContent;
                    break;
                }
            }
        }
        if ($text != '') {
            $node['text'] = $text;
        }
        if ($parent) {
            $node['parent'] = $parent;
        }
        $this->returnObject['content'][$node['slug']] = $node;
        array_push($this->returnObject['bodyBlocks'], $node['slug']);
        return $node['slug'];
    }

    //need to add in check for existing image if this is needed
    // public function storeImages() {
    //     $images = $this->fetchAllImages();
    //     if (!empty($images)) {
    //         $type = Type::where('type', 's-image')->first();
    //         foreach($images as $image) {
    //             $string = explode('.' , $image);
    //             $string = $string[0];
    //             $string = ucwords(str_replace('-', ' ', $string));
    //             $slug = Uuid::uuid4();
    //             $data['type'] = 's-image';
    //             $data['src'] = 'https://stellifysoftware.s3.eu-west-2.amazonaws.com/'.$image;
    //             $data['slug'] = $slug;
    //             $data['alt'] = $string;
    //             $imageBlock = Block::updateOrCreate(
    //                 ['slug' => $slug],
    //                 [
    //                     'user_id' => $this->userid,
    //                     'name' => $string,
    //                     'slug' => $slug,
    //                     'data' => json_encode($data)
    //                 ]
    //             );
    //             $type->blocks()->save($imageBlock);
    //         }
    //         exit('Finished');
    //     }
    // }
    public function fetchProfile(Request $request) {
        $inputData = $this->validate($request, array(
            'profile' => 'required'
        ));
        $profile = Profile::select('data')
                ->where('slug', $inputData['profile'])
                ->first();
        if (!empty($profile)) {
            $this->profile = json_decode($profile->data, true);
        }
        return $this->profile;
    }

    public function fetchMethods(Request $request) {
        $profileDecoded = [];
        $profile = \Ironopolis\Skeleton\Profile::select('data')
            ->where('slug', 'methods')
            ->first();
        if (!empty($profile)) {
            $profileDecoded = json_decode($profile->data, true);
        }
        return $profileDecoded;
    }

    public function fetchPage(Request $request) {
        $inputData = $this->validate($request, array(
            'source' => 'required',
            'siteid' => 'required',
            'currentPage' => 'required',
            'offset' => 'required'
        ));
        $data = [];
        if (!empty($inputData['source']) && $inputData['source'] == 'Body') {
            $NamespacedModel = '\\Ironopolis\\Skeleton\\'.$inputData['source'];
            $records = $NamespacedModel::where('site_id', $inputData['siteid'])->get();
            if (!empty($records)) {
                foreach($records as $record) {
                    $data[$record->slug] = json_decode($record->data);
                }
            }
            return $data;
        } else if (!empty($inputData['source']) && $inputData['source'] != 'undefined') {
            $source = Source::where('slug', $inputData['source'])->first();
            $sourceData = json_decode($source->data, true);
            $paginatedRecords = array_slice($sourceData['data'], $inputData['currentPage'] * $inputData['offset'], $inputData['offset']);
            if (!empty($paginatedRecords)) {
                $records = Record::whereIn('slug', $paginatedRecords)->get();
                foreach($records as $record) {
                    $data[$record->slug] = json_decode($record->data);
                }
            }
            return $data;
        }
    }

    public function fetchSections(Request $request) {
        $inputData = $this->validate($request, array(
            'section' => 'required'
        ));
        $section = Body::select()
            ->where('path', 'LIKE', '%'.$inputData['section'].'%')
            ->where('site_id', $this->siteid)
            ->get();
        return $section;
    }

    public function fetchSection(Request $request) {
        $inputData = $this->validate($request, array(
            'slug' => 'required'
        ));
        $returnData = [];
        $section = Body::select()
            ->where('slug', $inputData['slug'])
            // ->where('site_id', $this->siteid)
            ->first();
        $decodedData = json_decode($section['data'], true);
        $returnData['roots'] = $decodedData['data'];
        $blocks = Block::whereIn('slug', $decodedData['blocks'])->get();
        foreach($blocks as $block) {
            $returnData['blocks'][$block->slug] = json_decode($block->data);
        }
        return $returnData;
    }

    public function fetchAssets(Request $request) {
        $files = \Storage::disk('s3')->files();
        dd($files);
        return $files;
    }

    public function saveUser(Request $request) {
        $userData = $request->input();
        if (!empty($userData)) {
            State::updateOrCreate(
                ['slug' => $this->user->slug],
                [
                    'slug' => $this->user->slug,
                    'data' => json_encode($request->input())
                ]
            );
        }
        return response()->json(['response' => 'Saved']);
    }

    public function fetchForms(Request $request) {
        $forms = Form::select()->where('site_id', $this->siteid)->get();
        return $forms;
    }

    public function attachForm(Request $request) {
        $inputData = $this->validate($request, array(
            'slug' => 'required',
            'target' => 'required'
        ));
        $form = Form::select()->where(['site_id' => $this->siteid, 'slug' => $inputData['slug']])->first();
        $decodedData = json_decode($form->data, true);
        if (empty($decodedData['data'])) {
            $decodedData['data'] = [];
        }
        if (!in_array($inputData['target'], $decodedData['data'])) {
            array_push($decodedData['data'], $inputData['target']);
        }
        Form::updateOrCreate(
            ['slug' => $inputData['slug'], 'site_id' => $this->siteid],
            ['data' => json_encode($decodedData)]
        );
        return response()->json(['saved' => true]);
    }

    public function storeFormSubmission(Request $request) {
        $inputData = $this->validate($request, array(
            'slug' => '',
            'redirect' => '',
            'redirectMessage' => ''
        ));
        $uuid = Uuid::uuid4();
        FormSubmissions::updateOrCreate(
            ['slug' => $uuid],
            [
                'site_id' => $this->siteid,
                'user_id' => $this->userid,
                'form_id' => $inputData['slug'],
                'slug' => $uuid,
                'actioned' => 0,
                'data' => json_encode($request->input())
            ]
        );
        if (!empty($inputData['redirect'])) {
            return redirect()->route($inputData['redirect'])->with('message',$inputData['redirectMessage']);
        }
        return back()->with('message', $inputData['redirectMessage']);
    }

    public function storeUserInput(Request $request) {
        $inputData = $this->validate($request, array(
            'slug' => 'required',
            'value' => 'required',
            'form' => 'required'
        ));
        $form = Form::select()->where(['site_id' => $this->siteid, 'slug' => $inputData['form']])->first();
        $formDecoded = json_decode($form->data, true);
        $blocks = Block::whereIn('slug', $formDecoded['data'])->get();
        $submissions = Submissions::whereIn('slug', $formDecoded['data'])->get();
        $structuredSubmissions = [];
        foreach($submissions as $submission) {
            $structuredSubmissions[$submission->slug] = $submission;
        }
        $errors = [];
        foreach($blocks as $block) {
            $blockDecoded = json_decode($block->data);
            if (!empty($blockDecoded->required) && empty($structuredSubmissions[$block->slug]['value'])) {
                $errors[$blockDecoded->slug] = $blockDecoded->name . ' is a required field.';
            }
            if (!empty($blockDecoded->max) && !empty($structuredSubmissions[$block->slug]['value']) && $structuredSubmissions[$block->slug]['value'] <= $blockDecoded->max) {
                $errors[$blockDecoded->slug] = $blockDecoded->name . ' is above the maximum value permitted.';
            }
            if (!empty($blockDecoded->min) && !empty($structuredSubmissions[$block->slug]['value']) && $structuredSubmissions[$block->slug]['value'] >= $blockDecoded->min) {
                $errors[$blockDecoded->slug] = $blockDecoded->name . ' is below the minimum value permitted.';
            }
            if (!empty($blockDecoded->maxlength) && !empty($structuredSubmissions[$block->slug]['value']) && $structuredSubmissions[$block->slug]['value'] <= $blockDecoded->maxlength) {
                $errors[$blockDecoded->slug] = $blockDecoded->name . ' is beyond the maximum length permitted.';
            }
            if (!empty($blockDecoded->minlength) && !empty($structuredSubmissions[$block->slug]['value']) && $structuredSubmissions[$block->slug]['value'] >= $blockDecoded->minlength) {
                $errors[$blockDecoded->slug] = $blockDecoded->name . ' is beneath the minimum length permitted.';
            }
        }
        if (empty($errors)) {
            Submissions::updateOrCreate(
                ['slug' => $inputData['slug'], 'user_id' => $this->userid],
                ['user_id' => $this->userid, 'value' => $inputData['value']]
            );
            return response()->json(['saved' => true]);
        }
        return $errors;
    }

    public function defineComponent(Request $request) {
        $inputData = $this->validate($request, array(
            'root' => 'required',
            'global' => 'required',
            'targetSlug' => 'required'
        ));

        if ($inputData['global'] == 'true') {

        } else {
            $body = Body::where(['slug' => $inputData['targetSlug'], 'site_id' => $this->siteid])->first();
            $bodyData = json_decode($body->data, true);
            $bodyData["components"] = [];
            array_push($bodyData["components"], $inputData['root']);
            $body->data = json_encode($bodyData);
            $body->save();
            //$block = Block::where(['slug' => $inputData['root']])->first();
            // $blockData = json_decode($block->data, true);
            // dd($blockData);
            // if (!empty($blockData['data'])) {

            // }
        }
        return 'Saved';
    }

    public function storeProfileImage(Request $request) {
        if (!empty($request->file('image'))) {
            $extension = $request->file('image')->getClientOriginalExtension();
            $logoRules = '';
            if ($extension != 'svg') {
                $logoRules .= 'image|mimes:jpeg,png,jpg,gif,svg,html,image/svg|max:2048';
            }
            $inputData = $this->validate($request, array(
                'image' => $logoRules
            ));
            if ($file = $request->file('image')) { 
                $ext = $file->guessClientExtension();
                $fileURL = env('AWS_URL', '');
                $filename = $fileURL . $file->storeAs('projects/images', $this->user->slug.'.'.$ext, 's3');
            }
            if (!empty($filename)) {
                $data['src'] = $filename;
                $data['slug'] = $this->user->slug;
                $data['type'] = $ext;
                $imageBlock = Image::updateOrCreate(
                    ['slug' => $this->user->slug],
                    [
                        'user_id' => $this->userid,
                        'site_id' => $this->siteid,
                        'slug' => $this->user->slug,
                        'data' => json_encode($data)
                    ]
                );
            }
        }
        
        $userTable = User::where('slug', $this->user->slug)->first();
        $userDataTable = State::where('slug', $this->user->slug)->first();
        if (!empty($userTable) && !empty($userDataTable)) {
            $userData = json_decode($userDataTable, true);
            if (!empty($filename)) {
                $userData['profileImage'] = $filename;
            }
            User::updateOrCreate(
                ['slug' => $this->user->slug],
                [
                    'name' => !empty($request->input('name')) ? $request->input('name') : $userTable->name,
                    'email' => !empty($request->input('email')) ? $request->input('email') : $userTable->email,
                    'postal_code' => !empty($request->input('postal_code')) ? $request->input('postal_code') : $userTable->postal_code,
                    'country' => !empty($request->input('country')) ? $request->input('country') : $userTable->country,
                    'profileImage' => !empty($filename) ? $filename : ''
                ]
            );
            State::updateOrCreate(
                ['slug' => $this->user->slug],
                [
                    'data' => json_encode($userData)
                ]
            );
            return redirect('/account');
            // return response()->json(['response' => $filename]);
        }

        return response()->json(['response' => 'There was a problem']);
    }

    public function storeImageAjax(Request $request) {
        //intervention is installed and is using imagick
        // $img = Image::make('clientlogos.jpg')->resize(300, 200);
        // return $img->response('jpg');
        $extension = $request->file('image')->getClientOriginalExtension();
        $logoRules = '';
        if ($extension != 'svg') {
            $logoRules .= 'image|mimes:jpeg,png,jpg,gif,svg,html,image/svg|max:2048';
        }
        $inputData = $this->validate($request, array(
            'alt' => '',
            'image' => $logoRules,
            'caption' => '',
            'name' => '',
            'user' => ''
        ));
        $slug = Uuid::uuid4();
        if ($file = $request->file('image')) { 
            $ext = $file->guessClientExtension();
            $fileURL = env('AWS_URL', '');
            $filename = $fileURL . $file->storeAs('images/' . $this->userid, $slug.'.'.$ext, 's3');
        }
        if (!empty($filename)) {
            $data['src'] = $filename;
            $data['slug'] = $slug;
            $data['type'] = $ext;
            $imageBlock = Image::updateOrCreate(
                ['slug' => $slug],
                [
                    'user_id' => $this->userid,
                    'type' => $ext,
                    'dimensions' => '',
                    'slug' => $slug,
                    'data' => json_encode($data)
                ]
            );

            return response()->json(['response' => $filename]);
        }
        return response()->json(['response' => 'There was a problem']);
    }

    public function storeImage(Request $request) {
        //intervention is installed and is using imagick
        // $img = Image::make('clientlogos.jpg')->resize(300, 200);
        // return $img->response('jpg');
        $inputData = $this->validate($request, array(
            'imageAlt' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'caption' => ''
        ));
        $this->userid = Auth::id();
        if ($file = $request->file('image')) { 
            $ext = $file->guessClientExtension();
            $slug = Uuid::uuid4();
            $filename = $file->storeAs('images/' . $this->userid, $slug.'.'.$ext, 's3');
        }

        if (!empty($filename)) {
            $data['type'] = 's-imagenew';
            $data['imageName'] = $filename;
            $data['slug'] = $slug;
            $data['caption'] = $request->input('caption');
            $data['imageAlt'] = $request->input('imageAlt');
            $imageBlock = Block::updateOrCreate(
                ['slug' => $slug],
                [
                    'user_id' => $this->userid,
                    'name' => $data['imageAlt'],
                    'slug' => $slug,
                    'data' => json_encode($data)
                ]
            );
            $type->blocks()->save($imageBlock);
            return redirect('/media-editor');
        }
        return response()->json(['response' => 'There was a problem']);
    }

    public function slugify($text) {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    public function storeForm(Request $request) {
        $inputData = $this->validate($request, array(
            'routes' => 'required'
        ));
        $this->userid = Auth::id();
        $bodies = Body::whereIn('slug', $inputData['routes'])->where('user_id', $this->userid)->get();
        if (!empty($bodies)) {
            $existingId = $request->input('id');
            if (!empty($existingId)) {
                $savedBlock = Block::updateOrCreate(
                    ['slug' => $request->input('name')],
                    [
                        'name' =>  '', 
                        'data' => json_encode($request->input())
                    ]
                );
                foreach($bodies as $body) {
                    $body->blocks()->save($savedBlock);
                }
            } else {
                //generate id
                $id = Uuid::uuid4();
                //create the form block
                $block = [
                    'type' => 's-formnew',
                    'id' => $id,
                    'routes' => $inputData['routes']
                ];
                $savedBlock = Block::updateOrCreate(
                    ['slug' => $id],
                    [
                        'name' =>  '', 
                        'user_id' => $this->userid, 
                        'slug' => $id,
                        'data' => json_encode($block)
                    ]
                );
                //link to specified bodies
                foreach($bodies as $body) {
                    $body->blocks()->save($savedBlock);
                }
            }
        }
        return $block;
    }

    public function createProject(Request $request) {
        if (!empty($this->user)) {
            $this->user->site_id = Uuid::uuid4()->toString();
            if (!empty($this->user->data)) {
                $userData = (array) $this->user->data;
            } else {
                $userData = [];
            }
            if (!empty($this->user->data->projects) && is_array($this->user->data->projects)) {
                $userData['projects'][] = $this->user->site_id;
            } else {
                $userData['projects'] = [];
                $userData['projects'][] = $this->user->site_id;
            }
            $this->user->data = json_encode($userData);
            $this->user->save();
            $settingsData['name'] = $request->input('name');
            $settingsData['description'] = $request->input('description');
            $settingsData['privacy'] = $request->input('privacy');
            $settingsData['site_id'] = $this->user->site_id;
            Settings::updateOrCreate(
                ['site_id' => $this->user->site_id],
                ['data' => json_encode($settingsData)]
            );
            return redirect('/control');
        }
        return redirect('/');
    }

    public function createPage(Request $request) {
        $uuid = Uuid::uuid4();
        $data = [];
        $data['name'] = !empty($inputData['name']) ? $inputData['name'] : 'New page';
        $data["data"] = [];
        Body::updateOrCreate(
            ['slug' => $uuid],
            [
                'site_id' => $this->siteid,
                'user_id' => $this->userid,
                'slug' => $uuid,
                'path' => $uuid,
                'data' => json_encode($data)
            ]
        );
        return redirect('/'.$uuid.'?edit');
    }

    public function deletePage(Request $request) {
        $inputData = $this->validate($request, array(
            'path' => 'required'
        ));
        $body = Body::where(['site_id' => $this->siteid, 'path' => $inputData['path']])
            ->first();
        if (!empty($body)) {
            $bodyData = json_decode($body->data, true);
            if (!empty($bodyData['data'])) {
                foreach($bodyData['data'] as $block) {
                    $this->fetchBlockRecursive($block);
                }
            }
            $this->deleteBlocks($body, null, $bodyData, null);
            $body->delete();
            return redirect('/control');
        }
        return response()->json(['response' => 'We could not find that page. Please contact us.']); 
    }

    public function getImages(Request $request) {
        $inputData = $this->validate($request, array(
            'image' => 'required'
        ));
        $user_id = Auth::id();
        $type = Type::where('type', 's-image')->first();
        $blocks = $type->blocks()
            ->where('name', 'LIKE', '%'.$inputData['image'].'%')
            ->where('user_id', $user_id ? $user_id : 0)
            ->get();
        foreach ($blocks as $block) {
            $images[] = json_decode($block->data);
        }
        return !empty($images) ? collect($images) : '[]';
    }
    //used by columns edit
    public function getBlocks(Request $request) {
        $query = $request->input('query');
        $type = $request->input('type');
        if (!empty($type) && $type != 'null') {
            $type = Type::where('type', $type)->first();
            if (!empty($type)) {
                $block = $type->blocks()->where('user_id', Auth::id())->get();
            }
            $result['results'] = $block->toArray();
            return $result;
        } else {
            $block = Block::select('name','slug','data')
                ->where('name', 'LIKE', '%'.$query.'%')
                ->where('user_id', 'LIKE', Auth::id())
                ->get();
            $result['results'] = $block->toArray();
            return $result;
        }
        return [];
    }

    public function fetchContent(Request $request) {
        $this->userid = Auth::id();
        if (empty($this->userid)) {
            $this->userid = 0;
        }
        $type = Type::where('type', $request->input('category'))->first();
        $items = $type->blocks()->where('user_id', $this->userid)->get();
        return $items;
        dd($request->input(), $type, $items);
        $query = $request->input('query');
        $block = Block::select('name','slug','data')
            ->where('name', 'LIKE', '%'.$query.'%')
            ->where('user_id', 'LIKE', Auth::id())
            ->get();
        $result['results'] = $block->toArray();
        return $result;
    }

    public function getRoute(Request $request) {
        //dd(Auth::id());
        $query = $request->input('query');
        $body = Body::select('slug')
            ->where('name', 'LIKE', '%'.$query.'%')
            ->where('user_id', Auth::id())
            ->get();
        $data['results'] = $body->toArray();
        return $data;
    }

    public function saveBlock(Request $request) {
        $inputData = $this->validate($request, array(
            'slug' => 'required'
        ));
        Block::updateOrCreate(
            ['slug' => $inputData['slug'], 'user_id' => $this->userid],
            ['data' => json_encode($request->input())]
        );
        return response()->json(['response' => 'Saved']);
    }

    public function saveBodyData(Request $request) {
        $inputData = $this->validate($request, array(
            'body' => 'required'
        ));
        $body = Body::where(['slug' => $inputData['body'], 'site_id' => $this->siteid])->first();
        $bodyDataArray = json_decode($body->data, true);
        $bodyDataArray['data'] = $inputData['blocks'];
        $body->data = json_encode($bodyDataArray);
        $body->save();
        return response()->json(['response' => 'Saved']);
    }

    public function saveBody(Request $request) {
        $inputData = $this->validate($request, array(
            'slug' => 'required'
        ));
        Body::updateOrCreate(
            ['slug' => $inputData['slug']],
            [
                'path' => $request->input('path'),
                'searchable' => $request->input('searchable'),
                'data' => json_encode($request->input())
            ]
        );
        return response()->json(['response' => 'Saved']);
    }

    public function insertBlock(Request $request) {
        $inputData = $this->validate($request, array(
            'type' => 'required'
        ));
        $blockTypes = [
            's-wrapper',
            's-svg',
            's-path',
            's-carousel',
            's-transition',
            's-media',
            's-table',
            's-date',
            's-form',
            's-input',
            's-iframe',
            's-shape',
            's-source'
        ];
        $typeIndex = array_search($inputData['type'], $blockTypes);
        $uuid = Uuid::uuid4();
        $block = [
            'type' => $inputData['type'],
            'tag' => 'div',
            'classes' => [],
            'slug' => $uuid
        ];
        if (!empty($request->input('selection'))) {
            $block['parent'] = $request->input('selection');
        }
        if (!empty($request->input('key'))) {
            $settings = Settings::select('data')->where('site_id', $this->siteid)->first();
            $settingsData = json_decode($settings->data, true);
            if (!empty($settingsData)) {
                if (empty($request->input('key'))) {
                    if (empty($settingsData[$request->input('key')])) {
                        $settingsData[$request->input('key')] = [];
                    }
                    array_push($settingsData[$request->input('key')], $uuid);
                }
                if (!empty($request->input('selection'))) {
                    $parent = Block::where(['slug' => $request->input('selection'), 'user_id' => $this->userid])->first(); 
                    $parentData = json_decode($parent->data, true);
                    if (empty($parentData['data'])) {
                        $parentData['data'] = [];
                    }
                    array_push($parentData['data'], $uuid);
                    Block::updateOrCreate(
                        ['slug' => $request->input('selection'), 'user_id' => $this->userid],
                        ['data' => json_encode($parentData)]
                    );
                }
                if (empty($settingsData['blocks'])) {
                    $settingsData['blocks'] = [];
                }
                array_push($settingsData['blocks'], $uuid);
                Settings::updateOrCreate(
                    ['site_id' => $this->siteid],
                    ['data' => json_encode($settingsData)]
                );
            }
        } else {
            $body = Body::where(['slug' => $request->input('body'), 'site_id' => $this->siteid])->first();
            $bodyData = json_decode($body->data, true);
            if (!empty($request->input('selection'))) {
                $parent = Block::where(['slug' => $request->input('selection'), 'user_id' => $this->userid])->first(); 
                $parentData = json_decode($parent->data, true);
                if (empty($parentData['data'])) {
                    $parentData['data'] = [];
                }
                array_push($parentData['data'], $uuid);
                Block::updateOrCreate(
                    ['slug' => $request->input('selection'), 'user_id' => $this->userid],
                    ['data' => json_encode($parentData)]
                );
            } else {
                array_push($bodyData['data'], $uuid);
            }
            if (empty($bodyData['blocks'])) {
                $bodyData['blocks'] = [];
            }
            array_push($bodyData['blocks'], $uuid);
            Body::updateOrCreate(
                ['slug' => $request->input('body')],
                ['data' => json_encode($bodyData)]
            );
        }
        $savedBlock = Block::updateOrCreate(
            ['slug' => $uuid],
            [
                'user_id' => $this->userid,
                'typeIndex' => $typeIndex,
                'slug' => $uuid,
                'data' => json_encode($block)
            ]
        );
        return $savedBlock;
    }

    public function collectBlocks($slug) {
        $block = Block::where(['slug' => $slug])->first();
        $blockData = json_decode($block['data'], true);
        array_push($this->returnObject['content'], $slug);
        array_push($this->returnObject['blocks'], $block);
        if (!empty($blockData['data'])) {
            foreach($blockData['data'] as $slug) {
                $this->collectBlocks($slug);
            }
        }
        return;
    }

    public function storeAsTemplate(Request $request) {
        $inputData = $this->validate($request, array(
            'root' => 'required'
        ));
        $block = Block::where('slug', $inputData['root'])->first();
        $blockData = json_decode($block->data);
        $this->returnObject['root'] = $inputData['root'];
        $this->collectBlocks($inputData['root']);
        //store the definitions as template blocks
        foreach($this->returnObject['blocks'] as $item) {
            TemplateBlocks::updateOrCreate(
                ['slug' => $item['slug']],
                [
                    'user_id' => $this->userid,
                    'typeIndex' => $item['typeIndex'],
                    'slug' => $item['slug'],
                    'data' => $item['data']
                ]
            );
        }
        $newSlug = Uuid::uuid4();
        $template = [];
        $template['root'] = $inputData['root'];
        $template['data'] = $this->returnObject['content'];
        //store the template data
        Template::updateOrCreate(
            ['slug' => $newSlug],
            [
                'user_id' => $this->userid,
                'site_id' => $this->siteid,
                'image' => '',
                'name' => $request->input('name'),
                'slug' => $newSlug,
                'type' => $request->input('type'),
                'data' => json_encode($template)
            ]
        );
        return;
    }

    public function loadDemoTemplate(Request $request) {
        $inputData = $this->validate($request, array(
            'template' => 'required'
        ));
        $returnObject = [];
        $template = Template::where('slug', $inputData['template'])->first();
        if (!empty($template)) {
            $data = json_decode($template->data);
            $returnObject['root'] = $data->root;
            $returnObject['content'] = [];
            if (!empty($data->data)) {
                $content = Templateblocks::whereIn('slug', $data->data)->get();
                foreach($content as $item) {
                    array_push($returnObject['content'], json_decode($item->data));
                }

            }
            return $returnObject;
        }
    }

    public function loadTemplate(Request $request) {
        $inputData = $this->validate($request, array(
            'template' => 'required'
        ));
        $returnObject = [];
        $template = Template::where('slug', $inputData['template'])->first();
        if (!empty($template)) {
            if (!empty($request->input('key'))) {
                $settings = Settings::select('data')->where('site_id', $this->siteid)->first();
                $settingsData = json_decode($settings->data, true);
            } else {
                if (!empty($request->input('selection'))) {
                    $targetBlock = Block::where(['slug' => $request->input('selection')])->first();
                    $targetBlockData = json_decode($targetBlock->data, true);
                }
                $body = Body::where(['slug' => $request->input('body')])->first();
                $bodyData = json_decode($body->data, true);
            }
            $templateData = json_decode($template->data);
            if (!empty($templateData->data)) {
                if ($templateData->root) {
                    $uuid = Uuid::uuid4();
                    $newBlockSlug = $uuid->toString();
                    //fetch the orginal block
                    $block = Templateblocks::where(['slug' => $templateData->root])->first(); 
                    $blockData = json_decode($block->data);
                    $blockData->slug = $newBlockSlug;
                    //structure return object
                    $this->returnObject['history'] = [];
                    $this->returnObject['content'][$newBlockSlug] = $blockData;
                    $this->returnObject['history'][$templateData->root] = $newBlockSlug;
                    array_push($this->returnObject['blocks'], $newBlockSlug);
                    $this->returnObject['root'] = $newBlockSlug;
                    if (!empty($blockData->data)) {
                        foreach($blockData->data as $slug) {
                            $this->duplicateTemplateBlock($slug, $newBlockSlug);
                        }
                    }
                    //dd($this->returnObject);
                    $content = [];
                    //loop and add all blocks in one go
                    //dd($this->returnObject['content']);
                    $blockTypes = [
                        's-wrapper',
                        's-svg',
                        's-path',
                        's-carousel',
                        's-transition',
                        's-media',
                        's-table',
                        's-date',
                        's-form',
                        's-input',
                        's-iframe',
                        's-shape',
                        's-source'
                    ];
                    //dd($this->returnObject['content']);
                    foreach($this->returnObject['content'] as $item) {
                        if (!empty($item->watch)) {
                            $item->watch = $this->returnObject['history'][$item->watch];
                        }
                        if (!empty($item->target)) {
                            $item->target = $this->returnObject['history'][$item->target];
                        }
                        array_push($content, $item);
                        $typeIndex = array_search($item->type, $blockTypes);
                        Block::updateOrCreate(
                            ['slug' => $item->slug],
                            [
                                'user_id' => $this->userid, 
                                'slug' => $item->slug,
                                'typeIndex' => $typeIndex,
                                'data' => json_encode($item)
                            ]
                        );
                    }
                    //flatten content to array
                    $this->returnObject['content'] = $content;
                    if (!empty($request->input('key'))) {
                        //save new root item to body
                        if (empty($settingsData['blocks'])) {
                            $settingsData['blocks'] = [];
                        }
                        $settingsData['blocks'] = array_merge($this->returnObject['blocks'], $settingsData['blocks']);
                        if (empty($settingsData[$request->input('key')])) {
                            $settingsData[$request->input('key')] = [];
                        }
                        array_push($settingsData[$request->input('key')], $this->returnObject['root']);
                        //add to settings
                        Settings::updateOrCreate(
                            ['site_id' => $this->siteid],
                            ['data' => json_encode($settingsData)]
                        );
                    } else {
                        if (!empty($request->input('selection'))) {
                            if (empty($targetBlockData['data'])) {
                                $targetBlockData['data'] = [];
                            }
                            array_push($targetBlockData['data'], $this->returnObject['root']);
                            Block::updateOrCreate(
                                ['slug' => $request->input('selection')],
                                [
                                    'user_id' => $this->userid, 
                                    'slug' => $request->input('selection'),
                                    'data' => json_encode($targetBlockData)
                                ]
                            );
                        } else {
                            if (empty($bodyData['data'])) {
                                $bodyData['data'] = [];
                            }
                            array_push($bodyData['data'], $this->returnObject['root']);
                        }
                        //save new root item to body
                        if (empty($bodyData['blocks'])) {
                            $bodyData['blocks'] = [];
                        }
                        $bodyData['blocks'] = array_merge($this->returnObject['blocks'], $bodyData['blocks']);
                        Body::updateOrCreate(
                            ['slug' => $request->input('body')],
                            ['data' => json_encode($bodyData)]
                        );
                    }
                    return $this->returnObject;
                }
            }
        }
        return [];
    }

    public function duplicateTemplateBlock($block, $target) {
        $templateBlock = Templateblocks::where(['slug' => $block])->first(); 
        if (!empty($templateBlock->data)) {
            $blockData = json_decode($templateBlock->data);
            $uuid = Uuid::uuid4();
            $newBlockSlug = $uuid->toString();
            $blockData->slug = $newBlockSlug;
            if (!empty($blockData->target)) {
                //update the target to the new block ref
                $blockData->target = $this->returnObject['history'][$blockData->target];
            }
            if (!empty($blockData->parent)) {
                //update the parent to the new block ref
                $blockData->parent = $this->returnObject['history'][$blockData->parent];
            }
            $offset = array_search($block, $this->returnObject['content'][$target]->data);
            array_splice($this->returnObject['content'][$target]->data, $offset, 1);
            array_push($this->returnObject['blocks'], $newBlockSlug);
            array_push($this->returnObject['content'][$target]->data, $newBlockSlug);
            $this->returnObject['history'][$block] = $newBlockSlug;
            $this->returnObject['content'][$newBlockSlug] = $blockData;
            if (!empty($blockData->data)) {
                foreach($blockData->data as $slug) {
                    $this->duplicateTemplateBlock($slug, $newBlockSlug);
                }
            }
        }
        return;
    }
    
    public function fetchBlockRecursive($selection) {
        array_push($this->returnObject['blocks'], $selection);
        $block = Block::where('slug', $selection)
            //->where('user_id', $this->userid)
            ->first();
        if (!empty($block->data)) {
            $blockData = json_decode($block->data, true);
        }
        if (!empty($blockData["data"])) {
            foreach($blockData["data"] as $slug) {
                $this->fetchBlockRecursive($slug);
            }
        }
        return;
    }

    public function deleteBlocks($body, $settings, $bodyData, $settingsData) {
        //dd($this->returnObject['blocks']);
        if (!empty($this->returnObject['blocks'])) {
            foreach ($this->returnObject['blocks'] as $block) {
                //dd($block);
                if (!empty($bodyData) && !empty($bodyData['data'])) {
                    $bodyDataPos = array_search($block, $bodyData['data']);
                    if ($bodyDataPos !== false) {
                        array_splice($bodyData['data'], $bodyDataPos, 1);;
                    }
                }
                if (!empty($bodyData) && !empty($bodyData['blocks'])) {
                    $bodyBlockPos = array_search($block, $bodyData['blocks']);
                    if ($bodyBlockPos !== false) {
                        array_splice($bodyData['blocks'], $bodyBlockPos, 1);;
                    }
                }
                if (!empty($body) && !empty($body->data)) {
                    $body->data = json_encode($bodyData);
                    $body->save();
                }
                //check settings
                if (!empty($settingsData['blocks'])) {
                    if (!empty($settingsData['header'])) {
                        $headerDataPos = array_search($block, $settingsData['header']);
                        if ($headerDataPos !== false) {
                            array_splice($settingsData['header'], $headerDataPos, 1);
                        }
                    }
                    if (!empty($settingsData['footer'])) {
                        $footerDataPos = array_search($block, $settingsData['footer']);
                        if ($footerDataPos !== false) {
                            array_splice($settingsData['footer'], $footerDataPos, 1);
                        }
                    }
                    $settingBlockPos = array_search($block, $settingsData['blocks']);
                    if ($settingBlockPos !== false) {
                        array_splice($settingsData['blocks'], $settingBlockPos, 1);
                    }
                    $settings->data = json_encode($settingsData);
                    $settings->save();
                }
                $currentBlock = Block::where('slug', $block)
                    ->where('user_id', $this->userid)
                    ->first();
                if (!empty($currentBlock)) {
                    $currentBlock->delete(); 
                }
            }
        }
        return;
    }

    public function deleteBlock(Request $request) {
        $inputData = $this->validate($request, array(
            'body' => 'required',
            'selection' => 'required'
        ));
        $body = Body::where('slug', $inputData['body'])->first();
        $bodyData = json_decode($body->data, true);
        $settings = Settings::where('site_id', $this->siteid)->first();
        $settingsData = json_decode($settings->data, true);
        //find the root block
        $block = Block::where('slug', $inputData['selection'])
            ->where('user_id', $this->userid)->first();
        if (!empty($block->data)) {
            $blockData = json_decode($block->data, true);
        }
        //delete the parent block ref
        if (!empty($blockData['parent'])) {
            $parent = Block::where('slug', $blockData['parent'])
                ->where('user_id', $this->userid)->first();
            if (!empty($parent) && !empty($parent->data)) {
                $parentData = json_decode($parent->data, true);
                if (!empty($parentData['data'])) {
                    $parentBlockPos = array_search($inputData['selection'], $parentData['data']);
                    if ($parentBlockPos !== false) {
                        array_splice($parentData['data'], $parentBlockPos, 1);;
                    }
                    Block::updateOrCreate(
                        ['slug' => $parent['slug']],
                        ['data' => json_encode($parentData)]
                    );
                }
            }
        }
        $this->fetchBlockRecursive($inputData['selection']);
        //dd('halt', $this->returnObject);
        $this->deleteBlocks($body, $settings, $bodyData, $settingsData);
        return response()->json(['response' => 'Blocks removed']);
    }

    public function clearBlocks(Request $request) {
        $settings = Settings::where('site_id', $this->siteid)->first();
        $settingsData = json_decode($settings->data, true);
        if (!empty($request->input('body')) || (!empty($request->input('key')) && $request->input('key') == 'all')) {
            $body = Body::where(['slug' => $request->input('body'), 'site_id' => $this->siteid])->first();
            $bodyData = json_decode($body->data, true);
            if (!empty($bodyData['data'])) {
                foreach ($bodyData['data'] as $block) {
                    $this->fetchBlockRecursive($block);
                }
            }
        } else {
            $body = null;
            $bodyData = null;
        }
        if (!empty($inputData['key'])) {
            if (!empty($settingsData[$inputData['key']])) {
                foreach ($settingsData[$inputData['key']] as $block) {
                    $this->fetchBlockRecursive($block);
                }
            }
        }
        $this->deleteBlocks($body, $settings, $bodyData, $settingsData);
        return response()->json(['response' => 'Deleted']);
    }

    public function storeSettings(Request $request) {
        Settings::updateOrCreate(
            ['site_id' => $this->siteid],
            ['data' => json_encode($request->input())]
        );
        return response()->json(['response' => 'Settings saved']);
    }

    public function storeData(Request $request) {
        if (!empty($this->userid)) {
            UserData::updateOrCreate(
                ['id' => $this->userid],
                ['data' => json_encode($request->input())]
            );
            return response()->json(['response' => 'Data saved']);
        }
    }

    public function storePageData(Request $request) {
        $input = $request->input();
        if (!empty($input['slug'])) {
            Body::updateOrCreate(
                ['slug' => $input['slug']],
                ['data' => json_encode($input)]
            );
            return response()->json(['response' => 'Data saved']);
        }
    }

    public function storePageMetaData(Request $request) {
        $inputData = $this->validate($request, array(
            'path' => 'required',
            'title' => '',
            'description' => ''
        ));
        Body::updateOrCreate(
            ['slug' => $inputData['path'], 'user_id' => $this->userid],
            ['title' => $inputData['title'], 'metadescription' => $inputData['description']]
        );
        return redirect($inputData['path']);
    }

    public function fetchAllPages(Request $request) {
        return Body::where(['site_id' => $this->siteid])->get();
    }

    public function fetchPages(Request $request) {
        $inputData = $this->validate($request, array(
            'path' => 'required',
        ));
        return Body::where('path', 'LIKE', '%'.$inputData['path'].'%')
            ->where(['site_id' => $this->siteid ? $this->siteid : 0])->get();
    }

    public function fetchPageData(Request $request) {
        $inputData = $this->validate($request, array(
            'path' => 'required',
        ));
        $data = [];
        $bodies = Body::where('path', 'LIKE', '%'.$inputData['path'].'%')
            ->where(['user_id' => $this->userid ? $this->userid : 0])->get();
        foreach($bodies as $index => $body) {
            $data[$index] = $body;
            $bodyData = json_decode($body->data);
            foreach($bodyData as $bodyDataKey => $bodyDataItem) {
                $data[$index][$bodyDataKey] = $bodyDataItem;
            }
        }
        return $data;
    }

    public function fetchAllForms(Request $request) {
        return Forms::where(['user_id' => $this->userid])->get();
    }

    public function fetchForm(Request $request) {
        $forms = Form::where(['site_id' => $this->siteid])->get();
        $formArray = [];
        foreach($forms as $formkey => $formvalue) {
            $formArray[$formvalue->slug] = $formvalue->name;
        }
        $formsubmissions = Submissions::all()->groupBy(['form_id', 'user_id']);
        $submissionArray = [];
        foreach($formsubmissions as $formKey => $formValue) {
            $submissionArray[$formKey]['count'] = count($formValue);
            $submissionArray[$formKey]['name'] = $formArray[$formKey];
            $submissionArray[$formKey]['slug'] = $formKey;
        }
        return $submissionArray;
    }

    public function fetchFormSubmissions(Request $request) {
        $inputData = $this->validate($request, array(
            'slug' => 'required',
        ));
        $form = Form::where('slug', $inputData['slug'])->first();
        $formsubmissions = Submissions::where('form_id', $inputData['slug'])->get()->groupBy('user_id');
        foreach($formsubmissions as $formKey => $formValue) {
            $submissionArray[$formKey]['name'] = $form->name;
            $submissionArray[$formKey]['user'] = $formValue[0]->user_id;
            $submissionArray[$formKey]['date'] = $formValue[0]->updated_at;
        }
        return $submissionArray;
    }

    public function checkSlugAvailability(Request $request) {
        $inputData = $this->validate($request, array(
            'slug' => 'required'
        ));
        $slug = '/' . $this->slugify($inputData['slug']);
        $body = Body::where('path', $slug)
            ->where('user_id', $this->userid)
            ->first();
        // $allBodies = Body::where(['user_id' => $this->userid]);
        // if ($allBodies->count() > 10) {
        //     return response()->json(['response' => 'If you require lots of bespoke pages then please contact us to discuss your requirements.', 'valid' => false]); 
        // }
        if (!empty($body)) {
            //add to (or create and add to) site-specific sitemap
            return response()->json(['response' => 'A page with that name already exists', 'valid' => false]); 
        }
        return response()->json(['response' => 'Your page name is available.', 'slug' => $slug, 'valid' => true]); 
    }

    public function checkFormNameAvailability(Request $request) {
        $inputData = $this->validate($request, array(
            'name' => 'required'
        ));
        $form = Forms::where('name', $inputData['name'])
            ->where('user_id', $this->userid)
            ->first();
        $allForms = Forms::where(['user_id' => $this->userid]);
        if ($allForms->count() > 10) {
            return response()->json(['response' => 'If you require lots of tables then please contact us to discuss your requirements.', 'valid' => false]); 
        }
        if (!empty($form)) {
            return response()->json(['response' => 'A table with that name already exists', 'valid' => false]); 
        }
        return response()->json(['response' => 'Your table name is available.', 'name' => $inputData['name'], 'valid' => true]); 
    }

    public function renamePage(Request $request) {
        $inputData = $this->validate($request, array(
            'pageName' => 'required',
            'slug' => 'required'
        ));
        $path = '/' . $this->slugify($inputData['pageName']);
        // $allBodies = Body::where(['user_id' => $this->userid]);
        // if ($allBodies->count() > 10) {
        //     return response()->json(['response' => 'If you require lots of bespoke pages then please contact us to discuss your requirements.', 'valid' => false]); 
        // }
        $body = Body::where('slug', $inputData['slug'])
            ->where('user_id', $this->userid ? $this->userid : 0)
            ->first();
        $data = json_decode($body->data);
        $data->link = $path;
        $data->path = $path;
        $body->data = json_encode($data);
        $body->path = $path;
        $body->save();
        return redirect($path.'?edit');
    }

    public function variant() {
        $inputData = $this->validate($request, array(
            'variant' => 'required'
        ));
        $variant = Variant::where(['variant' => $inputData])->first();

        $attributes = $variant->attributes;

        return $attributes;
    }

    public function fetchAttributes(Request $request) {
        $inputData = $this->validate($request, array(
            'variant' => 'required'
        ));
        $content = [];
        $attributes = [];
        $parentVariant = Variant::where(['slug' => $inputData['variant']])->first();
        $data = json_decode($parentVariant['data']);
        if (!empty($data->variants)) {
            foreach($data->variants as $variant) {
                $childVariant = Variant::where(['slug' => $variant])->first();
                $attributes[$childVariant['slug']] = $childVariant;
                $childVariantData = json_decode($childVariant->data);
                foreach($childVariantData as $childVariantDataKey => $childVariantDataItem) {
                    $attributes[$childVariant['slug']][$childVariantDataKey] = $childVariantDataItem;
                }
                if ($childVariant['data']) {
                    $variantData = json_decode($childVariant['data']);
                    if (!empty($variantData->attributes)) {
                        $variantAttributes = Attribute::whereIn('slug', $variantData->attributes)->get();
                        $content[$variant] = $variantAttributes->pluck('slug');
                        foreach ($variantAttributes as $variantAttribute) {
                            $attributes[$variantAttribute->slug] = $variantAttribute;
                        }
                    }
                }
            }
        } elseif (!empty($data->attributes)) {
            $variantAttributes = Attribute::whereIn('slug', $data->attributes)->get();
            foreach ($variantAttributes as $variantAttribute) {
                $content[$inputData['variant']] = $variantAttributes->pluck('slug');
                $attributes[$variantAttribute->slug] = $variantAttribute;
            }
        }
        $attributes[$parentVariant['slug']] = $parentVariant;
        $attributes[$parentVariant['slug']]['matrix'] = $content;
        $attributes[$parentVariant['slug']]['variants'] = $data->variants;        
        return $attributes; 
    }

    public function fetchBrands(Request $request) {
        $inputData = $this->validate($request, array(
            'brand' => 'required'
        ));
        $brands = Brand::where('name', 'LIKE', '%'.$inputData['brand'].'%')
            ->get();
        return $brands; 
    }
    
    public function fetchVariantAttributes(Request $request) {
        $inputData = $this->validate($request, array(
            'variants' => 'required'
        ));
        foreach(explode(',', $inputData['variants']) as $variant) {
            $currentVariant = Variant::where('slug', $variant)->first();
            $attributes[$variant] = $currentVariant->attributes;
        }
        return $attributes; 
    }

    public function fetchVariants(Request $request) {
        $variants = Variant::all();
        return $variants; 
    }

    public function fetchPrice(Request $request) {
        $attributeKey = '';
        $items = $request->input();
        foreach($items as $item) {
            $attributeKey .= $item;
        }
        $price = Price::where('attribute_key', $attributeKey)->first();
        if ($price) {
            return $price;
        }
        return 'No price found';
    }

    public function fetchCategories(Request $request) {
        $inputData = $this->validate($request, array(
            'bodies' => 'required',
        ));
        //dd($inputData['bodies'], json_decode($inputData['bodies']));
        return Body::whereIn('slug', json_decode($inputData['bodies']))
            ->where(['user_id' => $this->userid])->get();
    }

    public function fetchCategory(Request $request) {
        $inputData = $this->validate($request, array(
            'name' => 'required',
        ));
        return Category::where('name', 'LIKE', '%'.$inputData['name'].'%')
            ->where(['user_id' => $this->userid])
            ->get();
    }
}
