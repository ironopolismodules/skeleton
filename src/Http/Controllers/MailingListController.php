<?php

namespace Ironopolis\Skeleton\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MailingListController extends Controller
{
    protected $url;
    protected $auth;

    public function __construct(Request $request)
    {
        $this->url = env('SUBSCRIBE_URL');
        $this->auth = env('SUBSCRIBE_API_KEY');
        if (empty($this->url) || empty($this->auth)) {
            throw new \InvalidArgumentException("You must set env variables");
        }
    }
    
    public function subscribe(Request $request) {
        $data = $request->input('email_address');
        $json = [
            "email_address" => $data,
            "status" => "subscribed"
        ];
        //dd($data, json_encode($json));
        //dd($this->url);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url.'/members',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($json),
            CURLOPT_HTTPHEADER => array(
                "Authorization: auth " . $this->auth,
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return redirect('/');
            //return '{error: "There was an error encountered when making the request"}';
        } else {
            return redirect('/');
            //return $response;
        }
    }
}
