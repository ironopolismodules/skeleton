<?php

namespace Ironopolis\Skeleton;

use Illuminate\Database\Eloquent\Model;

class AttributeGroups extends Model
{
  protected $fillable = [
    'slug', 'name', 'data'
  ];
}
