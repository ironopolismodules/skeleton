<?php

namespace Ironopolis\Skeleton;

use Illuminate\Database\Eloquent\Model;

class Bodytemplates extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'template', 'slug', 'data', 'dependencies', 'name', 'title'
    ];
}
