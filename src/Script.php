<?php

namespace Ironopolis\Skeleton;

use Illuminate\Database\Eloquent\Model;

class Script extends Model
{
  protected $fillable = [
    'site_id', 'slug', 'code'
  ];
}
