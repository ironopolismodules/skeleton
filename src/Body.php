<?php

namespace Ironopolis\Skeleton;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Body extends Model
{
    //use SoftDeletes;
    protected $fillable = [
        'slug', 'site_id', 'type', 'data', 'searchable', 'path'
    ];

    protected $hidden = array('pivot');

    public function blocks()
    {
        return $this->belongsToMany('Ironopolis\Skeleton\Block');
    }

    public function prices()
    {
        return $this->belongsToMany('Ironopolis\Skeleton\Price');
    }

    public function variants()
    {
        return $this->belongsToMany('Ironopolis\Skeleton\Variant');
    }

    public function categories()
    {
        return $this->belongsToMany('Ironopolis\Skeleton\Category');
    }
}
