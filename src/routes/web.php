<?php

Route::get('/fetchRecord', 'Ironopolis\Skeleton\Http\Controllers\AppController@fetchRecord');

Route::get('/demoFetchSources', 'Ironopolis\Skeleton\Http\Controllers\CmsController@demoFetchSources');

Route::get('/loadDemoTemplate', 'Ironopolis\Skeleton\Http\Controllers\AppController@loadDemoTemplate');

Route::get('/testFetchRecords', '\Ironopolis\Skeleton\Http\Controllers\CmsController@testFetchRecords');

Route::get('/fetchRecords', '\Ironopolis\Skeleton\Http\Controllers\CmsController@fetchRecords');

Route::get('/fetchEntities', '\Ironopolis\Skeleton\Http\Controllers\AjaxController@fetchEntities');

Route::get('/fetchTemplates', '\Ironopolis\Skeleton\Http\Controllers\AjaxController@fetchTemplates');

Route::get('/fetchMethods', '\Ironopolis\Skeleton\Http\Controllers\AppController@fetchMethods');

Route::get('/fetchProfile', '\Ironopolis\Skeleton\Http\Controllers\AppController@fetchProfile');

Route::get('/fetchPage', '\Ironopolis\Skeleton\Http\Controllers\AppController@fetchPage');

Route::get('/fetchSections', '\Ironopolis\Skeleton\Http\Controllers\AppController@fetchSections');

Route::get('/fetchSection', '\Ironopolis\Skeleton\Http\Controllers\AppController@fetchSection');

Route::post('/storeFormSubmission', 'Ironopolis\Skeleton\Http\Controllers\AppController@storeFormSubmission')->middleware('web');

Route::post('/uploadHtml', 'Ironopolis\Skeleton\Http\Controllers\AppController@uploadHtml')->middleware('web');

Route::group(['middleware' => ['web', 'auth']], function () {

    Route::get('/pasteBlock', '\Ironopolis\Skeleton\Http\Controllers\AppController@pasteBlock');

    Route::get('/fetchForms', '\Ironopolis\Skeleton\Http\Controllers\AppController@fetchForms');

    Route::get('/duplicatePage', '\Ironopolis\Skeleton\Http\Controllers\AppController@duplicatePage');

    Route::post('/attachForm', '\Ironopolis\Skeleton\Http\Controllers\AppController@attachForm');

    Route::post('/storeUserInput', '\Ironopolis\Skeleton\Http\Controllers\AppController@storeUserInput');

    Route::get('/loadTemplate', 'Ironopolis\Skeleton\Http\Controllers\AppController@loadTemplate');

    Route::post('/storeAsTemplate', 'Ironopolis\Skeleton\Http\Controllers\AppController@storeAsTemplate');

    Route::post('/submit', 'Ironopolis\Skeleton\Http\Controllers\ContactController@contact');

    Route::post('/saveUser', 'Ironopolis\Skeleton\Http\Controllers\AppController@saveUser');

    Route::get('/fetchAssets', '\Ironopolis\Skeleton\Http\Controllers\AppController@fetchAssets');

    Route::get('/profile', '\Ironopolis\Skeleton\Http\Controllers\PagesController@profile');

    Route::post('/create', 'Ironopolis\Skeleton\Http\Controllers\AppController@create');

    Route::post('/createProject', 'Ironopolis\Skeleton\Http\Controllers\AppController@createProject');

    Route::post('/store-settings', 'Ironopolis\Skeleton\Http\Controllers\AppController@storeSettings');

    Route::post('/registerFavourite', 'Ironopolis\Skeleton\Http\Controllers\AppController@registerFavourite');

    Route::post('/store-setting', 'Ironopolis\Skeleton\Http\Controllers\AppController@storeSetting');

    Route::post('/store-page', 'Ironopolis\Skeleton\Http\Controllers\AppController@storePage');

    Route::post('/fetch-content', 'Ironopolis\Skeleton\Http\Controllers\AppController@fetchContent');

    Route::post('/fetch-blocks', 'Ironopolis\Skeleton\Http\Controllers\AppController@fetchBlocks');

    Route::post('/fetch-products', 'Ironopolis\Skeleton\Http\Controllers\AppController@fetchProducts');

    Route::post('/fetch-collection', 'Ironopolis\Skeleton\Http\Controllers\AppController@fetchCollection');

    Route::post('/store-content', 'Ironopolis\Skeleton\Http\Controllers\AppController@storeContent');

    Route::post('/store-image', 'Ironopolis\Skeleton\Http\Controllers\AppController@storeImage');

    Route::post('/store-columns', 'Ironopolis\Skeleton\Http\Controllers\AppController@storeColumns');

    Route::get('/getImages', 'Ironopolis\Skeleton\Http\Controllers\AppController@getImages');

    Route::get('/getBlocks', 'Ironopolis\Skeleton\Http\Controllers\AppController@getBlocks');

    Route::get('/getRoute', 'Ironopolis\Skeleton\Http\Controllers\AppController@getRoute');

    Route::get('/saveAsTemplate', 'Ironopolis\Skeleton\Http\Controllers\AppController@saveAsTemplate');

    Route::get('/fetchBlockStyles', 'Ironopolis\Skeleton\Http\Controllers\AppController@fetchBlockStyles');

    Route::get('/applyTemplateToBlock', 'Ironopolis\Skeleton\Http\Controllers\AppController@applyTemplateToBlock');

    Route::get('/applyTemplateToBody', 'Ironopolis\Skeleton\Http\Controllers\AppController@applyTemplateToBody');

    Route::get('/fetchBlockTemplateChoices', 'Ironopolis\Skeleton\Http\Controllers\AppController@fetchBlockTemplateChoices');
    
    Route::get('/clearBlocks', 'Ironopolis\Skeleton\Http\Controllers\AppController@clearBlocks');

    Route::get('/defineComponent', 'Ironopolis\Skeleton\Http\Controllers\AppController@defineComponent');

    Route::post('/saveBlock', 'Ironopolis\Skeleton\Http\Controllers\AppController@saveBlock');

    Route::post('/saveBodyData', 'Ironopolis\Skeleton\Http\Controllers\AppController@saveBodyData');

    Route::post('/saveBody', 'Ironopolis\Skeleton\Http\Controllers\AppController@saveBody');

    Route::get('/insertBlock', 'Ironopolis\Skeleton\Http\Controllers\AppController@insertBlock');

    Route::get('/insertDuplicateBlock', 'Ironopolis\Skeleton\Http\Controllers\AppController@insertDuplicateBlock');

    Route::get('/deleteBlock', 'Ironopolis\Skeleton\Http\Controllers\AppController@deleteBlock');

    Route::post('/storeSettings', 'Ironopolis\Skeleton\Http\Controllers\AppController@storeSettings');

    Route::post('/storeData', 'Ironopolis\Skeleton\Http\Controllers\AppController@storeData');

    Route::get('/storePageMetaData', 'Ironopolis\Skeleton\Http\Controllers\AppController@storePageMetaData');

    Route::post('/storeImageAjax', 'Ironopolis\Skeleton\Http\Controllers\AppController@storeImageAjax');

    Route::post('/storeProfileImage', 'Ironopolis\Skeleton\Http\Controllers\AppController@storeProfileImage');

    //Route::post('/notify', 'Ironopolis\Skeleton\Http\Controllers\MailablesController@index');
    
    Route::get('/fetchAllPages', '\Ironopolis\Skeleton\Http\Controllers\AppController@fetchAllPages');
    Route::get('/fetchPages', '\Ironopolis\Skeleton\Http\Controllers\AppController@fetchPages');
    Route::get('/fetchPageData', '\Ironopolis\Skeleton\Http\Controllers\AppController@fetchPageData');
    Route::post('/deletePage', '\Ironopolis\Skeleton\Http\Controllers\AppController@deletePage');
    // Route::post('/checkSlugAvailability', 'Ironopolis\Skeleton\Http\Controllers\AppController@checkSlugAvailability');
    Route::post('/renamePage', 'Ironopolis\Skeleton\Http\Controllers\AppController@renamePage');
    Route::post('/createPage', 'Ironopolis\Skeleton\Http\Controllers\AppController@createPage');

    Route::get('/storeImages', '\Ironopolis\Skeleton\Http\Controllers\AppController@storeImages');
    Route::post('/storePageData', 'Ironopolis\Skeleton\Http\Controllers\AppController@storePageData');



    //cms routes
    Route::get('/fetchFilterables', 'Ironopolis\Skeleton\Http\Controllers\CmsController@fetchFilterables');

    Route::post('/saveFilterables', 'Ironopolis\Skeleton\Http\Controllers\CmsController@saveFilterables');

    Route::get('/fetchSources', 'Ironopolis\Skeleton\Http\Controllers\CmsController@fetchSources');
    
    Route::post('/createSource', '\Ironopolis\Skeleton\Http\Controllers\CmsController@createSource');

    Route::get('/fetchFields', 'Ironopolis\Skeleton\Http\Controllers\CmsController@fetchFields');

    Route::get('/fetchAllSources', 'Ironopolis\Skeleton\Http\Controllers\CmsController@fetchAllSources');

    Route::post('/saveSource', '\Ironopolis\Skeleton\Http\Controllers\CmsController@saveSource');

    Route::post('/saveRecord', '\Ironopolis\Skeleton\Http\Controllers\CmsController@saveRecord');

    Route::post('/addRecord', '\Ironopolis\Skeleton\Http\Controllers\CmsController@addRecord');

    Route::post('/deleteSource', '\Ironopolis\Skeleton\Http\Controllers\CmsController@deleteSource');

    Route::get('/deleteRecord', '\Ironopolis\Skeleton\Http\Controllers\CmsController@deleteRecord');

    Route::get('/fetchCollections', 'Ironopolis\Skeleton\Http\Controllers\CmsController@fetchCollections');



    Route::get('/sitemap', '\Ironopolis\Skeleton\Http\Controllers\AppController@sitemap');

});
//Route::get('{all}', 'Ironopolis\Skeleton\Http\Controllers\PagesController@index')->where('all', '.*')->middleware('web');
