<?php
//call using: vendor/bin/phpunit packages/larapackages/course/
namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SkeletonTest extends TestCase
{
    //use RefreshDatabase;

    protected $skeleton;

    public function setUp() 
    {
        parent::setUp();
       
        $this->course = factory('Ironopolis\Skeleton\Settings')->create();

    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_a_user_can_browse_courses()
    {
        $this->signIn(factory('App\User')->create());

        $response = $this->get('/');

        $response->assertSee($this->skeleton->title);

        //$response->assertStatus(200);
    }

    public function test_a_user_can_view_modules_related_to_a_thread()
    {
        $this->signIn(factory('App\User')->create());

        $response = $this->get('/');
    }
}
