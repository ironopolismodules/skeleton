<?php

namespace Ironopolis\Skeleton\View\Components;

use Illuminate\View\Component;

class Date extends Component
{
    public $opts;

    public $settings;

    public $content;

    public $is;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($opts, $settings, $content, $is)
    {
        $this->opts = $opts;
        $this->settings = $settings;
        $this->content = $content;
        $this->is = $is;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.date');
    }
}
